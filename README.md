# IV-KAPhE Evaluation Scripts

These are the analysis scripts used to evaluate the IV-KAPhE method
for protein kinase-substrate assignment.  See: Invergo BM. Accurate,
high-coverage assignment of in vivo protein kinases to phosphosites
from in vitro phosphoproteomic specificity data.  bioRxiv
2021.08.31.458376; doi: https://doi.org/10.1101/2021.08.31.458376

Please note that these analyses were part of a much larger analysis
that grew organically over time.  The scripts that are relevant to the
main analysis have been excised to simplify reviewing and using the
code.  There may still be vestigial remnants of other, unused
analyses.

## Usage

The pipeline is implemented in
[Make](https://www.gnu.org/software/make).  To run the full pipeline,
just run `make setup prepare analysis`.  You'll benefit from some
parallel execution by passing the `-j` flag (e.g. `-j 32` to run 32
concurrent processes).  I cannot be held responsible for any destroyed
hard drives.

Pre-publication note: at the time of submission, some steps are yet to
be incorporated into the automated pipeline and have to be run
manually (mainly around the Random Forest model training and
validation; e.g. `rand-forest.r` for training the model and producing
predictions, and `pmapperval-multilabel-eval.r` for evaluating the
ProtMapper-based external validation set).  These will be incorporated
before publication.  Scripts are included for formatting GPS 5.0 and
NetworKIN 3.0 results, but running the original programs is outside
the scope of the pipeline (e.g. GPS is a graphical program), so they
must be run manually.

The full disk usage following the complete analysis is 74G
(e.g. scoring all kinases vs all sites for multiple methods and
parameter settings), so I cannot include all of the results in this
repository.  Every effort has been made to automate downloading
upstream data, but please notify me if I missed anything.

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
