library(igraph)
library(tidyverse)

kinases <- readLines("data/kinases.txt")

kinsec <- read_tsv("data/raw/buljan-et-al-2020/Final_interactome_2021-03-15.txt") %>%
    select(Bait_ID, Protein_ID) %>%
    rename(prot1 = Bait_ID, prot2 = Protein_ID) %>%
    filter(prot1 != prot2)
kinsec_id <- apply(kinsec, 1, function(x) paste(sort(x), collapse = " "))
kinsec_dups <- duplicated(kinsec_id)
kinsec <- kinsec[!kinsec_dups,]

biogrid <- read_tsv("data/human-biogrid.tsv", col_names = c("prot1", "prot2"))

bioplex <- read_tsv("data/raw/BioPlex_293T_Network_10K_Dec_2019.tsv") %>%
    select(UniprotA, UniprotB) %>%
    rename(prot1 = UniprotA, prot2 = UniprotB) %>%
    distinct() %>%
    filter(prot1 != prot2)

kinsec_net <- graph_from_data_frame(kinsec, directed = FALSE)
biogrid_net <- graph_from_data_frame(biogrid, directed = FALSE)
bioplex_net <- graph_from_data_frame(bioplex, directed = FALSE)

total_net <- igraph::union(kinsec_net, biogrid_net, bioplex_net, byname = TRUE)

total_net <- simplify(total_net)

first_intxns <- NULL
second_intxns <- NULL
for (kin in kinases){
    if (!(kin %in% V(total_net)$name))
        next
    intxns <- neighbors(total_net, kin)
    intxns_2nd <- neighbors(total_net, intxns)
    if (is.null(second_intxns)){
        first_intxns <- tibble(kinase = rep(kin, length(intxns)),
                               intxn = intxns$name)
        second_intxns <- tibble(kinase = rep(kin, length(intxns_2nd)),
                                intxn = intxns_2nd$name)
    }else{
        first_intxns_tmp <- tibble(kinase = rep(kin, length(intxns)),
                                   intxn = intxns$name)
        first_intxns <- bind_rows(first_intxns, first_intxns_tmp)
        second_intxns_tmp <- tibble(kinase = rep(kin, length(intxns_2nd)),
                                    intxn = intxns_2nd$name)
        second_intxns <- bind_rows(second_intxns, second_intxns_tmp)
    }
}

write_tsv(first_intxns, "data/human-kinase-interactions.tsv")
write_tsv(second_intxns, "data/human-kinase-2nd-interactions.tsv")
