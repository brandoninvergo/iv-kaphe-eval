library(tidyverse)
library(slider)
library(cowplot)
library(RColorBrewer)
theme_set(theme_cowplot())

argv <- commandArgs(TRUE)
n_folds <- as.integer(argv[1])
win_len <- as.integer(argv[2])

half_winlen <- floor(win_len/2)
s_regexp <- paste0(".{", half_winlen, "}[S].{", half_winlen, "}")
t_regexp <- paste0(".{", half_winlen, "}[T].{", half_winlen, "}")
y_regexp <- paste0(".{", half_winlen, "}[Y].{", half_winlen, "}")

cv_data <- NULL
for (k in 1:n_folds){
    fold_dir <- paste0("out/sugiyama-folds/fold-", k)
    cv_sub <- read_tsv(paste0(fold_dir, "/sug-fold-cv.tsv"))
    if (is.null(cv_data)){
        cv_data <- cv_sub
    }else{
        cv_data <- bind_rows(cv_data, cv_sub)
    }
}
cv_data <- filter(cv_data, metric != "ml.acc") %>%
    extract(metric, into = c("averaging", "metric"), regex = "([[:alpha:]]+)\\.([[:alnum:].]+)")
cv_data$metric <- factor(cv_data$metric,
                         levels = c("prec", "prec.cal", "rec", "f1", "f1.cal", "fb",
                                    "fb.cal", "wf1", "wfb"),
                         labels = c("precision", "calibrated precision", "recall",
                                    "F1", "calibrated F1", "Fbeta", "calibrated Fbeta",
                                    "weighted F1", "weighted Fbeta"))
cv_data$averaging <- factor(cv_data$averaging,
                            levels = c("micro", "macro"),
                            labels = c("micro-averaging", "macro-averaging"))

cv_data_sum <- group_by(cv_data, method, averaging, metric, cutoff) %>%
    summarise(mean.value = mean(value, na.rm = TRUE),
              se.value = sd(value, na.rm = TRUE)/sqrt(n()),
              ci.value = qnorm(0.975)*sd(value, na.rm = TRUE)/sqrt(n()),
              lower.value = mean.value - ci.value,
              upper.value = mean.value + ci.value) %>%
    ungroup() %>%
    mutate(win.avg.value = slide_dbl(mean.value, mean, .before = 5)) %>%
    ungroup()
cv_data_sum$method <- factor(cv_data_sum$method,
                             levels = c("PFM",
                                        "PSSM proteome bg.",
                                        "PSSM phosphoproteome bg.",
                                        "Naive Bayes",
                                        "Naive Bayes+"),
                             labels = c("PFM",
                                        "PSSM proteome",
                                        "PSSM phospho.",
                                        "Naive Bayes",
                                        "Naive Bayes+"))

best_co <- filter(cv_data_sum, averaging == "macro-averaging",
       metric == "F1", cutoff > 0, cutoff < 1) %>%
    group_by(method, metric) %>%
    arrange(-cutoff) %>%
    slice_max(mean.value, n = 1, with_ties = FALSE) %>%
    select(-se.value, -ci.value, -lower.value, -upper.value) %>%
    write_tsv("out/sugiyama-ml-cv-best-cutoffs.tsv")
ml_plot <- filter(cv_data_sum, averaging == "macro-averaging", metric == "F1") %>%
    ggplot(aes(x = cutoff, y = mean.value)) +
    geom_ribbon(aes(ymin = lower.value, ymax = upper.value, fill = method), alpha = 0.2) +
    geom_line(aes(color = method)) +
    geom_point(data = best_co, aes(color = method)) +
    scale_fill_brewer(guide = NULL, palette = "Dark2") +
    scale_color_brewer(guide = NULL, palette = "Dark2") +
    scale_x_continuous("cutoff", breaks = c(0, 0.5, 1)) +
    scale_y_continuous("macro-F1", breaks = c(0, 0.5, 1.0), limits = c(0, 1))
save_plot("img/sugiyama-ml-cv-eval.pdf", ml_plot, ncol = 1, nrow = 1)
precs <- filter(cv_data_sum, averaging == "macro-averaging", metric == "precision")
best_precs <- left_join(best_co, precs, by = c("method", "averaging", "cutoff"),
                        suffix = c(".F1", ".prec")) %>%
    select(method, cutoff, mean.value.prec, lower.value, upper.value)
recs <- filter(cv_data_sum, averaging == "macro-averaging", metric == "recall")
best_recs <- left_join(best_co, recs, by = c("method", "averaging", "cutoff"),
          suffix = c(".F1", ".rec")) %>%
    select(method, cutoff, mean.value.rec, lower.value, upper.value)
best_pr <- inner_join(best_precs, best_recs, by = c("method", "cutoff"),
                      suffix = c(".prec", ".rec"))
pr_plot <- filter(cv_data_sum, averaging == "macro-averaging",
                  metric %in% c("precision", "recall")) %>%
    arrange(cutoff) %>%
    select(-win.avg.value, -se.value, -ci.value, -lower.value, -upper.value) %>%
    pivot_wider(names_from = metric, values_from = mean.value) %>%
    ggplot(aes(y = precision, x = recall, color = method)) +
    scale_color_brewer("model", palette = "Dark2") +
    geom_path() +
    geom_point(data = best_pr, aes(y = mean.value.prec, x = mean.value.rec)) +
    geom_errorbar(data = best_pr,
                  aes(y = mean.value.prec, x = mean.value.rec,
                      xmin = lower.value.rec, xmax = upper.value.rec),
                  width = 0.02, color = "black", alpha = 0.75) +
    geom_errorbar(data = best_pr,
                  aes(y = mean.value.prec, x = mean.value.rec,
                      ymin = lower.value.prec, ymax = upper.value.prec),
                  width = 0.01, color = "black", alpha = 0.75) +
    scale_x_continuous("macro-recall", breaks = c(0, 0.5, 1)) +
    scale_y_continuous("macro-precision", breaks = c(0, 0.5, 1), limits = c(0, 1)) +
    theme(legend.position = c(0.45, 0.8))
save_plot("img/sugiyama-ml-cv-prec-rec.pdf", pr_plot)

grepl_v <- Vectorize(grepl)

akt1_pos <- read_tsv("out/sugiyama-folds/fold-1/pos-bg/P31749-sug-fold-scores.tsv",
                     col_names = c("kinase", "site.id", "seq.win", "score")) %>%
    mutate(method = rep("PSSM phospho.", n()))
akt1_nb <- read_tsv("out/sugiyama-folds/fold-1/bayes/P31749-sug-fold-scores.tsv",
                    col_names = c("kinase", "site.id", "seq.win", "score")) %>%
    mutate(method = rep("Naive Bayes", n()))
akt1 <- bind_rows(akt1_pos, akt1_nb) %>%
    mutate(y.site = grepl_v(y_regexp, seq.win),
           seq.type = if_else(y.site, "Y", "S/T")) %>%
    select(-y.site) %>%
    pivot_wider(names_from = method, values_from = score)
akt1_p <- ggplot(akt1, aes(x = `PSSM phospho.`, y = `Naive Bayes`, color = seq.type)) +
    geom_point(alpha = 0.5) +
    scale_x_continuous(, breaks = c(0, 0.5, 1.0)) +
    scale_y_continuous(, breaks = c(0, 0.5, 1.0)) +
    scale_color_brewer("phosphoacceptor", palette = "Set2") +
    xlab("PSSM phospho.\nnormalized score") +
    ylab("Naive Bayes\nposterior probability") +
    ggtitle("AKT1") +
    theme(legend.position = c(0.1, 0.85))
fyn_pos <- read_tsv("out/sugiyama-folds/fold-1/pos-bg/P06241-sug-fold-scores.tsv",
                     col_names = c("kinase", "site.id", "seq.win", "score")) %>%
    mutate(method = "PSSM phospho.")
fyn_nb <- read_tsv("out/sugiyama-folds/fold-1/bayes/P06241-sug-fold-scores.tsv",
                    col_names = c("kinase", "site.id", "seq.win", "score")) %>%
    mutate(method = "Naive Bayes")
fyn <- bind_rows(fyn_pos, fyn_nb) %>%
    mutate(y.site = grepl_v(y_regexp, seq.win),
           seq.type = if_else(y.site, "Y", "S/T")) %>%
    select(-y.site) %>%
    pivot_wider(names_from = method, values_from = score)
fyn_p <- ggplot(fyn, aes(x = `PSSM phospho.`, y = `Naive Bayes`, color = seq.type)) +
    geom_point(alpha = 0.5) +
    scale_x_continuous(, breaks = c(0, 0.5, 1.0)) +
    scale_y_continuous(, breaks = c(0, 0.5, 1.0)) +
    scale_color_brewer("phosphoacceptor", palette = "Set2") +
    xlab("PSSM phospho.\nnormalized score") +
    ylab("Naive Bayes\nposterior probability") +
    ggtitle("FYN") +
    theme(legend.position = c(0.1, 0.85))
load("img/bayes-pssm-logistic-relationship.Robj")
p <- plot_grid(pr_plot, ml_plot, akt1_p, fyn_p, nrow = 1, ncol = 4,
               labels = c("a", "b", "c", ""), align = "hv")
p_final <- plot_grid(p, bayes_logistic_p, nrow = 2)
save_plot("img/sugiyama-ml-cv.pdf", p_final, ncol = 4, nrow = 2,
          base_asp = 1.0, base_height = 3.71)
