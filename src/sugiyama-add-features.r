library(igraph)
library(tidyverse)

argv <- commandArgs(TRUE)
sugiyama_file <- argv[1]
sub_doms_file <- argv[2]

site_in_domain <- Vectorize(function(protein, site){
    pfam_prot <- filter(pfam, `seq id` == protein) %>%
        filter(`envelope start` <= site,
               `envelope end` >= site)
    return (nrow(pfam_prot) > 0)
})

kin_ints_tbl <- read_tsv("data/human-kinase-interactions.tsv")
kin_ints <- graph_from_data_frame(kin_ints_tbl, directed = FALSE)
kin_2nd_ints_tbl <- read_tsv("data/human-kinase-2nd-interactions.tsv")
kin_2nd_ints <- graph_from_data_frame(kin_2nd_ints_tbl, directed = FALSE)

pfam <- read_tsv("data/pfam-human-domains.tsv")
pfam_compact <- select(pfam, `seq id`, `hmm acc`) %>% chop(`hmm acc`)
kinases <- readLines("data/kinases.txt")
sig_doms1 <- read_tsv("out/kinase-int-domains.tsv")
sig_doms2 <- read_tsv(sub_doms_file)
sig_doms <- bind_rows(sig_doms1, sig_doms2) %>%
    mutate(p.adj = p.adjust(p, method = "BH")) %>%
    filter(p.adj < 0.05) %>%
    select(kinase, domain) %>%
    distinct()

sugiyama <- read_tsv(sugiyama_file) %>%
    separate(col = substrate.id.site, into = c("substrate", "site"),
             sep = "-[STY]", remove = FALSE, convert = TRUE)
sugiyama_feat <- select(sugiyama, substrate.id.site, substrate, site, seq.win) %>%
    distinct() %>%
    mutate(in.domain = site_in_domain(substrate, site)) %>%
    left_join(pfam_compact, by = c("substrate" = "seq id"))

out_dir <- paste0(dirname(sugiyama_file), "/kinase-features/")
if (!dir.exists(out_dir))
    dir.create(out_dir)
for (kin in kinases){
    out_file <- paste0(out_dir, "sugiyama-", kin, "-feats.tsv")
    ## if (file.exists(out_file)){
    ##     message(paste("Skipping", kin, "because its file already exists"))
    ##     next
    ## }
    kin_subs <- filter(sugiyama, kinase == kin) %>% pull(substrate.id.site)
    kin_doms <- filter(sig_doms, kinase == kin) %>% pull(domain)
    if (kin %in% V(kin_ints)$name){
        kin_neighbors <- neighbors(kin_ints, kin)$name
        kin_2nd_neighbors <- neighbors(kin_2nd_ints, kin)$name
    }else{
        kin_neighbors <- c()
        kin_2nd_neighbors <- c()
    }
    sugiyama_feat <- mutate(sugiyama_feat,
           phys.int = substrate %in% kin_neighbors,
           phys.int.2nd = substrate %in% kin_2nd_neighbors,
           is.substrate = substrate.id.site %in% kin_subs,
           enriched.dom = unlist(lapply(`hmm acc`,
                                        function(sub_doms){
                                            any(sub_doms %in% kin_doms)
                                        })))
    if (grepl("train", out_dir)){
        select(sugiyama_feat, substrate.id.site, is.substrate, seq.win, phys.int, phys.int.2nd, in.domain, enriched.dom) %>%
            distinct() %>%
            write_tsv(out_file, col_names = FALSE)
    }else{
        select(sugiyama_feat, substrate.id.site, seq.win, phys.int, phys.int.2nd, in.domain, enriched.dom) %>%
            distinct() %>%
            write_tsv(out_file, col_names = FALSE)
    }
}

