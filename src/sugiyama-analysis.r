library(MASS)
library(tidyverse)
library(cowplot)
theme_set(theme_cowplot())

args <- commandArgs(TRUE)
win_len <- as.integer(args[1])
half_winlen <- floor(win_len/2)
s_regexp <- paste0(".{", half_winlen, "}[S].{", half_winlen, "}")
t_regexp <- paste0(".{", half_winlen, "}[T].{", half_winlen, "}")
y_regexp <- paste0(".{", half_winlen, "}[Y].{", half_winlen, "}")

uniprot_id_map <- read_tsv("data/raw/UP000005640_9606.idmapping",
                           col_names = c("id", "source", "alt.id"))

kinase_id_map <- read_tsv("data/kinase-id-map.tsv",
                          col_names = c("id", "prot.name", "gene.name"))
## dual specificity kinases
dsk <- kinase_id_map$id[which(grepl("Dual specificity", kinase_id_map$prot.name))]
y_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase"))
st_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase"))

sugiyama <- read_tsv("data/sugiyama-kinase-substrates-full.tsv",
                     col_names = c("code", "name", "substrate", "site",
                                   "sidic", "ptmscore", "kinase", "seq.win")) %>%
    filter(!is.na(seq.win)) %>%
    mutate(is.dsk = kinase %in% dsk)

## sugiyama_weights <- read_tsv("out/weights/sugiyama-full-weights.tsv",
##                              col_names = c("kinase", "seq.win", "weight"))
## sugiyama <- left_join(sugiyama, sugiyama_weights, by = c("kinase", "seq.win"))

uniprot_id_map_sub <- filter(uniprot_id_map, alt.id %in% sugiyama$substrate)
sugiyama <- left_join(sugiyama, uniprot_id_map_sub, by = c("substrate" = "alt.id"))
sugiyama$substrate.id <- sugiyama$id
sugiyama$id <- NULL
sugiyama$source <- NULL

sugiyama$main.id <- unlist(lapply(strsplit(sugiyama$kinase, split = "-"),
                                  function(split) split[1]))
sugiyama$is.y.kinase <- sugiyama$main.id %in% y_kinases$kinase
sugiyama$kinase.type <- rep(NA, nrow(sugiyama))
sugiyama$kinase.type[sugiyama$is.y.kinase] <- "Y"
sugiyama$kinase.type[!sugiyama$is.y.kinase] <- "S/T"
sugiyama$is.y.site <- grepl(y_regexp, sugiyama$seq.win)
sugiyama$seq.type <- rep(NA, nrow(sugiyama))
sugiyama$seq.type[sugiyama$is.y.site] <- "Y"
sugiyama$seq.type[!sugiyama$is.y.site] <- "S/T"

## Filter out autophosphorylation
sugiyama <- filter(sugiyama, main.id != substrate.id)

## A site is "suspicious" if the phosphosite doesn't match the
## expected phosphoreceptor of the kinase (e.g. an S/T for a tyrosine
## kinase or a Y for an STK) and a different, "correct"
## phosphoreceptor is within the sequence window.  This is a rough
## approximation to get an idea of the level of error in the two site
## identification methods used by Sugiyama et al.
suspicious_st_re1 <- "^.{0,6}Y.{0,6}"
suspicious_st_re2 <- ".{0,6}Y.{0,6}$"
suspicious_y_re1 <- "^.{0,6}[ST].{0,6}"
suspicious_y_re2 <- ".{0,6}[ST].{0,6}$"
sugiyama$suspicious <- ((sugiyama$is.y.kinase &
                         !sugiyama$is.y.site &
                        (grepl(suspicious_st_re1, sugiyama$seq.win) |
                         grepl(suspicious_st_re2, sugiyama$seq.win))) |
                        (!sugiyama$is.y.kinase &
                         sugiyama$is.y.site &
                         (grepl(suspicious_y_re1, sugiyama$seq.win) |
                          grepl(suspicious_y_re2, sugiyama$seq.win))))

## Check whether either site localization method generates more
## suspicious sites
table(list(sidic = sugiyama$sidic, suspicious = sugiyama$suspicious, ptmscore = sugiyama$ptmscore)) / nrow(sugiyama)
## So, sites where PTMscore > 0.75 but SIDIC failed, 0.5% of sites are
## suspicious.  Where PTMscore < 0.75 but SIDIC localized the site, 4%
## are suspicious.  Overall, 2.4% are suspicious when both methods
## localized the site.

sugiyama_by_kinase <- group_by(sugiyama, kinase, kinase.type, is.dsk) %>%
    summarise(n = n(),
              num.unexpected = sum(seq.type != kinase.type),
              perc.unexpected = num.unexpected / n,
              num.suspicious = sum(suspicious),
              perc.suspicious = num.suspicious / n)

ggplot(sugiyama_by_kinase, aes(x = kinase.type, y = perc.unexpected - perc.suspicious, 
                              color = is.dsk)) +
    geom_violin(draw_quantiles = c(0.25, 0.5, 0.75), color = "black") +
    geom_jitter()

ggplot(sugiyama_by_kinase, aes(x = kinase.type, y = perc.suspicious,
                               color = is.dsk)) +
    geom_violin(draw_quantiles = c(0.25, 0.5, 0.75), color = "black") +
    geom_jitter()

## Filter out sites that are only identified by SIDIC
sugiyama_filt <- filter(sugiyama, ptmscore & sidic)
## sugiyama_filt <- filter(sugiyama, ptmscore)
## sugiyama_filt <- sugiyama

## Filter out sites with weights in the top fifth-percentile
## sugiyama_filt$weight.co <- with(sugiyama_filt,
##                                 ave(weight, kinase,
##                                     FUN = function(x) quantile(x, 0.85)))
## sugiyama_filt <- filter(sugiyama_filt, weight < weight.co)
## sugiyama_filt_by_kinase <- group_by(sugiyama_filt, kinase, kinase.type, is.dsk) %>%
##     summarise(n = n(),
##               num.unexpected = sum(seq.type != kinase.type),
##               perc.unexpected = num.unexpected / n,
##               num.suspicious = sum(suspicious),
##               perc.suspicious = num.suspicious / n)
## ggplot(sugiyama_filt_by_kinase, aes(x = kinase.type, y = perc.unexpected - perc.suspicious,
##                                color = is.dsk)) +
##     geom_violin(draw_quantiles = c(0.25, 0.5, 0.75), color = "black") +
##     geom_jitter()

## Filter out any site-kinase mismatches unless they're annotated
## as dual-specificity
## sugiyama_filt <- filter(sugiyama_filt,
##                         is.dsk | seq.type == kinase.type)

write_tsv(sugiyama_filt, "data/sugiyama-kinase-substrates-filt.tsv")
