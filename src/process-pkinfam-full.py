import re

FAMILY_RE = re.compile(r"family$")
KINASE_RE = re.compile(r"\w+\s+\w+_HUMAN\s+\((\w+)\s*\).*$")


def is_family_line(line):
    return FAMILY_RE.search(line) is not None or line == "Other"


def print_pkinfam(pkinfam_h):
    header_done = False
    family = None
    for line in pkinfam_h:
        line = line.strip()
        if not line:
            continue
        if line[0] == "-" or line.strip()[0] == "=":
            continue
        if line == "Swiss-Prot entries for protein kinases":
            header_done = True
            continue
        if not header_done:
            continue
        if is_family_line(line):
            family = line
        kin_match = KINASE_RE.match(line)
        if not kin_match:
            continue
        uniprot_id = kin_match.groups()[0]
        print("\t".join([uniprot_id, family]))


if __name__ == "__main__":
    with open("data/raw/pkinfam.txt") as h:
        print_pkinfam(h)
