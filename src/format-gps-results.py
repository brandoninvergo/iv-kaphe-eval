import sys
import os
import os.path


def read_sites(h, site_id_col):
    all_sites = set()
    for line in h:
        line_spl = line.strip().split("\t")
        site_id = line_spl[site_id_col]
        all_sites.add(site_id)
    return all_sites


def read_kinase_id_map(h):
    kinase_id_map = dict()
    for line in h:
        uniprot_id, _, gene_id = line.strip().split("\t")
        kinase_id_map[gene_id] = uniprot_id
    return kinase_id_map


def parse_gps_file(h, kinase_id_map, all_sites):
    # Skip header line
    foo = h.readline()
    # Get the substrate Uniprot ID
    substrate_line = h.readline()
    substrate = substrate_line.split("|")[1]
    gps_res = set()
    # Read the sites
    for line in h:
        # If you rerun GPS on the same sequence accidentally, it
        # appends to the existing results file instead of overwriting
        # it.  Rather than go back and find which files are affected,
        # I'll just work around it here.  Sorry.
        if line[0] == ">":
            return gps_res
        site, res, kinase_str, seq_win, score, cutoff = line.strip().split()
        # Site ID, e.g. P42544-S14
        site_id = substrate + "-" + res + site
        if site_id not in all_sites:
            continue
        seq_win = seq_win.replace("*", "_")
        kinase_hier = kinase_str.split("/")
        kinase = kinase_hier[-1]
        if kinase not in kinase_id_map:
            continue
        kinase = kinase_id_map[kinase]
        gps_res.add((kinase, site_id, seq_win))
    return gps_res


def format_gps_results(gps_files, all_sites, kinase_id_map):
    full_gps_res = set()
    for f in gps_files:
        with open(f) as h:
            gps_res = parse_gps_file(h, kinase_id_map, all_sites)
            full_gps_res = full_gps_res.union(gps_res)
    return full_gps_res


def print_gps_results(gps_results):
    print("\t".join(["kinase", "site.id", "seq.win"]))
    for site in gps_results:
        print("\t".join(site))


if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.exit("USAGE: <script> GPS_DIR SITES_FILE SITE_ID_COL")
    gps_dir = sys.argv[1]
    sites_file = sys.argv[2]
    site_id_col = int(sys.argv[3]) - 1
    gps_files = [os.path.join(gps_dir, f) for f in os.listdir(gps_dir)]
    with open(sites_file) as h:
        all_sites = read_sites(h, site_id_col)
    with open("data/kinase-id-map.tsv") as h:
        kinase_id_map = read_kinase_id_map(h)
    gps_results = format_gps_results(gps_files, all_sites, kinase_id_map)
    print_gps_results(gps_results)
