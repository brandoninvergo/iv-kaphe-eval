 library(tidyverse)
library(tidytext)
library(GGally)
library(viridis)
library(cowplot)
theme_set(theme_cowplot())

phoproteome <- read_tsv("data/human-phosphoproteome.tsv", col_names = c("site.id", "seq.win"))
phoproteome_scores <- read_tsv("out/phoproteome/phoproteome-scores-bayes-plus.tsv")
phoproteome_preds_full <- read_tsv("out/phoproteome/phoproteome-scores-randforest.tsv")

phoproteome_preds <- filter(phoproteome_preds_full, score > 0.5) %>%
    select(kinase, site.id) %>%
    mutate(source = rep("IV-KAPhE", n()))
nb_scores <- filter(phoproteome_scores, score > 0.5) %>%
    select(kinase, site.id) %>%
    mutate(source = rep("Naive Bayes+", n()))
psp <- read_tsv("data/psp-kinase-substrates-all.tsv",
                col_names = c("kinase", "site.id", "seq.win")) %>%
    select(kinase, site.id) %>%
    mutate(source = rep("PhosphoSitePlus", n()))
linkphinder <- read_tsv("data/linkphinder.tsv") %>%
    filter(score > 0.672) %>%
    select(kinase, site.id) %>%
    mutate(source = rep("LinkPhinder", n()))
kinases <- readLines("data/kinases.txt")

kin_sub_dat <- bind_rows(phoproteome_preds, psp, nb_scores, linkphinder) %>%
    extract(col = site.id, into = c("substrate", "residue", "site"),
            regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", remove = FALSE,
            convert = TRUE) %>%
    mutate(is.kinase = substrate %in% kinases,
           seq.type = if_else(residue == "Y", "Y", "S/T"))
kin_sub_dat$source <- factor(kin_sub_dat$source,
                             levels = c("PhosphoSitePlus",
                                        "Naive Bayes+",
                                        "LinkPhinder",
                                        "IV-KAPhE"))

## filter(phoproteome_preds_full, site.id == "P07900-S709") %>%
##     pivot_longer(nb.score:experimental) %>%
##     ggplot(aes(x = value, color = pred.substrate)) +
##     geom_density() +
##     facet_wrap(vars(name), scales = "free")

## filter(phoproteome_preds_full, site.id == "Q02750-S298") %>%
##     ggplot(aes(x = nb.score, y = coexpression, color = pred.substrate)) +
##     geom_point()

## filter(phoproteome_preds_full, site.id == "O00571-S590",
##        nb.score < 0.5) %>%
##     pivot_longer(nb.score:experimental) %>%
##     ggplot(aes(x = value, color = pred.substrate)) +
##     geom_density() +
##     xlim(0, 1) +
##     facet_wrap(vars(name), scales = "free_y")

## filter(phoproteome_preds_full, site.id == "P07900-S709",
##        nb.score < 0.5, pred.substrate) %>% arrange(nb.score)

## group_by(kin_sub_dat, source, site.id) %>%
##     summarise(n.kin = n()) %>%
##     group_by(source, n.kin) %>%
##     summarise(n = n()) %>%
##     slice_max(n, n = 20) %>%
##     arrange(-n)

group_by(kin_sub_dat, source, site.id) %>%
    summarise(n.kin = n()) %>%
    ## slice_max(n.kin, n = 20) %>%
    arrange(-n.kin) %>%
    filter(source == "IV-KAPhE") %>%
    as.data.frame() %>%
    head(40)

group_by(kin_sub_dat, source, substrate, site.id) %>%
    summarise(n.kin = n()) %>%
    ungroup(site.id) %>%
    summarise(avg.n.kin = mean(n.kin)) %>%
    pivot_wider(names_from = source, values_from = avg.n.kin) %>%
    ggpairs(2:5)

med_kins_hist <- group_by(kin_sub_dat, source, substrate, site.id) %>%
    summarise(n.kin = n()) %>%
    ungroup(site.id) %>%
    summarise(med.n.kin = median(n.kin)) %>%
    ## ggplot(aes(x = reorder_within(substrate, avg.n.kin, source), y = avg.n.kin)) +
    ggplot(aes(x = med.n.kin)) +
    geom_histogram() +
    xlab("median # kinases assigned per site") +
    ylab("# proteins") +
    facet_wrap(vars(source), scales = "free") +
    theme(panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())

num_kins_hist <- group_by(kin_sub_dat, source, is.kinase, seq.type, site.id) %>%
    summarise(n.kin = n()) %>%
    ggplot(aes(x = n.kin, color = seq.type, fill = seq.type)) +
    geom_histogram() +
    xlab("# assigned kinases") +
    ylab("# phosphosites") +
    facet_wrap(vars(source), scales = "free") +
    scale_fill_brewer("phospho-\nacceptor", palette = "Dark2") +
    scale_color_brewer(guide = NULL, palette = "Dark2") +
    theme(legend.position = c(0.225, 0.875),
          legend.text = element_text(size = 9),
          legend.title = element_text(size = 10),
          panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())

p <- plot_grid(num_kins_hist, med_kins_hist, ncol = 2, labels = c("a", "b"))
save_plot("img/phosphoproteome-assignments.pdf", p, base_asp = 1.0, ncol = 4,
          nrow = 2, base_height = 2.3)

foo <- group_by(kin_sub_dat, source, is.kinase, seq.type, site.id) %>%
    summarise(n.kin = n()) %>%
    filter(source == "Naive Bayes+",
           seq.type == "S/T") %>%
    pull(n.kin) %>%
    hist(breaks = 30)
