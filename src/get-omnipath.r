library(OmnipathR)
library(tidyverse)

human <- 9606
all_dbs <- OmnipathR::get_interaction_resources()
raw_interactions <- as_tibble(import_omnipath_interactions(organism = human,
                                                           resources = all_dbs))

kinases <- readLines("data/kinases.txt")
interactions <- dplyr::filter(raw_interactions,
                              source %in% kinases,
                              target %in% kinases,
                              is_directed == 1,
                              xor(consensus_stimulation, consensus_inhibition)) %>%
    mutate(effect = consensus_stimulation - consensus_inhibition) %>%
    dplyr::select(source_genesymbol, target_genesymbol, effect) %>%
    distinct()

names(interactions) <- c("source", "target", "effect")

write_tsv(interactions, "data/kinase-omnipath.tsv")
