library(org.Hs.eg.db)
library(GOSemSim)
library(doParallel)
library(tidyverse)

args <- commandArgs(TRUE)
sites_file <- args[1]

sites <- read_tsv(sites_file)
if ("site.id" %in% colnames(sites)){
    sites <- extract(sites, site.id, "substrate", "([[:alnum:]-]+)-[STY][[:digit:]]+",
            remove = FALSE)
}else{
    sites <- extract(sites, substrate.id.site, "substrate", "([[:alnum:]-]+)-[STY][[:digit:]]+",
                     remove = FALSE)
}

hsGO_CC <- godata("org.Hs.eg.db", ont = "CC")
hsGO_BP <- godata("org.Hs.eg.db", ont = "BP")

eg2uniprot <- toTable(org.Hs.egUNIPROT)
eg2ensembl <- toTable(org.Hs.egENSEMBL)

kin_eg_m <- match(sites$kinase, eg2uniprot$uniprot_id)
kin_eg <- eg2uniprot$gene_id[kin_eg_m]
sites$kinase.eg <- kin_eg
sub_eg_m <- match(sites$substrate, eg2uniprot$uniprot_id)
sub_eg <- eg2uniprot$gene_id[sub_eg_m]
sites$substrate.eg <- sub_eg
semsim <- select(sites, kinase, substrate, kinase.eg, substrate.eg) %>%
    distinct()

registerDoParallel(cores = 63)
cl <- makeCluster(56)
semsim_res <- foreach(i = 1:nrow(semsim), .combine = "c", .maxcombine = 1000) %dopar% {
    kin_eg = semsim$kinase.eg[i]
    sub_eg = semsim$substrate.eg[i]
    gs_cc_res <- geneSim(kin_eg, sub_eg, semData = hsGO_CC, measure = "Wang", combine = "BMA")
    if (is.na(gs_cc_res) || !is.list(gs_cc_res)){
        gs_cc <- NA
    }else{
        gs_cc <- gs_cc_res$geneSim
    }
    gs_bp_res <- geneSim(kin_eg, sub_eg, semData = hsGO_BP, measure = "Wang", combine = "BMA")
    if (is.na(gs_bp_res) || !is.list(gs_bp_res)){
        gs_bp <- NA
    }else{
        gs_bp <- gs_bp_res$geneSim
    }
    c(gs_cc, gs_bp)
}
stopCluster(cl)
semsim_m <- matrix(semsim_res, ncol = 2, byrow = TRUE)
semsim$cc.semsim <- semsim_m[,1]
semsim$bp.semsim <- semsim_m[,2]
semsim_out_file <- sub(".tsv", "-go-semsim.tsv", sites_file)
select(semsim, -kinase.eg, -substrate.eg) %>%
    write_tsv(semsim_out_file)
