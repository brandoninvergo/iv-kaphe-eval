library(igraph)
library(tidyverse)

site_in_domain <- Vectorize(function(protein, site){
    pfam_prot <- filter(pfam, `seq id` == protein) %>%
        filter(`envelope start` <= site,
               `envelope end` >= site)
    return (nrow(pfam_prot) > 0)
})

argv <- commandArgs(TRUE)
subs_file <- argv[1]

kin_ints_tbl <- read_tsv("data/human-kinase-interactions.tsv")
kin_ints <- graph_from_data_frame(kin_ints_tbl, directed = FALSE)
kin_2nd_ints_tbl <- read_tsv("data/human-kinase-2nd-interactions.tsv")
kin_2nd_ints <- graph_from_data_frame(kin_2nd_ints_tbl, directed = FALSE)

pfam <- read_tsv("data/pfam-human-domains.tsv")
pfam_compact <- select(pfam, `seq id`, `hmm acc`) %>% chop(`hmm acc`)
kinases <- readLines("data/kinases.txt")
sig_doms1 <- read_tsv("out/kinase-int-domains-sig.tsv")
sig_doms2 <- read_tsv("out/kinase-sub-domains-sig.tsv")
sig_doms <- bind_rows(sig_doms1, sig_doms2) %>%
    select(kinase, domain) %>%
    distinct()

subs_tbl <- read_tsv(subs_file,
                     col_names = c("site.id", "seq.win")) %>%
    separate(col = site.id, into = c("substrate", "site"),
             sep = "-[STY]", remove = FALSE, convert = TRUE)
subs_feat <- select(subs_tbl, site.id, substrate, site, seq.win) %>%
    distinct() %>%
    mutate(in.domain = site_in_domain(substrate, site)) %>%
    left_join(pfam_compact, by = c("substrate" = "seq id"))

data_name <- strsplit(basename(subs_file), split = "\\.")[[1]][1]
out_dir <- paste0("data/", data_name, "-kinase-features/")
if (!dir.exists(out_dir)){
    dir.create(out_dir)
}

pb <- txtProgressBar(max = length(kinases), style = 3)
for (i in 1:length(kinases)){
    setTxtProgressBar(pb, i)
    kin <- kinases[i]
    out_file <- paste0(out_dir, data_name, "-", kin, "-feats.tsv")
    kin_doms <- filter(sig_doms, kinase == kin) %>% pull(domain)
    if (kin %in% V(kin_ints)$name){
        kin_neighbors <- neighbors(kin_ints, kin)$name
        kin_2nd_neighbors <- neighbors(kin_2nd_ints, kin)$name
    }else{
        kin_neighbors <- c()
        kin_2nd_neighbors <- c()
    }
    mutate(subs_feat,
           phys.int = substrate %in% kin_neighbors,
           phys.int.2nd = substrate %in% kin_2nd_neighbors,
           enriched.dom = unlist(lapply(`hmm acc`,
                                        function(sub_doms){
                                            any(sub_doms %in% kin_doms)
                                        }))) %>%
        select(site.id, seq.win, phys.int, phys.int.2nd, in.domain, enriched.dom) %>%
        distinct() %>%
        write_tsv(out_file, col_names = FALSE)
}
close(pb)
