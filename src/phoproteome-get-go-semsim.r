library(org.Hs.eg.db)
library(GOSemSim)
library(doParallel)
library(tidyverse)

sites <- read_tsv("data/human-phosphoproteome.tsv",
                  col_names = c("site.id", "seq.win")) %>%
    extract(site.id, "substrate", "([[:alnum:]-]+)-[STY][[:digit:]]+",
            remove = FALSE)

kinases <- readLines("data/kinases.txt")
substrates <- unique(sites$substrate)

hsGO_CC <- godata("org.Hs.eg.db", ont = "CC")
hsGO_BP <- godata("org.Hs.eg.db", ont = "BP")

eg2uniprot <- toTable(org.Hs.egUNIPROT)
eg2ensembl <- toTable(org.Hs.egENSEMBL)

## semsim <- select(sites, kinase, substrate) %>%
##     distinct() %>%
##     expand(kinase, substrate)
semsim <- expand_grid(kinase = kinases, substrate = substrates)


semsim$kinase.main <- unlist(lapply(strsplit(semsim$kinase, split = "-"),
                                    function (split) split[1]))

semsim$substrate.main <- unlist(lapply(strsplit(semsim$substrate, split = "-"),
                                       function (split) split[1]))

kin_eg_m <- match(semsim$kinase.main, eg2uniprot$uniprot_id)
kin_eg <- eg2uniprot$gene_id[kin_eg_m]
semsim$kinase.eg <- kin_eg
sub_eg_m <- match(semsim$substrate.main, eg2uniprot$uniprot_id)
sub_eg <- eg2uniprot$gene_id[sub_eg_m]
semsim$substrate.eg <- sub_eg

registerDoParallel(cores = 24)
cl <- makeCluster(24)
semsim_res <- foreach(i = 1:nrow(semsim), .combine = "c") %dopar% {
    kin_eg = semsim$kinase.eg[i]
    sub_eg = semsim$substrate.eg[i]
    gs_cc_res <- geneSim(kin_eg, sub_eg, semData = hsGO_CC, measure = "Wang",
                         combine = "BMA", drop = NULL)
    if (is.na(gs_cc_res) || !is.list(gs_cc_res)){
        gs_cc <- NA
    }else{
        gs_cc <- gs_cc_res$geneSim
    }
    gs_bp_res <- geneSim(kin_eg, sub_eg, semData = hsGO_BP, measure = "Wang",
                         combine = "BMA", drop = NULL)
    if (is.na(gs_bp_res) || !is.list(gs_bp_res)){
        gs_bp <- NA
    }else{
        gs_bp <- gs_bp_res$geneSim
    }
    c(gs_cc, gs_bp)
}
stopCluster(cl)
semsim_m <- matrix(semsim_res, ncol = 2, byrow = TRUE)
semsim$cc.semsim <- semsim_m[,1]
semsim$bp.semsim <- semsim_m[,2]
semsim_out_file <- "data/human-phosphoproteome-go-semsim.tsv"
select(semsim, -kinase.eg, -substrate.eg, -kinase.main, -substrate.main) %>%
    write_tsv(semsim_out_file)
