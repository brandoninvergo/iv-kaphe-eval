library(tidyverse)
library(ggseqlogo)
library(cowplot)
library(rhdf5)
library(gtools)
theme_set(theme_cowplot())

phoproteome_file <- "out/phoproteome-motif.h5"
phoproteome_st_file <- "out/phoproteome-ST-motif.h5"
phoproteome_y_file <- "out/phoproteome-Y-motif.h5"

ic_base <- h5readAttributes(phoproteome_file, "/IC", native = TRUE)$`logarithm base`

phoproteome_pfm  <- h5read(phoproteome_file, "/PFM", native = TRUE)
rownames(phoproteome_pfm) <- chr(h5readAttributes(phoproteome_file, "/",
                                            native = TRUE)$rownames)
phoproteome_relh <- h5read(phoproteome_file, "/relH", native = TRUE)
phoproteome_ic <- h5read(phoproteome_file, "/IC", native = TRUE)
phoproteome_dat <- tibble(position = 1:length(phoproteome_relh), relh = phoproteome_relh,
                    ic = phoproteome_ic)

phoproteome_st_pfm  <- h5read(phoproteome_st_file, "/PFM", native = TRUE)
rownames(phoproteome_st_pfm) <- chr(h5readAttributes(phoproteome_st_file, "/",
                                               native = TRUE)$rownames)
phoproteome_st_relh <- h5read(phoproteome_st_file, "/relH", native = TRUE)
phoproteome_st_ic <- h5read(phoproteome_st_file, "/IC", native = TRUE)
phoproteome_st_dat <- tibble(position = 1:length(phoproteome_st_relh), relh = phoproteome_st_relh,
                       ic = phoproteome_st_ic)

phoproteome_y_pfm  <- h5read(phoproteome_y_file, "/PFM", native = TRUE)
rownames(phoproteome_y_pfm) <- chr(h5readAttributes(phoproteome_y_file, "/",
                                               native = TRUE)$rownames)
phoproteome_y_relh <- h5read(phoproteome_y_file, "/relH", native = TRUE)
phoproteome_y_ic <- h5read(phoproteome_y_file, "/IC", native = TRUE)
phoproteome_y_dat <- tibble(position = 1:length(phoproteome_y_relh), relh = phoproteome_y_relh,
                       ic = phoproteome_y_ic)

ic_max <- log(20) / log(ic_base)
phoproteome_logo <- ggseqlogo(phoproteome_pfm) + ylim(0, ic_max) + theme(legend.position = "none")
phoproteome_st_logo <- ggseqlogo(phoproteome_st_pfm) + ylim(0, ic_max) + theme(legend.position = "none")
phoproteome_y_logo <- ggseqlogo(phoproteome_y_pfm) + ylim(0, ic_max) + theme(legend.position = "none")

phoproteome_relh_bar <- ggplot(phoproteome_dat, aes(x = position, y = relh)) +
    geom_col() +
    ylab("relative entropy vs. proteome frequencies")
phoproteome_ic_bar <- ggplot(phoproteome_dat, aes(x = position, y = ic)) +
    geom_col() +
    ylab("information content")

phoproteome_st_relh_bar <- ggplot(phoproteome_st_dat, aes(x = position, y = relh)) +
    geom_point() +
    geom_path() +
    scale_y_log10("relative entropy", limits = c(0.01, 6)) +
    annotation_logticks(sides = "l")
phoproteome_st_ic_bar <- ggplot(phoproteome_st_dat, aes(x = position, y = ic)) +
    geom_point() +
    geom_path() +
    scale_y_log10("information content", limits = c(0.01, 6)) +
    annotation_logticks(sides = "l")
phoproteome_y_relh_bar <- ggplot(phoproteome_y_dat, aes(x = position, y = relh)) +
    geom_point() +
    geom_path() +
    scale_y_log10("relative entropy", limits = c(0.01, 6)) +
    annotation_logticks(sides = "l")
phoproteome_y_ic_bar <- ggplot(phoproteome_y_dat, aes(x = position, y = ic)) +
    geom_point() +
    geom_path() +
    scale_y_log10("information content", limits = c(0.01, 6)) +
    annotation_logticks(sides = "l")
p <- plot_grid(phoproteome_st_logo, phoproteome_y_logo,
               phoproteome_st_ic_bar, phoproteome_y_ic_bar,
               phoproteome_st_relh_bar, phoproteome_y_relh_bar,
               nrow = 3, align="v", labels = c("a", "d", "b", "e", "c", "f"))

save_plot("img/phoproteome-vs-proteome.pdf", p, nrow = 3, ncol = 2, base_height = 2.5)
