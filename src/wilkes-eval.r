library(tidyverse)
library(viridis)
library(cowplot)
library(parallel)
library(ksea)                           # devtools::install_github("evocellnet/ksea")
theme_set(theme_cowplot())
source("src/rocr-ggplot2.r")

prepare_scores <- function(scores, good_pssms, Y_kinases){
    scores_mod <- scores
    if (!is.null(good_pssms))
        scores_mod <- filter(scores_mod, kinase %in% good_pssms)
    scores_mod$main.id <- unlist(lapply(strsplit(scores_mod$kinase, split = "-"),
                                      function(split) split[1]))
    scores_mod$y.kinase <- scores_mod$main.id %in% Y_kinases$kinase
    scores_mod$kinase.type <- rep(NA, nrow(scores_mod))
    scores_mod$kinase.type[which(scores_mod$y.kinase)] <- "Y"
    scores_mod$kinase.type[which(!scores_mod$y.kinase)] <- "S/T"
    scores_mod$kinase.type <- as.factor(scores_mod$kinase.type)
    scores_mod$y.kinase <- NULL
    return(scores_mod)
}

get_scores_bysite <- function(scores, method.name){
    scores_bysite <- group_by(scores, site.id) %>%
        mutate(score.rank = rank(-score),
               method = rep(method.name, n())) %>%
        arrange(site.id, kinase)
    return(scores_bysite)
}

add_seq_type <- function(scores_bysite, y_regexp){
    y.site <- grepl(y_regexp, scores_bysite$seq.win)
    scores_bysite$seq.type <- rep(NA, nrow(scores_bysite))
    scores_bysite$seq.type[y.site] <- "Y"
    scores_bysite$seq.type[!y.site] <- "S/T"
    return(scores_bysite)
}

get_regulons <- function(substrate_tbl){
    regulons <- list()
    for (kin in unique(substrate_tbl$kinase)){
        kin_subs <- filter(substrate_tbl, kinase == kin) %>%
            pull(site.id)
        regulons[[kin]] <- kin_subs
    }
    return(regulons)
}

do_ksea <- function(cond_names, regulons, substrate_tbl, num_trials){
    kin_act_tbl <- NULL
    pb <- txtProgressBar(max = length(cond_names), style = 3)
    for (i in 1:length(cond_names)){
        setTxtProgressBar(pb, i)
        cond <- cond_names[i]
        pvals <- paste0(cond, ".fdr")
        quant_tbl <- substrate_tbl[, c("site.id", cond, pvals)]
        quant_data <- deframe(quant_tbl[cond])
        pvals_data <- deframe(quant_tbl[pvals])
        sig <- which(pvals_data < 0.05)
        names(quant_data) <- quant_tbl$site.id
        quant_data <- quant_data[sig]
        quant_data <- quant_data[order(quant_data, decreasing = TRUE)]
        kin_act <- ksea_batchKinases(names(quant_data), quant_data, regulons,
                                     trial = num_trials)
        for (kin in names(regulons)){
            kin_quant <- deframe(filter(quant_tbl[sig,],
                                        site.id %in% regulons[[kin]])[cond])
            median_fc <- median(kin_quant)
            fc_sign <- sign(median_fc)
            ksea_i <- which(grepl(paste0("^", kin, "\\."), names(kin_act)))
            if (length(ksea_i) == 0)
                next
            names(kin_act)[ksea_i] <- kin
            if (is.na(kin_act[kin]))
                next
            if (kin_act[ksea_i] == 0)
                kin_act[ksea_i] <- 1/num_trials
            if (is.numeric(kin_act[ksea_i])){
                kin_act[ksea_i] <- -log10(kin_act[ksea_i]) * fc_sign
            }else{
                kin_act[ksea_i] <- NA
            }
        }
        if (is.null(kin_act_tbl)){
            kin_act_tbl <- tibble(kinase = names(kin_act))
        }
        kin_act_tbl[cond] <- kin_act
    }
    close(pb)
    return(kin_act_tbl)
}

do_ztest <- function(cond_names, regulons, substrate_tbl){
    kin_act_tbl <- NULL
    pb <- txtProgressBar(max = length(cond_names), style = 3)
    for (i in 1:length(cond_names)){
        setTxtProgressBar(pb, i)
        cond <- cond_names[i]
        pvals <- paste0(cond, ".fdr")
        quant_tbl <- substrate_tbl[c("site.id", cond)]
        quant_data <- deframe(quant_tbl[cond])
        mean_fc <- mean(quant_data, na.rm = TRUE)
        sd_fc <- sd(quant_data, na.rm = TRUE)
        kin_act <- c()
        for (kin in names(regulons)){
            kin_quant <- deframe(filter(quant_tbl, site.id %in% regulons[[kin]])[cond])
            n <- length(kin_quant)
            if (n < 3){
                act <- NA
            }else{
                kin_mean_fc <- mean(kin_quant, na.rm = TRUE)
                ## kin_sd_fc <- sd(kin_quant, na.rm = TRUE)
                ## se <- kin_sd_fc / sqrt(n)
                se <- sd_fc / sqrt(n)
                z_score <- (kin_mean_fc - mean_fc) / se
                ## fc_sign <- sign(z_score)
                fc_sign <- sign(kin_mean_fc)
                z_p <- 2 * pnorm(abs(z_score), lower.tail = FALSE)
                act <- fc_sign * -log10(z_p)
            }
            kin_act <- c(kin_act, act)
        }
        if (is.null(kin_act_tbl)){
            kin_act_tbl <- tibble(kinase = names(regulons))
        }
        kin_act_tbl[cond] <- kin_act
    }
    close(pb)
    return(kin_act_tbl)
}

args <- commandArgs(TRUE)
win_len <- as.integer(args[1])

## phosphosite regular expressions
half_winlen <- floor(win_len/2)
s_regexp <- paste0(".{", half_winlen, "}[S].{", half_winlen, "}")
t_regexp <- paste0(".{", half_winlen, "}[T].{", half_winlen, "}")
y_regexp <- paste0(".{", half_winlen, "}[Y].{", half_winlen, "}")

## various data
pssm_info <- read_tsv("out/pssm-info.tsv")
good_pssms <- unique(filter(pssm_info, seq.n >= 20)$kinase)
Y_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase"))
ST_kinases <- read_tsv("data/kinases-ST.txt", col_names = c("kinase"))

kinase_id_map <- read_tsv("data/kinase-id-map.tsv",
                          col_names = c("kinase", "prot.name", "gene.name"))

## Get the quantitative phosphoproteomics data
wilkes_quant <- read_tsv("data/wilkes-et-al-2015-inhib.tsv")
wilkes_cond_names <- colnames(wilkes_quant)[3:24]

## wilkes_bayesp <- prepare_scores("out/wilkes-inhib/wilkes-inhib-scores-bayes-plus.tsv",
##                                 good_pssms, Y_kinases) %>%
##     add_seq_type(y_regexp)##  %>%
    ## left_join(wilkes_quant, by = c("site.id", "seq.win"))

ivkaphe <- read_tsv("out/wilkes-inhib/wilkes-inhib-scores-randforest.tsv") %>%
    prepare_scores(good_pssms, Y_kinases) %>%
    add_seq_type(y_regexp)

networkin <- read_tsv("out/wilkes-inhib/wilkes-inhib-scores-networkin.tsv") %>%
    prepare_scores(NULL, Y_kinases) %>%
    add_seq_type(y_regexp)

linkphinder <- read_tsv("data/linkphinder.tsv") %>%
    prepare_scores(NULL, Y_kinases) %>%
    inner_join(wilkes_quant, by = "site.id") %>%
    add_seq_type(y_regexp) %>%
    select(kinase, site.id, seq.win, score, main.id, kinase.type, seq.type)

gps <- read_tsv("out/wilkes-inhib/wilkes-inhib-assignments-gps.tsv") %>%
    prepare_scores(NULL, Y_kinases) %>%
    add_seq_type(y_regexp)

psp <- read_tsv("data/psp-kinase-substrates.tsv",
                col_names = c("kinase", "site.id", "seq.win")) %>%
    prepare_scores(NULL, Y_kinases) %>%
    add_seq_type(y_regexp)

ivkaphe_filt <- filter(ivkaphe, score > 0.5)
ivkaphe_filt_regulons_full <- get_regulons(ivkaphe_filt)

networkin_filt <- filter(networkin, score > 1.0)
networkin_filt_regulons_full <- get_regulons(networkin_filt)

linkphinder_filt <- filter(linkphinder, site.id %in% wilkes_quant$site.id,
                           score > 0.672)
linkphinder_filt_regulons_full <- get_regulons(linkphinder_filt)

gps_filt <- gps
gps_filt_regulons_full <- get_regulons(gps_filt)

psp_filt <- psp
psp_filt_regulons_full <- get_regulons(psp_filt)

out_dir <- "out/wilkes-inhib-kinact/"
if (!dir.exists(out_dir))
    dir.create(out_dir)

ivkaphe_kin_act <- do_ztest(wilkes_cond_names, ivkaphe_filt_regulons_full, wilkes_quant)
write_tsv(ivkaphe_kin_act, paste0(out_dir, "wilkes-ivkaphe-kin-act.tsv"))

networkin_kin_act <- do_ztest(wilkes_cond_names, networkin_filt_regulons_full, wilkes_quant)
write_tsv(networkin_kin_act, paste0(out_dir, "wilkes-networkin-kin-act.tsv"))

linkphinder_kin_act <- do_ztest(wilkes_cond_names, linkphinder_filt_regulons_full, wilkes_quant)
write_tsv(linkphinder_kin_act, paste0(out_dir, "wilkes-linkphinder-kin-act.tsv"))

gps_kin_act <- do_ztest(wilkes_cond_names, gps_filt_regulons_full, wilkes_quant)
write_tsv(gps_kin_act, paste0(out_dir, "wilkes-gps-kin-act.tsv"))

psp_kin_act <- do_ztest(wilkes_cond_names, psp_filt_regulons_full, wilkes_quant)
write_tsv(psp_kin_act, paste0(out_dir, "wilkes-psp-kin-act.tsv"))

inhib_kin_genes <- c("AKT1 AKT2 AKT3", #https://www.selleckchem.com/products/akti-1-2.html
                     "AKT1 AKT2 AKT3", #https://www.selleckchem.com/products/MK-2206.html
                     "CAMK2A CAMK2B CAMK2D CAMK2G", #https://www.selleckchem.com/products/kn-93.html
                     "CAMK2A CAMK2B CAMK2D CAMK2G CAMK1 CAMK1D CAMK1G CAMK4", #https://www.selleckchem.com/products/kn-62.html
                     "EGFR",        #https://www.selleckchem.com/products/pd168393.html
                     "EGFR", #https://www.selleckchem.com/products/pd153035.html
                     "MAPK1 MAPK3", #https://www.sigmaaldrich.com/catalog/product/mm/328007?lang=en&region=GB
                     "MAPK1 MAPK3", #https://www.sigmaaldrich.com/catalog/product/mm/328006m?lang=en&region=GB
                     "MAP2K1 MAP2K2", #https://www.selleckchem.com/products/gsk1120212-jtp-74057.html
                     "MAP2K1 MAP2K2", #https://www.medchemexpress.com/u-0126.html
                     "MTOR",          #https://pubchem.ncbi.nlm.nih.gov/compound/Ku-0063794#section=Biomolecular-Interactions-and-Pathways
                     "MTOR",          #https://www.cellsignal.com/products/activators-inhibitors/torin-1/14379
                     "RPS6KB1",       #https://www.selleckchem.com/products/pf-4708671.html
                     "RPS6KB1",       #https://www.sigmaaldrich.com/catalog/product/mm/559274?lang=en&region=GB
                     "PIK3CA PIK3CB PIK3CD PIK3CG", #https://www.stemcell.com/gdc-0941.html
                     "PIK3CA PIK3CB PIK3CD PIK3CG MTOR", #https://www.stemcell.com/gdc-0941.html
                     "PRKCA PRKCB PRKCG PRKCD PRKCZ JAK2 FLT3",          #https://www.selleckchem.com/products/go6976.html https://pubmed.ncbi.nlm.nih.gov/16956345/
                     "PRKCA PRKCB PRKCG", #https://www.selleckchem.com/products/gf109203x.html
                     "ROCK2 CAMK2A CAMK2B CAMK2D CAMK2G PRKG1 PRKG2 AURKA PRKACA PRKACB PRKACG PRKCA PRKCB PRKCB PRKCG PRKCD PRKCE PRKCH PRKCQ PRKCI PRKCZ",            #https://www.selleckchem.com/products/h-1152-dihydrochloride.html
                     "ROCK1 ROCK2"  #https://www.selleckchem.com/products/Y-27632.html
                     )

kinnet <- read_tsv("data/kinase-regulatory-net.tsv") %>%
    mutate(int.id = paste(source, target))
## kinnet <- read_tsv("data/kinase-omnipath.tsv") %>%
##     mutate(effect = if_else(effect > 0, "activating", "inhibiting"),
##            int.id = paste(source, target))

pull_reg_effect <- Vectorize(function(id){
    filter(kinnet, int.id == id) %>%
        pull(effect)
})

## The phosphoproteomics data was collected from MCF7 breast cancer cells
breast_cancer_kinases <- read_tsv("data/raw/protein-atlas/pathology.tsv") %>%
    filter(Cancer == "breast cancer", `Gene name` %in% kinase_id_map$gene.name) %>%
    mutate(total.patients = High + Medium + Low + `Not detected`,
           ## in.breast.cancer = High + Medium + Low > 0.50 * total.patients) %>%
           in.breast.cancer = High + Medium + Low > 0) %>%
    filter(in.breast.cancer) %>%
    pull(`Gene name`)

ivkaphe_kin_act_f <- select(ivkaphe_kin_act, -PP2A1, -PP2A2) %>%
    mutate(source = rep("IV-KAPhE", n())) %>%
    pivot_longer(cols = 2:21, names_to = "condition", values_to = "activity") %>%
    mutate(inhib.kinase = rep(inhib_kin_genes, length(unique(kinase))))
networkin_kin_act_f <- select(networkin_kin_act, -PP2A1, -PP2A2) %>%
    mutate(source = rep("NetworKIN 3.0", n())) %>%
    pivot_longer(cols = 2:21, names_to = "condition", values_to = "activity") %>%
    mutate(inhib.kinase = rep(inhib_kin_genes, length(unique(kinase))))
linkphinder_kin_act_f <- select(linkphinder_kin_act, -PP2A1, -PP2A2) %>%
    mutate(source = rep("LinkPhinder", n())) %>%
    pivot_longer(cols = 2:21, names_to = "condition", values_to = "activity") %>%
    mutate(inhib.kinase = rep(inhib_kin_genes, length(unique(kinase))))
gps_kin_act_f <- select(gps_kin_act, -PP2A1, -PP2A2) %>%
    mutate(source = rep("GPS 5.0", n())) %>%
    pivot_longer(cols = 2:21, names_to = "condition", values_to = "activity") %>%
    mutate(inhib.kinase = rep(inhib_kin_genes, length(unique(kinase))))
psp_kin_act_f <- select(psp_kin_act, -PP2A1, -PP2A2) %>%
    mutate(source = rep("PhosphoSitePlus", n())) %>%
    pivot_longer(cols = 2:21, names_to = "condition", values_to = "activity") %>%
    mutate(inhib.kinase = rep(inhib_kin_genes, length(unique(kinase))))

grepl_v <- Vectorize(grepl)
is_target <- Vectorize(function(kinases, substrate){
    int.ids <- paste(strsplit(kinases, split = " ")[[1]], substrate)
    return (any(int.ids %in% kinnet$int.id))
})
get_effect <- Vectorize(function(kinases, substrate){
    int.ids <- paste(strsplit(kinases, split = " ")[[1]], substrate)
    regs <- filter(kinnet, int.id %in% int.ids) %>% pull(effect)
    if (is.na(regs) || length(regs) == 0)
        return (NA)
    regs_t <- table(regs)
    return (names(regs_t)[which.max(regs_t)])
})

kin_act_full <- bind_rows(ivkaphe_kin_act_f, networkin_kin_act_f,
                          linkphinder_kin_act_f, gps_kin_act_f,
                          psp_kin_act_f) %>%
    left_join(kinase_id_map, by = "kinase") %>%
    mutate(downstream.target = is_target(inhib.kinase, gene.name),
           effect = get_effect(inhib.kinase, gene.name),
           is.inhibited = grepl_v(gene.name, inhib.kinase),
           class = rep(NA, n())) %>%
    filter(gene.name %in% breast_cancer_kinases,
           !is.na(activity))
kin_act_full$source <- factor(kin_act_full$source,
                              levels = c("NetworKIN 3.0",
                                         "GPS 5.0",
                                         "LinkPhinder",
                                         "IV-KAPhE",
                                         "PhosphoSitePlus"))
down_reg_subs <- kin_act_full$downstream.target & kin_act_full$effect == "inhibiting"
up_reg_subs <- kin_act_full$downstream.target & kin_act_full$effect == "activating"
kin_act_full$class[down_reg_subs] <- "inhibited\nsubstrate"
kin_act_full$class[up_reg_subs] <- "activated\nsubstrate"
kin_act_full$class[kin_act_full$is.inhibited] <- "target\nkinase"
kin_act_full$class <- factor(kin_act_full$class,
                             levels = c("target\nkinase",
                                        "activated\nsubstrate",
                                        "inhibited\nsubstrate"))

inhib_plot <- filter(kin_act_full, !is.na(class)) %>%
    arrange(is.inhibited) %>%
    ggplot(aes(x = condition, y = activity, color = class)) +
    geom_hline(yintercept = 2.5, lty = "dashed", color = "gray") +
    geom_hline(yintercept = -2.5, lty = "dashed", color = "gray") +
    geom_hline(yintercept = 0, lty = "dashed", color = "black") +
    geom_jitter(aes(shape = class), width = 0.15, alpha = 0.75) +
    facet_wrap(vars(source), ncol = 2, scales="free_y") +
    scale_x_discrete(labels = c("AKT1" = expression(AKT^1),
                                "AKT2" = expression(AKT^2),
                                "CAMK1" = expression(CAMK^1),
                                "CAMK2" = expression(CAMK^2),
                                "EGFR1" = expression(EGFR^1),
                                "EGFR2" = expression(EGFR^2),
                                "ERK1" = expression(ERK^1),
                                "ERK2" = expression(ERK^2),
                                "MEK1" = expression(MEK^1),
                                "MEK2" = expression(MEK^2),
                                "MTOR1" = expression(MTOR^1),
                                "MTOR2" = expression(MTOR^2),
                                "P70S6K1" = expression(P70S6K^1),
                                "P70S6K2" = expression(P70S6K^2),
                                "PI3K1" = expression(PI3K^1),
                                "PI3K2" = expression(PI3K^2),
                                "PKC1" = expression(PKC^1),
                                "PKC2" = expression(PKC^2),
                                "ROCK1" = expression(ROCK^1),
                                "ROCK2" = expression(ROCK^2))) +
    scale_color_brewer(NULL, palette="Dark2") +
    scale_shape_discrete(NULL) +
    ylab("kinase activity") +
    xlab("kinase inhibitor condition") + 
    ## coord_flip() +
    theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1),
          legend.position = c(0.55, 0.0), legend.direction = "horizontal",
          legend.background = element_blank(),
          legend.box.background = element_rect(color = "black"),
          legend.box.margin = margin(5, 5, 5, 5))
save_plot("img/wilkes-inhib.pdf", inhib_plot, base_asp = 1.0, ncol = 2,
          nrow = 2, base_height = 5)

kin_act_downs <- filter(kin_act_full, class %in% c("target\nkinase",
                                                   "activated\nsubstrate"))
method_ns <- group_by(kin_act_downs, source) %>%
    select(source, kinase) %>%
    distinct() %>%
    summarise(n = n())
method_ns
kin_act_downs$source <- factor(kin_act_downs$source,
                               levels = c("NetworKIN 3.0",
                                          "GPS 5.0",
                                          "LinkPhinder",
                                          "IV-KAPhE",
                                          "PhosphoSitePlus"),
                               labels = paste0(c("NetworKIN 3.0", "GPS 5.0",
                                                 "LinkPhinder", "IV-KAPhE",
                                                 "PhosphoSitePlus"), "\n(n = ",
                                               pull(method_ns, n), ")"))

violin_plot <- ggplot(kin_act_downs, aes(x = source, y = activity)) +
    geom_hline(yintercept = 2.5, lty = "dashed", color = "gray") +
    geom_hline(yintercept = -2.5, lty = "dashed", color = "gray") +
    geom_hline(yintercept = 0, lty = "dashed", color = "black") +
    geom_jitter(aes(color = class, shape = class), width = 0.2, alpha = 0.75) +
    geom_violin(draw_quantiles = 0.5, alpha = 0.4) +
    ylab("kinase activity") +
    xlab("model\n(n = unique kinases)") +
    scale_shape_discrete("kinase", guide = NULL) +
    scale_color_brewer("kinase", palette = "Dark2", guide = NULL)

p <- plot_grid(inhib_plot, violin_plot, nrow = 2, rel_heights = c(1.7, 1),
               labels = c("a", "b"))
save_plot("img/wilkes-eval.pdf", p, nrow = 3, base_asp = 3.5, base_height = 2.5)

iv_acts <- filter(kin_act_downs, source == "IV-KAPhE\n(n = 97)") %>%
    select(kinase, condition, activity)
pvals <- c()
wt_stats <- c()
other_sources <- c()
ns <- c()
mean_diffs <- c()
for (other_source in levels(kin_act_downs$source)){
    if (other_source == "IV-KAPhE\n(n = 97)")
        next
    other_acts <- filter(kin_act_downs, source == other_source) %>%
        select(kinase, condition, activity)
    comp_acts <- inner_join(iv_acts, other_acts, by = c("kinase", "condition"),
                            suffix = c(".ivkaphe", ".other"))
    wt <- wilcox.test(comp_acts$activity.ivkaphe, comp_acts$activity.other,
                      paired=TRUE, alternative = "less")
    pvals <- c(pvals, wt$p.value)
    wt_stats <- c(wt_stats, wt$statistic)
    other_sources <- c(other_sources, other_source)
    ns <- c(ns, nrow(comp_acts))
    mean_diffs <- c(mean_diffs, mean(comp_acts$activity.ivkaphe - comp_acts$activity.other))
}
res_tbl <- tibble(source = other_sources,
                  stat = wt_stats, p.value = pvals,
                  n = ns, mean.diff = mean_diffs) %>%
    mutate(p.adj = p.adjust(pvals, method = "fdr"))
res_tbl
