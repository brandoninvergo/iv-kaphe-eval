library(tidyverse)

argv <- commandArgs(TRUE)
n_folds <- as.integer(argv[1])

sugiyama <- read_tsv("data/sugiyama-kinase-substrates.tsv") %>%
    slice(sample(1:n()))

sites <- select(sugiyama, substrate.id.site) %>% distinct() %>%
    slice(sample(1:n()))
n <- nrow(sites)
test_size <- n %/% n_folds
train_size <- n - test_size

out_dir <- "data/sugiyama-folds/"
if (!dir.exists(out_dir))
    dir.create(out_dir)

fold_starts <- seq(1, n, by = test_size)[1:n_folds]
fold_ends <- fold_starts + test_size - 1
fold_ends[n_folds] <- n

for (k in 1:n_folds){
    fold_dir <- paste0(out_dir, "fold-", k, "/")
    if (!dir.exists(fold_dir))
        dir.create(fold_dir)
    if (!dir.exists(paste0(fold_dir, "/test")))
        dir.create(paste0(fold_dir, "/test"))
    if (!dir.exists(paste0(fold_dir, "/train")))
        dir.create(paste0(fold_dir, "/train"))
    fold_start <- fold_starts[k]
    fold_end <- fold_ends[k]
    slice(sites, fold_start:fold_end) %>%
        left_join(sugiyama, by = "substrate.id.site") %>%
        arrange(kinase, substrate.id.site) %>%
        select(kinase, substrate.id.site, seq.win) %>%
        write_tsv(paste0(fold_dir, "test/sugiyama-test.tsv")) %>%
        select(-kinase) %>%
        distinct() %>%
        write_tsv(paste0(fold_dir, "test/sugiyama-test-seqwins.tsv"))
    slice(sites, -(fold_start:fold_end)) %>%
        left_join(sugiyama, by = "substrate.id.site") %>%
        arrange(kinase, substrate.id.site) %>%
        select(kinase, substrate.id.site, seq.win) %>%
        write_tsv(paste0(fold_dir, "train/sugiyama-train.tsv"))
}


