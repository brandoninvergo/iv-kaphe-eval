import sys
from Bio import SeqIO


def read_proteome(proteome_h):
    proteome = dict()
    for seqrec in SeqIO.parse(proteome_h, "fasta"):
        prot_id = seqrec.id.split("|")[1]
        proteome[prot_id] = str(seqrec.seq)
    return proteome


def read_prot_sites():
    line = sys.stdin.readline()
    while line:
        prot_id, site = line.split('\t')
        yield((prot_id, site))
        line = sys.stdin.readline()


def seq_window(prot_id, seq, site_str, winlen):
    res = site_str[0]
    site = int(site_str[1:])
    site -= 1
    half = winlen//2
    try:
        assert(site >= 0 and site < len(seq))
    except AssertionError:
        return ("NA", "NA")
    try:
        assert(seq[site] in ('S', 'T', 'Y'))
    except AssertionError:
        sys.stderr.write("Bad seq: " + prot_id + " " + site_str + "\n")
        return ("NA", "NA")
    try:
        assert(seq[site] == res)
    except AssertionError:
        sys.stderr.write("Wrong residue: " + prot_id + " - " + site_str + "\n")
        return ("NA", "NA")
    start = site-half
    end = site+half+1
    if start < 0:
        startstr = -start * "_" + seq[0:(half+start)+1]
    else:
        startstr = seq[start:start+half+1]
    if end > len(seq):
        endstr = seq[start+half+1:len(seq)] + (end-len(seq)) * "_"
    else:
        endstr = seq[start+half+1:end]
    return("-".join([prot_id, str(site+1)]), startstr+endstr)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        sys.exit("Usage: <script> WINDOW_LENGTH PROTEOME")
    winlen = int(sys.argv[1])
    proteome_file = sys.argv[2]
    with open(proteome_file) as h:
        proteome = read_proteome(h)
    for prot_id, site in read_prot_sites():
        if prot_id not in proteome:
            print("NA")
            continue
        seq = proteome[prot_id]
        window = seq_window(prot_id, seq, site, winlen)
        print(window[1])
