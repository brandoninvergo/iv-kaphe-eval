import os.path
from Bio import Entrez
import time

from urllib.error import HTTPError

def get_gene_ids(kinome):
    kinome_entrez = dict()
    with open("data/uniprot-to-entrez.tsv") as h:
        for line in h:
            uniprot, entrez = line.strip().split("\t")
            if uniprot in kinome:
                kinome_entrez[entrez] = uniprot
    return kinome_entrez


def read_entrez_data():
    kinome_citations = dict()
    kinome_citations_filt = dict()
    with open("data/kinome-entrez-data.tsv") as h:
        for line in h:
            kinase, num_cites, num_cites_filt = line.strip().split()
            kinome_citations[kinase] = num_cites
            kinome_citations_filt[kinase] = num_cites_filt
    return (kinome_citations, kinome_citations_filt)


def get_citations(kinome_entrez):
    if os.path.exists("data/kinome-entrez-data.tsv"):
        kinome_citations, kinome_citations_filt = read_entrez_data()
        return (kinome_citations, kinome_citations_filt)
    Entrez.email = "invergo@ebi.ac.uk"
    kinome_citations = dict()
    kinome_num_citations = dict()
    kinome_num_citations_filt = dict()
    pubs = dict()
    for entrez in kinome_entrez.keys():
        kinase = kinome_entrez[entrez]
        print(kinase)
        attempt = 0
        while attempt < 3:
            attempt += 1
            try:
                handle = Entrez.elink(dbfrom="gene", id=entrez, db="pubmed",
                                      retmode="xml")
            except HTTPError as err:
                if 500 <= err.code <= 599:
                    print("Received error from server {0}".format(err))
                    print("Attempt {0} of 3".format(attempt))
                    time.sleep(15)
                else:
                    raise
            record = Entrez.read(handle)
            handle.close()
            kinome_num_citations[kinase] = len(record[0]["LinkSetDb"][0]["Link"])
            kinome_citations[kinase] = set([pub["Id"] for pub in
                                            record[0]["LinkSetDb"][0]["Link"]])
            for pub_rec in record[0]["LinkSetDb"][0]["Link"]:
                pub = pub_rec["Id"]
                if pub not in pubs:
                    pubs[pub] = 1
                else:
                    pubs[pub] = pubs[pub] + 1
    bad_pubs = set()
    for pub in pubs:
        if pubs[pub] <= 10:
            continue
        bad_pubs.add(pub)
    for kinase in kinome_citations:
        kin_pubs = kinome_citations[kinase]
        good_pubs = set(kin_pubs) - bad_pubs
        kinome_num_citations_filt[kinase] = len(good_pubs)
    return (kinome_num_citations, kinome_num_citations_filt)


def write_num_citations(kinome_citations, kinome_citations_filt):
    with open("data/kinome-entrez-data.tsv", 'w') as h:
        for kinase in kinome_citations:
            num_citations = kinome_citations[kinase]
            num_citations_filt = kinome_citations_filt[kinase]
            h.write("\t".join([kinase, str(num_citations), str(num_citations_filt)])+"\n")


if __name__ == "__main__":
    kinome = set()
    with open("data/kinases.txt") as h:
        for line in h:
            kinome.add(line.strip())
    kinome_entrez = get_gene_ids(kinome)
    kinome_citations, kinome_citations_filt = get_citations(kinome_entrez)
    write_num_citations(kinome_citations, kinome_citations_filt)
    with open("data/kinase-citations.tsv", 'w') as h:
        h.write("\t".join(["kinase", "num.cites", "num.cites.filt"])+"\n")
        for kinase in kinome:
            if kinase not in kinome_citations:
                num_cites = 0
                num_cites_filt = 0
            else:
                num_cites = kinome_citations[kinase]
                num_cites_filt = kinome_citations_filt[kinase]
            h.write("\t".join([kinase, str(num_cites), str(num_cites_filt)])+"\n")
