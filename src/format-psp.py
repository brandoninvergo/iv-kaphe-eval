import sys
import re
from Bio import SeqIO


PROT_ID_RE = re.compile(r"^([A-Za-z0-9-]+)")
SITES_RE = re.compile(r"p-([STY][0-9]+)")
HGNC_RE = re.compile(r"GN=([A-Za-z0-9-]+)")


def parse_psp_file(psp_h, in_vivo):
    psp = dict()
    # skip first three lines
    psp_h.readline()
    psp_h.readline()
    psp_h.readline()
    header = psp_h.readline()
    header_spl = header.strip().split("\t")
    for line in psp_h:
        line_spl = line.strip().split("\t")
        # Only take human phosphorylation
        if line_spl[3] != "human" or line_spl[8] != "human":
            continue
        # Skip autophosphorylation
        if line_spl[2] == line_spl[6]:
            continue
        if in_vivo:
            # Only keep in vivo sites
            if line_spl[13] != "X":
                continue
        kinase = line_spl[2]
        substrate = line_spl[6]
        site = line_spl[9]
        seq_win = line_spl[11].upper()
        if kinase not in psp:
            psp[kinase] = [(substrate, site, seq_win)]
        else:
            psp[kinase].append((substrate, site, seq_win))
    return psp


def parse_proteome(proteome_h):
    proteome = dict()
    for seqrec in SeqIO.parse(proteome_h, "fasta"):
        prot_id = seqrec.id.split("|")[1]
        proteome[prot_id] = str(seqrec.seq)
    return proteome


def seq_window(prot_id, seq, site_str, winlen):
    res = site_str[0]
    site = int(site_str[1:])
    site -= 1
    half = winlen//2
    try:
        assert(site >= 0 and site < len(seq))
    except AssertionError:
        return ("NA", "NA")
    try:
        assert(seq[site] in ('S', 'T', 'Y'))
    except AssertionError:
        sys.stderr.write("Bad seq: " + prot_id + " " + site_str + "\n")
        return ("NA", "NA")
    try:
        assert(seq[site] == res)
    except AssertionError:
        sys.stderr.write("Wrong residue: " + prot_id + " - " + site_str + "\n")
        return ("NA", "NA")
    start = site-half
    end = site+half+1
    if start < 0:
        startstr = -start * "_" + seq[0:(half+start)+1]
    else:
        startstr = seq[start:start+half+1]
    if end > len(seq):
        endstr = seq[start+half+1:len(seq)] + (end-len(seq)) * "_"
    else:
        endstr = seq[start+half+1:end]
    return("-".join([prot_id, str(site+1)]), startstr+endstr)


def print_data(psp, proteome, winlen):
    for kinase, sites in psp.items():
        for site_info in sites:
            substrate, site, psp_seqwin = site_info
            try:
                prot_seq = proteome[substrate]
            except KeyError:
                continue
            site_id = substrate + "-" + site
            seqwin = seq_window(substrate, prot_seq, site, winlen)[1]
            if seqwin == "NA":
                continue
            mid = winlen // 2 - 1
            if seqwin[mid-5:mid+6] != psp_seqwin[1:12]:
                continue
            line = [kinase, site_id, seqwin]
            print("\t".join(line))


if __name__ == "__main__":
    if len(sys.argv) not in [4, 5]:
        sys.exit("USAGE: <script> PSP_FILE PROTEOME WINLEN [--in-vivo]")
    psp_file = sys.argv[1]
    proteome_file = sys.argv[2]
    winlen = int(sys.argv[3])
    if len(sys.argv) == 5 and sys.argv[4] == "--in-vivo":
        in_vivo = True
    else:
        in_vivo = False
    with open(psp_file, 'r', errors="replace") as h:
        psp = parse_psp_file(h, in_vivo)
    with open(proteome_file) as h:
        proteome = parse_proteome(h)
    print_data(psp, proteome, winlen)
