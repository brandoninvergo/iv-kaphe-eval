library(rhdf5)
library(parallel)
library(tidyverse)

argv <- commandArgs(TRUE)
n_folds <- as.integer(argv[1])
win_len <- as.integer(argv[2])

half_winlen <- floor(win_len/2)
s_regexp <- paste0(".{", half_winlen, "}[S].{", half_winlen, "}")
t_regexp <- paste0(".{", half_winlen, "}[T].{", half_winlen, "}")
y_regexp <- paste0(".{", half_winlen, "}[Y].{", half_winlen, "}")

Y_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase"))
ST_kinases <- read_tsv("data/kinases-ST.txt", col_names = c("kinase"))

get_motif_info <- function(pssm_dir){
    kinases <- NULL
    seq_ns <- NULL
    for (motif_file in list.files(pssm_dir)){
        if (!endsWith(motif_file, "-motif.h5"))
            next
        motif_path <- paste(pssm_dir, motif_file, sep = "/")
        motif_name <- h5readAttributes(motif_path, "/", native = TRUE)$name
        name_split <- strsplit(motif_name, split = "-")[[1]]
        if (length(name_split) == 2){
            kinase <- name_split[1]
        }else{
            kinase <- paste(name_split[1:2], collapse = "-")
        }
        seqn <- h5readAttributes(motif_path, "/", native = TRUE)$`number of sequences`
        h5closeAll()
        if (is.null(kinases)){
            kinases <- kinase
            seq_ns <- seqn
        }else{
            kinases <- c(kinases, kinase)
            seq_ns <- c(seq_ns, seqn)
        }
    }
    motif_info <- tibble(kinase = kinases, seq.n = seq_ns)
    return(motif_info)
}

prepare_scores <- function(score_file, motif_dir, Y_kinases){
    motif_info <- get_motif_info(motif_dir)
    good_pssms <- filter(motif_info, seq.n >= 20) %>% pull(kinase)
    scores <- read_tsv(score_file) %>%
        filter(kinase %in% good_pssms)
    scores$main.id <- unlist(lapply(strsplit(scores$kinase, split = "-"),
                                    function(split) split[1]))
    scores$y.kinase <- scores$main.id %in% Y_kinases$kinase
    scores$kinase.type <- rep(NA, nrow(scores))
    scores$kinase.type[which(scores$y.kinase)] <- "Y"
    scores$kinase.type[which(!scores$y.kinase)] <- "S/T"
    scores$kinase.type <- as.factor(scores$kinase.type)
    scores$y.kinase <- NULL
    return(scores)
}

get_scores_bysite <- function(scores, method.name, true_pos){
    scores_bysite <- group_by(scores, site.id) %>%
        mutate(score.rank = rank(-score),
               method = rep(method.name, n())) %>%
        ## arrange(site.id, kinase) %>%
        inner_join(true_pos, by = c("site.id", "seq.win")) %>%
        ungroup()
    return(scores_bysite)
}

## micro-averaged multi-label evaluation
micro_ml <- function(scores_bysite_cut, with.ci = FALSE){
    if (with.ci){
        micro_ml_sum <- select(scores_bysite_cut, site.id, kinase, method, tp, fn, fp, tn) %>%
            group_by(method, kinase) %>%
            summarise(tp.sum = sum(tp),
                      fp.sum = sum(fp),
                      tn.sum = sum(tn),
                      fn.sum = sum(fn)) %>%
            ungroup(kinase) %>%
            summarise(micro.prec.value = ifelse(sum(tp.sum) + sum(fp.sum) == 0, 0,
                                          sum(tp.sum) / (sum(tp.sum) + sum(fp.sum))),
                      micro.rec.value = ifelse(sum(tp.sum) + sum(fn.sum) == 0, 0,
                                         sum(tp.sum) / (sum(tp.sum) + sum(fn.sum))),
                      micro.f1.value = ifelse(micro.prec.value == 0 && micro.rec.value == 0, 0,
                                        2 * ((micro.prec.value * micro.rec.value)/(micro.prec.value + micro.rec.value))),
                      micro.fb.value = ifelse(micro.prec.value == 0 && micro.rec.value == 0, 0,
                                        2 * ((micro.prec.value * micro.rec.value)/((0.5^2)*micro.prec.value + micro.rec.value))),
                      micro.prec.upper = NA,
                      micro.rec.upper = NA,
                      micro.f1.upper = NA,
                      micro.fb.upper = NA,
                      micro.prec.lower = NA,
                      micro.rec.lower = NA,
                      micro.f1.lower = NA,
                      micro.fb.lower = NA)
    }else{
        micro_ml_sum <- select(scores_bysite_cut, site.id, kinase, method, tp, fn, fp, tn) %>%
            group_by(method, kinase) %>%
            summarise(tp.sum = sum(tp),
                      fp.sum = sum(fp),
                      tn.sum = sum(tn),
                      fn.sum = sum(fn)) %>%
            ungroup(kinase) %>%
            summarise(micro.prec = ifelse(sum(tp.sum) + sum(fp.sum) == 0, 0,
                                          sum(tp.sum) / (sum(tp.sum) + sum(fp.sum))),
                      micro.rec = ifelse(sum(tp.sum) + sum(fn.sum) == 0, 0,
                                         sum(tp.sum) / (sum(tp.sum) + sum(fn.sum))),
                      micro.f1 = ifelse(micro.prec == 0 && micro.rec == 0, 0,
                                        2 * ((micro.prec * micro.rec)/(micro.prec + micro.rec))),
                      micro.fb = ifelse(micro.prec == 0 && micro.rec == 0, 0,
                                        2 * ((micro.prec * micro.rec)/((0.5^2)*micro.prec + micro.rec))))
    }
    return (micro_ml_sum)
}

## macro-averaged multi-label evaluation
macro_ml <- function(scores_bysite_cut, with.ci = FALSE){
    if (with.ci){
        macro_ml_sum <- select(scores_bysite_cut, site.id, kinase, method,
                               tp, fn, fp, tn) %>%
            group_by(method, kinase) %>%
            summarise(prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                                    sum(tp) / (sum(tp) + sum(fp))),
                      rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                                   sum(tp) / (sum(tp) + sum(fn))),
                      f1 = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/(prec + rec)),
                      fb = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/((0.5^2)*prec + rec)),
                      fdr = ifelse(sum(tp) + sum(fp) == 0, 0,
                                   sum(fp) / (sum(tp) + sum(fp))),
                      fpr = ifelse(sum(fp) + sum(tn) == 0, 0,
                                   sum(fp) / (sum(fp) + sum(tn))),
                      tpr = ifelse(sum(tp) + sum(fn) == 0, 0,
                                   sum(tp) / (sum(tp) + sum(fn))),
                      lr = if_else(fpr == 0, 0, tpr / fpr),
                      kinase.n = sum(tp) + sum(fn),
                      weight = kinase.n
                      ## weight = if_else(kinase.n == 0, 0, 1/kinase.n)
                      ) %>%
            ungroup(kinase) %>%
            summarise(macro.prec.value = mean(prec),
                      macro.rec.value = mean(rec),
                      macro.f1.value = mean(f1),
                      macro.fb.value = mean(fb),
                      macro.fdr.value = mean(fdr),
                      macro.fpr.value = mean(fpr),
                      macro.tpr.value = mean(tpr),
                      macro.lr.value = mean(lr),
                      macro.wprec.value = weighted.mean(prec, w = weight),
                      macro.wrec.value = weighted.mean(rec, w = weight),
                      macro.wf1.value = weighted.mean(f1, w = weight),
                      macro.wfb.value = weighted.mean(fb, w = weight),
                      macro.wtpr.value = weighted.mean(tpr, w = weight),
                      macro.wfdr.value = weighted.mean(fdr, w = weight),
                      macro.wfpr.value = weighted.mean(fpr, w = weight),
                      macro.wlr.value = weighted.mean(lr, w = weight),
                      macro.prec.ci = qnorm(0.975)*sd(prec)/sqrt(n()),
                      macro.rec.ci = qnorm(0.975)*sd(rec)/sqrt(n()),
                      macro.f1.ci = qnorm(0.975)*sd(f1)/sqrt(n()),
                      macro.fb.ci = qnorm(0.975)*sd(fb)/sqrt(n()),
                      macro.fdr.ci = qnorm(0.975)*sd(fdr)/sqrt(n()),
                      macro.fpr.ci = qnorm(0.975)*sd(fpr)/sqrt(n()),
                      macro.tpr.ci = qnorm(0.975)*sd(tpr)/sqrt(n()),
                      macro.lr.ci = qnorm(0.975)*sd(lr)/sqrt(n()),
                      macro.wprec.ci = qnorm(0.975)*sqrt(wtd.var(prec, w = kinase.n))/sqrt(n()),
                      macro.wrec.ci = qnorm(0.975)*sqrt(wtd.var(rec, w = kinase.n))/sqrt(n()),
                      macro.wf1.ci = qnorm(0.975)*sqrt(wtd.var(f1, w = kinase.n))/sqrt(n()),
                      macro.wfb.ci = qnorm(0.975)*sqrt(wtd.var(fb, w = kinase.n))/sqrt(n()),
                      macro.wfdr.ci = qnorm(0.975)*sqrt(wtd.var(fdr, w = kinase.n))/sqrt(n()),
                      macro.wfpr.ci = qnorm(0.975)*sqrt(wtd.var(fpr, w = kinase.n))/sqrt(n()),
                      macro.wtpr.ci = qnorm(0.975)*sqrt(wtd.var(tpr, w = kinase.n))/sqrt(n()),
                      macro.wlr.ci = qnorm(0.975)*sqrt(wtd.var(lr, w = kinase.n))/sqrt(n()),
                      macro.prec.upper = macro.prec.value + macro.prec.ci,
                      macro.rec.upper = macro.rec.value + macro.rec.ci,
                      macro.f1.upper = macro.f1.value + macro.f1.ci,
                      macro.fb.upper = macro.fb.value + macro.fb.ci,
                      macro.wprec.upper = macro.wprec.value + macro.wprec.ci,
                      macro.wrec.upper = macro.wrec.value + macro.wrec.ci,
                      macro.wf1.upper = macro.wf1.value + macro.wf1.ci,
                      macro.wfb.upper = macro.wfb.value + macro.wfb.ci,
                      macro.fdr.upper = macro.fdr.value + macro.fdr.ci,
                      macro.fpr.upper = macro.fpr.value + macro.fpr.ci,
                      macro.tpr.upper = macro.tpr.value + macro.tpr.ci,
                      macro.lr.upper = macro.lr.value + macro.lr.ci,
                      macro.wtpr.upper = macro.wtpr.value + macro.wtpr.ci,
                      macro.wfpr.upper = macro.wfpr.value + macro.wfpr.ci,
                      macro.wfdr.upper = macro.wfdr.value + macro.wfdr.ci,
                      macro.wlr.upper = macro.wlr.value + macro.wlr.ci,
                      macro.prec.lower = macro.prec.value - macro.prec.ci,
                      macro.rec.lower = macro.rec.value - macro.rec.ci,
                      macro.f1.lower = macro.f1.value - macro.f1.ci,
                      macro.fb.lower = macro.fb.value - macro.fb.ci,
                      macro.wprec.lower = macro.wprec.value - macro.wprec.ci,
                      macro.wrec.lower = macro.wrec.value - macro.wrec.ci,
                      macro.wf1.lower = macro.wf1.value - macro.wf1.ci,
                      macro.wfb.lower = macro.wfb.value - macro.wfb.ci,
                      macro.fdr.lower = macro.fdr.value - macro.fdr.ci,
                      macro.fpr.lower = macro.fpr.value - macro.fpr.ci,
                      macro.tpr.lower = macro.tpr.value - macro.tpr.ci,
                      macro.lr.lower = macro.lr.value - macro.lr.ci,
                      macro.wtpr.lower = macro.wtpr.value - macro.wtpr.ci,
                      macro.wfpr.lower = macro.wfpr.value - macro.wfpr.ci,
                      macro.wfdr.lower = macro.wfdr.value - macro.wfdr.ci,
                      macro.wlr.lower = macro.wlr.value - macro.wlr.ci) %>%
            select(-macro.prec.ci, -macro.rec.ci, -macro.f1.ci, -macro.fb.ci,
                   -macro.wprec.ci, -macro.wrec.ci, -macro.wf1.ci, -macro.wfb.ci,
                   -macro.wtpr.ci, -macro.wfpr.ci, -macro.wfdr.ci, -macro.wlr.ci)
    }else{
        meth_pi0 <- select(scores_bysite_cut, site.id, kinase, method,
                      tp, fn, fp, tn) %>%
            group_by(method, kinase) %>%
            summarise(kinase.n = sum(tp) + sum(fn),
                      pi = kinase.n / n()) %>%
            ungroup(kinase) %>%
            summarise(pi0 = mean(pi)) %>%
            ungroup()
        macro_ml_sum <- select(scores_bysite_cut, site.id, kinase, method,
                               tp, fn, fp, tn) %>%
            left_join(meth_pi0) %>%
            group_by(method, kinase) %>%
            summarise(kinase.n = sum(tp) + sum(fn),
                      pi = kinase.n / n(),
                      cal = (pi*(1-pi0))/(pi0*(1-pi)),
                      prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                                    sum(tp) / (sum(tp) + sum(fp))),
                      prec.cal = ifelse(sum(tp) + sum(fp) == 0, 0,
                                        sum(tp) / (sum(tp) + cal*sum(fp))),
                      rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                                   sum(tp) / (sum(tp) + sum(fn))),
                      f1 = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/(prec + rec)),
                      f1.cal = if_else(prec.cal == 0 && rec == 0, 0,
                                   2 * (prec.cal * rec)/(prec.cal + rec)),
                      fb = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/((0.5^2)*prec + rec)),
                      fb.cal = if_else(prec.cal == 0 && rec == 0, 0,
                                   2 * (prec.cal * rec)/((0.5^2)*prec.cal + rec)),
                      weight = kinase.n
                      ## weight = if_else(kinase.n == 0, 0, 1/kinase.n)
                      ) %>%
            select(-pi, -cal) %>%
            ungroup(kinase) %>%
            summarise(macro.prec = mean(prec),
                      macro.prec.cal = mean(prec.cal),
                      macro.rec = mean(rec),
                      macro.f1 = mean(f1),
                      macro.f1.cal = mean(f1.cal),
                      macro.fb = mean(fb),
                      macro.fb.cal = mean(fb.cal),
                      macro.wprec = weighted.mean(prec, w = weight),
                      macro.wrec = weighted.mean(rec, w = weight),
                      macro.wf1 = weighted.mean(f1, w = weight),
                      macro.wfb = weighted.mean(fb, w = weight))
    }
    return (macro_ml_sum)
}

## sample-averaged multi-label evaluation
sample_ml <- function(scores_bysite_cut){
    samp_ml_sum <- select(scores_bysite_cut, site.id, kinase, method,
                            tp, fn, fp, tn) %>%
        group_by(method, site.id) %>%
        summarise(prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                                sum(tp) / (sum(tp) + sum(fp))),
                  rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                               sum(tp) / (sum(tp) + sum(fn))),
                  f1 = if_else(prec == 0 && rec == 0, 0,
                               2 * (prec * rec)/(prec + rec)),
                  fb = if_else(prec == 0 && rec == 0, 0,
                               2 * (prec * rec)/((0.5^2)*prec + rec))) %>%
        ungroup(site.id) %>%
        summarise(samp.prec = mean(prec),
                  samp.rec = mean(rec),
                  samp.f1 = mean(f1),
                  samp.fb = mean(fb))
    return (samp_ml_sum)
}

ml_eval <- function(bysite, num_cutoffs){
    pb <- txtProgressBar(max = num_cutoffs, style = 3)
    i <- 0
    multilabel_eval <- NULL
    suppressMessages(
        for (cutoff in seq(0, 1, length.out = num_cutoffs)){
            setTxtProgressBar(pb, i)
            i <- i+1
            bysite_cut <- mutate(bysite,
                                 pred.substrate = !is.na(score) & score >= cutoff) %>%
                select(kinase, site.id, method, pred.substrate, is.substrate) %>%
                mutate(tp = as.integer(pred.substrate & is.substrate),
                       fn = as.integer(!pred.substrate & is.substrate),
                       fp = as.integer(pred.substrate & !is.substrate),
                       tn = as.integer(!pred.substrate & !is.substrate))
            micro_ml_sum <- micro_ml(bysite_cut)
            macro_ml_sum <- macro_ml(bysite_cut)
            multilabel_sum <- left_join(micro_ml_sum, macro_ml_sum, by = "method") %>%
                mutate(cutoff = rep(cutoff, n())) %>%
                pivot_longer(cols=micro.prec:macro.wfb, names_to = "metric")
            if (is.null(multilabel_eval)){
                multilabel_eval <- multilabel_sum
            }else{
                multilabel_eval <- bind_rows(multilabel_eval, multilabel_sum)
            }
        }
    )
    close(pb)
    return(multilabel_eval)
}

greplv <- Vectorize(grepl)
num_cutoffs <- 100

for (k in 1:n_folds){
    fold_dir <- paste0("out/sugiyama-folds/fold-", k)
    motif_dir <- paste0(fold_dir, "/motifs")
    data_dir <- paste0("data/sugiyama-folds/fold-", k)
    true_pos <- read_tsv(paste0(data_dir, "/test/sugiyama-test.tsv")) %>%
        rename(true.kinase = kinase,
               site.id = substrate.id.site)
    true_pos_sum <- group_by(true_pos, site.id, seq.win) %>%
        summarise(true.kinases = paste(true.kinase, collapse = " "))
    glob_job <- mcparallel(prepare_scores(paste0(fold_dir, "/sug-fold-scores-glob.tsv"),
                                          paste0(motif_dir, "/global-bg"), Y_kinases) %>%
                           get_scores_bysite("PSSM proteome bg.", true_pos_sum))
    pos_job <- mcparallel(prepare_scores(paste0(fold_dir, "/sug-fold-scores-pos.tsv"),
                                         paste0(motif_dir, "/pos-bg"), Y_kinases) %>%
                          get_scores_bysite("PSSM phosphoproteome bg.", true_pos_sum))
    freq_job <- mcparallel(prepare_scores(paste0(fold_dir, "/sug-fold-scores-freq.tsv"),
                                         paste0(motif_dir, "/freq"), Y_kinases) %>%
                          get_scores_bysite("PFM", true_pos_sum))
    bayes_job <- mcparallel(prepare_scores(paste0(fold_dir, "/sug-fold-scores-bayes.tsv"),
                                           paste0(motif_dir, "/bayes"), Y_kinases) %>%
                            get_scores_bysite("Naive Bayes", true_pos_sum))
    bayesp_job <- mcparallel(prepare_scores(paste0(fold_dir, "/sug-fold-scores-bayes-plus.tsv"),
                                            paste0(motif_dir, "/bayes-plus"), Y_kinases) %>%
                             get_scores_bysite("Naive Bayes+", true_pos_sum))
    data <- mccollect(list(glob_job, pos_job, freq_job, bayes_job, bayesp_job))
    glob_bysite <- data[[1]]
    pos_bysite <- data[[2]]
    freq_bysite <- data[[3]]
    bayes_bysite <- data[[4]]
    bayesp_bysite <- data[[5]]
    scores_bysite <- bind_rows(glob_bysite, pos_bysite, freq_bysite, bayes_bysite, bayesp_bysite)
    scores_bysite$method <- factor(scores_bysite$method,
                            levels = c("PSSM phosphoproteome bg.",
                                       "PSSM proteome bg.",
                                       "PFM",
                                       "Naive Bayes",
                                       "Naive Bayes+"))
    bysite_filt <- filter(scores_bysite,
                          kinase %in% true_pos$true.kinase | main.id %in% true_pos$true.kinase) %>%
        mutate(is.substrate = greplv(kinase, true.kinases) | greplv(main.id, true.kinases))
    ml_eval(bysite_filt, num_cutoffs) %>%
        mutate(fold = rep(k, n())) %>%
        write_tsv(paste0(fold_dir, "/sug-fold-cv.tsv"))
}
