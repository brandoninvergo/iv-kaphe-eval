library(tidyverse)

argv <- commandArgs(TRUE)
sugiyama_file <- argv[1]

out_dir <- sub("data/", "out/", dirname(sugiyama_file))

pfam <- read_tsv("data/pfam-human-domains.tsv") %>%
    mutate(temp.id = paste(`seq id`, `hmm acc`))

kinases <- readLines("data/kinases.txt")

sugiyama <- read_tsv(sugiyama_file) %>%
    separate(col = substrate.id.site, into = c("substrate", "site"),
             sep = "-[STY]", remove = FALSE, convert = TRUE) %>%
    left_join(pfam, by = c("substrate" = "seq id"))

all_sub_doms <- filter(pfam, `seq id` %in% unique(sugiyama$substrate)) %>%
    select(`seq id`, `hmm acc`) %>%
    distinct() %>%
    pull(`hmm acc`)
unique_doms <- unique(all_sub_doms)

sub_dom_enrich <- NULL
for (kin in unique(sugiyama$kinase)){
    kin_dat <- filter(sugiyama, kinase == kin)
    kin_subs <- pull(kin_dat, substrate) %>% unique()
    sub_doms <- filter(pfam, `seq id` %in% kin_subs) %>%
        select(`seq id`, `hmm acc`) %>%
        distinct() %>%
        pull(`hmm acc`)
    unique_sub_doms <- unique(sub_doms)
    dom_tbl_init <- tibble(substrate = unique(sugiyama$substrate)) %>%
        mutate(in.kin.sub = substrate %in% kin_subs)
    ft_est <- NULL
    ft_p <- NULL
    for (dom in unique_sub_doms){
        dom_tbl <- mutate(dom_tbl_init,
                          has.domain = paste(substrate, dom) %in% pfam$temp.id)
        cont_tbl <- table(dom_tbl$in.kin.sub, dom_tbl$has.domain)
        ft <- fisher.test(cont_tbl, alternative = "greater")
        if (is.null(ft_est)){
            ft_est <- ft$estimate
            ft_p <- ft$p.value
        }else{
            ft_est <- c(ft_est, ft$estimate)
            ft_p <- c(ft_p, ft$p.value)
        }
    }
    if (is.null(sub_dom_enrich)){
        sub_dom_enrich <- tibble(domain = unique_sub_doms,
                             odds.ratio = ft_est,
                             p = ft_p) %>%
            mutate(kinase = rep(kin, n())) %>%
            select(kinase, domain, odds.ratio, p)
    }else{
        sub_dom_enrich_tmp <- tibble(domain = unique_sub_doms,
                                 odds.ratio = ft_est,
                                 p = ft_p) %>%
            mutate(kinase = rep(kin, n())) %>%
            select(kinase, domain, odds.ratio, p)
        sub_dom_enrich <- bind_rows(sub_dom_enrich, sub_dom_enrich_tmp)
    }
}

if (!dir.exists(out_dir))
    dir.create(out_dir, recursive = TRUE)
write_tsv(sub_dom_enrich, paste0(out_dir, "/kinase-sub-domains.tsv"))
