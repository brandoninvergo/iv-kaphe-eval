library(mice)
library(ranger)
library(Hmisc)
library(tidyverse)
library(cowplot)
library(e1071)
theme_set(theme_cowplot())

set.seed(20210519)

args <- commandArgs(TRUE)
win_len <- as.integer(args[1])

half_winlen <- floor(win_len/2)
s_regexp <- paste0(".{", half_winlen, "}[S].{", half_winlen, "}")
t_regexp <- paste0(".{", half_winlen, "}[T].{", half_winlen, "}")
y_regexp <- paste0(".{", half_winlen, "}[Y].{", half_winlen, "}")

kinases <- readLines("data/kinases.txt")
Y_kinases <- readLines("data/kinases-Y.txt")
ST_kinases <- readLines("data/kinases-ST.txt")

prepare_kinsub <- function(kinsub, Y_kinases, y_regexp){
    if ("kinase" %in% colnames(kinsub)){
        kinsub$main.id <- unlist(lapply(strsplit(kinsub$kinase, split = "-"),
                                        function(split) split[1]))
        kinsub$y.kinase <- kinsub$main.id %in% Y_kinases
        kinsub$kinase.type <- rep(NA, nrow(kinsub))
        kinsub$kinase.type[which(kinsub$y.kinase)] <- "Y"
        kinsub$kinase.type[which(!kinsub$y.kinase)] <- "S/T"
        kinsub$kinase.type <- as.factor(kinsub$kinase.type)
        kinsub$y.kinase <- NULL
    }
    y.site <- grepl(y_regexp, kinsub$seq.win)
    kinsub$seq.type <- rep(NA, nrow(kinsub))
    kinsub$seq.type[y.site] <- "Y"
    kinsub$seq.type[!y.site] <- "S/T"
    return(kinsub)
}

add_string <- function(data_set, stringdb, uniprot_id_map, is_train){
    out_tbl <- left_join(data_set, uniprot_id_map, by = c("kinase" = "uniprot")) %>%
    rename(kinase.embl = ensembl) %>%
    left_join(uniprot_id_map, by = c("substrate" = "uniprot")) %>%
    rename(substrate.embl = ensembl) %>%
    rowwise() %>%
    mutate(int.id = paste(sort(c(kinase.embl, substrate.embl)), collapse = " ")) %>%
    ungroup() %>%
    left_join(stringdb, by = "int.id") %>%
    mutate(neighborhood = if_else(is.na(neighborhood) | neighborhood/1000 < stringdb_prior,
                                  stringdb_prior, neighborhood/1000),
           fusion = if_else(is.na(fusion) | fusion/1000 < stringdb_prior,
                                  stringdb_prior, fusion/1000),
           cooccurence = if_else(is.na(cooccurence) | cooccurence/1000 < stringdb_prior,
                                  stringdb_prior, cooccurence/1000),
           coexpression = if_else(is.na(coexpression) | coexpression/1000 < stringdb_prior,
                                  stringdb_prior, coexpression/1000),
           experimental = if_else(is.na(experimental) | experimental/1000 < stringdb_prior,
                                  stringdb_prior, experimental/1000)) %>%
    select(-int.id, -protein1, -protein2, -kinase.embl, -substrate.embl) %>%
        distinct()
    if (is_train){
        out_tbl <- group_by(out_tbl, kinase, substrate, residue, site,
                            site.id, seq.win, site.type, kinase.type, nb.pred, is.substrate) %>%
        ## The Uniprot-to-Ensembl mapping is one-to-many so if there are
        ## multiple matches in STRING just take the mean
        summarise(nb.score = mean(nb.score),
                  cc.semsim = mean(cc.semsim),
                  bp.semsim = mean(bp.semsim),
                  neighborhood = mean(neighborhood),
                  fusion = mean(fusion),
                  cooccurence = mean(cooccurence),
                  coexpression = mean(coexpression),
                  experimental = mean(experimental),
                  is.kinase = any(is.kinase)) %>%
        ungroup()
    }else{
        out_tbl <- group_by(out_tbl, kinase, substrate, residue, site, site.id, seq.win,
                            site.type, kinase.type, nb.pred, is.kinase) %>%
        ## The Uniprot-to-Ensembl mapping is one-to-many so if there are
        ## multiple matches in STRING just take the mean
        summarise(nb.score = mean(nb.score),
                  cc.semsim = mean(cc.semsim),
                  bp.semsim = mean(bp.semsim),
                  neighborhood = mean(neighborhood),
                  fusion = mean(fusion),
                  cooccurence = mean(cooccurence),
                  coexpression = mean(coexpression),
                  experimental = mean(experimental),
                  is.kinase = any(is.kinase)) %>%
            ungroup()
    }
    return(out_tbl)
}

## micro-averaged multi-label evaluation
micro_ml <- function(scores_bysite_cut, with.ci = FALSE){
    if (with.ci){
        micro_ml_sum <- select(scores_bysite_cut, site.id, kinase, tp, fn, fp, tn) %>%
            group_by(kinase) %>%
            summarise(tp.sum = sum(tp),
                      fp.sum = sum(fp),
                      tn.sum = sum(tn),
                      fn.sum = sum(fn)) %>%
            summarise(micro.prec.value = ifelse(sum(tp.sum) + sum(fp.sum) == 0, 0,
                                          sum(tp.sum) / (sum(tp.sum) + sum(fp.sum))),
                      micro.rec.value = ifelse(sum(tp.sum) + sum(fn.sum) == 0, 0,
                                         sum(tp.sum) / (sum(tp.sum) + sum(fn.sum))),
                      micro.f1.value = ifelse(micro.prec.value == 0 && micro.rec.value == 0, 0,
                                        2 * ((micro.prec.value * micro.rec.value)/(micro.prec.value + micro.rec.value))),
                      micro.fb.value = ifelse(micro.prec.value == 0 && micro.rec.value == 0, 0,
                                        2 * ((micro.prec.value * micro.rec.value)/((0.5^2)*micro.prec.value + micro.rec.value))),
                      micro.prec.upper = NA,
                      micro.rec.upper = NA,
                      micro.f1.upper = NA,
                      micro.fb.upper = NA,
                      micro.prec.lower = NA,
                      micro.rec.lower = NA,
                      micro.f1.lower = NA,
                      micro.fb.lower = NA)
    }else{
        micro_ml_sum <- select(scores_bysite_cut, site.id, kinase, tp, fn, fp, tn) %>%
            group_by(kinase) %>%
            summarise(tp.sum = sum(tp),
                      fp.sum = sum(fp),
                      tn.sum = sum(tn),
                      fn.sum = sum(fn)) %>%
            summarise(micro.prec = ifelse(sum(tp.sum) + sum(fp.sum) == 0, 0,
                                          sum(tp.sum) / (sum(tp.sum) + sum(fp.sum))),
                      micro.rec = ifelse(sum(tp.sum) + sum(fn.sum) == 0, 0,
                                         sum(tp.sum) / (sum(tp.sum) + sum(fn.sum))),
                      micro.f1 = ifelse(micro.prec == 0 && micro.rec == 0, 0,
                                        2 * ((micro.prec * micro.rec)/(micro.prec + micro.rec))),
                      micro.fb = ifelse(micro.prec == 0 && micro.rec == 0, 0,
                                        2 * ((micro.prec * micro.rec)/((0.5^2)*micro.prec + micro.rec))))
    }
    return (micro_ml_sum)
}

## macro-averaged multi-label evaluation
macro_ml <- function(scores_bysite_cut, with.ci = FALSE){
    if (with.ci){
    macro_ml_sum <- select(scores_bysite_cut, site.id, kinase,
                       tp, fn, fp, tn) %>%
        group_by(kinase) %>%
        summarise(prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                                sum(tp) / (sum(tp) + sum(fp))),
                  rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                               sum(tp) / (sum(tp) + sum(fn))),
                  f1 = if_else(prec == 0 && rec == 0, 0,
                               2 * (prec * rec)/(prec + rec)),
                  fb = if_else(prec == 0 && rec == 0, 0,
                               2 * (prec * rec)/((0.5^2)*prec + rec)),
                  fdr = ifelse(sum(tp) + sum(fp) == 0, 0,
                               sum(fp) / (sum(tp) + sum(fp))),
                  fpr = ifelse(sum(fp) + sum(tn) == 0, 0,
                               sum(fp) / (sum(fp) + sum(tn))),
                  tpr = ifelse(sum(tp) + sum(fn) == 0, 0,
                               sum(tp) / (sum(tp) + sum(fn))),
                  lr = if_else(fpr == 0, 0, tpr / fpr),
                  kinase.n = sum(tp) + sum(fn),
                  weight = kinase.n
                  ## weight = if_else(kinase.n == 0, 0, 1/kinase.n)
                  ) %>%
        summarise(macro.prec.value = mean(prec),
                  macro.rec.value = mean(rec),
                  macro.f1.value = mean(f1),
                  macro.fb.value = mean(fb),
                  macro.fdr.value = mean(fdr),
                  macro.fpr.value = mean(fpr),
                  macro.tpr.value = mean(tpr),
                  macro.lr.value = mean(lr),
                  macro.wprec.value = weighted.mean(prec, w = weight),
                  macro.wrec.value = weighted.mean(rec, w = weight),
                  macro.wf1.value = weighted.mean(f1, w = weight),
                  macro.wfb.value = weighted.mean(fb, w = weight),
                  macro.wtpr.value = weighted.mean(tpr, w = weight),
                  macro.wfdr.value = weighted.mean(fdr, w = weight),
                  macro.wfpr.value = weighted.mean(fpr, w = weight),
                  macro.wlr.value = weighted.mean(lr, w = weight),
                  macro.prec.ci = qnorm(0.975)*sd(prec)/sqrt(n()),
                  macro.rec.ci = qnorm(0.975)*sd(rec)/sqrt(n()),
                  macro.f1.ci = qnorm(0.975)*sd(f1)/sqrt(n()),
                  macro.fb.ci = qnorm(0.975)*sd(fb)/sqrt(n()),
                  macro.fdr.ci = qnorm(0.975)*sd(fdr)/sqrt(n()),
                  macro.fpr.ci = qnorm(0.975)*sd(fpr)/sqrt(n()),
                  macro.tpr.ci = qnorm(0.975)*sd(tpr)/sqrt(n()),
                  macro.lr.ci = qnorm(0.975)*sd(lr)/sqrt(n()),
                  macro.wprec.ci = qnorm(0.975)*sqrt(wtd.var(prec, w = kinase.n))/sqrt(n()),
                  macro.wrec.ci = qnorm(0.975)*sqrt(wtd.var(rec, w = kinase.n))/sqrt(n()),
                  macro.wf1.ci = qnorm(0.975)*sqrt(wtd.var(f1, w = kinase.n))/sqrt(n()),
                  macro.wfb.ci = qnorm(0.975)*sqrt(wtd.var(fb, w = kinase.n))/sqrt(n()),
                  macro.wfdr.ci = qnorm(0.975)*sqrt(wtd.var(fdr, w = kinase.n))/sqrt(n()),
                  macro.wfpr.ci = qnorm(0.975)*sqrt(wtd.var(fpr, w = kinase.n))/sqrt(n()),
                  macro.wtpr.ci = qnorm(0.975)*sqrt(wtd.var(tpr, w = kinase.n))/sqrt(n()),
                  macro.wlr.ci = qnorm(0.975)*sqrt(wtd.var(lr, w = kinase.n))/sqrt(n()),
                  macro.prec.upper = macro.prec.value + macro.prec.ci,
                  macro.rec.upper = macro.rec.value + macro.rec.ci,
                  macro.f1.upper = macro.f1.value + macro.f1.ci,
                  macro.fb.upper = macro.fb.value + macro.fb.ci,
                  macro.wprec.upper = macro.wprec.value + macro.wprec.ci,
                  macro.wrec.upper = macro.wrec.value + macro.wrec.ci,
                  macro.wf1.upper = macro.wf1.value + macro.wf1.ci,
                  macro.wfb.upper = macro.wfb.value + macro.wfb.ci,
                  macro.fdr.upper = macro.fdr.value + macro.fdr.ci,
                  macro.fpr.upper = macro.fpr.value + macro.fpr.ci,
                  macro.tpr.upper = macro.tpr.value + macro.tpr.ci,
                  macro.lr.upper = macro.lr.value + macro.lr.ci,
                  macro.wtpr.upper = macro.wtpr.value + macro.wtpr.ci,
                  macro.wfpr.upper = macro.wfpr.value + macro.wfpr.ci,
                  macro.wfdr.upper = macro.wfdr.value + macro.wfdr.ci,
                  macro.wlr.upper = macro.wlr.value + macro.wlr.ci,
                  macro.prec.lower = macro.prec.value - macro.prec.ci,
                  macro.rec.lower = macro.rec.value - macro.rec.ci,
                  macro.f1.lower = macro.f1.value - macro.f1.ci,
                  macro.fb.lower = macro.fb.value - macro.fb.ci,
                  macro.wprec.lower = macro.wprec.value - macro.wprec.ci,
                  macro.wrec.lower = macro.wrec.value - macro.wrec.ci,
                  macro.wf1.lower = macro.wf1.value - macro.wf1.ci,
                  macro.wfb.lower = macro.wfb.value - macro.wfb.ci,
                  macro.fdr.lower = macro.fdr.value - macro.fdr.ci,
                  macro.fpr.lower = macro.fpr.value - macro.fpr.ci,
                  macro.tpr.lower = macro.tpr.value - macro.tpr.ci,
                  macro.lr.lower = macro.lr.value - macro.lr.ci,
                  macro.wtpr.lower = macro.wtpr.value - macro.wtpr.ci,
                  macro.wfpr.lower = macro.wfpr.value - macro.wfpr.ci,
                  macro.wfdr.lower = macro.wfdr.value - macro.wfdr.ci,
                  macro.wlr.lower = macro.wlr.value - macro.wlr.ci) %>%
        select(-macro.prec.ci, -macro.rec.ci, -macro.f1.ci, -macro.fb.ci,
               -macro.wprec.ci, -macro.wrec.ci, -macro.wf1.ci, -macro.wfb.ci,
               -macro.wtpr.ci, -macro.wfpr.ci, -macro.wfdr.ci, -macro.wlr.ci)
    }else{
        macro_ml_sum <- select(scores_bysite_cut, site.id, kinase,
                               tp, fn, fp, tn) %>%
            group_by(kinase) %>%
            summarise(prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                                    sum(tp) / (sum(tp) + sum(fp))),
                      rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                                   sum(tp) / (sum(tp) + sum(fn))),
                      f1 = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/(prec + rec)),
                      fb = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/((0.5^2)*prec + rec)),
                      kinase.n = sum(tp) + sum(fn),
                      weight = kinase.n
                      ## weight = if_else(kinase.n == 0, 0, 1/kinase.n)
                      ) %>%
            summarise(macro.prec = mean(prec),
                      macro.rec = mean(rec),
                      macro.f1 = mean(f1),
                      macro.fb = mean(fb),
                      macro.wprec = weighted.mean(prec, w = weight),
                      macro.wrec = weighted.mean(rec, w = weight),
                      macro.wf1 = weighted.mean(f1, w = weight),
                      macro.wfb = weighted.mean(fb, w = weight))
    }
    return (macro_ml_sum)
}

get_cutoffs <- function(scores, num_cutoffs){
    num_vals_tbl <- mutate(scores, int.id = paste(kinase, site.id)) %>%
        drop_na() %>%
        select(feature, int.id) %>% group_by(feature) %>% summarise(n = n())
    all_cutoffs <- NULL
    for (feat in levels(scores$feature)){
        num_vals <- filter(num_vals_tbl, feature == feat) %>% pull(n)
        cutoff_is <- floor(seq.int(1, num_vals, length.out = num_cutoffs))
        feat_cuts <- filter(scores, feature == feat) %>%
            arrange(value) %>%
            slice(cutoff_is) %>%
            select(feature, value) %>%
            rename(cutoff = value)
        if (is.null(all_cutoffs)){
            all_cutoffs <- feat_cuts
        }else{
            all_cutoffs <- bind_rows(all_cutoffs, feat_cuts)
        }
    }
    return(all_cutoffs)
}

micro_ml_feat <- function(scores_bysite_cut, with.ci = FALSE){
    if (with.ci){
        micro_ml_sum <- select(scores_bysite_cut, site.id, kinase, tp, fn, fp, tn) %>%
            group_by(feature, kinase) %>%
            summarise(tp.sum = sum(tp),
                      fp.sum = sum(fp),
                      tn.sum = sum(tn),
                      fn.sum = sum(fn)) %>%
            summarise(micro.prec.value = ifelse(sum(tp.sum) + sum(fp.sum) == 0, 0,
                                          sum(tp.sum) / (sum(tp.sum) + sum(fp.sum))),
                      micro.rec.value = ifelse(sum(tp.sum) + sum(fn.sum) == 0, 0,
                                         sum(tp.sum) / (sum(tp.sum) + sum(fn.sum))),
                      micro.f1.value = ifelse(micro.prec.value == 0 && micro.rec.value == 0, 0,
                                        2 * ((micro.prec.value * micro.rec.value)/(micro.prec.value + micro.rec.value))),
                      micro.fb.value = ifelse(micro.prec.value == 0 && micro.rec.value == 0, 0,
                                        2 * ((micro.prec.value * micro.rec.value)/((0.5^2)*micro.prec.value + micro.rec.value))),
                      micro.prec.upper = NA,
                      micro.rec.upper = NA,
                      micro.f1.upper = NA,
                      micro.fb.upper = NA,
                      micro.prec.lower = NA,
                      micro.rec.lower = NA,
                      micro.f1.lower = NA,
                      micro.fb.lower = NA)
    }else{
        micro_ml_sum <- select(scores_bysite_cut, site.id, kinase, tp, fn, fp, tn) %>%
            group_by(feature, kinase) %>%
            summarise(tp.sum = sum(tp),
                      fp.sum = sum(fp),
                      tn.sum = sum(tn),
                      fn.sum = sum(fn)) %>%
            summarise(micro.prec = ifelse(sum(tp.sum) + sum(fp.sum) == 0, 0,
                                          sum(tp.sum) / (sum(tp.sum) + sum(fp.sum))),
                      micro.rec = ifelse(sum(tp.sum) + sum(fn.sum) == 0, 0,
                                         sum(tp.sum) / (sum(tp.sum) + sum(fn.sum))),
                      micro.f1 = ifelse(micro.prec == 0 && micro.rec == 0, 0,
                                        2 * ((micro.prec * micro.rec)/(micro.prec + micro.rec))),
                      micro.fb = ifelse(micro.prec == 0 && micro.rec == 0, 0,
                                        2 * ((micro.prec * micro.rec)/((0.5^2)*micro.prec + micro.rec))))
    }
    return (micro_ml_sum)
}

## macro-averaged multi-label evaluation
macro_ml_feat <- function(scores_bysite_cut, with.ci = FALSE){
    if (with.ci){
    macro_ml_sum <- select(scores_bysite_cut, site.id, kinase,
                       tp, fn, fp, tn) %>%
        group_by(feature, kinase) %>%
        summarise(prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                                sum(tp) / (sum(tp) + sum(fp))),
                  rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                               sum(tp) / (sum(tp) + sum(fn))),
                  f1 = if_else(prec == 0 && rec == 0, 0,
                               2 * (prec * rec)/(prec + rec)),
                  fb = if_else(prec == 0 && rec == 0, 0,
                               2 * (prec * rec)/((0.5^2)*prec + rec)),
                  fdr = ifelse(sum(tp) + sum(fp) == 0, 0,
                               sum(fp) / (sum(tp) + sum(fp))),
                  fpr = ifelse(sum(fp) + sum(tn) == 0, 0,
                               sum(fp) / (sum(fp) + sum(tn))),
                  tpr = ifelse(sum(tp) + sum(fn) == 0, 0,
                               sum(tp) / (sum(tp) + sum(fn))),
                  lr = if_else(fpr == 0, 0, tpr / fpr),
                  kinase.n = sum(tp) + sum(fn),
                  weight = kinase.n
                  ## weight = if_else(kinase.n == 0, 0, 1/kinase.n)
                  ) %>%
        summarise(macro.prec.value = mean(prec),
                  macro.rec.value = mean(rec),
                  macro.f1.value = mean(f1),
                  macro.fb.value = mean(fb),
                  macro.fdr.value = mean(fdr),
                  macro.fpr.value = mean(fpr),
                  macro.tpr.value = mean(tpr),
                  macro.lr.value = mean(lr),
                  macro.wprec.value = weighted.mean(prec, w = weight),
                  macro.wrec.value = weighted.mean(rec, w = weight),
                  macro.wf1.value = weighted.mean(f1, w = weight),
                  macro.wfb.value = weighted.mean(fb, w = weight),
                  macro.wtpr.value = weighted.mean(tpr, w = weight),
                  macro.wfdr.value = weighted.mean(fdr, w = weight),
                  macro.wfpr.value = weighted.mean(fpr, w = weight),
                  macro.wlr.value = weighted.mean(lr, w = weight),
                  macro.prec.ci = qnorm(0.975)*sd(prec)/sqrt(n()),
                  macro.rec.ci = qnorm(0.975)*sd(rec)/sqrt(n()),
                  macro.f1.ci = qnorm(0.975)*sd(f1)/sqrt(n()),
                  macro.fb.ci = qnorm(0.975)*sd(fb)/sqrt(n()),
                  macro.fdr.ci = qnorm(0.975)*sd(fdr)/sqrt(n()),
                  macro.fpr.ci = qnorm(0.975)*sd(fpr)/sqrt(n()),
                  macro.tpr.ci = qnorm(0.975)*sd(tpr)/sqrt(n()),
                  macro.lr.ci = qnorm(0.975)*sd(lr)/sqrt(n()),
                  macro.wprec.ci = qnorm(0.975)*sqrt(wtd.var(prec, w = kinase.n))/sqrt(n()),
                  macro.wrec.ci = qnorm(0.975)*sqrt(wtd.var(rec, w = kinase.n))/sqrt(n()),
                  macro.wf1.ci = qnorm(0.975)*sqrt(wtd.var(f1, w = kinase.n))/sqrt(n()),
                  macro.wfb.ci = qnorm(0.975)*sqrt(wtd.var(fb, w = kinase.n))/sqrt(n()),
                  macro.wfdr.ci = qnorm(0.975)*sqrt(wtd.var(fdr, w = kinase.n))/sqrt(n()),
                  macro.wfpr.ci = qnorm(0.975)*sqrt(wtd.var(fpr, w = kinase.n))/sqrt(n()),
                  macro.wtpr.ci = qnorm(0.975)*sqrt(wtd.var(tpr, w = kinase.n))/sqrt(n()),
                  macro.wlr.ci = qnorm(0.975)*sqrt(wtd.var(lr, w = kinase.n))/sqrt(n()),
                  macro.prec.upper = macro.prec.value + macro.prec.ci,
                  macro.rec.upper = macro.rec.value + macro.rec.ci,
                  macro.f1.upper = macro.f1.value + macro.f1.ci,
                  macro.fb.upper = macro.fb.value + macro.fb.ci,
                  macro.wprec.upper = macro.wprec.value + macro.wprec.ci,
                  macro.wrec.upper = macro.wrec.value + macro.wrec.ci,
                  macro.wf1.upper = macro.wf1.value + macro.wf1.ci,
                  macro.wfb.upper = macro.wfb.value + macro.wfb.ci,
                  macro.fdr.upper = macro.fdr.value + macro.fdr.ci,
                  macro.fpr.upper = macro.fpr.value + macro.fpr.ci,
                  macro.tpr.upper = macro.tpr.value + macro.tpr.ci,
                  macro.lr.upper = macro.lr.value + macro.lr.ci,
                  macro.wtpr.upper = macro.wtpr.value + macro.wtpr.ci,
                  macro.wfpr.upper = macro.wfpr.value + macro.wfpr.ci,
                  macro.wfdr.upper = macro.wfdr.value + macro.wfdr.ci,
                  macro.wlr.upper = macro.wlr.value + macro.wlr.ci,
                  macro.prec.lower = macro.prec.value - macro.prec.ci,
                  macro.rec.lower = macro.rec.value - macro.rec.ci,
                  macro.f1.lower = macro.f1.value - macro.f1.ci,
                  macro.fb.lower = macro.fb.value - macro.fb.ci,
                  macro.wprec.lower = macro.wprec.value - macro.wprec.ci,
                  macro.wrec.lower = macro.wrec.value - macro.wrec.ci,
                  macro.wf1.lower = macro.wf1.value - macro.wf1.ci,
                  macro.wfb.lower = macro.wfb.value - macro.wfb.ci,
                  macro.fdr.lower = macro.fdr.value - macro.fdr.ci,
                  macro.fpr.lower = macro.fpr.value - macro.fpr.ci,
                  macro.tpr.lower = macro.tpr.value - macro.tpr.ci,
                  macro.lr.lower = macro.lr.value - macro.lr.ci,
                  macro.wtpr.lower = macro.wtpr.value - macro.wtpr.ci,
                  macro.wfpr.lower = macro.wfpr.value - macro.wfpr.ci,
                  macro.wfdr.lower = macro.wfdr.value - macro.wfdr.ci,
                  macro.wlr.lower = macro.wlr.value - macro.wlr.ci) %>%
        select(-macro.prec.ci, -macro.rec.ci, -macro.f1.ci, -macro.fb.ci,
               -macro.wprec.ci, -macro.wrec.ci, -macro.wf1.ci, -macro.wfb.ci,
               -macro.wtpr.ci, -macro.wfpr.ci, -macro.wfdr.ci, -macro.wlr.ci)
    }else{
        macro_ml_sum <- select(scores_bysite_cut, site.id, kinase,
                               tp, fn, fp, tn) %>%
            group_by(feature, kinase) %>%
            summarise(prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                                    sum(tp) / (sum(tp) + sum(fp))),
                      rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                                   sum(tp) / (sum(tp) + sum(fn))),
                      f1 = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/(prec + rec)),
                      fb = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/((0.5^2)*prec + rec)),
                      kinase.n = sum(tp) + sum(fn),
                      weight = kinase.n
                      ## weight = if_else(kinase.n == 0, 0, 1/kinase.n)
                      ) %>%
            summarise(macro.prec = mean(prec),
                      macro.rec = mean(rec),
                      macro.f1 = mean(f1),
                      macro.fb = mean(fb),
                      macro.wprec = weighted.mean(prec, w = weight),
                      macro.wrec = weighted.mean(rec, w = weight),
                      macro.wf1 = weighted.mean(f1, w = weight),
                      macro.wfb = weighted.mean(fb, w = weight))
    }
    return (macro_ml_sum)
}

good_kinases <- read_tsv("out/pssm-info.tsv") %>%
    filter(seq.n >= 20) %>%
    pull(kinase) %>%
    unique()

## This is defined by the STRING project.  Any STRING scores of 0 are
## replaced by this value
stringdb_prior <- 0.041

psp_go_semsim <- read_tsv("data/psp-kinase-substrates-go-semsim.tsv")
## med_cc_semsim <- median(psp_go_semsim$cc.semsim, na.rm = TRUE)
## med_bp_semsim <- median(psp_go_semsim$bp.semsim, na.rm = TRUE)
## psp_go_semsim <- mutate(psp_go_semsim,
##                     cc.semsim = if_else(is.na(cc.semsim), stringdb_prior, cc.semsim),
##                     bp.semsim = if_else(is.na(bp.semsim), stringdb_prior, bp.semsim))

psp_nb_scores <- read_tsv("out/psp/psp-scores-bayes-plus.tsv") %>%
    filter(kinase %in% good_kinases) %>%
    extract(col = site.id, into = c("substrate", "residue", "site"),
            regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", convert = TRUE,
            remove = FALSE) %>%
    rename(nb.score = score)
psp <- read_tsv("data/psp-kinase-substrates.tsv",
                col_names = c("kinase", "site.id", "seq.win")) %>%
    prepare_kinsub(Y_kinases, y_regexp) %>%
    mutate(int.id = paste(kinase, site.id))
psp_dat <- inner_join(psp_nb_scores, psp_go_semsim) %>%
    mutate(nb.pred = nb.score > 0.5,
           int.id = paste(kinase, site.id),
           is.substrate = int.id %in% psp$int.id,
           is.kinase = substrate %in% kinases,
           kinase.type = if_else(kinase %in% Y_kinases, "Y", "S/T"),
           site.type = if_else(residue == "Y", "Y", "S/T"))

## Get the S/T and Y kinases with assignments in PSP (including
## duplicates).  Shuffle in advance for random assignments to negative
## sites
psp_st_kins_st <- filter(psp, kinase.type == "S/T", seq.type == "S/T") %>%
    pull(kinase) %>%
    sample()
psp_st_kins_y <- filter(psp, kinase.type == "S/T", seq.type == "Y") %>%
    pull(kinase) %>%
    sample()
psp_y_kins_st <- filter(psp, kinase.type == "Y", seq.type == "S/T") %>%
    pull(kinase) %>%
    sample()
psp_y_kins_y <- filter(psp, kinase.type == "Y", seq.type == "Y") %>%
    pull(kinase) %>%
    sample()
## All kinases that phosphorylate S/T and Y sites, respectively
psp_kins_st <- sample(c(psp_st_kins_st, psp_y_kins_st))
psp_kins_y <- sample(c(psp_st_kins_y, psp_y_kins_y))
## Get the number of S/T and Y sites, including duplicates, for random
## assignment in the negative set
n_st <- length(psp_kins_st)
n_y <- length(psp_kins_y)
perc_sub_kins <- length(which(psp_dat$substrate %in% kinases)) / nrow(psp_dat)

## Load our final external evaluation set
pmapperval <- read_tsv("data/pmapperval-kinase-substrates.tsv",
                       col_names = c("kinase", "site.id", "seq.win")) %>%
    rowwise() %>%
    mutate(int.id = paste(kinase, site.id)) %>%
    ungroup()

## Load the Phoproteome dataset to generate negatives.  Get rid of any sites
## that are in the positives (PSP) or evaluation set (pmapperval)
phoproteome <- read_tsv("data/human-phosphoproteome.tsv",
                  col_names = c("site.id", "seq.win")) %>%
    filter(!(site.id %in% pmapperval$site.id),
           !(site.id %in% psp$site.id)) %>%
    extract(col = site.id, into = c("substrate", "residue", "site"),
            regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", remove = FALSE,
            convert = TRUE) %>%
    prepare_kinsub(Y_kinases, y_regexp) %>%
    mutate(is.kinase = substrate %in% kinases)
## Get S/T sites to assign to the shuffled list of S/T kinases
phoproteome_subset_st_kin <- filter(phoproteome, seq.type == "S/T", is.kinase) %>%
    slice_sample(n = floor(n_st * perc_sub_kins))
phoproteome_subset_st <- filter(phoproteome, seq.type == "S/T", !is.kinase) %>%
    slice_sample(n = ceiling(n_st * (1.0 - perc_sub_kins))) %>%
    bind_rows(phoproteome_subset_st_kin) %>%
    mutate(kinase = psp_kins_st)
## Get Y sites to assign to the shuffled list of Y kinases
phoproteome_subset_y_kin <- filter(phoproteome, seq.type == "Y", is.kinase) %>%
    slice_sample(n = floor(n_y * perc_sub_kins))
phoproteome_subset_y <- filter(phoproteome, seq.type == "Y", !is.kinase) %>%
    slice_sample(n = ceiling(n_y * (1.0 - perc_sub_kins))) %>%
    bind_rows(phoproteome_subset_y_kin) %>%
    mutate(kinase = psp_kins_y)
## Make the combined negative training set
phoproteome_train <- bind_rows(phoproteome_subset_st, phoproteome_subset_y)

phoproteome_scores <- read_tsv("out/phoproteome/phoproteome-scores-bayes-plus.tsv") %>%
    filter(kinase %in% good_kinases) %>%
    inner_join(phoproteome_train, by = c("kinase", "site.id", "seq.win")) %>%
    extract(col = site.id, into = c("substrate", "residue", "site"),
            regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", remove = FALSE,
            convert = TRUE) %>%
    rename(nb.score = score)

phoproteome_semsim <- read_tsv("data/human-phosphoproteome-go-semsim.tsv")
## med_cc_semsim <- median(phoproteome_semsim$cc.semsim, na.rm = TRUE)
## med_bp_semsim <- median(phoproteome_semsim$bp.semsim, na.rm = TRUE)
## phoproteome_semsim <- mutate(phoproteome_semsim,
##                     cc.semsim = if_else(is.na(cc.semsim), stringdb_prior, cc.semsim),
##                     bp.semsim = if_else(is.na(bp.semsim), stringdb_prior, bp.semsim))

## Finalize the negative training set
phoproteome_dat <- inner_join(phoproteome_scores, phoproteome_semsim) %>%
    mutate(nb.pred = nb.score > 0.5,
           int.id = paste(kinase, site.id),
           is.substrate = rep(FALSE, n()),
           kinase.type = if_else(kinase %in% Y_kinases, "Y", "S/T"),
           site.type = if_else(residue == "Y", "Y", "S/T"))

## Import STRING data
uniprot_id_map <- read_tsv("data/raw/Homo_sapiens.GRCh38.104.uniprot.tsv") %>%
    select(protein_stable_id, xref) %>%
    rename(ensembl = protein_stable_id,
           uniprot = xref)

if (!file.exists("data/human-stringdb.tsv")){
    stringdb <- read_delim("data/raw/9606.protein.links.detailed.v11.0.txt",
                           delim = " ") %>%
        select(protein1, protein2, neighborhood, fusion, cooccurence, coexpression,
               experimental) %>%
        rename(protein.id1 = protein1,
               protein.id2 = protein2) %>%
        extract(protein.id1, "protein1", regex = "9606\\.([[:alnum:]]+)") %>%
        extract(protein.id2, "protein2", regex = "9606\\.([[:alnum:]]+)") %>%
        rowwise() %>%
        mutate(int.id = paste(sort(c(protein1, protein2)), collapse = " ")) %>%
        ungroup()
    write_tsv(stringdb, "data/human-stringdb.tsv")
}else{
    stringdb <- read_tsv("data/human-stringdb.tsv")
}

## Build the final training set.
train_dat_full <- filter(psp_dat, is.substrate) %>%
    bind_rows(phoproteome_dat) %>%
    add_string(stringdb, uniprot_id_map, TRUE) %>%
    slice(sample(1:n()))

train_dat_impute <- mice(train_dat_full, m = 1, maxit = 40)
train_dat_full <- as_tibble(complete(train_dat_impute, 1)) %>%
    group_by(kinase, is.substrate) %>%
    arrange(kinase, is.substrate) %>%
    mutate(pair.id = 1:n()) %>%
    ungroup() %>% ungroup()

## Keep pairs of one true one false kinase-site pairs together for
## cross-validation
kinase_pairs <- select(train_dat_full, kinase, pair.id) %>%
    distinct() %>%
    slice(sample(1:n()))

## Set up k-fold CV
n_folds <- 10
n <- nrow(kinase_pairs)
test_size <- n %/% n_folds
train_size <- n - test_size
fold_starts <- seq(1, n, by = test_size)[1:n_folds]
fold_ends <- fold_starts + test_size - 1
fold_ends[n_folds] <- n

## k-fold CV
set.seed(20210519)
num_cutoffs <- 100
k_fold_ml_sum <- NULL
k_fold_ml_sum2 <- NULL
k_fold_feat_sum <- NULL
k_fold_imp <- NULL
feat_cutoffs <- select(train_dat_full, kinase, site.id, is.substrate,
                       nb.score, cc.semsim, bp.semsim, coexpression,
                       experimental) %>%
        pivot_longer(cols = c(nb.score, cc.semsim, bp.semsim, coexpression, experimental), names_to = "feature",
                     values_to = "value") %>%
    mutate(feature = factor(feature)) %>%
    get_cutoffs(num_cutoffs)
cutoffs <- seq(0.01, 1, length.out = num_cutoffs)
pb <- txtProgressBar(max = n_folds, style = 3)
for (k in 1:n_folds){
    setTxtProgressBar(pb, k)
    fold_start <- fold_starts[k]
    fold_end <- fold_ends[k]
    train_kin_pairs <- slice(kinase_pairs, -(fold_start:fold_end))
    train_dat_tbl <- inner_join(train_dat_full, train_kin_pairs)
    ## train_dat_tbl <- slice(train_dat_full, -(fold_start:fold_end))
    m <- ranger(as.factor(is.substrate) ~ nb.score + cc.semsim + bp.semsim +
                    coexpression + experimental + is.kinase + site.type + kinase.type,
                data = train_dat_tbl, num.trees = 500, importance = "impurity",
                splitrule = "gini", probability = TRUE)
    m2 <- svm(as.factor(is.substrate) ~ nb.score + cc.semsim + bp.semsim +
                    coexpression + experimental + is.kinase + site.type + kinase.type,
              data = train_dat_tbl, probability = TRUE)
    imp <- importance(m)
    k_fold_imp_tmp <- tibble(fold = rep(k, 8),
                             feature = names(imp),
                             importance = imp)
    if (is.null(k_fold_imp)){
        k_fold_imp <- k_fold_imp_tmp
    }else{
        k_fold_imp <- bind_rows(k_fold_imp, k_fold_imp_tmp)
    }
    test_kin_pairs <- slice(kinase_pairs, fold_start:fold_end)
    test_dat_tbl <- inner_join(train_dat_full, test_kin_pairs)
    ## test_dat_tbl <- slice(train_dat_full, fold_start:fold_end)
    preds <- predict(m, test_dat_tbl)$predictions[,"TRUE"]
    preds2 <- attr(predict(m2, test_dat_tbl, probability = TRUE), "probabilities")[,"TRUE"]
    pred_tbl <- select(test_dat_tbl, kinase, site.id, is.substrate) %>%
        mutate(prob = preds,
               fold = rep(k, n())) %>%
        as_tibble()
    pred2_tbl <- select(test_dat_tbl, kinase, site.id, is.substrate) %>%
        mutate(prob = preds2,
               fold = rep(k, n())) %>%
        as_tibble()
    feat_tbl <- select(test_dat_tbl, kinase, site.id, is.substrate,
                       nb.score, cc.semsim, bp.semsim, coexpression,
                       experimental) %>%
        mutate(fold = rep(k, n())) %>%
        pivot_longer(cols = c(nb.score, cc.semsim, bp.semsim, coexpression, experimental), names_to = "feature",
                     values_to = "value")
    ml_eval <- NULL
    ml_eval2 <- NULL
    feat_eval <- NULL
    for (cutoff_i in 1:num_cutoffs){
        cutoff <- cutoffs[cutoff_i]
        pred_tbl_cut <- mutate(pred_tbl,
                               pred.substrate = !is.na(prob) & prob >= cutoff) %>%
            select(kinase, site.id, pred.substrate, is.substrate, fold) %>%
            mutate(tp = as.integer(pred.substrate & is.substrate),
                   fn = as.integer(!pred.substrate & is.substrate),
                   fp = as.integer(pred.substrate & !is.substrate),
                   tn = as.integer(!pred.substrate & !is.substrate))
        micro_ml_sum <- micro_ml(pred_tbl_cut, TRUE)
        macro_ml_sum <- macro_ml(pred_tbl_cut, TRUE)
        multilabel_sum <- bind_cols(micro_ml_sum, macro_ml_sum) %>%
            mutate(fold = k,
                   cutoff = rep(cutoff, n())) %>%
            pivot_longer(cols=micro.prec.value:macro.wlr.lower,
                         names_to = c("metric", ".value"),
                         names_pattern = "(m[ai]cro\\.[[:alnum:]]+)\\.([[:alpha:]]+)$")
        if (is.null(ml_eval)){
            ml_eval <- multilabel_sum
        }else{
            ml_eval <- bind_rows(ml_eval, multilabel_sum)
        }
        pred2_tbl_cut <- mutate(pred2_tbl,
                               pred.substrate = !is.na(prob) & prob >= cutoff) %>%
            select(kinase, site.id, pred.substrate, is.substrate, fold) %>%
            mutate(tp = as.integer(pred.substrate & is.substrate),
                   fn = as.integer(!pred.substrate & is.substrate),
                   fp = as.integer(pred.substrate & !is.substrate),
                   tn = as.integer(!pred.substrate & !is.substrate))
        micro_ml_sum <- micro_ml(pred2_tbl_cut, TRUE)
        macro_ml_sum <- macro_ml(pred2_tbl_cut, TRUE)
        multilabel_sum <- bind_cols(micro_ml_sum, macro_ml_sum) %>%
            mutate(fold = k,
                   cutoff = rep(cutoff, n())) %>%
            pivot_longer(cols=micro.prec.value:macro.wlr.lower,
                         names_to = c("metric", ".value"),
                         names_pattern = "(m[ai]cro\\.[[:alnum:]]+)\\.([[:alpha:]]+)$")
        if (is.null(ml_eval2)){
            ml_eval2 <- multilabel_sum
        }else{
            ml_eval2 <- bind_rows(ml_eval2, multilabel_sum)
        }
        feat_cutoff <- group_by(feat_cutoffs, feature) %>%
            slice(cutoff_i)
        feat_tbl_cut <- left_join(feat_tbl, feat_cutoff) %>%
            mutate(pred.substrate = !is.na(value) & value >= cutoff) %>%
            select(kinase, site.id, feature, pred.substrate, is.substrate, fold) %>%
            group_by(feature) %>%
            mutate(tp = as.integer(pred.substrate & is.substrate),
                   fn = as.integer(!pred.substrate & is.substrate),
                   fp = as.integer(pred.substrate & !is.substrate),
                   tn = as.integer(!pred.substrate & !is.substrate))
        micro_ml_sum <- micro_ml_feat(feat_tbl_cut, TRUE) %>% ungroup()
        macro_ml_sum <- macro_ml_feat(feat_tbl_cut, TRUE) %>% ungroup()
        multilabel_sum <- inner_join(micro_ml_sum, macro_ml_sum, by = "feature") %>%
            mutate(fold = k) %>%
            left_join(feat_cutoff, by = "feature") %>%
            pivot_longer(cols=micro.prec.value:macro.wlr.lower,
                         names_to = c("metric", ".value"),
                         names_pattern = "(m[ai]cro\\.[[:alnum:]]+)\\.([[:alpha:]]+)$")
        if (is.null(feat_eval)){
            feat_eval <- multilabel_sum
        }else{
            feat_eval <- bind_rows(feat_eval, multilabel_sum)
        }
    }
    if (is.null(k_fold_ml_sum)){
        k_fold_ml_sum <- ml_eval
    }else{
        k_fold_ml_sum <- bind_rows(k_fold_ml_sum, ml_eval)
    }
    if (is.null(k_fold_ml_sum2)){
        k_fold_ml_sum2 <- ml_eval2
    }else{
        k_fold_ml_sum2 <- bind_rows(k_fold_ml_sum2, ml_eval2)
    }
    if (is.null(k_fold_feat_sum)){
        k_fold_feat_sum <- feat_eval
    }else{
        k_fold_feat_sum <- bind_rows(k_fold_feat_sum, feat_eval)
    }
}
close(pb)
## k_fold_ml <- pivot_longer(k_fold_ml_sum, cols = -fold, names_to = "metric") %>%
##     group_by(metric) %>%
##     summarise(mean.value = mean(value),
##               sd.value = sd(value))
## print(k_fold_ml)

k_fold_ml <- separate(k_fold_ml_sum, metric, into = c("averaging", "metric"))
k_fold_ml$metric <- factor(k_fold_ml$metric,
                           levels = c("prec", "rec", "f1", "fb", "wprec",
                                      "wrec", "wf1", "wfb"),
                           labels = c("precision", "recall", "F1", "Fbeta",
                                      "weighted prec", "weighted rec",
                                      "weighted F1", "weighted Fbeta"))
k_fold_ml$averaging <- factor(k_fold_ml$averaging,
                                    levels = c("micro", "macro"),
                                    labels = c("micro-averaging", "macro-averaging"))
best_co <- filter(k_fold_ml, averaging == "macro-averaging",
                  ## metric %in% c("precision", "recall", "F1"),
                  metric == "F1",
                  cutoff == 0.5) %>%
    group_by(averaging, metric, cutoff) %>%
    summarise(value = mean(value, na.rm = TRUE)) %>%
    ungroup(cutoff)
    ## arrange(-cutoff) %>%
    ## slice_max(value, n = 1, with_ties = FALSE)
print(best_co)
k_fold_ml2 <- separate(k_fold_ml_sum2, metric, into = c("averaging", "metric"))
k_fold_ml2$metric <- factor(k_fold_ml2$metric,
                           levels = c("prec", "rec", "f1", "fb", "wprec",
                                      "wrec", "wf1", "wfb"),
                           labels = c("precision", "recall", "F1", "Fbeta",
                                      "weighted prec", "weighted rec",
                                      "weighted F1", "weighted Fbeta"))
k_fold_ml2$averaging <- factor(k_fold_ml2$averaging,
                                    levels = c("micro", "macro"),
                                    labels = c("micro-averaging", "macro-averaging"))
best_co2 <- filter(k_fold_ml2, averaging == "macro-averaging",
                  metric %in% c("precision", "recall", "F1"),
                  ## metric == "F1",
                  cutoff == 0.5) %>%
    group_by(averaging, metric, cutoff) %>%
    summarise(value = mean(value, na.rm = TRUE)) %>%
    ungroup(cutoff)
    ## arrange(-cutoff) %>%
    ## slice_max(value, n = 1, with_ties = FALSE)
print(best_co2)
k_fold_feat <- separate(k_fold_feat_sum, metric, into = c("averaging", "metric"))
k_fold_feat$metric <- factor(k_fold_feat$metric,
                           levels = c("prec", "rec", "f1", "fb", "wprec",
                                      "wrec", "wf1", "wfb"),
                           labels = c("precision", "recall", "F1", "Fbeta",
                                      "weighted prec", "weighted rec",
                                      "weighted F1", "weighted Fbeta"))
k_fold_feat$averaging <- factor(k_fold_feat$averaging,
                                    levels = c("micro", "macro"),
                                    labels = c("micro-averaging", "macro-averaging"))
k_fold_feat$feature <- factor(k_fold_feat$feature,
                              levels = c("nb.score", "bp.semsim", "cc.semsim",
                                         "coexpression", "experimental"),
                              labels = c("Naive Bayes+ prob.", "GO BP sem. sim.",
                                         "GO CC sem. sim.", "STRING coexpr.",
                                         "STRING exper."))

kfold_ml_plot <- filter(k_fold_ml, averaging == "macro-averaging", metric == "F1") %>%
    group_by(cutoff) %>%
    summarise(mean.value = mean(value),
              ci = qnorm(0.975)*sd(value)/sqrt(n()),
              upper = mean.value + ci,
              lower = mean.value - ci) %>%
    mutate(value = mean.value) %>%
    ggplot(aes(x = cutoff, y = value)) +
    geom_ribbon(aes(ymin = lower, ymax = upper), alpha = 0.2) +
    geom_line() +
    geom_point(data = best_co) +
    scale_x_continuous("cutoff", breaks = c(0, 0.5, 1)) +
    scale_y_continuous("macro-F1", breaks = c(0, 0.5, 1.0), limits = c(0, 1))
save_plot(paste0("img/randforest-kfold-ml-eval.pdf"), kfold_ml_plot)
save(kfold_ml_plot, file = "img/rand-forest-kfold-ml-eval.Robj")
precs <- filter(k_fold_ml, averaging == "macro-averaging", metric == "precision")
best_precs <- left_join(best_co, precs, by = c("averaging", "cutoff"),
                        suffix = c(".F1", ".prec")) %>%
    select(fold, cutoff, value.prec) %>%
    group_by(cutoff) %>%
    summarise(mean.value.prec = mean(value.prec),
              ci = qnorm(0.975)*sd(value.prec, na.rm = TRUE)/sqrt(n()),
              upper = mean.value.prec + ci,
              lower = mean.value.prec - ci) %>%
    mutate(value.prec = mean.value.prec) %>%
    select(-mean.value.prec, -ci)
recs <- filter(k_fold_ml, averaging == "macro-averaging", metric == "recall")
best_recs <- left_join(best_co, recs, by = c("averaging", "cutoff"),
          suffix = c(".F1", ".rec")) %>%
    select(fold, cutoff, value.rec) %>%
    group_by(cutoff) %>%
    summarise(mean.value.rec = mean(value.rec),
              ci = qnorm(0.975)*sd(value.rec, na.rm = TRUE)/sqrt(n()),
              upper = mean.value.rec + ci,
              lower = mean.value.rec - ci) %>%
    mutate(value.rec = mean.value.rec) %>%
    select(-mean.value.rec, -ci)
best_pr <- inner_join(best_precs, best_recs, by = c("cutoff"), suffix = c(".prec", ".rec"))
print(best_pr)
kfold_pr_plot <- filter(k_fold_ml, averaging == "macro-averaging",
                  metric %in% c("precision", "recall")) %>%
    group_by(cutoff, averaging, metric) %>%
    summarise(mean.value = mean(value),
              ci = qnorm(0.975)*sd(value)/sqrt(n()),
              upper = mean.value + ci,
              lower = mean.value - ci) %>%
    mutate(value = mean.value) %>%
    select(-mean.value, -ci) %>%
    arrange(cutoff) %>%
    pivot_wider(names_from = metric, values_from = c(value, lower, upper), names_sep = ".") %>%
    ggplot(aes(x = value.recall, y = value.precision)) +
    geom_ribbon(aes(ymin = lower.precision, ymax = upper.precision), alpha = 0.2) +
    geom_path() +
    geom_point(data = best_pr, aes(x = value.rec, y = value.prec)) +
    scale_x_continuous("macro-recall", breaks = c(0, 0.5, 1), limits = c(0, 1))   +
    scale_y_continuous("macro-precision", breaks = c(0, 0.5, 1), limits = c(0, 1))
    ## theme(legend.position = c(0.5, 0.35))
save_plot("img/randforest-kfold-ml-prec-rec.pdf", kfold_pr_plot)
save(kfold_pr_plot, file = "img/rand-forest-kfold-ml-prec-rec.Robj")

feat_precs <- filter(k_fold_feat, averaging == "macro-averaging", metric == "precision")
feat_recs <- filter(k_fold_feat, averaging == "macro-averaging", metric == "recall")
kfold_feat_pr_plot <- filter(k_fold_feat, averaging == "macro-averaging",
                  metric %in% c("precision", "recall")) %>%
    group_by(feature, cutoff, averaging, metric) %>%
    summarise(mean.value = mean(value),
              ci = qnorm(0.975)*sd(value)/sqrt(n()),
              upper = mean.value + ci,
              lower = mean.value - ci) %>%
    rename(value = mean.value) %>%
    select(-ci) %>%
    arrange(cutoff) %>%
    pivot_wider(names_from = metric, values_from = c(value, lower, upper), names_sep = ".") %>%
    ggplot(aes(x = value.recall, y = value.precision)) +
    geom_ribbon(aes(ymin = lower.precision, ymax = upper.precision, fill = feature), alpha = 0.2) +
    geom_line(aes(color = feature)) +
    scale_x_continuous("macro-recall", breaks = c(0, 0.5, 1), limits = c(0, 1))   +
    scale_y_continuous("macro-precision", breaks = c(0, 0.5, 1), limits = c(0, 1))
    ## theme(legend.position = c(0.5, 0.35))
save_plot("img/randforest-kfold-feat-prec-rec.pdf", kfold_feat_pr_plot)
save(kfold_feat_pr_plot, file = "img/rand-forest-kfold-feat-prec-rec.Robj")

k_fold_imp_sum <- group_by(k_fold_imp, feature) %>%
    summarise(mean.importance = mean(importance),
              se.importance = sd(importance)/sqrt(n()))
k_fold_imp_sum$feature <- factor(k_fold_imp_sum$feature,
                                 levels = rev(c("nb.score", "bp.semsim", "cc.semsim",
                                                "coexpression", "experimental",
                                                "fusion", "cooccurence",
                                                "neighborhood", "is.kinase",
                                                "kinase.type", "residue", "site.type")),
                                 labels = rev(c("Naive Bayes+ prob.",
                                                "GO BP sem. sim.",
                                                "GO CC sem. sim.",
                                                "STRING coexpr.",
                                                "STRING exper.",
                                                "STRING fusion",
                                                "STRING cooccur.",
                                                "STRING neighb.",
                                                "substrate is kinase",
                                                "kinase type",
                                                "site type",
                                                "site type")))
print(k_fold_imp_sum)

## Random Forest importance analysis
rf_imp_plot <- ggplot(k_fold_imp_sum, aes(x = feature, y = mean.importance)) +
    geom_col() +
    ylab("mean importance") +
    geom_errorbar(aes(ymin = mean.importance - se.importance, ymax = mean.importance + se.importance), width = 0.25)
save(rf_imp_plot, file = "img/rand-forest-importance.Robj")

## Build a model from the full training set to apply to the external
## evaluation set
final_m <- ranger(as.factor(is.substrate) ~ nb.score + cc.semsim + bp.semsim +
                      coexpression + experimental + is.kinase + kinase.type + site.type,
                data = train_dat_full, num.trees = 500, importance = "impurity",
                splitrule = "gini", probability = TRUE)

## Test on ProtMapper data that is not in PSP (external evaluation).
## This set already includes random negative assignments from the Phoproteome set.

protmapper <- read_tsv("data/protmapper-kinase-substrates.tsv",
                       col_names = c("kinase", "site.id", "seq.win")) %>%
    prepare_kinsub(Y_kinases, y_regexp) %>%
    mutate(int.id = paste(kinase, site.id))
pmapperval_go_semsim <- read_tsv("data/pmapperval-kinase-substrates-go-semsim.tsv")
pmapperval_nb_scores <- read_tsv("out/pmapperval/pmapperval-scores-bayes-plus.tsv") %>%
    rowwise() %>%
    mutate(int.id = paste(kinase, site.id)) %>%
    ungroup() %>%
    filter(int.id %in% pmapperval$int.id) %>%
    extract(col = site.id, into = c("substrate", "residue", "site"),
            regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", convert = TRUE,
            remove = FALSE) %>%
    rename(nb.score = score)
pmapperval_dat <- inner_join(pmapperval_nb_scores, pmapperval_go_semsim) %>%
    mutate(nb.pred = nb.score > 0.5,
           int.id = paste(kinase, site.id),
           is.substrate = int.id %in% protmapper$int.id,
           is.kinase = substrate %in% kinases,
           kinase.type = if_else(kinase %in% Y_kinases, "Y", "S/T"),
           site.type = if_else(residue == "Y", "Y", "S/T"))
test_dat_full <- add_string(pmapperval_dat, stringdb, uniprot_id_map, FALSE) %>%
    slice(sample(1:n()))
test_dat_impute <- mice(test_dat_full, m = 1, maxit = 40)
test_dat_full <- complete(test_dat_impute, 1)
test_dat_full$score <- predict(final_m, test_dat_full)$predictions[,"TRUE"]
select(test_dat_full, kinase, site.id, seq.win, score) %>%
    write_tsv("out/pmapperval/pmapperval-scores-randforest.tsv")

## Get predictions for all kinases with assignments vs all sites with
## *true assignments* in the pmapperval subset.

pmapperval_expand <- read_tsv("out/pmapperval/pmapperval-scores-bayes-plus.tsv") %>%
    filter(kinase %in% pmapperval$kinase,
           site.id %in% protmapper$site.id) %>%
    extract(col = site.id, into = c("substrate", "residue", "site"),
            regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", remove = FALSE,
            convert = TRUE) %>%
    rename(nb.score = score) %>%
    inner_join(pmapperval_go_semsim) %>%
    mutate(nb.pred = nb.score > 0.5,
           int.id = paste(kinase, site.id),
           is.substrate = int.id %in% protmapper$int.id,
           is.kinase = substrate %in% kinases,
           kinase.type = if_else(kinase %in% Y_kinases, "Y", "S/T"),
           site.type = if_else(residue == "Y", "Y", "S/T"))
test_dat_expand <- add_string(pmapperval_expand, stringdb, uniprot_id_map, FALSE) %>%
    slice(sample(1:n()))
test_dat_expand_impute <- mice(test_dat_expand, m = 1, maxit = 40)
test_dat_expand <- complete(test_dat_expand_impute, 1)
test_dat_expand$score <- predict(final_m, test_dat_expand)$predictions[,"TRUE"]
select(test_dat_expand, kinase, site.id, seq.win, score) %>%
    write_tsv("out/pmapperval/pmapperval-scores-randforest-expand.tsv")

## Build a final model that uses all of PhosphoSitePlus and ProtMapper
## true_pos_dat <- filter(pmapperval_dat, int.id %in% protmapper$int.id) %>%
##     bind_rows(psp_dat) %>%
##     filter(is.substrate, kinase %in% good_kinases) %>%
##     distinct()
## n_true <- length(which(true_pos_dat$is.substrate))
## true_pos_st_kins_st <- filter(true_pos_dat, kinase.type == "S/T", site.type == "S/T") %>%
##     pull(kinase) %>%
##     sample()
## true_pos_st_kins_y <- filter(true_pos_dat, kinase.type == "S/T", site.type == "Y") %>%
##     pull(kinase) %>%
##     sample()
## true_pos_y_kins_st <- filter(true_pos_dat, kinase.type == "Y", site.type == "S/T") %>%
##     pull(kinase) %>%
##     sample()
## true_pos_y_kins_y <- filter(true_pos_dat, kinase.type == "Y", site.type == "Y") %>%
##     pull(kinase) %>%
##     sample()
## true_pos_kins_st <- sample(c(true_pos_st_kins_st, true_pos_y_kins_st))
## true_pos_kins_y <- sample(c(true_pos_st_kins_y, true_pos_y_kins_y))
## n_st <- length(true_pos_kins_st)
## n_y <- length(true_pos_kins_y)
## perc_sub_kins <- length(which(true_pos_dat$is.kinase)) / nrow(true_pos_dat)

## reread phoproteome to do different filtering
## phoproteome <- read_tsv("data/human-phosphoproteome.tsv",
##                   col_names = c("site.id", "seq.win")) %>%
##     filter(!(site.id %in% protmapper$site.id),
##            !(site.id %in% psp$site.id)) %>%
##     extract(col = site.id, into = c("substrate", "residue", "site"),
##             regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", remove = FALSE,
##             convert = TRUE) %>%
##     prepare_kinsub(Y_kinases, y_regexp) %>%
##     mutate(is.kinase = substrate %in% kinases)
## phoproteome_subset_st_kin <- filter(phoproteome, seq.type == "S/T", is.kinase) %>%
##     slice_sample(n = floor(n_st * perc_sub_kins))
## phoproteome_subset_st <- filter(phoproteome, seq.type == "S/T", !is.kinase) %>%
##     slice_sample(n = ceiling(n_st * (1.0 - perc_sub_kins))) %>%
##     bind_rows(phoproteome_subset_st_kin) %>%
##     mutate(kinase = true_pos_kins_st)
## phoproteome_subset_y_kin <- filter(phoproteome, seq.type == "Y", is.kinase) %>%
##     slice_sample(n = floor(n_y * perc_sub_kins))
## phoproteome_subset_y <- filter(phoproteome, seq.type == "Y", !is.kinase) %>%
##     slice_sample(n = ceiling(n_y * (1.0 - perc_sub_kins))) %>%
##     bind_rows(phoproteome_subset_y_kin) %>%
##     mutate(kinase = true_pos_kins_y)
## phoproteome_train <- bind_rows(phoproteome_subset_st, phoproteome_subset_y)

## phoproteome_subset2 <- read_tsv("data/human-phosphoproteome-randsubset2.tsv",
##                           col_names = c("site.id", "seq.win")) %>%
##     prepare_kinsub(Y_kinases, y_regexp)
## phoproteome_subset_full <- bind_rows(phoproteome_subset, phoproteome_subset2)
## phoproteome_scores <- read_tsv("out/phoproteome/phoproteome-scores-bayes-plus.tsv") %>%
##     filter(kinase %in% good_kinases) %>%
##     inner_join(phoproteome_train, by = c("kinase", "site.id", "seq.win")) %>%
##     extract(col = site.id, into = c("substrate", "residue", "site"),
##             regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", remove = FALSE,
##             convert = TRUE) %>%
##     rename(nb.score = score) %>%
##     ## Downsample
##     slice_sample(n = n_true)
## phoproteome_dat <- inner_join(phoproteome_scores, phoproteome_semsim) %>%
##     mutate(nb.pred = nb.score > 0.5,
##            int.id = paste(kinase, site.id),
##            is.substrate = rep(FALSE, n()),
##            is.kinase = substrate %in% kinases,
##            kinase.type = if_else(kinase %in% Y_kinases, "Y", "S/T"),
##            site.type = if_else(residue == "Y", "Y", "S/T"))

## train_dat_final <- bind_rows(true_pos_dat, phoproteome_dat) %>%
##     add_string(stringdb, uniprot_id_map, TRUE) %>%
##     slice(sample(1:n()))
## train_dat_impute <- mice(train_dat_final, m = 1, maxit = 40)
## train_dat_final <- as_tibble(complete(train_dat_impute, 1))
## final_final_m <- ranger(as.factor(is.substrate) ~ nb.score + cc.semsim + bp.semsim +
##                             coexpression + experimental + is.kinase + kinase.type + site.type,
##                         data = train_dat_final, importance = "impurity",
##                         splitrule = "gini", probability = TRUE)

## Make predictions for the Wilkes data set
wilkes_go_semsim <- read_tsv("data/wilkes-go-semsim.tsv")
wilkes <- read_tsv("data/wilkes-et-al-2015-inhib.tsv") %>%
    select(site.id, seq.win)
wilkes_nb_scores <- read_tsv("out/wilkes-inhib/wilkes-inhib-scores-bayes-plus.tsv") %>%
    filter(kinase %in% good_kinases) %>%
    extract(col = site.id, into = c("substrate", "residue", "site"),
            regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", convert = TRUE,
            remove = FALSE) %>%
    rename(nb.score = score) %>%
    mutate(nb.pred = nb.score > 0.5,
           is.kinase = substrate %in% kinases,
           kinase.type = if_else(kinase %in% Y_kinases, "Y", "S/T"),
           site.type = if_else(residue == "Y", "Y", "S/T"))
wilkes_dat <- inner_join(wilkes_nb_scores, wilkes_go_semsim) %>%
    add_string(stringdb, uniprot_id_map, FALSE) %>%
    slice(sample(1:n()))
wilkes_dat_impute <- mice(wilkes_dat, m = 1, maxit = 40)
wilkes_dat <- complete(wilkes_dat_impute, 1)
wilkes_dat$score <- predict(final_m, wilkes_dat)$predictions[,"TRUE"]
select(wilkes_dat, kinase, site.id, seq.win, score) %>%
    write_tsv("out/wilkes-inhib/wilkes-inhib-scores-randforest.tsv")

## Final full prediction of Phoproteome et al.
library(doParallel)
phoproteome_scores <- read_tsv("out/phoproteome/phoproteome-scores-bayes-plus.tsv") %>%
    filter(kinase %in% good_kinases) %>%
    extract(col = site.id, into = c("substrate", "residue", "site"),
            regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", convert = TRUE,
            remove = FALSE) %>%
    rename(nb.score = score) %>%
    mutate(nb.pred = nb.score > 0.5,
           is.kinase = substrate %in% kinases,
           kinase.type = if_else(kinase %in% Y_kinases, "Y", "S/T"),
           site.type = if_else(residue == "Y", "Y", "S/T"))
phoproteome_dat <- inner_join(phoproteome_scores, phoproteome_semsim)
n_splits <- 100
split_size <- ceiling(nrow(phoproteome_dat) / n_splits)
## phoproteome_dat_final <- NULL
if (!dir.exists("out/phoproteome/rand-forest"))
    dir.create("out/phoproteome/rand-forest")
## cl <- makeCluster(2)
## registerDoParallel(cl)
## phoproteome_dat_final <- foreach (i = 1:n_splits, .combine = rbind, .packages = c("tidyverse", "mice", "ranger")) %dopar% {
pb <- txtProgressBar(max = n_splits, style = 3)
for (i in 1:n_splits){
    setTxtProgressBar(pb, i)
    phoproteome_dat_split <- dplyr::slice(phoproteome_dat, ((i-1)*split_size + 1):((i-1)*split_size + split_size))
    phoproteome_dat_split <- add_string(phoproteome_dat_split, stringdb, uniprot_id_map, FALSE)
    phoproteome_dat_impute <- mice::mice(phoproteome_dat_split, m = 1, maxit = 40)
    phoproteome_dat_split <- tibble::as_tibble(complete(phoproteome_dat_impute, 1))
    ## phoproteome_dat_split$score <- predict(final_final_m, phoproteome_dat_split)$predictions[,"TRUE"]
    phoproteome_dat_split$score <- predict(final_m, phoproteome_dat_split)$predictions[,"TRUE"]
    select(phoproteome_dat_split, kinase, site.id, seq.win, nb.score, cc.semsim, bp.semsim,
           coexpression, experimental, is.kinase, kinase.type, site.type, score) %>%
        write_tsv(paste0("out/phoproteome/rand-forest/phoproteome-scores-randforest-", i, ".tsv"))
    ## phoproteome_dat_split
    ## if (is.null(phoproteome_dat_final)){
    ##     phoproteome_dat_final <- phoproteome_dat_split
    ## }else{
    ##     phoproteome_dat_final <- bind_rows(phoproteome_dat_final, phoproteome_dat_split)
    ## }
}
close(pb)
## stopCluster(cl)
## select(phoproteome_dat_final, kinase, site.id, seq.win, nb.score, cc.semsim, bp.semsim,
##        coexpression, experimental, is.kinase, kinase.type, site.type, score) %>%
##     write_tsv("out/phoproteome/phoproteome-scores-randforest.tsv")


