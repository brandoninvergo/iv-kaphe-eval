library(parallel)
library(tidyverse)
library(slider)
library(cowplot)
theme_set(theme_cowplot())

args <- commandArgs(TRUE)
win_len <- as.integer(args[1])

half_winlen <- floor(win_len/2)
s_regexp <- paste0(".{", half_winlen, "}[S].{", half_winlen, "}")
t_regexp <- paste0(".{", half_winlen, "}[T].{", half_winlen, "}")
y_regexp <- paste0(".{", half_winlen, "}[Y].{", half_winlen, "}")

pssm_info <- read_tsv("out/pssm-info.tsv")
good_pssms <- unique(filter(pssm_info, seq.n >= 20)$kinase)

true_pos <- read_tsv("data/protmapper-kinase-substrates.tsv",
                     col_names = c("true.kinase", "site.id", "seq.win"))##  %>%
    ## filter(true.kinase %in% good_pssms)
true_pos_sum <- group_by(true_pos, site.id, seq.win) %>%
    summarise(true.kinases = paste(true.kinase, collapse = " "))
Y_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase"))
ST_kinases <- read_tsv("data/kinases-ST.txt", col_names = c("kinase"))

prepare_scores <- function(score_file, good_pssms, Y_kinases){
    protmapper_scores <- read_tsv(score_file) %>%
        filter(kinase %in% good_pssms)
    protmapper_scores$main.id <- unlist(lapply(strsplit(protmapper_scores$kinase, split = "-"),
                                      function(split) split[1]))
    protmapper_scores$y.kinase <- protmapper_scores$main.id %in% Y_kinases$kinase
    protmapper_scores$kinase.type <- rep(NA, nrow(protmapper_scores))
    protmapper_scores$kinase.type[which(protmapper_scores$y.kinase)] <- "Y"
    protmapper_scores$kinase.type[which(!protmapper_scores$y.kinase)] <- "S/T"
    protmapper_scores$kinase.type <- as.factor(protmapper_scores$kinase.type)
    protmapper_scores$y.kinase <- NULL
    return(protmapper_scores)
}

## Calculate per-site score ranks and pull in the known kinase(s) for
## each site
get_protmapper_scores_bysite <- function(protmapper_scores, method.name, true_pos){
    if ("seq.win" %in% colnames(protmapper_scores)){
        protmapper_scores_bysite <- group_by(protmapper_scores, site.id) %>%
            mutate(score.rank = rank(-score),
                   method = rep(method.name, n())) %>%
            ## arrange(site.id, kinase) %>%
            inner_join(true_pos, by = c("site.id", "seq.win")) %>%
            ungroup()
    }else{
        protmapper_scores_bysite <- group_by(protmapper_scores, site.id) %>%
            mutate(score.rank = rank(-score),
                   method = rep(method.name, n())) %>%
            ## arrange(site.id, kinase) %>%
            inner_join(true_pos, by = "site.id") %>%
            ungroup()
    }
    return(protmapper_scores_bysite)
}

add_seq_type <- function(scores_bysite, y_regexp){
    y.site <- grepl(y_regexp, scores_bysite$seq.win)
    scores_bysite$seq.type <- rep(NA, nrow(scores_bysite))
    scores_bysite$seq.type[y.site] <- "Y"
    scores_bysite$seq.type[!y.site] <- "S/T"
    return(scores_bysite)
}

add_acceptors <- function(scores_bysite, y_regexp, s_regexp, t_regexp){
    y.site <- grepl(y_regexp, scores_bysite$seq.win)
    s.site <- grepl(s_regexp, scores_bysite$seq.win)
    t.site <- grepl(t_regexp, scores_bysite$seq.win)
    scores_bysite$acceptor <- rep(NA, nrow(scores_bysite))
    scores_bysite$acceptor[y.site] <- "Y"
    scores_bysite$acceptor[s.site] <- "S"
    scores_bysite$acceptor[t.site] <- "T"
    return(scores_bysite)
}

protmapper_glob_job <- mcparallel(prepare_scores("out/protmapper-norm/protmapper-scores-glob.tsv", good_pssms,
                                          Y_kinases) %>%
                           get_protmapper_scores_bysite("PSSM proteome bg.", true_pos_sum))
protmapper_pos_job <- mcparallel(prepare_scores("out/protmapper-norm/protmapper-scores-pos.tsv", good_pssms,
                                         Y_kinases) %>%
                          get_protmapper_scores_bysite("PSSM phosphoproteome bg.", true_pos_sum))
protmapper_bayes_job <- mcparallel(prepare_scores("out/protmapper/protmapper-scores-bayes.tsv", good_pssms,
                                          Y_kinases) %>%
                           get_protmapper_scores_bysite("Naive Bayes", true_pos_sum))
protmapper_bayesp_job <- mcparallel(prepare_scores("out/protmapper/protmapper-scores-bayes-plus.tsv", good_pssms,
                                          Y_kinases) %>%
                           get_protmapper_scores_bysite("Naive Bayes+", true_pos_sum))
protmapper_data <- mccollect(list(protmapper_glob_job, protmapper_pos_job,
                                  protmapper_bayes_job, protmapper_bayesp_job))
protmapper_glob_bysite <- protmapper_data[[1]]
protmapper_pos_bysite <- protmapper_data[[2]]
protmapper_bayes_bysite <- protmapper_data[[3]]
protmapper_bayesp_bysite <- protmapper_data[[4]]

protmapper_gps <- read_tsv("out/protmapper/protmapper-assignments-gps.tsv") %>%
    mutate(score = rep(1.0, n()))
protmapper_gps$main.id <- unlist(lapply(strsplit(protmapper_gps$kinase, split = "-"),
                                 function(split) split[1]))
protmapper_gps$y.kinase <- protmapper_gps$main.id %in% Y_kinases$kinase
protmapper_gps$kinase.type <- rep(NA, nrow(protmapper_gps))
protmapper_gps$kinase.type[which(protmapper_gps$y.kinase)] <- "Y"
protmapper_gps$kinase.type[which(!protmapper_gps$y.kinase)] <- "S/T"
protmapper_gps$kinase.type <- as.factor(protmapper_gps$kinase.type)
protmapper_gps$y.kinase <- NULL
protmapper_gps <- get_protmapper_scores_bysite(protmapper_gps, "GPS 5.0", true_pos_sum)##  %>%
    ## add_acceptors(y_regexp, s_regexp, t_regexp) %>%
    ## add_seq_type(y_regexp)

protmapper_lph <- read_tsv("data/linkphinder.tsv") %>%
    get_protmapper_scores_bysite("linkPhinder", true_pos_sum) %>%
    select(-seq.win, -score.rank)
protmapper_lph$main.id <- unlist(lapply(strsplit(protmapper_lph$kinase, split = "-"),
                                        function(split) split[1]))

## Just assess performance on the kinases covered by our methods and
## those of GPS
our_kinases <- unique(protmapper_glob_bysite$kinase)
gps_kinases <- unique(protmapper_gps$kinase)
lph_kinases <- unique(protmapper_lph$kinase)
shared_kinases <- intersect(our_kinases, gps_kinases)
shared_kinases <- intersect(shared_kinases, lph_kinases)

protmapper_bysite <- bind_rows(protmapper_glob_bysite, protmapper_pos_bysite,
                               protmapper_bayes_bysite, protmapper_bayesp_bysite,
                               protmapper_gps) %>%
    select(-kinase.type, -score.rank, -seq.win) %>%
    bind_rows(protmapper_lph) %>%
    filter(kinase %in% shared_kinases) %>%
    complete(method, nesting(kinase, main.id, site.id, true.kinases),
             fill = list(score = NA))
protmapper_bysite$method <- factor(protmapper_bysite$method,
                            levels = c("PSSM phosphoproteome bg.",
                                       "PSSM proteome bg.",
                                       "Naive Bayes",
                                       "Naive Bayes+",
                                       "GPS 5.0",
                                       "linkPhinder"))

greplv <- Vectorize(grepl)
protmapper_bysite_filt <- filter(protmapper_bysite,
                                 ## !is.na(score),
                                 kinase %in% true_pos$true.kinase | main.id %in% true_pos$true.kinase) %>%
    mutate(is.substrate = greplv(kinase, true.kinases) | greplv(main.id, true.kinases))

protmapper_bysite_cut <- mutate(protmapper_bysite_filt,
                                pred.substrate = (method %in% c("PSSM phosphoproteome bg.",
                                                                "PSSM proteome bg.") &
                                                  !is.na(score) & score >= 0.75) |
                                    (method %in% c("Naive Bayes", "Naive Bayes+") &
                                     !is.na(score) & score >= 0.5) |
                                    (method == "GPS 5.0" & !is.na(score)) |
                                    (method == "linkPhinder" & !is.na(score) & score >= 0.75)) %>%
    select(kinase, site.id, method, pred.substrate, is.substrate) %>%
    mutate(tp = as.integer(pred.substrate & is.substrate),
           fn = as.integer(!pred.substrate & is.substrate),
           fp = as.integer(pred.substrate & !is.substrate),
           tn = as.integer(!pred.substrate & !is.substrate))
micro_ml_sum <- select(protmapper_bysite_cut, site.id, kinase, method, tp, fn, fp, tn) %>%
    group_by(method, kinase) %>%
    summarise(tp.sum = sum(tp),
              fp.sum = sum(fp),
              tn.sum = sum(tn),
              fn.sum = sum(fn)) %>%
    ungroup(kinase) %>%
    summarise(micro.prec = sum(tp.sum) / (sum(tp.sum) + sum(fp.sum)),
              micro.rec = sum(tp.sum) / (sum(tp.sum) + sum(fn.sum)),
              micro.f1 = 2 * ((micro.prec * micro.rec)/(micro.prec + micro.rec)),
              micro.fb = 2 * ((micro.prec * micro.rec)/((0.5^2)*micro.prec + micro.rec)))
macro_ml_sum <- select(protmapper_bysite_cut, site.id, kinase, method,
                       tp, fn, fp, tn) %>%
    group_by(method, kinase) %>%
    summarise(prec = sum(tp) / (sum(tp) + sum(fp)),
              rec = sum(tp) / (sum(tp) + sum(fn)),
              f1 = 2 * (prec * rec)/(prec + rec),
              fb = 2 * (prec * rec)/((0.5^2)*prec + rec),
              kinase.n = sum(tp) + sum(fn)) %>%
    ungroup(kinase) %>%
    summarise(macro.prec = mean(prec, na.rm = TRUE),
              macro.rec = mean(rec, na.rm = TRUE),
              macro.f1 = mean(f1, na.rm = TRUE),
              macro.fb = mean(fb, na.rm = TRUE),
              macro.wf1 = weighted.mean(f1, w = kinase.n, na.rm = TRUE),
              macro.wfb = weighted.mean(fb, w = kinase.n, na.rm = TRUE))
multilabel_sum_final <- left_join(micro_ml_sum, macro_ml_sum, by = "method")
print(as.data.frame(multilabel_sum_final))
write_tsv(multilabel_sum_final, "out/protmapper-multilabel-performance.tsv")

multilabel_eval <- NULL
num_cutoffs <- 100
pb <- txtProgressBar(max = num_cutoffs, style = 3)
i <- 0
suppressMessages(for (cutoff in seq(0, 1, length.out = num_cutoffs)){
    setTxtProgressBar(pb, i)
    i <- i+1
    protmapper_bysite_cut <- mutate(protmapper_bysite_filt,
                             pred.substrate = !is.na(score) & score >= cutoff) %>%
        select(kinase, site.id, method, pred.substrate, is.substrate) %>%
        mutate(tp = as.integer(pred.substrate & is.substrate),
               fn = as.integer(!pred.substrate & is.substrate),
               fp = as.integer(pred.substrate & !is.substrate),
               tn = as.integer(!pred.substrate & !is.substrate))
    micro_ml_sum <- select(protmapper_bysite_cut, site.id, kinase, method, tp, fn, fp, tn) %>%
        group_by(method, kinase) %>%
        summarise(tp.sum = sum(tp),
                  fp.sum = sum(fp),
                  tn.sum = sum(tn),
                  fn.sum = sum(fn)) %>%
        ungroup(kinase) %>%
        summarise(micro.prec = sum(tp.sum) / (sum(tp.sum) + sum(fp.sum)),
                  micro.rec = sum(tp.sum) / (sum(tp.sum) + sum(fn.sum)),
                  micro.f1 = 2 * ((micro.prec * micro.rec)/(micro.prec + micro.rec)),
                  micro.fb = 2 * ((micro.prec * micro.rec)/((0.5^2)*micro.prec + micro.rec)))
    macro_ml_sum <- select(protmapper_bysite_cut, site.id, kinase, method,
                           tp, fn, fp, tn) %>%
        group_by(method, kinase) %>%
        summarise(prec = sum(tp) / (sum(tp) + sum(fp)),
                  rec = sum(tp) / (sum(tp) + sum(fn)),
                  f1 = 2 * (prec * rec)/(prec + rec),
                  fb = 2 * (prec * rec)/((0.5^2)*prec + rec),
                  kinase.n = sum(tp) + sum(fn)) %>%
        ungroup(kinase) %>%
        summarise(macro.prec = mean(prec, na.rm = TRUE),
                  macro.rec = mean(rec, na.rm = TRUE),
                  macro.f1 = mean(f1, na.rm = TRUE),
                  macro.fb = mean(fb, na.rm = TRUE),
                  macro.wf1 = weighted.mean(f1, w = kinase.n, na.rm = TRUE),
                  macro.wfb = weighted.mean(fb, w = kinase.n, na.rm = TRUE))
    multilabel_sum <- left_join(micro_ml_sum, macro_ml_sum, by = "method") %>%
        mutate(cutoff = rep(cutoff, n())) %>%
        pivot_longer(cols=micro.prec:macro.wfb, names_to = "metric")
    if (is.null(multilabel_eval)){
        multilabel_eval <- multilabel_sum
    }else{
        multilabel_eval <- bind_rows(multilabel_eval, multilabel_sum)
    }
})
close(pb)

multilabel <- separate(multilabel_eval, metric, into = c("averaging", "metric"))
multilabel$metric <- factor(multilabel$metric,
                                 levels = c("prec", "rec", "f1", "fb", "wf1", "wfb"),
                                 labels = c("precision", "recall", "F1", "Fbeta",
                                            "weighted F1", "weighted Fbeta"))
multilabel$averaging <- factor(multilabel$averaging,
                                    levels = c("micro", "macro"),
                                    labels = c("micro-averaging", "macro-averaging"))

multilabel <- group_by(multilabel, method, averaging, metric) %>%
    mutate(value.avg = slide_dbl(value, mean, .before = 2)) %>%
    ungroup()

filter(multilabel, metric == "F1") %>%
    group_by(method, averaging, metric) %>%
    arrange(-cutoff) %>%
    slice_max(value, n = 1, with_ties = FALSE) %>%
    write_tsv("out/protmapper-multilabel-best-cutoffs.tsv")

gps <- filter(multilabel_sum_final, method == "GPS 5.0") %>%
    pivot_longer(-method, names_to = "metric", values_to = "value") %>%
    separate(metric, into = c("averaging", "metric"))
gps$metric <- factor(gps$metric,
                     levels = c("prec", "rec", "f1", "fb", "wf1", "wfb"),
                     labels = c("precision", "recall", "F1", "Fbeta",
                                "weighted F1", "weighted Fbeta"))
gps$averaging <- factor(gps$averaging,
                        levels = c("micro", "macro"),
                        labels = c("micro-averaging", "macro-averaging"))

ml_plot <- ggplot(multilabel, aes(x = cutoff, y = value, color = method)) +
    geom_hline(data = gps, aes(yintercept = value, color = method), lty = "dashed") +
    geom_line() +
    ylab("value") +
    scale_x_continuous(breaks = c(0, 0.5, 1)) +
    facet_grid(cols = vars(averaging), rows = vars(metric), scales = "free") +
    theme(panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())
save_plot("img/protmapper-ml-eval.pdf", ml_plot, base_asp = 1.2, base_height = 2,
          ncol = 3, nrow = 6)

gps_prec_rec <- pivot_wider(gps, names_from = metric, values_from = value)

pr_plot <- filter(multilabel, metric %in% c("precision", "recall")) %>%
    arrange(cutoff) %>%
    select(-value.avg) %>%
    pivot_wider(names_from = metric, values_from = value) %>%
    ggplot(aes(x = precision, y = recall, color = method)) +
    facet_wrap(vars(averaging)) +
    geom_path() +
    geom_point(data = gps_prec_rec, aes(x = precision, y = recall, color = method)) +
    xlim(0, 1) +
    ylim(0, 1) +
    xlab("multi-label precision") +
    ylab("multi-label recall") +
    theme(panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())
save_plot("img/protmapper-ml-prec-rec.pdf", pr_plot, ncol = 3, base_asp = 1.0)
