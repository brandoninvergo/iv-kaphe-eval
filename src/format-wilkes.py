import sys
import re
from Bio import SeqIO


PROT_ID_RE = re.compile(r"^([A-Za-z0-9-]+)")
SITES_RE = re.compile(r"p-([STY][0-9]+)")
HGNC_RE = re.compile(r"GN=([A-Za-z0-9-]+)")


def parse_psite_id(psite_id):
    """Parse the "Phosphopeptide ID (format 2)" column in the peptide
database.  This is in the format HGNCID p-S1 (z= N) or, if there are multiple
sites HGNCID p-S1 p-T2 (z= N)"""
    prot_id = re.search(PROT_ID_RE, psite_id)
    try:
        assert prot_id
    except AssertionError:
        sys.exit("Error parsing protname in psite id " + psite_id)
    sites = re.findall(SITES_RE, psite_id)
    return sites


def parse_peptide_db(pepdb_h):
    pepdb = dict()
    # skip header
    pepdb_h.readline()
    for line in pepdb_h:
        line_spl = line.strip().split("\t")
        acc_no = line_spl[0]
        db_id = line_spl[30]
        psite_id = line_spl[27]
        sites = parse_psite_id(psite_id)
        pepdb[db_id] = (acc_no, sites)
    return pepdb


def parse_quant_file(quant_h, skip):
    quant = dict()
    pvals = dict()
    # skip first header line
    quant_h.readline()
    # The data files first contain columns with the log2 fold changes
    # and then they contain columns with p-values.  Both sets of
    # columns are preceded by a "Database ID" column.  So, pull apart
    # the main header line to figure out where the second "Database
    # ID" is in order to safely omit the p-values.
    header = quant_h.readline()
    header_spl = header.strip().split("\t")
    header_spl.reverse()
    try:
        quant_cut = header_spl.index("Database ID") + 1
    except ValueError:
        quant_cut = header_spl.index("db ID") + 1
    header_spl.reverse()
    pval_cut = len(header_spl) - quant_cut + 1
    quant_cols = header_spl[3:-quant_cut]
    quant_cols = [s.strip() for s in quant_cols]
    for line in quant_h:
        line_spl = line.strip().split("\t")
        db_id = line_spl[2]
        quant_data = line_spl[3:-quant_cut]
        quant_data = [val for n, val in enumerate(quant_data)
                      if n not in skip]
        pval_data = line_spl[pval_cut:]
        pval_data = [val for n, val in enumerate(pval_data)
                     if n not in skip]
        # We can't handle missing data
        if "NA" in quant_data:
            continue
        quant[db_id] = quant_data
        pvals[db_id] = pval_data
    return quant, pvals, quant_cols


def parse_proteome(proteome_h):
    proteome = dict()
    for seqrec in SeqIO.parse(proteome_h, "fasta"):
        prot_id = seqrec.id.split("|")[2]
        proteome[prot_id] = str(seqrec.seq)
    return proteome


def build_id_map(proteome_h):
    id_map = dict()
    for seqrec in SeqIO.parse(proteome_h, "fasta"):
        _, uniprot_id, prot_id = seqrec.id.split("|")
        id_map[prot_id] = uniprot_id
    return id_map


def seq_window(prot_id, seq, site_str, winlen):
    res = site_str[0]
    site = int(site_str[1:])
    site -= 1
    half = winlen//2
    try:
        assert(site >= 0 and site < len(seq))
    except AssertionError:
        return ("NA", "NA")
    try:
        assert(seq[site] in ('S', 'T', 'Y'))
    except AssertionError:
        sys.stderr.write("Bad seq: " + prot_id + " " + site_str + "\n")
        return ("NA", "NA")
    try:
        assert(seq[site] == res)
    except AssertionError:
        sys.stderr.write("Wrong residue: " + prot_id + " - " + site_str + "\n")
        return ("NA", "NA")
    start = site-half
    end = site+half+1
    if start < 0:
        startstr = -start * "_" + seq[0:(half+start)+1]
    else:
        startstr = seq[start:start+half+1]
    if end > len(seq):
        endstr = seq[start+half+1:len(seq)] + (end-len(seq)) * "_"
    else:
        endstr = seq[start+half+1:end]
    return("-".join([prot_id, str(site+1)]), startstr+endstr)


def calc_quant_range(quant_data):
    quant_num = [float(val) for val in quant_data]
    max_quant = max(quant_num)
    min_quant = min(quant_num)
    return max_quant - min_quant


def format_data(pepdb, quant, pvals, proteome, id_map, winlen):
    data = dict()
    for db_id, quant_data in quant.items():
        prot_id, sites = pepdb[db_id]
        try:
            prot_seq = proteome[prot_id]
        except KeyError:
            continue
        try:
            uniprot_id = id_map[prot_id]
        except KeyError:
            continue
        quant_range = calc_quant_range(quant_data)
        pvals_data = pvals[db_id]
        for site in sites:
            db_id = prot_id + "-" + site
            site_id = uniprot_id + "-" + site
            if site_id not in data:
                seqwin = seq_window(prot_id, prot_seq, site, winlen)[1]
                if seqwin == "NA":
                    continue
                data[site_id] = [seqwin, quant_data, pvals_data]
            else:
                # To simplify things, if a site is observed on
                # multiple peptides, just keep the one that shows the
                # greatest dynamic range.  It's not perfect but it
                # will do.
                old_range = calc_quant_range(data[site_id][1])
                if quant_range > old_range:
                    data[site_id][1] = quant_data
                    data[site_id][2] = pvals_data
    return data


def print_data(data, conditions):
    header = ["site.id", "seq.win"]
    header.extend(conditions)
    header.extend([c + ".fdr" for c in conditions])
    print("\t".join(header))
    for site_id, site_data in data.items():
        seqwin, quant_data, pvals_data = site_data
        line = [site_id, seqwin]
        line.extend(quant_data)
        line.extend(pvals_data)
        print("\t".join(line))


if __name__ == "__main__":
    if len(sys.argv) != 5:
        sys.exit("USAGE: <script> PEPTIDE_DB QUANT_FILE PROTEOME WINLEN")
    pepdb_file = sys.argv[1]
    quant_file = sys.argv[2]
    proteome_file = sys.argv[3]
    winlen = int(sys.argv[4])
    with open(pepdb_file) as h:
        pepdb = parse_peptide_db(h)
    # Don't include parental/baseline conditions from the quantitative
    # data because they're always equal to zero
    if "timecourse" in quant_file:
        skip = [0, 5]
    elif "stoich" in quant_file:
        skip = [0]
    else:
        skip = []
    with open(quant_file) as h:
        quant, pvals, conditions = parse_quant_file(h, skip)
    with open(proteome_file) as h:
        proteome = parse_proteome(h)
    with open(proteome_file) as h:
        id_map = build_id_map(h)
    data = format_data(pepdb, quant, pvals, proteome, id_map, winlen)
    print_data(data, conditions)
