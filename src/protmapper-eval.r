library(parallel)
library(tidyverse)
library(viridis)
library(tidytext)
library(cowplot)
library(ggnewscale)
library(ROCR)
theme_set(theme_cowplot())
source("src/rocr-ggplot2.r")

args <- commandArgs(TRUE)
win_len <- as.integer(args[1])

half_winlen <- floor(win_len/2)
s_regexp <- paste0(".{", half_winlen, "}[S].{", half_winlen, "}")
t_regexp <- paste0(".{", half_winlen, "}[T].{", half_winlen, "}")
y_regexp <- paste0(".{", half_winlen, "}[Y].{", half_winlen, "}")

pssm_info <- read_tsv("out/pssm-info.tsv")
good_pssms <- unique(filter(pssm_info, seq.n >= 20)$kinase)

true_pos <- read_tsv("data/protmapper-kinase-substrates.tsv",
                     col_names = c("true.kinase", "site.id", "seq.win")) %>%
    filter(true.kinase %in% good_pssms)
Y_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase"))
ST_kinases <- read_tsv("data/kinases-ST.txt", col_names = c("kinase"))

prepare_scores <- function(score_file, good_pssms, Y_kinases){
    protmapper_scores <- read_tsv(score_file) %>%
        filter(kinase %in% good_pssms)
    protmapper_scores$main.id <- unlist(lapply(strsplit(protmapper_scores$kinase, split = "-"),
                                      function(split) split[1]))
    protmapper_scores$y.kinase <- protmapper_scores$main.id %in% Y_kinases$kinase
    protmapper_scores$kinase.type <- rep(NA, nrow(protmapper_scores))
    protmapper_scores$kinase.type[which(protmapper_scores$y.kinase)] <- "Y"
    protmapper_scores$kinase.type[which(!protmapper_scores$y.kinase)] <- "S/T"
    protmapper_scores$kinase.type <- as.factor(protmapper_scores$kinase.type)
    protmapper_scores$y.kinase <- NULL
    return(protmapper_scores)
}

## Calculate per-site score ranks and pull in the known kinase(s) for
## each site
get_protmapper_scores_bysite <- function(protmapper_scores, method.name, true_pos){
    protmapper_scores_bysite <- group_by(protmapper_scores, site.id) %>%
        mutate(score.rank = rank(-score),
               method = rep(method.name, n())) %>%
        arrange(site.id, kinase) %>%
        inner_join(true_pos, by = c("site.id", "seq.win"))
    return(protmapper_scores_bysite)
}

get_sim_scores_bysite <- function(sim_scores, method.name, true_pos){
    sim_scores_bysite <- group_by(sim_scores, seq.win) %>%
        mutate(score.rank = rank(-score),
               method = rep(method.name, n())) %>%
        arrange(seq.win, kinase)
    return(sim_scores_bysite)
}

add_seq_type <- function(scores_bysite, y_regexp){
    y.site <- grepl(y_regexp, scores_bysite$seq.win)
    scores_bysite$seq.type <- rep(NA, nrow(scores_bysite))
    scores_bysite$seq.type[y.site] <- "Y"
    scores_bysite$seq.type[!y.site] <- "S/T"
    return(scores_bysite)
}

add_acceptors <- function(scores_bysite, y_regexp, s_regexp, t_regexp){
    y.site <- grepl(y_regexp, scores_bysite$seq.win)
    s.site <- grepl(s_regexp, scores_bysite$seq.win)
    t.site <- grepl(t_regexp, scores_bysite$seq.win)
    scores_bysite$acceptor <- rep(NA, nrow(scores_bysite))
    scores_bysite$acceptor[y.site] <- "Y"
    scores_bysite$acceptor[s.site] <- "S"
    scores_bysite$acceptor[t.site] <- "T"
    return(scores_bysite)
}

protmapper_glob_job <- mcparallel(prepare_scores("out/protmapper-norm/protmapper-scores-glob.tsv", good_pssms,
                                          Y_kinases) %>%
                           get_protmapper_scores_bysite("PSSM proteome bg.", true_pos) %>%
                           add_acceptors(y_regexp, s_regexp, t_regexp) %>%
                           add_seq_type(y_regexp))
protmapper_pos_job <- mcparallel(prepare_scores("out/protmapper-norm/protmapper-scores-pos.tsv", good_pssms,
                                         Y_kinases) %>%
                          get_protmapper_scores_bysite("PSSM phosphoproteome bg.", true_pos) %>%
                          add_acceptors(y_regexp, s_regexp, t_regexp) %>%
                          add_seq_type(y_regexp))
protmapper_bayes_job <- mcparallel(prepare_scores("out/protmapper/protmapper-scores-bayes.tsv", good_pssms,
                                          Y_kinases) %>%
                           get_protmapper_scores_bysite("Naive Bayes", true_pos) %>%
                           add_acceptors(y_regexp, s_regexp, t_regexp) %>%
                           add_seq_type(y_regexp))
protmapper_bayesp_job <- mcparallel(prepare_scores("out/protmapper/protmapper-scores-bayes-plus.tsv", good_pssms,
                                          Y_kinases) %>%
                           get_protmapper_scores_bysite("Naive Bayes+", true_pos) %>%
                           add_acceptors(y_regexp, s_regexp, t_regexp) %>%
                           add_seq_type(y_regexp))
protmapper_data <- mccollect(list(protmapper_glob_job, protmapper_pos_job, protmapper_bayes_job, protmapper_bayesp_job))
protmapper_glob_bysite <- protmapper_data[[1]]
protmapper_pos_bysite <- protmapper_data[[2]]
protmapper_bayes_bysite <- protmapper_data[[3]]
protmapper_bayesp_bysite <- protmapper_data[[4]]
protmapper_bysite <- bind_rows(protmapper_glob_bysite, protmapper_pos_bysite, protmapper_bayes_bysite,
                        protmapper_bayesp_bysite)
protmapper_bysite$method <- factor(protmapper_bysite$method,
                            levels = c("PSSM phosphoproteome bg.",
                                       "PSSM proteome bg.",
                                       "Naive Bayes",
                                       "Naive Bayes+"))

protmapper_bysite_w <- pivot_wider(protmapper_bysite,
                            names_from = method,
                            values_from = c(score, score.rank),
                            names_sep = ".") %>%
    mutate(rank.diff = `score.rank.Naive Bayes` - `score.rank.PSSM phosphoproteome bg.`)
write_tsv(protmapper_bysite_w, "out/protmapper-scores.tsv")

## What are the ranks of the known kinase(s) for each site?
protmapper_bysite_trues <- filter(protmapper_bysite, kinase == true.kinase)
protmapper_bysite_w_trues <- filter(protmapper_bysite_w, kinase == true.kinase)

protmapper_bysite_w_trues_st <- filter(protmapper_bysite_w_trues, kinase.type == "S/T")
protmapper_bysite_w_trues_y <- filter(protmapper_bysite_w_trues, kinase.type == "Y")
st_rank_diff_mean <- mean(protmapper_bysite_w_trues_st$rank.diff)
st_rank_diff_se <- sd(protmapper_bysite_w_trues_st$rank.diff)/sqrt(nrow(protmapper_bysite_w_trues_st))
y_rank_diff_mean <- mean(protmapper_bysite_w_trues_y$rank.diff)
y_rank_diff_se <- sd(protmapper_bysite_w_trues_y$rank.diff)/sqrt(nrow(protmapper_bysite_w_trues_y))
protmapper_bysite_sum <- tibble(kinase.type = c("S/T", "Y"),
                         rank.diff.mean = c(st_rank_diff_mean,
                                            y_rank_diff_mean),
                         rank.diff.se = c(st_rank_diff_se,
                                            y_rank_diff_se))

rank_dist_plot <- ggplot(protmapper_bysite_trues, aes(x = method, y = score.rank)) +
    geom_violin(draw_quantiles = c(0.25, 0.5, 0.75), fill = "gray") +
    facet_wrap(~kinase.type) +
    ylab("PSSM score rank") +
    xlab("background distribution")##  +
## scale_y_log10()
rank_diff_plot <- ggplot(protmapper_bysite_sum, aes(x = kinase.type, y = rank.diff.mean)) +
    geom_hline(yintercept = 0, lty = 2, color = "gray") +
    geom_col(fill = "gray") +
    geom_errorbar(aes(ymin = rank.diff.mean - rank.diff.se,
                      ymax = rank.diff.mean + rank.diff.se),
                  width = 0.2) +
    ylab("mean score rank difference\n(Naive Bayes - PSSM phosphoproteome bg.)") +
    xlab("kinase type")
protmapper_hist <- ggplot(protmapper_bysite_trues, aes(x = score.rank, fill = kinase.type,
                                         color = kinase.type)) +
    geom_bar() +
    scale_fill_brewer("kinase type", palette = "Dark2") +
    scale_color_brewer("", palette = "Dark2", guide = FALSE) +
    xlab("PSSM-score rank of true kinase-substrate relationships") +
    facet_wrap(~method) +
    theme(legend.position = c(0.34, 0.85),
          panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())
protmapper_plot_top <- plot_grid(protmapper_hist, rank_diff_plot, labels = "auto",
                      align = "h", axis = "l", nrow = 1, rel_widths = c(1, 0.5))

## Test that the phosphoproteome background gives better (lower) score
## ranks for known kinase-substrate relationships
wilcox.test(protmapper_bysite_w_trues$`score.rank.Naive Bayes`,
       protmapper_bysite_w_trues$`score.rank.PSSM phosphoproteome bg.`,
       alternative = "less", paired = TRUE)

wilcox.test(protmapper_bysite_w_trues_st$`score.rank.Naive Bayes`,
       protmapper_bysite_w_trues_st$`score.rank.PSSM phosphoproteome bg.`,
       alternative = "less", paired = TRUE)

wilcox.test(protmapper_bysite_w_trues_y$`score.rank.Naive Bayes`,
       protmapper_bysite_w_trues_y$`score.rank.PSSM phosphoproteome bg.`,
       alternative = "less", paired = TRUE)

wilcox.test(protmapper_bysite_w_trues$`score.rank.Naive Bayes`,
       protmapper_bysite_w_trues$`score.rank.Naive Bayes+`,
       alternative = "less", paired = TRUE)

wilcox.test(protmapper_bysite_w_trues_st$`score.rank.Naive Bayes`,
       protmapper_bysite_w_trues_st$`score.rank.Naive Bayes+`,
       alternative = "less", paired = TRUE)

wilcox.test(protmapper_bysite_w_trues_y$`score.rank.Naive Bayes`,
       protmapper_bysite_w_trues_y$`score.rank.Naive Bayes+`,
       alternative = "less", paired = TRUE)

## Load scores of simulated sequence windows
sim_glob_job <- mcparallel(prepare_scores("out/sim-norm/sim-scores-glob.tsv", good_pssms,
                                          Y_kinases) %>%
                           get_sim_scores_bysite("PSSM proteome bg.", true_pos) %>%
                           add_acceptors(y_regexp, s_regexp, t_regexp) %>%
                           add_seq_type(y_regexp))
sim_pos_job <- mcparallel(prepare_scores("out/sim-norm/sim-scores-pos.tsv", good_pssms,
                                         Y_kinases) %>%
                          get_sim_scores_bysite("PSSM phosphoproteome bg.", true_pos) %>%
                           add_acceptors(y_regexp, s_regexp, t_regexp) %>%
                           add_seq_type(y_regexp))
sim_bayes_job <- mcparallel(prepare_scores("out/sim/sim-scores-bayes.tsv", good_pssms,
                                          Y_kinases) %>%
                           get_sim_scores_bysite("Naive Bayes", true_pos) %>%
                           add_acceptors(y_regexp, s_regexp, t_regexp) %>%
                           add_seq_type(y_regexp))
sim_bayesp_job <- mcparallel(prepare_scores("out/sim/sim-scores-bayes-plus.tsv", good_pssms,
                                          Y_kinases) %>%
                           get_sim_scores_bysite("Naive Bayes+", true_pos) %>%
                           add_acceptors(y_regexp, s_regexp, t_regexp) %>%
                           add_seq_type(y_regexp))
sim_data <- mccollect(list(sim_glob_job, sim_pos_job, sim_bayes_job, sim_bayesp_job))
sim_glob_bysite <- sim_data[[1]]
sim_pos_bysite <- sim_data[[2]]
sim_bayes_bysite <- sim_data[[3]]
sim_bayesp_bysite <- sim_data[[4]]
sim_bysite <- bind_rows(sim_glob_bysite, sim_pos_bysite, sim_bayes_bysite,
                        sim_bayesp_bysite) %>%
    mutate(label = rep(FALSE, n())) %>%
    ungroup()
sim_bysite$method <- factor(sim_bysite$method,
                        levels = c("PSSM phosphoproteome bg.",
                                   "PSSM proteome bg.",
                                   "Naive Bayes",
                                   "Naive Bayes+"))
protmapper_bysite_trues <- ungroup(protmapper_bysite_trues)

val_kins <- unique(protmapper_bysite_trues$kinase)

protmapper_scores <- list()
protmapper_labels <- list()
num_reps <- 10

sample_sim_bysite <- function(scores_bysite, acceptors, residue){
    if (!is.na(acceptors[residue])){
        scores_bysite_sub <- sample_n(scores_bysite, acceptors[residue])
    }else{
        scores_bysite_sub <- NULL
    }
    return (scores_bysite_sub)
}

get_sim_bysite_sub <- function(sim_bysite, method.name, acceptors, kin){
    sim_bysite_sub <- NULL
    sim_bysite_filt <- filter(sim_bysite, method == method.name, kinase == kin)
    for (residue in names(acceptors)){
        sim_bysite_res <- filter(sim_bysite_filt, acceptor == residue)
        sim_bysite_res_samp <- sample_sim_bysite(sim_bysite_res, acceptors, residue)
        if (is.null(sim_bysite_sub)){
            sim_bysite_sub <- sim_bysite_res_samp
        }else{
            sim_bysite_sub <- bind_rows(sim_bysite_sub, sim_bysite_res_samp)
        }
    }
    return (sim_bysite_sub)
}

format_scores <- function(val_set, method.name, scores_list){
    val_set_meth <- filter(val_set, method == method.name)
    for (kin.type in c("S/T", "Y")){
        val_set_res <- filter(val_set_meth, kinase.type == kin.type)
        if (is.null(scores_list[[method.name]][[kin.type]])){
            scores_list[[method.name]][[kin.type]] <- val_set_res$score
        }else{
            scores_list[[method.name]][[kin.type]] <- cbind(scores_list[[method.name]][[kin.type]],
                                                            val_set_res$score)
        }
    }
    return (scores_list)
}

format_labels <- function(val_set, method.name, labels_list){
    val_set_meth <- filter(val_set, method == method.name)
    for (kin.type in c("S/T", "Y")){
        val_set_res <- filter(val_set_meth, kinase.type == kin.type)
        if (is.null(labels_list[[method.name]][[kin.type]])){
            labels_list[[method.name]][[kin.type]] <- val_set_res$label
        }else{
            labels_list[[method.name]][[kin.type]] <- cbind(labels_list[[method.name]][[kin.type]],
                                                            val_set_res$label)
        }
    }
    return (labels_list)
}

n_meth <- length(levels(protmapper_bysite_trues$method))
for (r in 1:num_reps){
    sim_bysite_filt <- NULL
    pb <- txtProgressBar(max = length(val_kins), style = 3)
    for (i in 1:length(val_kins)){
        setTxtProgressBar(pb, i)
        k <- val_kins[i]
        ## Get the same number of simulated S/T and Y substrates as there
        ## are for this kinase in PROTMAPPER.
        acceptors <- table(filter(protmapper_bysite_trues, kinase == k)$acceptor) / n_meth
        sim_bysite_sub <- NULL
        sim_glob_job <- mcparallel(get_sim_bysite_sub(sim_bysite, "PSSM proteome bg.",
                                                      acceptors, k))
        sim_pos_job <- mcparallel(get_sim_bysite_sub(sim_bysite, "PSSM phosphoproteome bg.",
                                                      acceptors, k))
        sim_bayes_job <- mcparallel(get_sim_bysite_sub(sim_bysite, "Naive Bayes",
                                                      acceptors, k))
        sim_bayesp_job <- mcparallel(get_sim_bysite_sub(sim_bysite, "Naive Bayes+",
                                                        acceptors, k))
        sim_bysite_subs <- mccollect(list(sim_glob_job, sim_pos_job, sim_bayes_job,
                                          sim_bayesp_job))
        sim_bysite_sub <- bind_rows(sim_bysite_subs[[1]], sim_bysite_subs[[2]],
                                    sim_bysite_subs[[3]], sim_bysite_subs[[4]])
        if (is.null(sim_bysite_filt)){
            sim_bysite_filt <- sim_bysite_sub
        }else{
            sim_bysite_filt <- bind_rows(sim_bysite_filt, sim_bysite_sub)
        }
    }
    close(pb)
    ## Construct a validation set for ROC analysis
    protmapper_val_set <- protmapper_bysite_trues %>%
        select(kinase, seq.win, score, kinase.type, score.rank, method,
               seq.type) %>%
        mutate(label = rep(TRUE, n())) %>%
        bind_rows(sim_bysite_filt)
    for (method.name in levels(protmapper_val_set$method)){
        protmapper_scores <- format_scores(protmapper_val_set, method.name, protmapper_scores)
        protmapper_labels <- format_labels(protmapper_val_set, method.name, protmapper_labels)
    }
}

protmapper_preds <- list()
for (method.name in levels(protmapper_bysite$method)){
    protmapper_preds[[method.name]] <- list()
    for (kin.type in c("S/T", "Y")){
        pred <- prediction(protmapper_scores[[method.name]][[kin.type]],
                           protmapper_labels[[method.name]][[kin.type]])
        protmapper_preds[[method.name]][[kin.type]] <- pred
    }
}

protmapper_aucs <- list()
for (method.name in levels(protmapper_bysite$method)){
    protmapper_aucs[[method.name]] <- list()
    for (kin.type in c("S/T", "Y")){
        auc <- mean(unlist(performance(protmapper_preds[[method.name]][[kin.type]], "auc")@y.values))
        protmapper_aucs[[method.name]][[kin.type]] <- auc
    }
}

auc_tbl <- tibble(kinase.type = c("S/T kinase", "Y kinase",
                                  "S/T kinase", "Y kinase",
                                  "S/T kinase", "Y kinase",
                                  "S/T kinase", "Y kinase"),
                  method = c("Naive Bayes+", "Naive Bayes+",
                             "Naive Bayes", "Naive Bayes",
                             "PSSM proteome bg.", "PSSM proteome bg.",
                             "PSSM phosphoproteome bg.", "PSSM phosphoproteome bg."),
                  auc = c(protmapper_aucs[["Naive Bayes+"]][["S/T"]], protmapper_aucs[["Naive Bayes+"]][["Y"]],
                          protmapper_aucs[["Naive Bayes"]][["S/T"]], protmapper_aucs[["Naive Bayes"]][["Y"]],
                          protmapper_aucs[["PSSM proteome bg."]][["S/T"]], protmapper_aucs[["PSSM proteome bg."]][["Y"]],
                          protmapper_aucs[["PSSM phosphoproteome bg."]][["S/T"]], protmapper_aucs[["PSSM phosphoproteome bg."]][["Y"]]),
                  str.y = c(0.98, 0.98,
                            0.9, 0.9,
                            0.82, 0.82,
                            0.76, 0.76)) %>%
    mutate(auc.str = paste("AUC==", format(auc, digits = 3)))

fpr_cutoff <- 0.1

protmapper_val_roc <- NULL
protmapper_val_roc_a <- NULL
for (method.name in levels(protmapper_bysite$method)){
    for (kin.type in c("S/T", "Y")){
        roc <- build.cvperf2d.df(protmapper_preds[[method.name]][[kin.type]],
                                 "fpr", "tpr", method.name, num_reps, TRUE) %>%
            mutate(kinase.type = rep(paste(kin.type, "kinase"), n()))
        if (is.null(protmapper_val_roc)){
            protmapper_val_roc <- roc
        }else{
            protmapper_val_roc <- bind_rows(protmapper_val_roc, roc)
        }
        roc_a <- build.cvperf2d.df(protmapper_preds[[method.name]][[kin.type]],
                                   "fpr", "tpr", method.name, num_reps, FALSE) %>%
            group_by(n) %>%
            filter(x < fpr_cutoff) %>%
            slice(n()) %>%
            ungroup() %>%
            select(x, y, alpha) %>%
            colMeans() %>%
            t() %>%
            as_tibble() %>%
            mutate(method = c(method.name),
                   kinase.type = c(paste(kin.type, "kinase")))
        if (is.null(protmapper_val_roc_a)){
            protmapper_val_roc_a <- roc_a
        }else{
            protmapper_val_roc_a <- bind_rows(protmapper_val_roc_a, roc_a)
        }
    }
}
protmapper_val_roc$method <- factor(protmapper_val_roc$method,
                             levels = c("Naive Bayes+", "Naive Bayes",
                                        "PSSM proteome bg.",
                                        "PSSM phosphoproteome bg."))
protmapper_val_roc_a$method <- factor(protmapper_val_roc_a$method,
                               levels = c("Naive Bayes+", "Naive Bayes",
                                          "PSSM proteome bg.",
                                          "PSSM phosphoproteome bg."))

protmapper_val_prec <- NULL
for (method.name in levels(protmapper_bysite$method)){
    for (kin.type in c("S/T", "Y")){
        prec <- build.cvperf2d.df(protmapper_preds[[method.name]][[kin.type]],
                                  "rec", "prec", method.name, num_reps, TRUE) %>%
            mutate(kinase.type = rep(paste(kin.type, "kinase"), n()))
        if (is.null(protmapper_val_prec)){
            protmapper_val_prec <- prec
        }else{
            protmapper_val_prec <- bind_rows(protmapper_val_prec, prec)
        }
    }
}
protmapper_val_prec$method <- factor(protmapper_val_prec$method,
                              levels = c("Naive Bayes+", "Naive Bayes",
                                         "PSSM proteome bg.",
                                         "PSSM phosphoproteome bg."))

roc <- rocr2d.ggplot2(protmapper_val_roc, "False Positive Rate", "True Positive Rate",
                      num_reps, c(0, 1), TRUE) +
    coord_fixed() +
    scale_color_hue("background") +
    theme(legend.position = c(0.10, 0.40),
          panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank()) +
    geom_text(data = auc_tbl, x = 0, hjust = "left", parse = TRUE,
              aes(label = auc.str, y = str.y), show.legend = FALSE) +
    facet_wrap(~kinase.type)
prec <- rocr2d.ggplot2(protmapper_val_prec, "Recall", "Precision",
                       num_reps, c(0.5, 0), TRUE) +
    coord_fixed() +
    scale_color_hue("background") +
    theme(legend.position = c(0.05, 0.40),
          panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank()) +
    facet_wrap(~kinase.type)

protmapper_plot_bot <- plot_grid(roc, prec, ncol = 2, labels = c("c", "d"))

protmapper_plot <- plot_grid(protmapper_plot_top, protmapper_plot_bot, nrow = 2, align = "v", axis = "l")
save_plot("img/protmapper-eval.pdf", protmapper_plot, nrow = 2, ncol = 3, base_asp = 1)

## For determining score cutoffs at the desired FPR (fpr_cutoff):
message("Here are your cutoffs:")
print(protmapper_val_roc_a)
write_tsv(protmapper_val_roc_a, "out/protmapper-pssm-cutoffs.tsv")
