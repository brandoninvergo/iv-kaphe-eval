library(tidyverse)
library(cowplot)
library(RVenn)

theme_set(theme_cowplot())

ivkaphe <- read_tsv("out/pmapperval/pmapperval-scores-randforest-expand.tsv")
networkin <- read_tsv("out/pmapperval/pmapperval-scores-networkin-expand.tsv")
linkphinder <- read_tsv("data/linkphinder.tsv")

id_map <- read_tsv("data/kinase-id-map.tsv", col_names = c("uniprot", "name", "gene"))

ivkaphe_kinases <- unique(ivkaphe$kinase)
networkin_kinases <- unique(networkin$kinase)
gps_kinases <- read_tsv("data/gps-kinases.txt", col_names = "kinase") %>%
    left_join(id_map, by = c("kinase" = "gene")) %>%
    pull(uniprot)
linkphinder_kinases <- unique(linkphinder$kinase)

kinases <- list("NetworKIN 3.0" = networkin_kinases,
                "GPS 5.0" = gps_kinases,
                "LinkPhinder" = linkphinder_kinases,
                "IV-KAPhE" = ivkaphe_kinases)

p <- ggvenn(kinases, set_name_size = 4, text_size = 6, show_percentage = FALSE) +
    scale_y_continuous()
save_plot("img/method-coverage.pdf", p, base_height = 4.5)
