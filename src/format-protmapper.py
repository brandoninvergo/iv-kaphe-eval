import sys
import re
from Bio import SeqIO


PROT_ID_RE = re.compile(r"^([A-Za-z0-9-]+)")
SITES_RE = re.compile(r"p-([STY][0-9]+)")
HGNC_RE = re.compile(r"GN=([A-Za-z0-9-]+)")


def parse_protmapper_file(protmapper_h, kinases):
    protmapper = dict()
    # Skip header
    protmapper_h.readline()
    for line in protmapper_h:
        line_spl = line.strip().split(",")
        # Only take sites where a kinase is listed as the "control"
        # and a Uniprot ID is available for it
        if line_spl[1] != "UP" or line_spl[2] not in kinases:
            continue
        kinase = line_spl[2]
        substrate = line_spl[5]
        # Skip autophosphorylation
        if kinase == substrate:
            continue
        residue = line_spl[7]
        site = line_spl[8]
        if kinase not in protmapper:
            protmapper[kinase] = [(substrate, site, residue)]
        else:
            protmapper[kinase].append((substrate, site, residue))
    return protmapper


def parse_proteome(proteome_h):
    proteome = dict()
    for seqrec in SeqIO.parse(proteome_h, "fasta"):
        prot_id = seqrec.id.split("|")[1]
        proteome[prot_id] = str(seqrec.seq)
    return proteome


def seq_window(prot_id, seq, site, res, winlen):
    site -= 1
    half = winlen//2
    try:
        assert(site >= 0 and site < len(seq))
    except AssertionError:
        return ("NA", "NA")
    try:
        assert(seq[site] in ('S', 'T', 'Y'))
    except AssertionError:
        sys.stderr.write("Bad seq: " + prot_id + " " + str(site) + "\n")
        return ("NA", "NA")
    try:
        assert(seq[site] == res)
    except AssertionError:
        sys.stderr.write("Wrong residue: " + prot_id + " - " + str(site) + "\n")
        return ("NA", "NA")
    start = site-half
    end = site+half+1
    if start < 0:
        startstr = -start * "_" + seq[0:(half+start)+1]
    else:
        startstr = seq[start:start+half+1]
    if end > len(seq):
        endstr = seq[start+half+1:len(seq)] + (end-len(seq)) * "_"
    else:
        endstr = seq[start+half+1:end]
    return("-".join([prot_id, str(site+1)]), startstr+endstr)


def print_data(protmapper, proteome, winlen):
    for kinase, sites in protmapper.items():
        for site_info in sites:
            substrate, site, residue = site_info
            try:
                prot_seq = proteome[substrate]
            except KeyError:
                continue
            site_id = substrate + "-" + residue + site
            seqwin = seq_window(substrate, prot_seq, int(site), residue, winlen)[1]
            if seqwin == "NA":
                continue
            line = [kinase, site_id, seqwin]
            print("\t".join(line))


if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.exit("USAGE: <script> PROTMAPPER_FILE PROTEOME WINLEN")
    protmapper_file = sys.argv[1]
    proteome_file = sys.argv[2]
    winlen = int(sys.argv[3])
    kinases = set()
    with open("data/kinases.txt") as h:
        for line in h:
            kinases.add(line.strip())
    with open(protmapper_file, 'r', errors="replace") as h:
        protmapper = parse_protmapper_file(h, kinases)
    with open(proteome_file) as h:
        proteome = parse_proteome(h)
    print_data(protmapper, proteome, winlen)
