{
    # Take only human kinases & substrates
    # Remove autophosphorylation
    # Only use STY phosphorylation
    if (($4 == "human" &&
         $9 == "human" &&
         $3 != $7 &&
         $12 ~ /[A-Za-z_]{7}[sty][A-Za-z_]{7}/)){
        if ($8 != "")
            printf("%s\t%s-%s\t%s\n", $3, $8, $10, $12)
        else
            printf("%s\t%s-%s\t%s\n", $3, $7, $10, $12)
    }
}
