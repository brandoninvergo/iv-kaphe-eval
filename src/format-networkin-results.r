library(tidyverse)

uniprot_id_map <- read_tsv("data/raw/Homo_sapiens.GRCh38.104.uniprot.tsv") %>%
    select(protein_stable_id, xref) %>%
    rename(ensembl = protein_stable_id,
           uniprot = xref)
kinases <- readLines("data/kinases.txt")

preds <- readr::read_tsv("out/pmapperval-networkin-preds.tsv") %>%
    dplyr::select(`#Name`, Position, `Kinase/Phosphatase/Phospho-binding domain`,
                  `NetworKIN score`, `Kinase/Phosphatase/Phospho-binding domain STRING ID`) %>%
    dplyr::rename(kinase.name = `Kinase/Phosphatase/Phospho-binding domain`,
                  substrate = `#Name`,
                  site = Position,
                  score = `NetworKIN score`,
                  ensembl = `Kinase/Phosphatase/Phospho-binding domain STRING ID`) %>%
    dplyr::left_join(uniprot_id_map, by = "ensembl") %>%
    dplyr::rename(kinase = uniprot) %>%
    dplyr::filter(kinase %in% kinases) %>%
    dplyr::mutate(site.id = paste(substrate, site, sep = "-"),
                  int.id = paste(kinase, site.id)) %>%
    dplyr::select(kinase, site.id, int.id, score)

pmapperval <- readr::read_tsv("data/pmapperval-kinase-substrates.tsv",
                              col_names = c("kinase", "site.id", "seq.win")) %>%
    dplyr::mutate(int.id = paste(kinase, site.id))

filter(preds, kinase %in% pmapperval$kinase, site.id %in% pmapperval$site.id) %>%
    left_join(select(pmapperval, site.id, seq.win), by = "site.id") %>%
    select(kinase, site.id, seq.win, score) %>%
    readr::write_tsv("out/pmapperval/pmapperval-scores-networkin-expand.tsv")

filter(preds, int.id %in% pmapperval$int.id) %>%
    select(-int.id) %>%
    left_join(select(pmapperval, site.id, seq.win), by = "site.id") %>%
    distinct() %>%
    select(kinase, site.id, seq.win, score) %>%
    readr::write_tsv("out/pmapperval/pmapperval-scores-networkin.tsv")

