library(rhdf5)
library(tidyverse)
library(viridis)
library(cowplot)
theme_set(theme_cowplot())

get_motif_info <- function(pssm_dir, win_len, scale){
    kinases <- NULL
    phys_int_ps <- NULL
    in_dom_ps <- NULL
    compl_phys_int_ps <- NULL
    compl_in_dom_ps <- NULL
    seq_ns <- NULL
    for (motif_file in list.files(pssm_dir)){
        if (!endsWith(motif_file, "-motif.h5"))
            next
        motif_path <- paste(pssm_dir, motif_file, sep = "/")
        compl_file <- sub("motif", "compl", motif_file)
        compl_path <- paste(pssm_dir, compl_file, sep = "/")
        motif_name <- h5readAttributes(motif_path, "/", native = TRUE)$name
        name_split <- strsplit(motif_name, split = "-")[[1]]
        if (length(name_split) == 2){
            kinase <- name_split[1]
        }else{
            kinase <- paste(name_split[1:2], collapse = "-")
        }
        bern_ps <- h5read(motif_path, "/features/bernoulli", native = TRUE)
        compl_bern_ps <- h5read(compl_path, "/features/bernoulli", native = TRUE)
        ## betas <- h5read(motif_path, "/features/beta", native = TRUE)
        ## compl_betas <- h5read(compl_path, "/features/beta", native = TRUE)
        seqn <- h5readAttributes(motif_path, "/", native = TRUE)$`number of sequences`
        if (is.null(bern_ps) || is.null(compl_bern_ps)){
            print(kinase)
            return(NULL)
        }
        h5closeAll()
        if (is.null(kinases)){
            kinases <- kinase
            phys_int_ps <- bern_ps[1]
            phys_int2_ps <- bern_ps[2]
            in_dom_ps <- bern_ps[3]
            enr_dom_ps <- bern_ps[4]
            ## go_cc_sim_ps <- bern_ps[4]
            ## go_bp_sim_ps <- bern_ps[5]
            compl_phys_int_ps <- compl_bern_ps[1]
            compl_phys_int2_ps <- compl_bern_ps[2]
            compl_in_dom_ps <- compl_bern_ps[3]
            compl_enr_dom_ps <- compl_bern_ps[4]
            ## compl_go_cc_sim_ps <- compl_bern_ps[4]
            ## compl_go_bp_sim_ps <- compl_bern_ps[5]
            seq_ns <- seqn
        }else{
            kinases <- c(kinases, kinase)
            phys_int_ps <- c(phys_int_ps, bern_ps[1])
            phys_int2_ps <- c(phys_int2_ps, bern_ps[2])
            in_dom_ps <- c(in_dom_ps, bern_ps[3])
            enr_dom_ps <- c(enr_dom_ps, bern_ps[4])
            ## go_cc_sim_ps <- c(go_cc_sim_ps, bern_ps[4])
            ## go_bp_sim_ps <- c(go_bp_sim_ps, bern_ps[5])
            compl_phys_int_ps <- c(compl_phys_int_ps, compl_bern_ps[1])
            compl_phys_int2_ps <- c(compl_phys_int2_ps, compl_bern_ps[2])
            compl_in_dom_ps <- c(compl_in_dom_ps, compl_bern_ps[3])
            compl_enr_dom_ps <- c(compl_enr_dom_ps, compl_bern_ps[4])
            ## compl_go_cc_sim_ps <- c(compl_go_cc_sim_ps, compl_bern_ps[4])
            ## compl_go_bp_sim_ps <- c(compl_go_bp_sim_ps, compl_bern_ps[5])
            seq_ns <- c(seq_ns, seqn)
        }
    }
    motif_info <- tibble(kinase = kinases,
                         phys.int = 10^phys_int_ps,
                         phys.int2 = 10^phys_int2_ps,
                         in.dom = 10^in_dom_ps,
                         enr.dom = 10^enr_dom_ps,
                         ## go.cc.sim = 10^go_cc_sim_ps,
                         ## go.bp.sim = 10^go_bp_sim_ps,
                         compl.phys.int = 10^compl_phys_int_ps,
                         compl.phys.int2 = 10^compl_phys_int2_ps,
                         compl.in.dom = 10^compl_in_dom_ps,
                         compl.enr.dom = 10^compl_enr_dom_ps,
                         ## compl.go.cc.sim = 10^compl_go_cc_sim_ps,
                         ## compl.go.bp.sim = 10^compl_go_bp_sim_ps,
                         seq.n = seq_ns)
    return(motif_info)
}

prepare_motif_info <- function(motif_dir, method_name, Y_kinases, win_len, scale){
    pssm_info <- get_motif_info(motif_dir, win_len, scale)
    pssm_info$main.id <- unlist(lapply(strsplit(pssm_info$kinase, split = "-"),
                                      function(split) split[1]))
    pssm_info$y.kinase <- pssm_info$main.id %in% Y_kinases$kinase
    pssm_info$kinase.type <- rep(NA, nrow(pssm_info))
    pssm_info$kinase.type[which(pssm_info$y.kinase)] <- "Y"
    pssm_info$kinase.type[which(!pssm_info$y.kinase)] <- "S/T"
    pssm_info$kinase.type <- as.factor(pssm_info$kinase.type)
    pssm_info$y.kinase <- NULL
    return(pssm_info)
}

y_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase"))
st_kinases <- read_tsv("data/kinases-ST.txt", col_names = c("kinase"))

bayes_pssm_info_full <- prepare_motif_info("out/motifs/bayes-plus", "Naive Bayes+", y_kinases,
                                           win_len, FALSE)

bayes_pssm_info <- filter(bayes_pssm_info_full, seq.n >= 20) %>%
    ## select(-seq.n) %>%
    ## pivot_longer(cols = phys.int:compl.go.bp.sim, names_to = "feature") %>%
    pivot_longer(cols = phys.int:compl.enr.dom, names_to = "feature") %>%
    mutate(substrates = !grepl("compl\\.", feature),
           feature = sub("compl.", "", feature))
bayes_pssm_info$feature <- factor(bayes_pssm_info$feature,
                                  ## levels = c("phys.int", "in.dom", "go.cc.sim.a", "go.cc.sim.b",
                                  ##            "go.bp.sim.a", "go.bp.sim.b"),
                                  levels = c("phys.int", "phys.int2", "in.dom", "enr.dom"),
                                             ## "go.cc.sim", "go.bp.sim"),
                                  labels = c("physical\ninteraction",
                                             "indirect\ninteraction",
                                             "site in\ndomain",
                                             "has enriched\ndomain"
                                             ## "similar\nGO CC",
                                             ## "similar\nGO BP"
                                             ))
bayes_pssm_info$substrates <- factor(bayes_pssm_info$substrates,
                                     levels = c(FALSE, TRUE),
                                     labels = c("other sites", "substrates"))
bayes_pssm_info$kinase.type <- paste(bayes_pssm_info$kinase.type, "kinases")

p <- ggplot(bayes_pssm_info, aes(x = value, color = substrates, fill = substrates)) +
    geom_density(alpha = 0.25) +
    facet_grid(rows = vars(feature), cols = vars(kinase.type),
               scales = "free") +
    xlab("empirical probability") +
    ylab("kernel density estimate") +
    scale_x_continuous(n.breaks = 3, limits = c(0, 1)) +
    scale_color_brewer("probability\namong:", palette = "Dark2") +
    scale_fill_brewer("probability\namong:", palette = "Dark2", direction = 1) +
    theme(panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())
save_plot("img/bayes-features-eval.pdf", p, ncol = 2, nrow = 4, base_height = 2)

p2 <- pivot_wider(bayes_pssm_info, names_from = "substrates", values_from = "value") %>%
    arrange(seq.n) %>%
    ggplot(aes(x = substrates, y = `other sites`, color = seq.n)) +
    facet_grid(rows = vars(feature), cols = vars(kinase.type), scales = "free") +
    geom_abline(intercept = 0, slope = 1, lty = 2, color = "gray") +
    ## geom_density2d() +
    geom_point() +
    scale_color_viridis() +
    scale_x_log10() +
    scale_y_log10() +
    xlim(0, 1) +
    ylim(0, 1) +
    theme(panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())
save_plot("img/bayes-features-eval-b.pdf", p2, ncol = 2, nrow = 4, base_height = 2.5)
