import sys
import re
from Bio import SeqIO


PROT_ID_RE = re.compile(r"^([A-Za-z0-9-]+)")
SITES_RE = re.compile(r"p-([STY][0-9]+)")
HGNC_RE = re.compile(r"GN=([A-Za-z0-9-]+)")


def parse_psp_file(psp_h):
    psp = set()
    # skip first three lines
    psp_h.readline()
    psp_h.readline()
    psp_h.readline()
    for line in psp_h:
        line_spl = line.strip().split("\t")
        # Only take human phosphorylation
        if line_spl[6] != "human":
            continue
        substrate = line_spl[2]
        site = line_spl[4][:-2]
        seq_win = line_spl[9].upper()
        psp.add((substrate, site, seq_win))
    return psp


def parse_proteome(proteome_h):
    proteome = dict()
    for seqrec in SeqIO.parse(proteome_h, "fasta"):
        prot_id = seqrec.id.split("|")[1]
        proteome[prot_id] = str(seqrec.seq)
    return proteome


def seq_window(prot_id, seq, site_str, winlen):
    res = site_str[0]
    site = int(site_str[1:])
    site -= 1
    half = winlen//2
    try:
        assert(site >= 0 and site < len(seq))
    except AssertionError:
        return ("NA", "NA")
    try:
        assert(seq[site] in ('S', 'T', 'Y'))
    except AssertionError:
        sys.stderr.write("Bad seq: " + prot_id + " " + site_str + "\n")
        return ("NA", "NA")
    try:
        assert(seq[site] == res)
    except AssertionError:
        sys.stderr.write("Wrong residue: " + prot_id + " - " + site_str + "\n")
        return ("NA", "NA")
    start = site-half
    end = site+half+1
    if start < 0:
        startstr = -start * "_" + seq[0:(half+start)+1]
    else:
        startstr = seq[start:start+half+1]
    if end > len(seq):
        endstr = seq[start+half+1:len(seq)] + (end-len(seq)) * "_"
    else:
        endstr = seq[start+half+1:end]
    return("-".join([prot_id, str(site+1)]), startstr+endstr)


def print_data(psp, proteome, winlen):
    for site_info in psp:
        substrate, site, psp_seqwin = site_info
        try:
            prot_seq = proteome[substrate]
        except KeyError:
            continue
        site_id = substrate + "-" + site
        seqwin = seq_window(substrate, prot_seq, site, winlen)[1]
        if seqwin == "NA":
            continue
        mid = winlen // 2 - 1
        if seqwin[mid-5:mid+6] != psp_seqwin[1:12]:
            continue
        line = [site_id, seqwin]
        print("\t".join(line))


if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.exit("USAGE: <script> PSP_FILE PROTEOME WINLEN")
    psp_file = sys.argv[1]
    proteome_file = sys.argv[2]
    winlen = int(sys.argv[3])
    with open(psp_file, 'r', errors="replace") as h:
        psp = parse_psp_file(h)
    with open(proteome_file) as h:
        proteome = parse_proteome(h)
    print_data(psp, proteome, winlen)
