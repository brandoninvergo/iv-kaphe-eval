library(tidyverse)

name2id <- read_tsv("data/uniprot-human-idmap.tsv",
                    col_names = c("uniprot.id", "uniprot.name"))

biogrid <- read_tsv("data/human-kinase-interactions.tsv",
                    col_names = c("prot.a", "prot.b")) %>%
    mutate(interaction.id = paste(prot.a, prot.b))

sugiyama <- read_tsv("data/sugiyama-kinase-substrates.tsv") %>%
    separate(substrate.site, into = c("sub.name", "site"), sep = "-") %>%
    left_join(name2id, by = c("sub.name" = "uniprot.name")) %>%
    mutate(substrate = uniprot.id,
           interaction.id1 = paste(kinase, substrate),
           interaction.id2 = paste(substrate, kinase),
           in.biogrid = (interaction.id1 %in% biogrid$interaction.id |
                         interaction.id2 %in% biogrid$interaction.id)) %>%
    select(kinase, substrate, site, seq.win, in.biogrid, interaction.id1, interaction.id2)

Y_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase")) %>%
    pull(kinase)

kinases <- unique(sugiyama$kinase)
kin_intxns <- NULL
for (kin in kinases){
    kin_subs <- filter(sugiyama, kinase == kin) %>%
        select(kinase, substrate, in.biogrid) %>%
        distinct() %>%
        mutate(substrate_set = rep("subs", n()))
    not_kin <- filter(sugiyama, kinase != kin) %>%
        select(kinase, substrate) %>%
        distinct() %>%
        mutate(kinase = rep(kin, n()),
               interaction.id1 = paste(kinase, substrate),
               interaction.id2 = paste(substrate, kinase),
               in.biogrid = (interaction.id1 %in% biogrid$interaction.id |
                             interaction.id2 %in% biogrid$interaction.id),
               substrate_set = rep("not_subs", n())) %>%
        select(-interaction.id1, -interaction.id2)
    foo <- bind_rows(kin_subs, not_kin)
    if (is.null(kin_intxns)){
        kin_intxns <- foo
    }else{
        kin_intxns <- bind_rows(kin_intxns, foo)
    }
}

kin_intxns_sum <- group_by(kin_intxns, kinase, substrate_set) %>%
    summarise(n = n(),
              n.in.biogrid = length(which(in.biogrid)),
              perc.in.biogrid = n.in.biogrid / n) %>%
    mutate(y_kinase = kinase %in% Y_kinases) %>%
    filter(n > 20) %>%
    select(-n, -n.in.biogrid)

ggplot(kin_intxns_sum, aes(x = substrate_set, y = perc.in.biogrid, color = substrate_set)) +
    geom_violin(draw_quantiles = 0.5)

kin_intxns_sum2 <- pivot_wider(kin_intxns_sum, names_from = substrate_set, values_from = perc.in.biogrid)

kin_intxns_sum2_st <- filter(kin_intxns_sum2, !y_kinase)
kin_intxns_sum2_y <- filter(kin_intxns_sum2, y_kinase)

wilcox.test(kin_intxns_sum2$subs, kin_intxns_sum2$not_subs, paired = TRUE, alternative = "greater")
wilcox.test(kin_intxns_sum2_st$subs, kin_intxns_sum2_st$not_subs, paired = TRUE, alternative = "greater")
wilcox.test(kin_intxns_sum2_y$subs, kin_intxns_sum2_y$not_subs, paired = TRUE, alternative = "greater")

ggplot(kin_intxns_sum2, aes(x = log10(not_subs), y = log10(subs), color = y_kinase)) +
    geom_abline(slope = 1, intercept = 0) +
    geom_point() +
    geom_density2d() +
    facet_wrap(~y_kinase)
