library(tidyverse)

uniprot_id_map <- read_tsv("data/raw/Homo_sapiens.GRCh38.104.uniprot.tsv") %>%
    select(protein_stable_id, xref) %>%
    rename(ensembl = protein_stable_id,
           uniprot = xref)
kinases <- readLines("data/kinases.txt")

wilkes <- readr::read_tsv("data/wilkes-et-al-2015-inhib.tsv") %>%
    dplyr::select(site.id, seq.win)

preds <- readr::read_tsv("out/wilkes-networkin-preds.tsv") %>%
    dplyr::select(`#Name`, Position, `Kinase/Phosphatase/Phospho-binding domain`,
                  `NetworKIN score`, `Kinase/Phosphatase/Phospho-binding domain STRING ID`) %>%
    dplyr::rename(kinase.name = `Kinase/Phosphatase/Phospho-binding domain`,
                  substrate = `#Name`,
                  site = Position,
                  score = `NetworKIN score`,
                  ensembl = `Kinase/Phosphatase/Phospho-binding domain STRING ID`) %>%
    dplyr::left_join(uniprot_id_map, by = "ensembl") %>%
    dplyr::rename(kinase = uniprot) %>%
    dplyr::filter(kinase %in% kinases) %>%
    dplyr::mutate(site.id = paste(substrate, site, sep = "-"),
                  int.id = paste(kinase, site.id)) %>%
    dplyr::select(kinase, site.id, int.id, score) %>%
    dplyr::left_join(wilkes, by = "site.id") %>%
    dplyr::select(kinase, site.id, seq.win, score) %>%
    readr::write_tsv("out/wilkes-inhib/wilkes-inhib-scores-networkin.tsv")




