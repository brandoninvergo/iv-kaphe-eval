import sys
import os.path
from Bio import SeqIO


def read_sites(h, site_col):
    proteins = set()
    for line in h:
        line_spl = line.strip().split()
        site_id = line_spl[site_col]
        protein, _, site = site_id.rpartition("-")
        proteins.add(protein)
    return proteins


def extract_proteome(proteins, out_dir):
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    proteome = SeqIO.parse("data/uniprot-human-proteome.fasta", "fasta")
    for prot in proteome:
        _, prot_id, _ = prot.id.split("|")
        if prot_id not in proteins:
            continue
        out_file = os.path.join(out_dir, prot_id + ".fasta")
        SeqIO.write(prot, out_file, "fasta")


if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.exit("USAGE: <script> SITES_FILE SITE_COL OUT_DIR")
    sites_file = sys.argv[1]
    site_col = int(sys.argv[2]) - 1
    out_dir = sys.argv[3]
    with open(sites_file) as h:
        proteins = read_sites(h, site_col)
    extract_proteome(proteins, out_dir)
