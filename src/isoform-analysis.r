library(tidyverse)
library(viridis)
library(cowplot)

theme_set(theme_cowplot())

psp <- read_tsv("data/psp-kinase-substrates-all.tsv",
                col_names = c("kinase", "site.id", "seq.win")) %>%
    mutate(int.id = paste(kinase, site.id))

kinase_id_map <- read_tsv("data/kinase-id-map.tsv",
                          col_names = c("uniprot", "kinase.name", "kinase.symbol"))

uniprot_id_map <- read_tsv("data/uniprot-human-idmap.tsv",
                           col_names = c("uniprot.id", "uniprot.name.full")) %>%
    extract(uniprot.name.full, "substrate.name", regex = "([[:alnum:]-]+)_HUMAN")

phoproteome_preds <- read_tsv("out/phoproteome/phoproteome-scores-randforest.tsv") %>%
    mutate(int.id = paste(kinase, site.id),
           pred.substrate = score > 0.5,
           in.psp = int.id %in% psp$int.id,
           is.substrate = in.psp) %>%
    extract(col = site.id, into = c("substrate", "residue", "site"),
            regex = "([[:alnum:]-]+)-([STY])([[:digit:]]+)", remove = FALSE,
            convert = TRUE) %>%
    left_join(uniprot_id_map, by = c("substrate" = "uniprot.id")) %>%
    left_join(kinase_id_map, by = c("kinase" = "uniprot")) %>%
    select(kinase, kinase.name, kinase.symbol, site.id, substrate, substrate.name,
           site, residue, seq.win, in.psp, is.substrate, pred.substrate, score,
           is.kinase, kinase.type, site.type, nb.score, cc.semsim, bp.semsim,
           coexpression, experimental)

filter(phoproteome_preds, pred.substrate) %>%
    select(-pred.substrate) %>%
    rename(bayesplus.score = nb.score,
       go.cc.semsim = cc.semsim,
       go.bp.semsim = bp.semsim,
       string.coexpression = coexpression,
       string.experimental = experimental) %>%
    write_tsv("out/phoproteome/supplemental-table-s1.tsv")

s6a <- filter(phoproteome_preds, startsWith(kinase.symbol, "RPS6KA"))
## s6a1_subs <- filter(s6a, kinase.symbol == "RPS6KA1", in.psp) %>% pull(site.id)
s6a1_subs <- filter(s6a, in.psp) %>% pull(site.id)
s6a1_multi <-filter(s6a, site.id %in% s6a1_subs) %>%
    mutate(site.id = paste0(substrate.name, "-", residue, site))
rename(s6a1_multi,
       bayesplus.score = nb.score,
       go.cc.semsim = cc.semsim,
       go.bp.semsim = bp.semsim,
       string.coexpression = coexpression,
       string.experimental = experimental) %>%
    write_tsv("out/phoproteome/supplemental-table-s2.tsv")
s6a1_multi_m <- select(s6a1_multi, kinase.symbol, site.id, score) %>%
    pivot_wider(names_from = site.id, values_from = score) %>%
    column_to_rownames("kinase.symbol") %>%
    as.matrix()
s6a1_ord <- hclust(dist(t(s6a1_multi_m), method = "euclidean"), method = "ward.D2")$order
s6a1_multi$site.id <- factor(s6a1_multi$site.id,
                             levels = colnames(s6a1_multi_m)[s6a1_ord])
s6a1_multi$kinase.symbol <- factor(s6a1_multi$kinase.symbol,
                                   levels = c("RPS6KA6",
                                              "RPS6KA5",
                                              "RPS6KA4",
                                              "RPS6KA3",
                                              "RPS6KA2",
                                              "RPS6KA1"),
                                   labels = c("S6K-\u03B1-6",
                                              "S6K-\u03B1-5",
                                              "S6K-\u03B1-4",
                                              "S6K-\u03B1-3",
                                              "S6K-\u03B1-2",
                                              "S6K-\u03B1-1"))

s6a1_subs1 <- levels(s6a1_multi$site.id)[1:55]
s6a1_subs2 <- levels(s6a1_multi$site.id)[56:length(s6a1_subs)]
s6a1_p1 <- filter(s6a1_multi, site.id %in% s6a1_subs1) %>%
    ggplot(aes(x = site.id, y = kinase.symbol, fill = score)) +
    geom_tile() +
    geom_point(aes(alpha = is.substrate)) +
    scale_fill_distiller("IV-KAPhE\nprobability", palette = "RdBu", limits = c(0, 1),
                         n.breaks = 3) +
    scale_alpha_discrete("annotated in\nPhosphoSitePlus", labels = c("no", "yes")) +
    xlab(NULL) +
    ylab(NULL) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1),
          legend.position = "top")
s6a1_p2 <- filter(s6a1_multi, site.id %in% s6a1_subs2) %>%
    ggplot(aes(x = site.id, y = kinase.symbol, fill = score)) +
    geom_tile() +
    geom_point(aes(alpha = is.substrate)) +
    scale_fill_distiller("IV-KAPhE\nprobability", palette = "RdBu", guide = "none") +
    scale_alpha_discrete(guide = "none") +
    xlab(NULL) +
    ylab(NULL) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1))

camk2 <- filter(phoproteome_preds, startsWith(kinase.symbol, "CAMK2"))
## camk2a_subs <- filter(camk2, kinase.symbol == "CAMK2A", in.psp) %>% pull(site.id)
camk2a_subs <- filter(camk2, in.psp) %>% pull(site.id)
camk2a_multi <-filter(camk2, site.id %in% camk2a_subs) %>%
    mutate(site.id = paste0(substrate.name, "-", residue, site))
rename(camk2a_multi,
       bayesplus.score = nb.score,
       go.cc.semsim = cc.semsim,
       go.bp.semsim = bp.semsim,
       string.coexpression = coexpression,
       string.experimental = experimental) %>%
    write_tsv("out/phoproteome/supplemental-table-s3.tsv")
camk2a_multi_m <- select(camk2a_multi, kinase.symbol, site.id, score) %>%
    pivot_wider(names_from = site.id, values_from = score) %>%
    column_to_rownames("kinase.symbol") %>%
    as.matrix()
camk2a_ord <- hclust(dist(t(camk2a_multi_m), method = "euclidean"), method = "ward.D2")$order
camk2a_multi$site.id <- factor(camk2a_multi$site.id,
                             levels = colnames(camk2a_multi_m)[camk2a_ord])
camk2a_multi$kinase.symbol <- factor(camk2a_multi$kinase.symbol,
                                     levels = c("CAMK2D",
                                                "CAMK2G",
                                                "CAMK2B",
                                                "CAMK2A"),
                                     labels = c("CaMK-II \u03B4",
                                                "CaMK-II \u03B3",
                                                "CaMK-II \u03B2",
                                                "CaMK-II \u03B1"))

camk2a_subs1 <- levels(camk2a_multi$site.id)[1:55]
camk2a_subs2 <- levels(camk2a_multi$site.id)[56:105]
camk2a_subs3 <- levels(camk2a_multi$site.id)[106:length(camk2a_subs)]
camk2a_p1 <- filter(camk2a_multi, site.id %in% camk2a_subs1) %>%
    ggplot(aes(x = site.id, y = kinase.symbol, fill = score)) +
    geom_tile() +
    geom_point(aes(alpha = is.substrate)) +
    scale_fill_distiller("IV-KAPhE\nprobability", palette = "RdBu", limits = c(0, 1), guide = "none") +
    scale_alpha_discrete(guide = "none") +
    xlab(NULL) +
    ylab(NULL) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1))
camk2a_p2 <- filter(camk2a_multi, site.id %in% camk2a_subs2) %>%
    ggplot(aes(x = site.id, y = kinase.symbol, fill = score)) +
    geom_tile() +
    geom_point(aes(alpha = is.substrate)) +
    scale_fill_distiller("IV-KAPhE\nprobability", palette = "RdBu", limits = c(0, 1), guide = "none") +
    scale_alpha_discrete(guide = "none") +
    xlab(NULL) +
    ylab(NULL) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1))
camk2a_p3 <- filter(camk2a_multi, site.id %in% camk2a_subs3) %>%
    ggplot(aes(x = site.id, y = kinase.symbol, fill = score)) +
    geom_tile() +
    geom_point(aes(alpha = is.substrate)) +
    scale_fill_distiller("IV-KAPhE\nprobability", palette = "RdBu", limits = c(0, 1), guide = "none") +
    scale_alpha_discrete(guide = "none") +
    xlab(NULL) +
    ylab(NULL) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1))

p <- plot_grid(s6a1_p1, s6a1_p2, camk2a_p1, camk2a_p2, camk2a_p3, nrow = 5,
               rel_heights = c(1.2, 1, 1, 1, 1), labels = c("a", "", "b", "", ""))
save_plot("img/isoform-analysis.pdf", p, nrow = 5, base_asp = 4.3, base_height = 3, device = cairo_pdf)


## https://www.biorxiv.org/content/10.1101/2021.08.20.457146v1.full
p38 <- filter(phoproteome_preds, kinase.symbol %in% c("MAPK11", "MAPK12", "MAPK13", "MAPK14",
                                                "MAPKAPK2", "MAPKAPK", "MAPKAPK5", "RPS6KA5",
                                                "RPS6KA4", "MKNK1"))
## p38a_subs <- filter(p38, kinase.symbol == "P38A", in.psp) %>% pull(site.id)
p38a_subs <- filter(p38, in.psp) %>% pull(site.id)
p38a_multi <-filter(p38, site.id %in% p38a_subs)
## rename(p38a_multi,
##        bayesplus.score = nb.score,
##        go.cc.semsim = cc.semsim,
##        go.bp.semsim = bp.semsim,
##        string.coexpression = coexpression,
##        string.experimental = experimental) %>%
##     write_tsv("out/phoproteome/supplemental-table-s3.tsv")
p38a_multi_m <- select(p38a_multi, kinase.symbol, site.id, score) %>%
    pivot_wider(names_from = site.id, values_from = score) %>%
    column_to_rownames("kinase.symbol") %>%
    as.matrix()
p38a_ord <- hclust(dist(t(p38a_multi_m), method = "euclidean"), method = "ward.D2")$order
p38a_multi$site.id <- factor(p38a_multi$site.id,
                             levels = colnames(p38a_multi_m)[p38a_ord])
p38a_multi$kinase.symbol <- factor(p38a_multi$kinase.symbol,
                                     levels = c("MAPK12",
                                                "MAPK13",
                                                "MAPK11",
                                                "MAPK14",
                                                "MAPKAPK2",
                                                "MAPKAPK3",
                                                "MAPKAPK5",
                                                "RPS6KA5",
                                                "RPS6KA4",
                                                "MKNK1"),
                                     labels = c("p38 \u03B4",
                                                "p38 \u03B3",
                                                "p38 \u03B2",
                                                "p38 \u03B1",
                                                "MK2",
                                                "MK3",
                                                "MK5",
                                                "S6K-\u03B1-5",
                                                "S6K-\u03B1-4",
                                                "MKNK1"))
p38a_p <- ggplot(p38a_multi, aes(x = site.id, y = kinase.symbol, fill = score)) +
    geom_tile() +
    geom_point(aes(alpha = is.substrate)) +
    scale_fill_distiller("IV-KAPhE\nprobability", palette = "RdBu", limits = c(0, 1)) +
    scale_alpha_discrete("annotated in\nPhosphoSitePlus", labels = c("no", "yes")) +
    xlab(NULL) +
    ylab(NULL) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1))

## p38a_cov_init <- filter(p38, substrate %in% c("P04792", "Q9Y580", "Q13263", "P18615", "Q4G0J3", "O15164"))
## p38_cov_pred_subs <- group_by(p38a_cov_init, site.id) %>%
##     summarise(num.preds = sum(as.integer(pred.substrate))) %>%
##     filter(num.preds > 0) %>%
##     pull(site.id)
## p38a_cov <- filter(p38, site.id %in% p38_cov_pred_subs)
p38a_cov <- mutate(p38, site.id.alt = paste(substrate, site)) %>%
    filter(## kinase %in% c("Q15759", "Q16539"),
           site.id.alt %in% c("Q9UKV3 710", "Q8IVF2 5175", "P39687 17", "Q86UU0 118",
                              "Q9UJV9 13", "Q9UJV9 23", "O43598 12", "O43598 10",
                              "Q9NSC5 159", "Q9NZL4 351", "Q9NZL4 347", "P04792 15",
                              "P04792 78", "P04792 82", "Q4G0J3 298", "Q4G0J3 299",
                              "Q4G0J3 300", "Q9UPQ0 472", "Q9UPQ0 471", "Q86UE4 298",
                              "P43243 195", "P43243 188", "Q8NFP9 1321", "P18615 51",
                              "Q86WB0 354", "Q86WB0 370", "P46087 58", "Q8TEW0 809",
                              "Q9H307 66", "Q7Z3K3 746", "Q9Y580 137", "Q5VT52 1099",
                              "Q9C0C2 833", "Q9C0C2 836", "P49368 252", "O15164 1042",
                              "Q13263 473", "Q13263 471", "Q9NS69 15", "Q9Y4A5 2051",
                              "Q9C0C9 839", "Q9GZS3 143"))
p38a_cov_m <- select(p38a_cov, kinase.symbol, site.id, score) %>%
    pivot_wider(names_from = site.id, values_from = score) %>%
    column_to_rownames("kinase.symbol") %>%
    as.matrix()
p38a_cov_ord <- hclust(dist(t(p38a_cov_m), method = "euclidean"), method = "ward.D2")$order
p38a_cov$site.id <- factor(p38a_cov$site.id,
                             levels = colnames(p38a_cov_m)[p38a_cov_ord])
p38a_cov$kinase.symbol <- factor(p38a_cov$kinase.symbol,
                                     levels = c("MAPK12",
                                                "MAPK13",
                                                "MAPK11",
                                                "MAPK14",
                                                "MAPKAPK2",
                                                "MAPKAPK3",
                                                "MAPKAPK5",
                                                "RPS6KA5",
                                                "RPS6KA4",
                                                "MKNK1"),
                                     labels = c("p38 \u03B4",
                                                "p38 \u03B3",
                                                "p38 \u03B2",
                                                "p38 \u03B1",
                                                "MK2",
                                                "MK3",
                                                "MK5",
                                                "S6K-\u03B1-5",
                                                "S6K-\u03B1-4",
                                                "MKNK1"))
p38a_cov_p <- ggplot(p38a_cov, aes(x = site.id, y = kinase.symbol, fill = score)) +
    geom_tile() +
    geom_point(aes(alpha = is.substrate)) +
    scale_fill_distiller("IV-KAPhE\nprobability", palette = "RdBu", limits = c(0, 1)) +
    scale_alpha_discrete("annotated in\nPhosphoSitePlus", labels = c("no", "yes")) +
    xlab(NULL) +
    ylab(NULL) +
    theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1))
