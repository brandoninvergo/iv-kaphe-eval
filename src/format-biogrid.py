import sys
from Bio import SeqIO


def read_proteome(h):
    uniprot_ids = set()
    for s in SeqIO.parse(h, "fasta"):
        uniprot_id = s.id.split("|")[1]
        uniprot_ids.add(uniprot_id)
    return uniprot_ids


def get_interactions(h, proteome):
    pairs = set()
    for line in h:
        line_spl = line.strip().split("\t")
        # Skip non-human stuff
        if line_spl[15] != "9606" or line_spl[16] != "9606":
            continue
        # Skip biochemical reactions to avoid circularity
        if line_spl[13] == "Biochemical Activity":
            continue
        # Skip genetic interactions
        if line_spl[12] == "genetic":
            continue
        uniprot_lst_a = line_spl[23].split("|")
        # Filter out some old(?) uniprot IDs
        uniprot_lst_a = [i for i in uniprot_lst_a if i in proteome]
        uniprot_lst_b = line_spl[26].split("|")
        uniprot_lst_b = [i for i in uniprot_lst_b if i in proteome]
        # Remove self-binding
        uniprot_lst_a = [i for i in uniprot_lst_a if i not in uniprot_lst_b]
        if len(uniprot_lst_a) == 0 or len(uniprot_lst_b) == 0:
            continue
        for prot_a in uniprot_lst_a:
            for prot_b in uniprot_lst_b:
                # These are undirected interactions so sort the ID
                # pair to avoid duplicates
                pair = [prot_a, prot_b]
                pair.sort()
                pairs.add(tuple(pair))
    return pairs


def print_interactions(pairs):
    for pair in pairs:
        print("\t".join(pair))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("USAGE: <script> RAW_BIOGRID")
    biogrid_raw = sys.argv[1]
    with open("data/uniprot-human-proteome.fasta") as h:
        proteome = read_proteome(h)
    with open(biogrid_raw) as h:
        pairs = get_interactions(h, proteome)
    print_interactions(pairs)
