library(zoo)
library(RColorBrewer)
library(viridis)
library(Hmisc)
library(parallel)
library(tidyverse)
## library(slider)
library(cowplot)
theme_set(theme_cowplot())

add_info <- function(scores, Y_kinases){
    scores$main.id <- unlist(lapply(strsplit(scores$kinase, split = "-"),
                                      function(split) split[1]))
    scores$y.kinase <- scores$main.id %in% Y_kinases
    scores$kinase.type <- rep(NA, nrow(scores))
    scores$kinase.type[which(scores$y.kinase)] <- "Y"
    scores$kinase.type[which(!scores$y.kinase)] <- "S/T"
    scores$kinase.type <- as.factor(scores$kinase.type)
    scores$y.kinase <- NULL
    return(scores)
}

prepare_scores <- function(score_file, good_pssms, Y_kinases){
    scores <- read_tsv(score_file) %>%
        filter(kinase %in% good_pssms)
    scores <- add_info(scores, Y_kinases)
    return(scores)
}

## Calculate per-site score ranks and pull in the known kinase(s) for
## each site
get_scores_bysite <- function(scores, method.name){
    if ("seq.win" %in% colnames(scores)){
        scores_bysite <- group_by(scores, site.id) %>%
            mutate(score.rank = rank(-score),
                   method = rep(method.name, n())) %>%
            mutate(int.id = paste(kinase, site.id)) %>%
            ungroup()
    }else{
        scores_bysite <- group_by(scores, site.id) %>%
            mutate(score.rank = rank(-score),
                   method = rep(method.name, n())) %>%
            mutate(int.id = paste(kinase, site.id)) %>%
            ungroup()
    }
    return(scores_bysite)
}

add_seq_type <- function(scores_bysite, y_regexp){
    y.site <- grepl(y_regexp, scores_bysite$seq.win)
    scores_bysite$seq.type <- rep(NA, nrow(scores_bysite))
    scores_bysite$seq.type[y.site] <- "Y"
    scores_bysite$seq.type[!y.site] <- "S/T"
    return(scores_bysite)
}

add_acceptors <- function(scores_bysite, y_regexp, s_regexp, t_regexp){
    y.site <- grepl(y_regexp, scores_bysite$seq.win)
    s.site <- grepl(s_regexp, scores_bysite$seq.win)
    t.site <- grepl(t_regexp, scores_bysite$seq.win)
    scores_bysite$acceptor <- rep(NA, nrow(scores_bysite))
    scores_bysite$acceptor[y.site] <- "Y"
    scores_bysite$acceptor[s.site] <- "S"
    scores_bysite$acceptor[t.site] <- "T"
    return(scores_bysite)
}

## micro-averaged multi-label evaluation
micro_ml <- function(scores_bysite_cut, with.ci = FALSE){
    if (with.ci){
        micro_ml_sum <- select(scores_bysite_cut, site.id, kinase, method, tp, fn, fp, tn) %>%
            group_by(method, kinase) %>%
            summarise(tp.sum = sum(tp),
                      fp.sum = sum(fp),
                      tn.sum = sum(tn),
                      fn.sum = sum(fn)) %>%
            ungroup(kinase) %>%
            summarise(micro.prec.value = ifelse(sum(tp.sum) + sum(fp.sum) == 0, 0,
                                          sum(tp.sum) / (sum(tp.sum) + sum(fp.sum))),
                      micro.rec.value = ifelse(sum(tp.sum) + sum(fn.sum) == 0, 0,
                                         sum(tp.sum) / (sum(tp.sum) + sum(fn.sum))),
                      micro.f1.value = ifelse(micro.prec.value == 0 && micro.rec.value == 0, 0,
                                        2 * ((micro.prec.value * micro.rec.value)/(micro.prec.value + micro.rec.value))),
                      micro.fb.value = ifelse(micro.prec.value == 0 && micro.rec.value == 0, 0,
                                        2 * ((micro.prec.value * micro.rec.value)/((0.5^2)*micro.prec.value + micro.rec.value))),
                      micro.prec.upper = NA,
                      micro.rec.upper = NA,
                      micro.f1.upper = NA,
                      micro.fb.upper = NA,
                      micro.prec.lower = NA,
                      micro.rec.lower = NA,
                      micro.f1.lower = NA,
                      micro.fb.lower = NA)
    }else{
        micro_ml_sum <- select(scores_bysite_cut, site.id, kinase, method, tp, fn, fp, tn) %>%
            group_by(method, kinase) %>%
            summarise(tp.sum = sum(tp),
                      fp.sum = sum(fp),
                      tn.sum = sum(tn),
                      fn.sum = sum(fn)) %>%
            ungroup(kinase) %>%
            summarise(micro.prec = ifelse(sum(tp.sum) + sum(fp.sum) == 0, 0,
                                          sum(tp.sum) / (sum(tp.sum) + sum(fp.sum))),
                      micro.rec = ifelse(sum(tp.sum) + sum(fn.sum) == 0, 0,
                                         sum(tp.sum) / (sum(tp.sum) + sum(fn.sum))),
                      micro.f1 = ifelse(micro.prec == 0 && micro.rec == 0, 0,
                                        2 * ((micro.prec * micro.rec)/(micro.prec + micro.rec))),
                      micro.fb = ifelse(micro.prec == 0 && micro.rec == 0, 0,
                                        2 * ((micro.prec * micro.rec)/((0.5^2)*micro.prec + micro.rec))))
    }
    return (micro_ml_sum)
}

## macro-averaged multi-label evaluation
macro_ml <- function(scores_bysite_cut, with.ci = FALSE){
    if (with.ci){
        macro_ml_sum <- select(scores_bysite_cut, site.id, kinase, method,
                               tp, fn, fp, tn) %>%
            group_by(method, kinase) %>%
            summarise(kinase.n = sum(tp) + sum(fn),
                      prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                                    sum(tp) / (sum(tp) + sum(fp))),
                      rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                                   sum(tp) / (sum(tp) + sum(fn))),
                      f1 = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/(prec + rec)),
                      fb = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/((0.5^2)*prec + rec)),
                      fdr = ifelse(sum(tp) + sum(fp) == 0, 0,
                                   sum(fp) / (sum(tp) + sum(fp))),
                      fpr = ifelse(sum(fp) + sum(tn) == 0, 0,
                                   sum(fp) / (sum(fp) + sum(tn))),
                      tpr = ifelse(sum(tp) + sum(fn) == 0, 0,
                                   sum(tp) / (sum(tp) + sum(fn))),
                      lr = if_else(fpr == 0, 0, tpr / fpr),
                      weight = kinase.n
                      ## weight = if_else(kinase.n == 0, 0, 1/kinase.n)
                      ) %>%
            ungroup(kinase) %>%
            summarise(macro.prec.value = mean(prec),
                      macro.rec.value = mean(rec),
                      macro.f1.value = mean(f1),
                      macro.fb.value = mean(fb),
                      macro.fdr.value = mean(fdr),
                      macro.fpr.value = mean(fpr),
                      macro.tpr.value = mean(tpr),
                      macro.lr.value = mean(lr),
                      macro.wprec.value = weighted.mean(prec, w = weight),
                      macro.wrec.value = weighted.mean(rec, w = weight),
                      macro.wf1.value = weighted.mean(f1, w = weight),
                      macro.wfb.value = weighted.mean(fb, w = weight),
                      macro.wtpr.value = weighted.mean(tpr, w = weight),
                      macro.wfdr.value = weighted.mean(fdr, w = weight),
                      macro.wfpr.value = weighted.mean(fpr, w = weight),
                      macro.wlr.value = weighted.mean(lr, w = weight),
                      macro.prec.ci = qnorm(0.975)*sd(prec)/sqrt(n()),
                      macro.rec.ci = qnorm(0.975)*sd(rec)/sqrt(n()),
                      macro.f1.ci = qnorm(0.975)*sd(f1)/sqrt(n()),
                      macro.fb.ci = qnorm(0.975)*sd(fb)/sqrt(n()),
                      macro.fdr.ci = qnorm(0.975)*sd(fdr)/sqrt(n()),
                      macro.fpr.ci = qnorm(0.975)*sd(fpr)/sqrt(n()),
                      macro.tpr.ci = qnorm(0.975)*sd(tpr)/sqrt(n()),
                      macro.lr.ci = qnorm(0.975)*sd(lr)/sqrt(n()),
                      macro.wprec.ci = qnorm(0.975)*sqrt(wtd.var(prec, w = kinase.n))/sqrt(n()),
                      macro.wrec.ci = qnorm(0.975)*sqrt(wtd.var(rec, w = kinase.n))/sqrt(n()),
                      macro.wf1.ci = qnorm(0.975)*sqrt(wtd.var(f1, w = kinase.n))/sqrt(n()),
                      macro.wfb.ci = qnorm(0.975)*sqrt(wtd.var(fb, w = kinase.n))/sqrt(n()),
                      macro.wfdr.ci = qnorm(0.975)*sqrt(wtd.var(fdr, w = kinase.n))/sqrt(n()),
                      macro.wfpr.ci = qnorm(0.975)*sqrt(wtd.var(fpr, w = kinase.n))/sqrt(n()),
                      macro.wtpr.ci = qnorm(0.975)*sqrt(wtd.var(tpr, w = kinase.n))/sqrt(n()),
                      macro.wlr.ci = qnorm(0.975)*sqrt(wtd.var(lr, w = kinase.n))/sqrt(n()),
                      macro.prec.upper = macro.prec.value + macro.prec.ci,
                      macro.rec.upper = macro.rec.value + macro.rec.ci,
                      macro.f1.upper = macro.f1.value + macro.f1.ci,
                      macro.fb.upper = macro.fb.value + macro.fb.ci,
                      macro.wprec.upper = macro.wprec.value + macro.wprec.ci,
                      macro.wrec.upper = macro.wrec.value + macro.wrec.ci,
                      macro.wf1.upper = macro.wf1.value + macro.wf1.ci,
                      macro.wfb.upper = macro.wfb.value + macro.wfb.ci,
                      macro.fdr.upper = macro.fdr.value + macro.fdr.ci,
                      macro.fpr.upper = macro.fpr.value + macro.fpr.ci,
                      macro.tpr.upper = macro.tpr.value + macro.tpr.ci,
                      macro.lr.upper = macro.lr.value + macro.lr.ci,
                      macro.wtpr.upper = macro.wtpr.value + macro.wtpr.ci,
                      macro.wfpr.upper = macro.wfpr.value + macro.wfpr.ci,
                      macro.wfdr.upper = macro.wfdr.value + macro.wfdr.ci,
                      macro.wlr.upper = macro.wlr.value + macro.wlr.ci,
                      macro.prec.lower = macro.prec.value - macro.prec.ci,
                      macro.rec.lower = macro.rec.value - macro.rec.ci,
                      macro.f1.lower = macro.f1.value - macro.f1.ci,
                      macro.fb.lower = macro.fb.value - macro.fb.ci,
                      macro.wprec.lower = macro.wprec.value - macro.wprec.ci,
                      macro.wrec.lower = macro.wrec.value - macro.wrec.ci,
                      macro.wf1.lower = macro.wf1.value - macro.wf1.ci,
                      macro.wfb.lower = macro.wfb.value - macro.wfb.ci,
                      macro.fdr.lower = macro.fdr.value - macro.fdr.ci,
                      macro.fpr.lower = macro.fpr.value - macro.fpr.ci,
                      macro.tpr.lower = macro.tpr.value - macro.tpr.ci,
                      macro.lr.lower = macro.lr.value - macro.lr.ci,
                      macro.wtpr.lower = macro.wtpr.value - macro.wtpr.ci,
                      macro.wfpr.lower = macro.wfpr.value - macro.wfpr.ci,
                      macro.wfdr.lower = macro.wfdr.value - macro.wfdr.ci,
                      macro.wlr.lower = macro.wlr.value - macro.wlr.ci) %>%
            select(-macro.prec.ci, -macro.rec.ci,
                   -macro.f1.ci, -macro.fb.ci,
                   -macro.fdr.ci, -macro.wprec.ci,
                   -macro.wrec.ci, -macro.wf1.ci, -macro.wfb.ci, -macro.wtpr.ci,
                   -macro.wfpr.ci, -macro.wfdr.ci, -macro.wlr.ci)
    }else{
        macro_ml_sum <- select(scores_bysite_cut, site.id, kinase, method,
                               tp, fn, fp, tn) %>%
            group_by(method, kinase) %>%
            summarise(kinase.n = sum(tp) + sum(fn),
                      prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                                    sum(tp) / (sum(tp) + sum(fp))),
                      rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                                   sum(tp) / (sum(tp) + sum(fn))),
                      f1 = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/(prec + rec)),
                      fb = if_else(prec == 0 && rec == 0, 0,
                                   2 * (prec * rec)/((0.5^2)*prec + rec)),
                      weight = kinase.n) %>%
            ungroup(kinase) %>%
            summarise(macro.prec = mean(prec),
                      macro.rec = mean(rec),
                      macro.f1 = mean(f1),
                      macro.fb = mean(fb),
                      macro.wprec = weighted.mean(prec, w = weight),
                      macro.wrec = weighted.mean(rec, w = weight),
                      macro.wf1 = weighted.mean(f1, w = weight),
                      macro.wfb = weighted.mean(fb, w = weight))
    }
    return (macro_ml_sum)
}

## sample-averaged multi-label evaluation
sample_ml <- function(scores_bysite_cut){
    samp_ml_sum <- select(scores_bysite_cut, site.id, kinase, method,
                            tp, fn, fp, tn) %>%
        group_by(method, site.id) %>%
        summarise(prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                                sum(tp) / (sum(tp) + sum(fp))),
                  rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                               sum(tp) / (sum(tp) + sum(fn))),
                  f1 = if_else(prec == 0 && rec == 0, 0,
                               2 * (prec * rec)/(prec + rec)),
                  fb = if_else(prec == 0 && rec == 0, 0,
                               2 * (prec * rec)/((0.5^2)*prec + rec))) %>%
        ungroup(site.id) %>%
        summarise(samp.prec = mean(prec),
                  samp.rec = mean(rec),
                  samp.f1 = mean(f1),
                  samp.fb = mean(fb))
    return (samp_ml_sum)
}

get_cutoffs <- function(scores, num_cutoffs){
    num_vals_tbl <- mutate(scores, int.id = paste(kinase, site.id)) %>%
        drop_na() %>%
        select(method, int.id) %>% group_by(method) %>% summarise(n = n())
    all_cutoffs <- NULL
    for (meth in levels(scores$method)){
        num_vals <- filter(num_vals_tbl, method == meth) %>% pull(n)
        cutoff_is <- floor(seq.int(1, num_vals, length.out = num_cutoffs))
        meth_cuts <- filter(scores, method == meth) %>%
            arrange(score) %>%
            slice(cutoff_is) %>%
            select(method, score) %>%
            rename(cutoff = score)
        if (is.null(all_cutoffs)){
            all_cutoffs <- meth_cuts
        }else{
            all_cutoffs <- bind_rows(all_cutoffs, meth_cuts)
        }
    }
    return(all_cutoffs)
}

args <- commandArgs(TRUE)
win_len <- as.integer(args[1])

true_pos_file <- "data/protmapper-kinase-substrates.tsv"
valset_file <- "data/pmapperval-kinase-substrates.tsv"

half_winlen <- floor(win_len/2)
s_regexp <- paste0(".{", half_winlen, "}[S].{", half_winlen, "}")
t_regexp <- paste0(".{", half_winlen, "}[T].{", half_winlen, "}")
y_regexp <- paste0(".{", half_winlen, "}[Y].{", half_winlen, "}")

pssm_info <- read_tsv("out/pssm-info.tsv")
good_pssms <- unique(filter(pssm_info, seq.n >= 20)$kinase)

## Load up the test set and related info
true_pos <- read_tsv(true_pos_file,
                     col_names = c("true.kinase", "site.id", "seq.win"))
valset <- read_tsv(valset_file, col_names = c("kinase", "site.id", "seq.win"))
valset_ints <- paste(valset$kinase, valset$site.id)
true_pos_sum <- filter(true_pos,
                       true.kinase %in% valset$kinase,
                       site.id %in% valset$site.id) %>%
    group_by(site.id, seq.win) %>%
    summarise(true.kinases = paste(true.kinase, collapse = " "))
valset_sum <- select(valset, site.id, seq.win) %>%
    distinct() %>%
    left_join(true_pos_sum) %>%
    mutate(true.kinases = if_else(is.na(true.kinases), "", true.kinases))

Y_kinases <- readLines("data/kinases-Y.txt")
ST_kinases <- readLines("data/kinases-ST.txt")

## Start loading all the various data sets
pos_job <- mcparallel(prepare_scores("out/pmapperval-norm/pmapperval-scores-pos.tsv",
                                     good_pssms, Y_kinases) %>%
                      get_scores_bysite("PSSM phospho."))
bayes_job <- mcparallel(prepare_scores("out/pmapperval/pmapperval-scores-bayes.tsv",
                                       good_pssms, Y_kinases) %>%
                        get_scores_bysite("Naive Bayes"))
bayesp_job <- mcparallel(prepare_scores("out/pmapperval/pmapperval-scores-bayes-plus.tsv",
                                        good_pssms, Y_kinases) %>%
                         get_scores_bysite("Naive Bayes+"))
randforest_job <- mcparallel(prepare_scores("out/pmapperval/pmapperval-scores-randforest.tsv",
                                        good_pssms, Y_kinases) %>%
                         get_scores_bysite("IV-KAPhE"))
randforest_expand_job <- mcparallel(prepare_scores("out/pmapperval/pmapperval-scores-randforest-expand.tsv",
                                        good_pssms, Y_kinases) %>%
                         get_scores_bysite("IV-KAPhE"))
scores_data <- mccollect(list(pos_job, bayes_job, bayesp_job,
                              randforest_job, randforest_expand_job))
pos_bysite_full <- scores_data[[1]]
bayes_bysite_full <- scores_data[[2]]
bayesp_bysite_full <- scores_data[[3]]
randforest <- scores_data[[4]]
randforest_expand <- scores_data[[5]]

pos_bysite <- filter(pos_bysite_full, int.id %in% valset_ints)
bayes_bysite <- filter(bayes_bysite_full, int.id %in% valset_ints)
bayesp_bysite <- filter(bayesp_bysite_full, int.id %in% valset_ints)

pos_bysite_expand <- filter(pos_bysite_full, kinase %in% valset$kinase,
                             site.id %in% true_pos$site.id)
bayes_bysite_expand <- filter(bayes_bysite_full, kinase %in% valset$kinase,
                             site.id %in% true_pos$site.id)
bayesp_bysite_expand <- filter(bayesp_bysite_full, kinase %in% valset$kinase,
                             site.id %in% true_pos$site.id)

## For the full "expansion" only consider the kinases that have
## assignments in the basic validation set
all_ints <- pull(bayesp_bysite_expand, int.id)
## expand_set <- select(bayesp_bysite_expand, kinase, site.id, seq.win)

expand_set <- select(valset, kinase, site.id) %>%
    complete(kinase, site.id) %>%
    left_join(select(valset, site.id, seq.win), by = "site.id") %>%
    distinct()

## Load external predictions: GPS 5.0, NetworKIN 3.0, and LinkPhinder.
## They all have different coverage and various quirks that require
## special handling.

## GPS 5.0
gps_preds <- read_tsv(paste0("out/pmapperval/pmapperval-assignments-gps.tsv")) %>%
    mutate(int.id = paste(kinase, site.id))
id_map <- read_tsv("data/kinase-id-map.tsv", col_names = c("uniprot", "name", "gene"))
gps_kins_all <- read_tsv("data/gps-kinases.txt", col_names = "kinase") %>%
    left_join(id_map, by = c("kinase" = "gene")) %>%
    pull(uniprot)
gps <- mutate(valset, int.id = paste(kinase, site.id),
              score = as.numeric(int.id %in% gps_preds$int.id)) %>%
    filter(kinase %in% gps_kins_all) %>%
    select(-int.id) %>%
    add_info(Y_kinases) %>%
    get_scores_bysite("GPS 5.0") %>%
    distinct()

gps_expand <- mutate(expand_set, int.id = paste(kinase, site.id),
              score = as.numeric(int.id %in% gps_preds$int.id)) %>%
    filter(kinase %in% gps_kins_all) %>%
    select(-int.id) %>%
    add_info(Y_kinases) %>%
    get_scores_bysite("GPS 5.0") %>%
    distinct()

## NetworKIN 3.0.  Due to id-mapping, there are some duplicates so
## take the max prediction.  Also, NetworKIN may have failed to find
## some sites, so set those scores to 0.
networkin <- read_tsv("out/pmapperval/pmapperval-scores-networkin.tsv") %>%
    group_by(kinase, site.id) %>%
    summarise(score = max(score)) %>%
    ungroup() %>% ungroup()
networkin <- right_join(networkin, filter(valset, kinase %in% networkin$kinase)) %>%
    ## mutate(score = if_else(is.na(score), 0, score)) %>%
    add_info(Y_kinases) %>%
    get_scores_bysite("NetworKIN 3.0")

networkin_expand <- read_tsv("out/pmapperval/pmapperval-scores-networkin-expand.tsv") %>%
    group_by(kinase, site.id) %>%
    summarise(score = max(score)) %>%
    ungroup() %>% ungroup()
networkin_expand <- right_join(networkin_expand, filter(expand_set, kinase %in% networkin_expand$kinase)) %>%
    ## mutate(score = if_else(is.na(score), 0, score)) %>%
    add_info(Y_kinases) %>%
    get_scores_bysite("NetworKIN 3.0") %>%
    distinct()

## LinkPhinder
lph_dat <- read_tsv("data/linkphinder.tsv")
lph <- get_scores_bysite(lph_dat, "LinkPhinder") %>%
    select(-score.rank) %>%
    filter(int.id %in% valset_ints)
## find all kinases in lph that have no positive cases
lph_good_kins1 <- group_by(lph, kinase) %>%
    summarise(foo = length(intersect(int.id, mutate(true_pos, int.id = paste(true.kinase, site.id))$int.id))) %>%
    filter(foo > 0) %>% pull(kinase)
## find all kinases in lph that have no negative cases
neg_cases <- setdiff(valset_ints, mutate(true_pos, int.id = paste(true.kinase, site.id))$int.id)
lph_good_kins2 <- group_by(lph, kinase) %>%
    summarise(foo = length(intersect(int.id, neg_cases))) %>%
    filter(foo > 0) %>% pull(kinase)
lph_good_kins <- intersect(lph_good_kins1, lph_good_kins2)
lph <- filter(lph, kinase %in% lph_good_kins)
lph$main.id <- unlist(lapply(strsplit(lph$kinase, split = "-"),
                                        function(split) split[1]))
lph_expand <- get_scores_bysite(lph_dat, "LinkPhinder") %>%
    select(-score.rank) %>%
    inner_join(expand_set) %>%
    add_info(Y_kinases)

## Find the subset of kinase-site pairs in the test set that is
## covered by all the methods
shared_ints <- intersect(pos_bysite$int.id, bayesp_bysite$int.id)
shared_ints <- intersect(shared_ints, gps$int.id)
shared_ints <- intersect(shared_ints, networkin$int.id)
shared_ints <- intersect(shared_ints, randforest$int.id)
shared_ints <- intersect(shared_ints, lph$int.id)

shared_exp_ints <- intersect(pos_bysite_expand$int.id, bayesp_bysite_expand$int.id)
shared_exp_ints <- intersect(shared_exp_ints, gps_expand$int.id)
shared_exp_ints <- intersect(shared_exp_ints, networkin_expand$int.id)
shared_exp_ints <- intersect(shared_exp_ints, randforest_expand$int.id)
shared_exp_ints <- intersect(shared_exp_ints, lph_expand$int.id)

## Main data set: all methods together with their full coverage of the
## test set
scores_bysite <- bind_rows(pos_bysite, bayesp_bysite,
                           randforest, gps, networkin) %>%
    select(-kinase.type, -score.rank, -seq.win) %>%
    bind_rows(lph) %>%
    left_join(select(valset_sum, site.id, true.kinases)) %>%
    select(-int.id)
scores_bysite$method <- factor(scores_bysite$method,
                            levels = c("NetworKIN 3.0",
                                       "GPS 5.0",
                                       "LinkPhinder",
                                       "PSSM phospho.",
                                       "Naive Bayes+",
                                       "IV-KAPhE"))
## Main data subset: all methods together, restricted to the subset of
## the test data that they all make predictions for
scores_bysite_shared <- bind_rows(pos_bysite, bayesp_bysite,
                           randforest, gps, networkin) %>%
    select(-kinase.type, -score.rank, -seq.win) %>%
    bind_rows(lph) %>%
    filter(int.id %in% shared_ints) %>%
    left_join(select(valset_sum, site.id, true.kinases)) %>%
    select(-int.id)
scores_bysite_shared$method <- factor(scores_bysite_shared$method,
                                      levels = c("NetworKIN 3.0",
                                                 "GPS 5.0",
                                                 "LinkPhinder",
                                                 "PSSM phospho.",
                                                 "Naive Bayes+",
                                                 "IV-KAPhE"))
## "Expanded" data: all methods together with all-vs-all predictions
## between kinases and sites with at least one known kinase
scores_bysite_expand <- bind_rows(pos_bysite_expand, bayesp_bysite_expand,
                           randforest_expand, gps_expand, networkin_expand, lph_expand) %>%
    add_seq_type(y_regexp) %>%
    select(-score.rank, -seq.win) %>%
    left_join(select(valset_sum, site.id, true.kinases)) %>%
    select(-int.id)
scores_bysite_expand$method <- factor(scores_bysite_expand$method,
                            levels = c("NetworKIN 3.0",
                                       "GPS 5.0",
                                       "LinkPhinder",
                                       "PSSM phospho.",
                                       "Naive Bayes+",
                                       "IV-KAPhE"))

greplv <- Vectorize(grepl)
## Add boolean column for whether the site is a known substrate of the kinase
scores_bysite_filt <- mutate(scores_bysite,
                             is.substrate = greplv(kinase, true.kinases) |
                                 greplv(main.id, true.kinases))
scores_bysite_shared_filt <- mutate(scores_bysite_shared,
                             is.substrate = greplv(kinase, true.kinases) |
                                 greplv(main.id, true.kinases))
scores_bysite_expand_filt <- mutate(scores_bysite_expand,
                                    is.substrate = greplv(kinase, true.kinases) |
                                        greplv(main.id, true.kinases))


## Calculate kinase coverage. GPS needs manual calculation because of
## how its dataset is formed
valset_nkins <- length(unique(valset$kinase))
gps_kins <- intersect(gps_kins_all, valset$kinase)
gps_nkins <- length(gps_kins)
coverage <- group_by(scores_bysite, method) %>%
    select(method, kinase) %>%
    distinct() %>%
    summarise(n = n(),
              coverage = n() / valset_nkins) %>%
    mutate(n = if_else(method == "GPS 5.0", gps_nkins, n),
           coverage = if_else(method == "GPS 5.0", gps_nkins / valset_nkins,
                              coverage))
coverage$method <- factor(coverage$method,
                          levels = rev(c("NetworKIN 3.0",
                                         "GPS 5.0",
                                         "LinkPhinder",
                                         "PSSM phospho.",
                                         "Naive Bayes+",
                                         "IV-KAPhE")))
coverage_p <- ggplot(coverage, aes(x = method, y = 100*coverage, fill = method)) +
    geom_col() +
    geom_text(aes(y = 24, label = paste0("n", "=", n))) +
    xlab("model") +
    ylab("test set kinase coverage (%)") +
    ylim(0, 100) + 
    scale_fill_brewer("model", palette = "Dark2", guide = "none", direction = -1) +
    coord_flip()

## Get the performance at each method's nominal/ideal cutoff.
scores_bysite_cut <- mutate(scores_bysite_filt,
                            pred.substrate = (method %in% c("PSSM phospho.",
                                                            "PSSM proteome bg.") &
                                              !is.na(score) & score >= 0.75) |
                                (method %in% c("Naive Bayes", "Naive Bayes+", "IV-KAPhE") & !is.na(score) & score >= 0.5) |
                                (method == "NetworKIN 3.0" & !is.na(score) & score >= 1.0) |
                                (method == "GPS 5.0" & !is.na(score) & score >= 0.5) |
                                (method == "LinkPhinder" & !is.na(score) & score >= 0.672)) %>%
    select(kinase, site.id, method, pred.substrate, is.substrate) %>%
    mutate(tp = as.integer(pred.substrate & is.substrate),
           fn = as.integer(!pred.substrate & is.substrate),
           fp = as.integer(pred.substrate & !is.substrate),
           tn = as.integer(!pred.substrate & !is.substrate))
micro_ml_sum <- micro_ml(scores_bysite_cut)
macro_ml_sum <- macro_ml(scores_bysite_cut)
samp_ml_sum <- sample_ml(scores_bysite_cut)
multilabel_sum_final <- left_join(micro_ml_sum, macro_ml_sum,
                                  by = "method") %>%
    left_join(samp_ml_sum)
print(as.data.frame(multilabel_sum_final))
write_tsv(multilabel_sum_final, paste0("out/pmapperval-multilabel-performance.tsv"))

## Look at the distribution of F1 scores per kinase per method, with
## an eye towards the number of substrates for each kinase
macro_ml_bykin <- select(scores_bysite_cut, site.id, kinase, method,
                       tp, fn, fp, tn) %>%
    group_by(method, kinase) %>%
    summarise(kinase.n = sum(tp) + sum(fn),
              prec = ifelse(sum(tp) + sum(fp) == 0, 0,
                            sum(tp) / (sum(tp) + sum(fp))),
              rec = ifelse(sum(tp) + sum(fn) == 0, 0,
                           sum(tp) / (sum(tp) + sum(fn))),
              f1 = if_else(prec == 0 && rec == 0, 0,
                           2 * (prec * rec)/(prec + rec)),
              fb = if_else(prec == 0 && rec == 0, 0,
                           2 * (prec * rec)/((0.5^2)*prec + rec)),
              weight = kinase.n) %>%
    ungroup(kinase) %>%
    mutate(y.kinase = kinase %in% Y_kinases,
           kinase.type = if_else(y.kinase, "Y", "S/T"))
macro_ml_bykin$method <- factor(macro_ml_bykin$method,
                                levels = c("NetworKIN 3.0",
                                               "GPS 5.0",
                                               "LinkPhinder",
                                               "PSSM phospho.",
                                               "Naive Bayes+",
                                               "IV-KAPhE"),
                                labels = c("NetworKIN\n3.0",
                                               "GPS 5.0",
                                               "LinkPhinder",
                                               "PSSM\nphospho.",
                                               "Naive\nBayes+",
                                               "IV-KAPhE"))
f1_bykin_p <- ggplot(macro_ml_bykin, aes(x = method, y = f1,
                                         color = kinase.type, size = log10(kinase.n))) +
    geom_violin(position = position_dodge(width = 0.8), size = 1) +
    geom_point(position = position_jitterdodge(dodge.width = 0.8), alpha = 0.5) +
    scale_color_brewer("kinase type", palette = "Dark2") +
    scale_size_identity("log10(#sites)", guide = "legend", breaks = c(0, 1, 2)) +
    xlab("model") +
    ylab("kinase F1 score")##  +
    ## theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1))##  +
    ## coord_flip()

## Do multi-label evaluation of the shared subset of the test data.
## NetworKIN is on a different scale than the other scores, so cutoffs
## have to be handled a bit abstractly.
multilabel_eval <- NULL
num_cutoffs <- 100
## num_vals <- length(shared_ints)
all_cutoffs <- get_cutoffs(scores_bysite_filt, num_cutoffs)
pb <- txtProgressBar(max = num_cutoffs, style = 3)
i <- 0
suppressMessages(for (cutoff_i in 1:num_cutoffs){
    setTxtProgressBar(pb, i)
    i <- i+1
    cutoffs <- group_by(all_cutoffs, method) %>%
        slice(cutoff_i) %>%
        mutate(cutoff = if_else(method == "GPS 5.0", 0.5, cutoff))
    scores_bysite_cut <- left_join(scores_bysite_filt, cutoffs) %>%
        mutate(pred.substrate = !is.na(score) & score >= cutoff) %>%
        select(kinase, site.id, method, cutoff, pred.substrate, is.substrate) %>%
        mutate(tp = as.integer(pred.substrate & is.substrate),
               fn = as.integer(!pred.substrate & is.substrate),
               fp = as.integer(pred.substrate & !is.substrate),
               tn = as.integer(!pred.substrate & !is.substrate))
    micro_ml_sum <- micro_ml(scores_bysite_cut, TRUE)
    macro_ml_sum <- macro_ml(scores_bysite_cut, TRUE)
    multilabel_sum <- left_join(micro_ml_sum, macro_ml_sum, by = "method") %>%
        mutate(cutoff_i = rep(cutoff_i, n())) %>%
        left_join(cutoffs, by = "method") %>%
        ## pivot_longer(cols=micro.prec:macro.wfb, names_to = "metric") %>%
        pivot_longer(cols=micro.prec.value:macro.wlr.lower,
                     names_to = c("metric", ".value"),
                     names_pattern = "(m[ai]cro\\.[[:alnum:]]+)\\.([[:alpha:]]+)$")
    if (is.null(multilabel_eval)){
        multilabel_eval <- multilabel_sum
    }else{
        multilabel_eval <- bind_rows(multilabel_eval, multilabel_sum)
    }
})
close(pb)

## Now do all the hard work to build up the various plots
multilabel <- separate(multilabel_eval, metric, into = c("averaging", "metric"))
multilabel$metric <- factor(multilabel$metric,
                            levels = c("prec", "rec", "f1", "fb", "fdr", "tpr", "fpr", "lr",
                                       "wprec", "wrec", "wf1", "wfb", "wfdr", "wtpr", "wlr"),
                            labels = c("precision", "recall", "F1", "Fbeta",
                                       "FDR", "TPR", "FPR", "LR+",
                                       "weighted precision", "weighted recall",
                                       "weighted F1", "weighted Fbeta",
                                       "weighted FDR", "weighted TPR",
                                       "weighted LR+"))
multilabel$averaging <- factor(multilabel$averaging,
                                    levels = c("micro", "macro"),
                                    labels = c("micro-averaging", "macro-averaging"))
multilabel$method <- factor(multilabel$method,
                            levels = c("NetworKIN 3.0", "GPS 5.0", "LinkPhinder",
                                       "PSSM phospho.", "Naive Bayes+",
                                       "IV-KAPhE"))

best_co_a <- filter(multilabel, averaging == "macro-averaging", metric == "F1") %>%
    group_by(method, metric) %>%
    arrange(-cutoff) %>%
    slice_max(value, n = 1, with_ties = FALSE) %>%
    select(-upper, -lower, -ci) %>%
    write_tsv(paste0("out/pmapperval-multilabel-best-cutoffs.tsv"))
best_lph_co <- filter(best_co_a, method == "LinkPhinder") %>% pull(value)
best_co <- filter(multilabel, averaging == "macro-averaging", metric == "F1",
                              ((method == "PSSM phospho." & cutoff < 0.75) |
                               (method == "Naive Bayes+" & cutoff < 0.5) |
                               (method == "IV-KAPhE" & cutoff < 0.5) |
                               (method == "LinkPhinder" & cutoff < 0.672) |
                               (method == "NetworKIN 3.0" & cutoff  < 1) |
                               (method %in% c("GPS 5.0") & cutoff_i == 1))) %>%
    select(-upper, -lower, -ci) %>%
    group_by(method) %>%
    slice_max(cutoff, n = 1, with_ties = FALSE)

best_co_2 <- best_co
best_co_2$cutoff[best_co$method %in% c("GPS 5.0")] <- NA
best_co_2$cutoff_i[best_co$method %in% c("GPS 5.0")] <- NA

ml_plot <- filter(multilabel, averaging == "macro-averaging", metric == "F1") %>%
    ggplot(aes(x = cutoff_i, y = value)) +
    geom_ribbon(aes(ymin = lower, ymax = upper, fill = method), alpha = 0.2) +
    geom_line(aes(color = method)) +
    geom_point(data = best_co_2, aes(color = method)) +
    scale_fill_brewer(guide = NULL, palette = "Dark2") +
    scale_color_brewer(guide = NULL, palette = "Dark2") +
    scale_x_continuous("cutoff", breaks = c(0, num_cutoffs), labels = c("low", "high")) +
    scale_y_continuous("macro-F1", breaks = c(0, 0.5, 1.0), limits = c(0, 1))
save_plot(paste0("img/pmapperval-ml-eval.pdf"), ml_plot)

precs <- filter(multilabel, averaging == "macro-averaging", metric == "precision")
best_precs <- left_join(best_co, precs, by = c("method", "averaging", "cutoff"),
                        suffix = c(".F1", ".prec")) %>%
    select(method, cutoff, value.prec, upper, lower)
recs <- filter(multilabel, averaging == "macro-averaging", metric == "recall")
best_recs <- left_join(best_co, recs, by = c("method", "averaging", "cutoff"),
          suffix = c(".F1", ".rec")) %>%
    select(method, cutoff, value.rec, upper, lower)
best_pr <- inner_join(best_precs, best_recs, by = c("method", "cutoff"), suffix = c(".prec", ".rec"))
best_pr$cutoff[best_pr$method %in% c("GPS 5.0")] <- NA
best_pr <- unique(best_pr)

pr_plot <- filter(multilabel, averaging == "macro-averaging",
                  metric %in% c("precision", "recall")) %>%
    arrange(cutoff) %>%
    select(-lower, -upper) %>%
    pivot_wider(names_from = metric, values_from = value) %>%
    ggplot(aes(y = precision, x = recall, color = method)) +
    scale_color_brewer("model", palette = "Dark2") +
    geom_path() +
    geom_point(data = best_pr, aes(y = value.prec, x = value.rec)) +
    geom_errorbar(data = best_pr,
                  aes(y = value.prec, x = value.rec, xmin = lower.rec, xmax = upper.rec),
                  width = 0.02) +
    geom_errorbar(data = best_pr,
                  aes(y = value.prec, x = value.rec, ymin = lower.prec, ymax = upper.prec),
                  width = 0.02) +
    scale_y_continuous("macro-precision", breaks = c(0, 0.5, 1), limits = c(0, 1)) +
    scale_x_continuous("macro-recall", breaks = c(0, 0.5, 1), limits = c(0, 1))##   +
    ## theme(legend.position = c(0.5, 0.35))
save_plot("img/pmapperval-ml-prec-rec.pdf", pr_plot)

tprs <- filter(multilabel, averaging == "macro-averaging", metric == "TPR")
best_tprs <- left_join(best_co, tprs, by = c("method", "averaging", "cutoff"),
                        suffix = c(".F1", ".tpr")) %>%
    select(method, cutoff, value.tpr, upper, lower)
fprs <- filter(multilabel, averaging == "macro-averaging", metric == "FPR")
best_fprs <- left_join(best_co, fprs, by = c("method", "averaging", "cutoff"),
          suffix = c(".F1", ".fpr")) %>%
    select(method, cutoff, value.fpr, upper, lower)
best_roc <- inner_join(best_tprs, best_fprs, by = c("method", "cutoff"), suffix = c(".tpr", ".fpr"))
best_roc$cutoff[best_roc$method %in% c("GPS 5.0")] <- NA
best_roc <- unique(best_roc)

roc_plot <- filter(multilabel, averaging == "macro-averaging",
                   metric %in% c("TPR", "FPR")) %>%
    arrange(cutoff) %>%
    select(-lower, -upper, -ci) %>%
    pivot_wider(names_from = metric, values_from = value) %>%
    ggplot(aes(y = TPR, x = FPR, color = method)) +
    scale_color_brewer("model", palette = "Dark2", guide = NULL) +
    geom_abline(intercept = 0, slope = 1, color = "gray", lty = 2) +
    geom_path() +
    geom_point(data = best_roc, aes(y = value.tpr, x = value.fpr)) +
    geom_errorbar(data = best_roc,
                  aes(y = value.tpr, x = value.fpr, xmin = lower.fpr, xmax = upper.fpr),
                  width = 0.02) +
    geom_errorbar(data = best_roc,
                  aes(y = value.tpr, x = value.fpr, ymin = lower.tpr, ymax = upper.tpr),
                  width = 0.02) +
    scale_y_continuous("macro-TPR", breaks = c(0, 0.5, 1), limits = c(0, 1)) +
    scale_x_continuous("macro-FPR", breaks = c(0, 0.5, 1), limits = c(0, 1))##   +
    ## theme(legend.position = c(0.5, 0.35))
save_plot("img/pmapperval-ml-roc.pdf", roc_plot)

auc <- filter(multilabel, averaging == "macro-averaging",
                   metric %in% c("TPR", "FPR")) %>%
    arrange(cutoff) %>%
    select(-lower, -upper, -ci) %>%
    pivot_wider(names_from = metric, values_from = value) %>%
    group_by(method, averaging) %>%
    arrange(-cutoff_i) %>%
    summarize(auc = sum(diff(FPR)*rollmean(TPR, 2)))
auc

scores_bysite_expand_cut1 <- mutate(scores_bysite_expand_filt,
                            pred.substrate = (method %in% c("PSSM phospho.",
                                                            "PSSM proteome bg.") &
                                              !is.na(score) & score >= 0.75) |
                                (method %in% c("Naive Bayes", "Naive Bayes+", "IV-KAPhE") &
                                 !is.na(score) & score >= 0.5) |
                                (method %in% c("NetworKIN 3.0") & !is.na(score) & score >= 1.0) |
                                (method %in% c("GPS 5.0")
                                    & !is.na(score) & score > 0) |
                                (method == "LinkPhinder" & !is.na(score) & score >= 0.672)) %>%
    select(kinase, site.id, method, pred.substrate, is.substrate, kinase.type, seq.type) %>%
    mutate(tp = as.integer(pred.substrate & is.substrate),
           fn = as.integer(!pred.substrate & is.substrate),
           fp = as.integer(pred.substrate & !is.substrate),
           tn = as.integer(!pred.substrate & !is.substrate))

multilabel_expand_eval <- NULL
all_cutoffs <- get_cutoffs(scores_bysite_expand_filt, num_cutoffs)
pb <- txtProgressBar(max = num_cutoffs, style = 3)
i <- 0
suppressMessages(for (cutoff_i in 1:num_cutoffs){
    setTxtProgressBar(pb, i)
    i <- i+1
    cutoffs <- group_by(all_cutoffs, method) %>%
        slice(cutoff_i) %>%
        mutate(cutoff = if_else(method == "GPS 5.0", 0.5, cutoff))
    scores_bysite_expand_cut <- left_join(scores_bysite_expand_filt, cutoffs) %>%
        mutate(pred.substrate = !is.na(score) & score >= cutoff) %>%
        select(kinase, site.id, method, cutoff, pred.substrate, is.substrate) %>%
        mutate(tp = as.integer(pred.substrate & is.substrate),
               fn = as.integer(!pred.substrate & is.substrate),
               fp = as.integer(pred.substrate & !is.substrate),
               tn = as.integer(!pred.substrate & !is.substrate))
    macro_ml_sum <- macro_ml(scores_bysite_expand_cut, TRUE) %>%
        mutate(cutoff_i = rep(cutoff_i, n())) %>%
        left_join(cutoffs, by = "method") %>%
        pivot_longer(cols=macro.prec.value:macro.wlr.lower,
                     names_to = c("metric", ".value"),
                     names_pattern = "(m[ai]cro\\.[[:alnum:]]+)\\.([[:alpha:]]+)$")
    if (is.null(multilabel_expand_eval)){
        multilabel_expand_eval <- macro_ml_sum
    }else{
        multilabel_expand_eval <- bind_rows(multilabel_expand_eval, macro_ml_sum)
    }
})
close(pb)

multilabel_expand <- separate(multilabel_expand_eval, metric, into = c("averaging", "metric"))
multilabel_expand$metric <- factor(multilabel_expand$metric,
                            levels = c("prec", "rec", "f1", "fb", "fdr", "fpr", "tpr", "lr",
                                       "wprec", "wrec", "wf1", "wfb", "wfdr", "wtpr", "wlr"),
                            labels = c("precision", "recall", "F1", "Fbeta",
                                       "FDR", "FPR", "TPR", "LR+",
                                       "weighted precision", "weighted recall",
                                       "weighted F1", "weighted Fbeta",
                                       "weighted FDR", "weighted TPR",
                                       "weighted LR+"))
multilabel_expand$averaging <- factor(multilabel_expand$averaging,
                                    levels = c("micro", "macro"),
                                    labels = c("micro-averaging", "macro-averaging"))
multilabel_expand$method <- factor(multilabel_expand$method,
                            levels = c("NetworKIN 3.0", "GPS 5.0", "LinkPhinder",
                                       "PSSM phospho.", "Naive Bayes+",
                                       "IV-KAPhE"))
expand_best_co <- filter(multilabel_expand, averaging == "macro-averaging",
                                            ((method == "PSSM phospho." & cutoff < 0.75) |
                                             (method == "Naive Bayes+" & cutoff < 0.5) |
                                             (method == "IV-KAPhE" & cutoff < 0.5) |
                                             (method == "LinkPhinder" & cutoff < 0.672) |
                                             (method == "NetworKIN 3.0" & cutoff < 1) |
                                             (method %in% c("GPS 5.0") & cutoff_i == 1))) %>%
    group_by(method) %>%
    slice_max(cutoff, n = 1, with_ties = TRUE)
expand_best_co_2 <- expand_best_co
expand_best_co_2$cutoff[expand_best_co$method %in% c("GPS 5.0")] <- NA

best_tprs <- filter(expand_best_co, metric == "TPR") %>%
    select(method, cutoff, value, upper, lower)
best_fdrs <- filter(expand_best_co, metric == "FDR") %>%
    select(method, cutoff, value, upper, lower)
best_fdr <- inner_join(best_tprs, best_fdrs,
                       by = c("method", "cutoff"), suffix = c(".tpr", ".fdr"))
best_fdr$cutoff[best_fdr$method %in% c("GPS 5.0")] <- NA
best_fdr <- unique(best_fdr)

fdr_plot <- filter(multilabel_expand, averaging == "macro-averaging",
                  metric %in% c("TPR", "FDR")) %>%
    arrange(cutoff) %>%
    select(-lower, -upper, -ci) %>%
    pivot_wider(names_from = metric, values_from = value) %>%
    ggplot(aes(x = TPR, y = FDR, color = method)) +
    scale_color_brewer("model", palette = "Dark2", guide = "none") +
    geom_path() +
    geom_point(data = best_fdr, aes(x = value.tpr, y = value.fdr)) +
    geom_errorbar(data = best_fdr,
                  aes(x = value.tpr, y = value.fdr, ymin = lower.fdr, ymax = upper.fdr),
                  width = 0.02) +
    geom_errorbar(data = best_fdr,
                  aes(x = value.tpr, y = value.fdr, xmin = lower.tpr, xmax = upper.tpr),
                  width = 0.02) +
    scale_x_continuous("macro-TPR", breaks = c(0, 0.5, 1), limits = c(0, 1)) +
    scale_y_continuous("macro-FDR", breaks = c(0, 0.5, 1), limits = c(0, 1))##   +

## plot Positive likelihood ratio 

new_site_assigns <- group_by(scores_bysite_expand_cut1, method, kinase.type, kinase) %>%
    summarise(n = sum(fp))
new_site_assigns$method <- factor(new_site_assigns$method,
                             levels = c("NetworKIN 3.0", "GPS 5.0", "LinkPhinder",
                                        "PSSM phospho.", "Naive Bayes+",
                                        "IV-KAPhE"))
new_kin_assigns <- group_by(scores_bysite_expand_cut1, method, seq.type, site.id) %>%
    summarise(n = sum(fp))
new_kin_assigns$method <- factor(new_kin_assigns$method,
                             levels = c("NetworKIN 3.0", "GPS 5.0", "LinkPhinder",
                                        "PSSM phospho.", "Naive Bayes+",
                                        "IV-KAPhE"))

new_sites_p <- ggplot(new_site_assigns, aes(x = method, y = n, color = kinase.type)) +
    ## geom_violin(draw_quantiles = 0.5) +
    ## geom_jitter(alpha = 0.25, width = 0.1) +
    geom_point(position = position_jitterdodge(jitter.width = 0.25), alpha = 0.25) +
    scale_color_brewer("kinase\ntype", palette = "Set2") +
    ylab("novel assignments\n(sites per kinase)") +
    xlab("model") +
    theme(legend.position = c(0.925, 0.8),
          legend.margin = margin(3, 3, 3, 3),
          legend.background = element_blank(),
          legend.box.background = element_rect(colour = "black"))
new_kins_p <- ggplot(new_kin_assigns, aes(x = method, y = n, color = seq.type)) +
    ## geom_violin(draw_quantiles = 0.5) +
    ## geom_jitter(alpha = 0.15, width = 0.1) +
    geom_point(position = position_jitterdodge(jitter.width = 0.25), alpha = 0.25) +
    scale_color_brewer("acceptor\nresidue", palette = "Set2") +
    ylab("novel assignments\n(kinases per site)") +
    xlab("model") +
    theme(legend.position = c(0.91, 0.8),
          legend.margin = margin(3, 3, 3, 3),
          legend.background = element_blank(),
          legend.box.background = element_rect(colour = "black"))

## Load rf_imp_plot
load("img/rand-forest-importance.Robj")
load("img/rand-forest-kfold-ml-eval.Robj")
load("img/rand-forest-kfold-ml-prec-rec.Robj")
load("img/rand-forest-kfold-feat-prec-rec.Robj")

rf_imp_plot <- rf_imp_plot + scale_y_continuous(breaks = c(0, 400, 800)) + coord_flip()

top_row <- plot_grid(rf_imp_plot, kfold_feat_pr_plot, kfold_pr_plot, kfold_ml_plot, ncol = 4,
                     labels = c("a", "b", "c", ""), rel_widths = c(1.3, 1.75, 1, 1))
mid_row <- plot_grid(coverage_p, f1_bykin_p, ncol = 2, labels = c("d", "e"),
                     rel_widths = c(1, 2.75), align = "h", axis = "bottom")
bottom_row <- plot_grid(pr_plot, ml_plot, roc_plot, fdr_plot, ncol = 4, labels = c("f", "g", "h", "i"),
                        rel_widths = c(1.5, 1, 1, 1))
p <- plot_grid(top_row, mid_row, bottom_row, nrow = 3, rel_heights = c(1, 1, 1))
save_plot("img/pmapperval-ml.pdf", p, ncol = 5, nrow = 3,
          base_asp = 1.0, base_height = 2.6)
