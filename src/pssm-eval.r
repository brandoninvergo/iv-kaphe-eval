library(drc)                            #Alternative logistic fitting
library(rhdf5)
library(tidyverse)
library(tidytext)
library(cowplot)
library(ggnewscale)
library(ggseqlogo)
library(parallel)
library(viridis)
theme_set(theme_cowplot())

get_score_range <- function(pssm, relh, win_len, scale, omit){
    pssm[is.infinite(pssm)] <- NA
    col_mins <- apply(pssm, 2, min, na.rm = TRUE)
    col_maxes <- apply(pssm, 2, max, na.rm = TRUE)
    sum_cols <- setdiff(1:win_len, omit)
    if (scale){
        n_omit <- length(omit)
        weights <- relh
        weights[omit] <- 0
        ## Weight = relH re-scaled to sum to n, the number of columns.
        weights <- weights * (win_len-n_omit) / sum(weights)
        col_mins <- col_mins * weights
        col_maxes <- col_maxes * weights
     }
    return(c(sum(col_mins[sum_cols]), sum(col_maxes[sum_cols])))
}

get_bayes_range <- function(pssm, prior, relh, compl_pssm, compl_prior, win_len,
                            scale, omit){
    ## For each column, find the row (residue) that either maximizes
    ## the difference between the complementary likelihood and the
    ## kinase likelihood, or vice versa
    min_rows <- apply(compl_pssm - pssm, 2, which.max)
    max_rows <- apply(pssm - compl_pssm, 2, which.max)
    ## Get the respective likelihoods corresponding to these rows
    col_mins <- sapply(1:win_len, function(i) pssm[min_rows[i], i])
    col_maxes <- sapply(1:win_len, function(i) pssm[max_rows[i], i])
    compl_mins <- sapply(1:win_len, function(i) compl_pssm[min_rows[i], i])
    compl_maxes <- sapply(1:win_len, function(i) compl_pssm[max_rows[i], i])
    sum_cols <- setdiff(1:win_len, omit)
    if (scale){
        n_omit <- length(omit)
        weights <- relh
        weights[omit] <- 0
        ## weights[acceptor] <- 0
        ## Weight = relH re-scaled to sum to n, the number of columns.
        weights <- weights * (win_len-n_omit) / sum(weights)
        col_mins <- col_mins * weights
        col_maxes <- col_maxes * weights
        compl_mins <- compl_mins * weights
        compl_maxes <- compl_maxes * weights
    }
    ## Get the total log10 likelihoods
    score_min <- sum(col_mins[sum_cols])
    compl_min <- sum(compl_mins[sum_cols])
    score_max <- sum(col_maxes[sum_cols])
    compl_max <- sum(compl_maxes[sum_cols])
    ## Calculate the corresponding minimum and maximum probabilities
    prob_min <- prior / (prior + compl_prior * 10^(compl_min - score_min))
    prob_max <- prior / (prior + compl_prior * 10^(compl_max - score_max))
    score_range <- c(prob_min, prob_max)
}

get_motif_info <- function(pssm_dir, win_len, scale, omit){
    kinases <- NULL
    pssm_maxes <- NULL
    pssm_mins <- NULL
    seq_ns <- NULL
    for (motif_file in list.files(pssm_dir)){
        if (!endsWith(motif_file, ".h5"))
            next
        motif_path <- paste(pssm_dir, motif_file, sep = "/")
        motif_name <- h5readAttributes(motif_path, "/", native = TRUE)$name
        name_split <- strsplit(motif_name, split = "-")[[1]]
        if ("bayes complement" %in% name_split)
            next
        if (length(name_split) == 2){
            kinase <- name_split[1]
        }else{
            kinase <- paste(name_split[1:2], collapse = "-")
        }
        pssm <- h5read(motif_path, "/PSSM", native = TRUE)
        relh <- h5read(motif_path, "/relH", native = TRUE)
        seqn <- h5readAttributes(motif_path, "/", native = TRUE)$`number of sequences`
        if (is.null(pssm) || is.null(relh)){
            print(kinase)
            return(NULL)
        }
        if ("bayes" %in% name_split)
        {
            prior <- h5readAttributes(motif_path, "/", native = TRUE)$prior[1]
            compl_path <- sub("-motif", "-compl", motif_path)
            compl_pssm <- h5read(compl_path, "/PSSM", native = TRUE)
            compl_prior <- h5readAttributes(compl_path, "/", native = TRUE)$prior[1]
            score_range <- get_bayes_range(pssm, prior, relh, compl_pssm, compl_prior,
                                           win_len, scale, omit)
        }else{
            score_range <- get_score_range(pssm, relh, win_len, scale, omit)
        }
        h5closeAll()
        if (length(score_range) < 2){
            print(kinase)
            return(NULL)
        }
        if (is.null(kinases)){
            kinases <- kinase
            pssm_mins <- score_range[1]
            pssm_maxes <- score_range[2]
            seq_ns <- seqn
        }else{
            kinases <- c(kinases, kinase)
            pssm_mins <- c(pssm_mins, score_range[1])
            pssm_maxes <- c(pssm_maxes, score_range[2])
            seq_ns <- c(seq_ns, seqn)
        }
    }
    motif_info <- tibble(kinase = kinases,
                         pssm.min = pssm_mins,
                         pssm.max = pssm_maxes,
                         pssm.range = pssm_maxes - pssm_mins,
                         seq.n = seq_ns)
    return(motif_info)
}

prepare_motif_info <- function(motif_dir, method_name, Y_kinases, win_len, scale, omit){
    pssm_info <- get_motif_info(motif_dir, win_len, scale, omit) %>%
        mutate(method = rep(method_name, n()))
    pssm_info$main.id <- unlist(lapply(strsplit(pssm_info$kinase, split = "-"),
                                      function(split) split[1]))
    pssm_info$y.kinase <- pssm_info$main.id %in% Y_kinases$kinase
    pssm_info$kinase.type <- rep(NA, nrow(pssm_info))
    pssm_info$kinase.type[which(pssm_info$y.kinase)] <- "Y"
    pssm_info$kinase.type[which(!pssm_info$y.kinase)] <- "S/T"
    pssm_info$kinase.type <- as.factor(pssm_info$kinase.type)
    pssm_info$y.kinase <- NULL
    return(pssm_info)
}

args <- commandArgs(TRUE)
win_len <- as.integer(args[1])

half_winlen <- floor(win_len/2)
s_regexp <- paste0(".{", half_winlen, "}[S].{", half_winlen, "}")
t_regexp <- paste0(".{", half_winlen, "}[T].{", half_winlen, "}")
y_regexp <- paste0(".{", half_winlen, "}[Y].{", half_winlen, "}")

acceptor <- ceiling(win_len / 2)

y_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase"))
st_kinases <- read_tsv("data/kinases-ST.txt", col_names = c("kinase"))

grepl_v <- Vectorize(grepl)

phoproteome_glob <- read_tsv("out/phoproteome/phoproteome-scores-glob.tsv") %>%
    filter(!grepl_v("_", seq.win))
phoproteome_glob_sum <- group_by(phoproteome_glob, kinase) %>%
    summarise(phoproteome.min = min(score),
              phoproteome.max = max(score),
              phoproteome.range = phoproteome.max - phoproteome.min)
glob_pssm_info <- prepare_motif_info("out/motifs/global-bg", "PSSM proteome bg.",
                                     y_kinases, win_len, TRUE, c()) %>%
    full_join(phoproteome_glob_sum, by = "kinase")
if (any(glob_pssm_info$phoproteome.max > glob_pssm_info$pssm.max |
        glob_pssm_info$phoproteome.min < glob_pssm_info$pssm.min))
    message("Calculating the PSSM theoretical range messed up.")

phoproteome_pos <- read_tsv("out/phoproteome/phoproteome-scores-pos.tsv") %>%
    filter(!grepl_v("_", seq.win))
phoproteome_pos_sum <- group_by(phoproteome_pos, kinase) %>%
    summarise(phoproteome.min = min(score),
              phoproteome.max = max(score),
              phoproteome.range = phoproteome.max - phoproteome.min)
pos_pssm_info <- prepare_motif_info("out/motifs/pos-bg", "PSSM phosphoproteome bg.",
                                    y_kinases, win_len, TRUE, c()) %>%
    full_join(phoproteome_pos_sum, by = "kinase")
if (any(pos_pssm_info$phoproteome.max > pos_pssm_info$pssm.max |
        pos_pssm_info$phoproteome.min < pos_pssm_info$pssm.min))
    message("Calculating the PSSM theoretical range messed up.")

phoproteome_freq <- read_tsv("out/phoproteome/phoproteome-scores-freq.tsv") %>%
    filter(!grepl_v("_", seq.win))
phoproteome_freq_sum <- group_by(phoproteome_freq, kinase) %>%
    summarise(phoproteome.min = min(score),
              phoproteome.max = max(score),
              phoproteome.range = 10^phoproteome.max - 10^phoproteome.min)
freq_pssm_info <- prepare_motif_info("out/motifs/freq", "PFM", y_kinases, win_len,
                                     TRUE, c()) %>%
    full_join(phoproteome_freq_sum, by = "kinase")
if (any(freq_pssm_info$phoproteome.max > freq_pssm_info$pssm.max |
        freq_pssm_info$phoproteome.min < freq_pssm_info$pssm.min))
    message("Calculating the PSSM theoretical range messed up.")

phoproteome_bayes <- read_tsv("out/phoproteome/phoproteome-scores-bayes.tsv") %>%
    filter(!grepl_v("_", seq.win))
phoproteome_bayes_sum <- group_by(phoproteome_bayes, kinase) %>%
    summarise(phoproteome.min = min(score),
              phoproteome.max = max(score),
              phoproteome.range = phoproteome.max - phoproteome.min)
bayes_pssm_info <- prepare_motif_info("out/motifs/bayes", "Naive Bayes", y_kinases,
                                      win_len, TRUE, c()) %>%
    full_join(phoproteome_bayes_sum, by = "kinase")
if (any(bayes_pssm_info$phoproteome.max > bayes_pssm_info$pssm.max |
        bayes_pssm_info$phoproteome.min < bayes_pssm_info$pssm.min))
    message("Calculating the PSSM theoretical range messed up.")


citations <- read_tsv("data/kinase-citations.tsv")

pssm_info <- bind_rows(glob_pssm_info, pos_pssm_info, freq_pssm_info, bayes_pssm_info) %>%
    mutate(method = factor(method, levels = c("PFM", "PSSM proteome bg.", "PSSM phosphoproteome bg.",
                                              "Naive Bayes"))) %>%
    left_join(citations, by = c("kinase"))

write_tsv(pssm_info, "out/pssm-info.tsv")

range_plot <- ggplot(pssm_info, aes(x = reorder_within(kinase, pssm.max, method, fun = median),
                                    ymin = pssm.min, ymax = pssm.max,
                                    color = kinase.type)) +
    geom_hline(yintercept = 0, lty = 2, col = "gray") +
    geom_linerange() +
    scale_color_brewer("kinase type", palette = "Set2") +
    new_scale_color() +
    geom_linerange(aes(ymin = phoproteome.min, ymax = phoproteome.max, color = kinase.type)) +
    scale_color_brewer("kinase type", palette = "Dark2") +
    geom_hline(yintercept = 0, lty = 2, col = "gray") +
    facet_wrap(~method, scales = "free") +
    scale_x_reordered() +
    ylab("possible score values") +
    xlab("kinase") +
    theme(axis.ticks.x = element_blank(),
          axis.text.x = element_blank(),
          panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())

pssm_info_w <- dplyr::select(pssm_info, kinase, method, kinase.type, pssm.range, phoproteome.range) %>%
    pivot_longer(cols = c("pssm.range", "phoproteome.range"),
                 names_to = c("origin", "meas"),
                 names_pattern = "(.*)\\.(.*)",
                 values_to = "value") %>%
    dplyr::select(-meas) %>%
    pivot_wider(names_from = "method", values_from = "value") ## , names_sep = ".")
pssm_info_w$origin <- factor(pssm_info_w$origin,
                             levels = c("pssm", "phoproteome"),
                             labels = c("theoretical", "empirical"))

scat_plot <- ggplot(pssm_info_w, aes(x = `PSSM phosphoproteome bg.`,
                                     y = `Naive Bayes`,
                                     color = kinase.type)) +
    ## geom_abline(intercept = 0, slope = 1, lty = 2, col = "gray") +
    geom_point() +
    xlab("PSSM score range\n(phosphoproteome background)") +
    ylab("Naive Bayes probability range") +
    scale_color_brewer("kinase type", palette = "Dark2") +
    ## xlim(0, 70) +
    ## ylim(0, 1) +
    facet_wrap(~origin) +
    theme(panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())

pssm_plot <- plot_grid(range_plot, scat_plot, nrow = 2, labels = "auto",
                       align = "v", axis = "lr")

cites_plot <- ggplot(pssm_info, aes(x = num.cites.filt, y = pssm.range,
                                    color = kinase.type)) +
    geom_point() +
    xlab("# citations") +
    ylab("PSSM score range") +
    scale_x_log10() +
    facet_wrap(~method, scales = "free")
cites_plot

range_plot2 <- ggplot(pssm_info, aes(x = pssm.range, y = phoproteome.range, color = kinase.type)) +
    geom_point(alpha = 0.25) +
    geom_density2d() +
    scale_color_brewer("kinase type", palette = "Set2") +
    facet_wrap(vars(method), scales = "free")

## pssm_plot
## save_plot("img/pssm-eval.pdf", pssm_plot, nrow = 2, ncol = 2, base_asp = 1.0)
save_plot("img/pssm-eval.pdf", range_plot, nrow = 2, ncol = 3, base_asp = 1.0, base_height = 2.5)

## For ranking, we want normalized scores
phoproteome_glob_norm <- read_tsv("out/phoproteome-norm/phoproteome-scores-glob.tsv") %>%
    filter(!grepl_v("_", seq.win))
phoproteome_pos_norm <- read_tsv("out/phoproteome-norm/phoproteome-scores-pos.tsv") %>%
    filter(!grepl_v("_", seq.win))

phoproteome_glob_bysite <- group_by(phoproteome_glob_norm, seq.win) %>%
    mutate(score.rank = rank(-score),
           method = rep("proteome", n())) %>%
    arrange(seq.win, kinase)
phoproteome_pos_bysite <- group_by(phoproteome_pos_norm, seq.win) %>%
    mutate(score.rank = rank(-score),
           method = rep("phosphoproteome", n())) %>%
    arrange(seq.win, kinase)
phoproteome_bayes_bysite <- group_by(phoproteome_bayes, site.id) %>%
    mutate(score.rank = rank(-score),
           method = rep("Naive Bayes", n())) %>%
    select(-site.id) %>%
    arrange(seq.win, kinase)

## phoproteome_bysite <- bind_rows(phoproteome_glob_bysite, phoproteome_pos_bysite)
phoproteome_bysite <- bind_rows(phoproteome_pos_bysite, phoproteome_bayes_bysite)
y.site <- grepl(y_regexp, phoproteome_bysite$seq.win)
phoproteome_bysite$seq.type <- rep(NA, nrow(phoproteome_bysite))
phoproteome_bysite$seq.type[y.site] <- "Y"
phoproteome_bysite$seq.type[!y.site] <- "S/T"

y_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase"))
st_kinases <- read_tsv("data/kinases-ST.txt", col_names = c("kinase"))
phoproteome_bysite$kinase.type <- rep(NA, nrow(phoproteome_bysite))
phoproteome_bysite$kinase.type[which(phoproteome_bysite$kinase %in% y_kinases$kinase)] <- "Y"
phoproteome_bysite$kinase.type[which(phoproteome_bysite$kinase %in% st_kinases$kinase)] <- "S/T"

good_kinases <- filter(pssm_info, seq.n >= 20)$kinase
phoproteome_bysite_filt <- filter(phoproteome_bysite, kinase %in% good_kinases)
kinase_ranks <- group_by(phoproteome_bysite_filt, kinase, method, seq.type, kinase.type) %>%
    summarise(median.rank = median(score.rank),
              q1.rank = quantile(score.rank, 0.25),
              q3.rank = quantile(score.rank, 0.75),
              worst.rank = max(score.rank),
              best.rank = min(score.rank),
              range.rank = worst.rank - best.rank) %>%
    arrange(kinase)
kinase_ranks$seq.type <- factor(kinase_ranks$seq.type,
                                levels = c("S/T", "Y"),
                                labels = c("S/T sites", "Y sites"))
kinase_ranks$method <- factor(kinase_ranks$method,
                          levels = c("phosphoproteome", "Naive Bayes"),
                          labels = c("PSSM phospho.", "Naive Bayes"))
kinase_ranks <- mutate(kinase_ranks, bg.seq.type = paste(method, seq.type, sep = " - "))
kinase_ranks$bg.seq.type <- factor(kinase_ranks$bg.seq.type,
                                   levels = c("PSSM phospho. - S/T sites",
                                              "Naive Bayes - S/T sites",
                                              "PSSM phospho. - Y sites",
                                              "Naive Bayes - Y sites"))

kinase_rank_plot <- ggplot(kinase_ranks,
                           aes(x = reorder_within(kinase, median.rank,
                                                  bg.seq.type, fun = max),
                               ymin = best.rank, ymax = worst.rank,
                               y = median.rank,
                               color = kinase.type)) +
    geom_linerange() +
    scale_color_brewer("kinase type", palette = "Set2") +
    new_scale_color() +
    geom_linerange(aes(ymin = q1.rank, ymax = q3.rank, color = kinase.type)) +
    scale_color_brewer("kinase type", palette = "Dark2") +
    geom_point(color = "#7570B3", size = 0.5) +
    xlab("kinase") +
    ylab("PSSM score ranks") +
    scale_x_reordered() +
    facet_wrap(~bg.seq.type, scales = "free") +
    theme(axis.ticks.x = element_blank(),
          axis.text.x = element_blank(),
          panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())
kinase_ranks_w <- dplyr::select(kinase_ranks, -bg.seq.type) %>%
    pivot_longer(cols = c("median.rank", "worst.rank",
                          "best.rank", "range.rank",
                          "q1.rank", "q3.rank"),
                 names_to = "prop",
                 names_pattern = "(.*)\\.rank",
                 values_to = "rank") %>%
    pivot_wider(names_from = "method", values_from = "rank") %>%
    filter(!(prop %in% c("q1", "q3")))
kinase_ranks_w$prop <- factor(kinase_ranks_w$prop,
                              levels = c("best", "worst", "range",
                                         "median"),
                              labels = c("best rank", "worst rank",
                                         "rank range", "median rank"))
scat_plot <- ggplot(kinase_ranks_w,
                    aes(x = `PSSM phospho.`,
                        y = `Naive Bayes`,
                        color = kinase.type)) +
    geom_abline(intercept = 0, slope = 1, lty = 2, col = "gray") +
    geom_point(size = 1, alpha = 0.2) +
    xlab("PSSM phospho. score rank") +
    ylab("Naive Bayes score rank") +
    scale_color_brewer("kinase type", palette = "Dark2") +
    facet_grid(vars(seq.type), vars(prop)) +
    theme(panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())
p <- plot_grid(kinase_rank_plot, scat_plot, nrow = 2, align = "v", axis = "l",
               labels = "auto")
save_plot("img/pssm-kinase-ranks.pdf", p, nrow = 2, ncol = 2, base_asp = 1.0)

## Check, e.g., how many kinases have a rank-1 site
foo <- filter(kinase_ranks, kinase.type == "S/T", seq.type == "S/T sites")
table(foo$method, foo$best.rank)

ysite_st_kinases <- filter(kinase_ranks, kinase.type == "S/T", seq.type == "Y sites",
                           method == "PSSM phosphoproteome bg.", best.rank == 1)$kinase

kinase_id_map <- read_tsv("data/kinase-id-map.tsv", col_names = c("id", "prot.name", "gene.name"))

filter(kinase_id_map, id %in% ysite_st_kinases)

## Exploring the relationship between the PSSM with phosphosite
## background to the Naive Bayes posterior probability
## phoproteome_bayesp <- read_tsv("out/phoproteome/phoproteome-scores-bayes-plus.tsv") %>%
##     filter(!grepl_v("_", seq.win))
comb_scores <- inner_join(phoproteome_bayes, phoproteome_pos, by = c("kinase", "seq.win"),
                          suffix = c(".bayes", ".pos")) %>%
    inner_join(phoproteome_pos_norm, by = c("kinase", "seq.win"))
colnames(comb_scores)[6] <- "score.pos.norm"

kins <- unique(comb_scores$kinase)

xmids <- c()
scals <- c()
xmids_norm <- c()
scals_norm <- c()
pb <- txtProgressBar(max = length(kins), style = 3)
for (i in 1:length(kins)){
    setTxtProgressBar(pb, i)
    kin <- kins[i]
    kin_scores <- filter(comb_scores, kinase == kin) %>%
        dplyr::select(score.bayes, score.pos)
    ## m <- tryCatch(nls(score.bayes ~ SSlogis(score.pos, Asym, xmid, scal),
    ##                   data = kin_scores), error = function(e) NA)
    m <- drm(score.bayes ~ score.pos, data = kin_scores, fct = L.3(fixed = c(NA, 1.0, NA)),
             type = "continuous")
    if (is.na(m)){
        xmid <- NA
        scal <- NA
    }else{
        xmid <- summary(m)$coefficients[2, 1]
        scal <- -summary(m)$coefficients[1, 1]
    }
    xmids <- c(xmids, xmid)
    scals <- c(scals, scal)
    kin_scores_norm <- filter(comb_scores, kinase == kin) %>%
        dplyr::select(score.bayes, score.pos.norm)
    ## m <- tryCatch(nls(score.bayes ~ SSlogis(score.pos.norm, Asym, xmid, scal),
    ##                   data = kin_scores_norm), error = function(e) NA)
    m <- drm(score.bayes ~ score.pos.norm, data = kin_scores_norm, fct = L.3(fixed = c(NA, 1.0, NA)),
             type = "continuous")
    if (is.na(m)){
        xmid_norm <- NA
        scal_norm <- NA
    }else{
        xmid_norm <- summary(m)$coefficients[2, 1]
        scal_norm <- -summary(m)$coefficients[1, 1]
    }
    xmids_norm <- c(xmids_norm, xmid_norm)
    scals_norm <- c(scals_norm, scal_norm)
}
close(pb)

nls_fits <- tibble(kinase = kins, xmid = xmids, scal = 1/scals,
                   xmid.norm = xmids_norm, scal.norm = 1/scals_norm) %>%
    left_join(pos_pssm_info, by = "kinase") %>%
    filter(seq.n > 20, xmid <= pssm.max, xmid >=pssm.min) %>%
    mutate(y.kinase = kinase %in% y_kinases$kinase,
           kinase.type = if_else(y.kinase, "Y", "S/T")) %>%
    arrange(seq.n) %>%
    dplyr::select(kinase, xmid, scal, xmid.norm, scal.norm, seq.n, pssm.min, pssm.max,
                  kinase.type, y.kinase)
pal <- viridis(max(nls_fits$seq.n))
curves <- NULL
curve_range <- -25:25
all_kins <- nls_fits$kinase[order(nls_fits$seq.n)]
for (kin in all_kins){
    xmid <- filter(nls_fits, kinase == kin) %>% pull(xmid)
    scal <- filter(nls_fits, kinase == kin) %>% pull(scal)
    if (is.na(xmid) || is.na(scal))
        next
    seq.n <- filter(nls_fits, kinase == kin) %>% pull(seq.n)
    curve_tbl <- tibble(kinase = rep(kin, length(curve_range)),
                        pssm.score = curve_range,
                        nb.prob = SSlogis(curve_range, xmid = xmid, scal = scal,
                                          Asym = 1),
                        seq.n = rep(seq.n, length(curve_range)))
    if (is.null(curves)){
        curves <- curve_tbl
    }else{
        curves <- rbind(curves, curve_tbl)
    }
}
p1 <- ggplot(curves, aes(x = pssm.score, y = nb.prob, color = seq.n, group = kinase)) +
    geom_line(alpha = 0.3) +
    scale_color_viridis("# substrates") +
    xlab("PSSM phospho.\nscore") +
    ylab("Naive Bayes\nposterior probability") +
    theme(legend.position = c(0.05, 0.8))
## p2 <- ggplot(nls_fits, aes(x = xmid, y = scal, color = seq.n)) +
##     geom_point(alpha = 0.5) +
##     scale_color_viridis("# substrates") +
##     xlab("fitted inflection point") +
##     ylab("fitted growth rate") +
##     xlim(-25, 25) +
##     theme(legend.position = c(0.05, 0.8))
nls_stats <- dplyr::select(nls_fits, kinase, kinase.type, xmid, scal, seq.n) %>%
    pivot_longer(c(xmid, scal))
nls_stats$name <- factor(nls_stats$name,
                         levels = c("xmid", "scal"),
                         labels = c("inflection point", "growth rate"))
p2b <- ggplot(nls_stats, aes(x = seq.n, y = value)) +
    geom_point(alpha = 0.25) +
    scale_x_continuous("# substrates", n.breaks = 3) +
    ylab("fitted value") +
    facet_grid(cols = vars(kinase.type), rows = vars(name), scales = "free") +
    theme(panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())
curves <- NULL
curve_range <- seq(0, 1, length.out = 50)
for (kin in all_kins){
    xmid <- filter(nls_fits, kinase == kin) %>% pull(xmid.norm)
    scal <- filter(nls_fits, kinase == kin) %>% pull(scal.norm)
    if (is.na(xmid) || is.na(scal))
        next
    seq.n <- filter(nls_fits, kinase == kin) %>% pull(seq.n)
    curve_tbl <- tibble(kinase = rep(kin, length(curve_range)),
                        pssm.score = curve_range,
                        nb.prob = SSlogis(curve_range, xmid = xmid, scal = scal,
                                          Asym = 1),
                        seq.n = rep(seq.n, length(curve_range)))
    if (is.null(curves)){
        curves <- curve_tbl
    }else{
        curves <- rbind(curves, curve_tbl)
    }
}
p3 <- ggplot(curves, aes(x = pssm.score, y = nb.prob, color = seq.n, group = kinase)) +
    geom_line(alpha = 0.3) +
    scale_color_viridis("# substrates") +
    xlab("PSSM phospho.\nnormalized score") +
    ylab("Naive Bayes\nposterior probability") +
    theme(legend.position = c(0.05, 0.8))
## p4 <- ggplot(nls_fits, aes(x = xmid.norm, y = scal.norm, color = seq.n)) +
##     geom_point(alpha = 0.5) +
##     scale_color_viridis("# substrates") +
##     xlab("fitted inflection point") +
##     ylab("fitted growth rate") +
##     xlim(0, 1) +
##     theme(legend.position = c(0.05, 0.8))
nls_stats_norm <- dplyr::select(nls_fits, kinase, kinase.type, xmid.norm, scal.norm, seq.n) %>%
    pivot_longer(c(xmid.norm, scal.norm))
nls_stats_norm$name <- factor(nls_stats_norm$name,
                         levels = c("xmid.norm", "scal.norm"),
                         labels = c("inflection point", "growth rate"))
p4b <- ggplot(nls_stats_norm, aes(x = seq.n, y = value)) +
    geom_point(alpha = 0.25) +
    scale_x_continuous("# substrates", n.breaks = 3) +
    ylab("fitted value") +
    facet_grid(cols = vars(kinase.type), rows = vars(name), scales = "free") +
    theme(panel.border = element_rect(color = "black", fill = NA),
          axis.line = element_blank())
bayes_logistic_p <- plot_grid(p1, p2b, p3, p4b, nrow = 1, labels = c("d", "", "e", ""),
                align = "hv", axis = "bottom")
save(bayes_logistic_p, file = "img/bayes-pssm-logistic-relationship.Robj")
save_plot("img/bayes-pssm-logistic-relationship.pdf", bayes_logistic_p, nrow = 2, ncol = 2,
          base_asp = 1.0)

write_tsv(nls_fits, "out/bayes-pssm-logistic-fits.tsv")
