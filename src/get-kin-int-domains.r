library(tidyverse)

pfam <- read_tsv("data/pfam-human-domains.tsv") %>%
    mutate(temp.id = paste(`seq id`, `hmm acc`))

kinases <- readLines("data/kinases.txt")

kin_ints_tbl <- read_tsv("data/human-kinase-interactions.tsv", col_names = c("prot1", "prot2"))
kin_ints_tbl$int.id <- apply(kin_ints_tbl, 1,
                         function(x) paste(sort(x), collapse = " "))

all_int_doms <- filter(pfam, `seq id` %in% unique(c(kin_ints_tbl$prot1, kin_ints_tbl$prot2))) %>%
    select(`seq id`, `hmm acc`) %>%
    distinct() %>%
    pull(`hmm acc`)
unique_int_doms <- unique(all_int_doms)

int_dom_enrich <- NULL
for (kin in kinases){
    kin_ints1 <- filter(kin_ints_tbl, prot1 == kin) %>%
        pull(prot2)
    kin_ints2 <- filter(kin_ints_tbl, prot2 == kin) %>%
        pull(prot1)
    kin_ints <- c(kin_ints1, kin_ints2)
    int_doms <- filter(pfam, `seq id` %in% kin_ints) %>%
        select(`seq id`, `hmm acc`) %>%
        distinct() %>%
        pull(`hmm acc`)
    unique_int_doms <- unique(int_doms)
    dom_tbl_init <- tibble(interactor = unique(c(kin_ints_tbl$prot1,
                                                 kin_ints_tbl$prot2))) %>%
        mutate(in.kin.sub = interactor %in% kin_ints)
    ft_est <- NULL
    ft_p <- NULL
    for (dom in unique_int_doms){
        dom_tbl <- mutate(dom_tbl_init,
                          has.domain = paste(interactor, dom) %in% pfam$temp.id)
        cont_tbl <- table(dom_tbl$in.kin.sub, dom_tbl$has.domain)
        ft <- fisher.test(cont_tbl, alternative = "greater")
        if (is.null(ft_est)){
            ft_est <- ft$estimate
            ft_p <- ft$p.value
        }else{
            ft_est <- c(ft_est, ft$estimate)
            ft_p <- c(ft_p, ft$p.value)
        }
    }
    if (is.null(int_dom_enrich)){
        int_dom_enrich <- tibble(domain = unique_int_doms,
                                 odds.ratio = ft_est,
                                 p = ft_p) %>%
            mutate(kinase = rep(kin, n())) %>%
            select(kinase, domain, odds.ratio, p)
    }else{
        dom_enrich_tmp <- tibble(domain = unique_int_doms,
                                 odds.ratio = ft_est,
                                 p = ft_p) %>%
            mutate(kinase = rep(kin, n())) %>%
            select(kinase, domain, odds.ratio, p)
        int_dom_enrich <- bind_rows(int_dom_enrich, dom_enrich_tmp)
    }
}

write_tsv(int_dom_enrich, "out/kinase-int-domains.tsv")

