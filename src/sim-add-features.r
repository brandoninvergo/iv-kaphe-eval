library(doParallel)
library(tidyverse)

site_in_domain <- Vectorize(function(protein, site){
    pfam_prot <- filter(pfam, `seq id` == protein) %>%
        filter(`envelope start` <= site,
               `envelope end` >= site)
    return (nrow(pfam_prot) > 0)
})

biogrid <- read_tsv("data/human-kinase-interactions.tsv", col_names = c("prot1", "prot2"))
biogrid$interaction.id <- apply(biogrid, 1,
                                function(x) paste(sort(x), collapse = " "))
bioplex <- read_tsv("data/raw/BioPlex_293T_Network_10K_Dec_2019.tsv") %>%
        select(UniprotA, UniprotB)
bioplex$interaction.id <- apply(bioplex, 1,
                                function(x) paste(sort(x), collapse = " "))
pfam <- read_tsv("data/pfam-human-domains.tsv")
kinases <- readLines("data/kinases.txt")
sugiyama <- read_tsv("data/sugiyama-kinase-substrates.tsv") %>%
    separate(substrate.id.site, into = c("substrate", "site"),
             sep = "-[STY]", remove = FALSE, convert = TRUE) %>%
    extract(substrate.id.site, regex = "-([STY])", into = "residue",
            remove = FALSE) %>%
    mutate(in.domain = site_in_domain(substrate, site))

sugiyama <- extract(sugiyama, substrate.id.site, regex = "-([STY])", into = "residue", remove = FALSE)

## dom.p <- length(which(sugiyama$in.domain)) / nrow(sugiyama)

dom.p <- t(apply(table(sugiyama$residue, sugiyama$in.domain), 1, function(row) row/sum(row)))[,2]

sim_feat <- read_tsv("data/simulated-seqwins.txt", col_names = c("seq.win")) %>%
    extract(seq.win, "acceptor", "[A-Z]{7}([STY])[A-Z]{7}", remove = FALSE)

rbernoulli_v <- Vectorize(rbernoulli)

cl <- makeCluster(63)
registerDoParallel(cl)
foreach (i=1:length(kinases), .packages="tidyverse") %dopar% {
    ## for(kin in kinases){
    kin <- kinases[i]
    out_file <- paste0("data/sim-kinase-features/sim-", kin, "-feats.tsv")
    ## if (file.exists(out_file)){
    ##     message(paste("Skipping", kin, "because its file already exists"))
    ##     next
    ## }
    sugiyama$interaction.id <- sapply(sugiyama$substrate,
                                      function(x) paste(sort(c(kin, x)), collapse = " "))
    sugiyama_kin <- mutate(sugiyama,
                           phys.int = (interaction.id %in% biogrid$interaction.id |
                                       interaction.id %in% bioplex$interaction.id)) %>%
        select(-interaction.id)
    int.p <- length(which(sugiyama_kin$phys.int)) / nrow(sugiyama_kin)
    ## int.p <- t(apply(table(sugiyama_kin$residue, sugiyama_kin$phys.int), 1, function(row) row/sum(row)))[,2]
    mutate(sim_feat,
           phys.int = rbernoulli_v(1, p = int.p),
           in.domain = rbernoulli_v(1, p = dom.p[acceptor])) %>%
        distinct() %>%
        select(-acceptor) %>%
        write_tsv(out_file, col_names = FALSE)
}
stopCluster(cl)
