library(tidyverse)

omnipath <- read_tsv("data/kinase-omnipath.tsv") %>%
    mutate(effect = if_else(effect > 0, "activating", "inhibiting"),
           int.id = paste(source, target))
kinnet_pred <- read_tsv("data/kinase-pred-net.tsv")

kinnet_filt <- filter(kinnet_pred, Mean_posterior_probability_of_regulation > 0.75) %>%
    mutate(effect = if_else(Mean_posterior_probability_of_sign_prediction > 0.5,
                            "activating", "inhibiting"),
           int.id = paste(Transducing_kinase, Substrate_kinase),
           in.omnipath = int.id %in% omnipath$int.id) %>%
    filter(!in.omnipath) %>%
    select(Transducing_kinase, Substrate_kinase, effect, int.id)
names(kinnet_filt)[1:2] <- c("source", "target")

bind_rows(omnipath, kinnet_filt) %>%
    select(-int.id) %>%
    write_tsv("data/kinase-regulatory-net.tsv")

