library(tidyverse)

set.seed(20210519)

args <- commandArgs(TRUE)
win_len <- as.integer(args[1])

half_winlen <- floor(win_len/2)
s_regexp <- paste0(".{", half_winlen, "}[S].{", half_winlen, "}")
t_regexp <- paste0(".{", half_winlen, "}[T].{", half_winlen, "}")
y_regexp <- paste0(".{", half_winlen, "}[Y].{", half_winlen, "}")

Y_kinases <- read_tsv("data/kinases-Y.txt", col_names = c("kinase"))
ST_kinases <- read_tsv("data/kinases-ST.txt", col_names = c("kinase"))

prepare_kinsub <- function(kinsub, Y_kinases, y_regexp){
    if ("kinase" %in% colnames(kinsub)){
        kinsub$main.id <- unlist(lapply(strsplit(kinsub$kinase, split = "-"),
                                        function(split) split[1]))
        kinsub$y.kinase <- kinsub$main.id %in% Y_kinases$kinase
        kinsub$kinase.type <- rep(NA, nrow(kinsub))
        kinsub$kinase.type[which(kinsub$y.kinase)] <- "Y"
        kinsub$kinase.type[which(!kinsub$y.kinase)] <- "S/T"
        kinsub$kinase.type <- as.factor(kinsub$kinase.type)
        kinsub$y.kinase <- NULL
    }
    y.site <- grepl(y_regexp, kinsub$seq.win)
    kinsub$seq.type <- rep(NA, nrow(kinsub))
    kinsub$seq.type[y.site] <- "Y"
    kinsub$seq.type[!y.site] <- "S/T"
    return(kinsub)
}

protmapper <- read_tsv("data/protmapper-kinase-substrates.tsv",
                       col_names = c("kinase", "site.id", "seq.win")) %>%
    prepare_kinsub(Y_kinases, y_regexp) %>%
    mutate(int.id = paste(kinase, site.id))
psp <- read_tsv("data/psp-kinase-substrates-all.tsv",
                       col_names = c("kinase", "site.id", "seq.win")) %>%
    prepare_kinsub(Y_kinases, y_regexp) %>%
    mutate(int.id = paste(kinase, site.id))
linkphinder <- read_tsv("data/linkphinder.tsv")
## phoproteome_subset <- read_tsv("data/human-phosphoproteome-randsubset.tsv",
##                   col_names = c("site.id", "seq.win")) %>%
##     prepare_kinsub(Y_kinases, y_regexp)
phoproteome <- read_tsv("data/human-phosphoproteome.tsv",
                  col_names = c("site.id", "seq.win")) %>%
    filter(site.id %in% linkphinder$site.id) %>%
    prepare_kinsub(Y_kinases, y_regexp)

## Get all the Protmapper assignments that are not in PSP
protmapper_val <- filter(protmapper,
                         !(int.id %in% psp$int.id))

## Get the S/T and Y kinases that are assigned to these sites.  We're
## going to use the same kinases in the same proportions.
protmapper_st_kins <- filter(protmapper_val, kinase.type == "S/T") %>%
    pull(kinase) %>% sample()
protmapper_y_kins <- filter(protmapper_val, kinase.type == "Y") %>%
    pull(kinase) %>% sample()
n_st <- length(protmapper_st_kins)
n_y <- length(protmapper_y_kins)

## Get a matching number of S/T and Y sites from the Phoproteome data set
## and randomly assign them to the appropriate kinases.
phoproteome_val_st <- filter(phoproteome, seq.type == "S/T") %>%
    slice_sample(n = n_st) %>%
    mutate(kinase = protmapper_st_kins)
phoproteome_val_y <- filter(phoproteome, seq.type == "Y") %>%
    slice_sample(n = n_y) %>%
    mutate(kinase = protmapper_y_kins)

## Create the final data set
phoproteome_val <- bind_rows(phoproteome_val_st, phoproteome_val_y) %>%
    bind_rows(protmapper_val) %>%
    select(kinase, site.id, seq.win) %>%
    slice(sample(1:n())) %>%
    write_tsv("data/pmapperval-kinase-substrates.tsv", col_names = FALSE)
