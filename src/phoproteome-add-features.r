library(tidyverse)

site_in_domain <- Vectorize(function(protein, site){
    pfam_prot <- filter(pfam, `seq id` == protein) %>%
        filter(`envelope start` <= site,
               `envelope end` >= site)
    return (nrow(pfam_prot) > 0)
})

kin_ints_tbl <- read_tsv("data/human-kinase-interactions.tsv", col_names = c("prot1", "prot2"))
kin_ints <- unique(apply(kin_ints_tbl, 1,
                         function(x) paste(sort(x), collapse = " ")))

pfam <- read_tsv("data/pfam-human-domains.tsv")
kinases <- readLines("data/kinases.txt")
phoproteome <- read_tsv("data/human-phosphoproteome.tsv",
                  col_names = c("substrate.id.site", "seq.win"))
phoproteome_feat <- separate(phoproteome, col = substrate.id.site, into = c("substrate", "site"),
                       sep = "-[STY]", remove = FALSE, convert = TRUE) %>%
    mutate(in.domain = site_in_domain(substrate, site))

for (kin in kinases){
    out_file <- paste0("data/human-phospho-kinase-features/phoproteome-", kin, "-feats.tsv")
    if (file.exists(out_file)){
        message(paste("Skipping", kin, "because its file already exists"))
        next
    }
    phoproteome_kin <- phoproteome_feat
    phoproteome_kin$interaction.id <- sapply(phoproteome_kin$substrate,
                                       function(x) paste(sort(c(kin, x)), collapse = " "))
    mutate(phoproteome_kin,
           phys.int = (interaction.id %in% biogrid$interaction.id |
                       interaction.id %in% bioplex$interaction.id)) %>%
        select(substrate.id.site, seq.win, phys.int, in.domain) %>%
        distinct() %>%
        write_tsv(out_file, col_names = FALSE)
}
