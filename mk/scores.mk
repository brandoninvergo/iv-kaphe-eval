SUG_FOLD_KINASE_SCORES_BAYES = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(foreach K,$(KINASES),$(OUTDIR)/sugiyama-folds/fold-$(F)/bayes/$(K)-sug-fold-scores.tsv))
SUG_FOLD_KINASE_SCORES_BAYESP = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(foreach K,$(KINASES),$(OUTDIR)/sugiyama-folds/fold-$(F)/bayes-plus/$(K)-sug-fold-scores.tsv))
SUG_FOLD_KINASE_NORM_SCORES_GLOBAL = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(foreach K,$(KINASES),$(OUTDIR)/sugiyama-folds/fold-$(F)/global-bg/$(K)-sug-fold-scores.tsv))
SUG_FOLD_KINASE_NORM_SCORES_FREQ = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(foreach K,$(KINASES),$(OUTDIR)/sugiyama-folds/fold-$(F)/freq/$(K)-sug-fold-scores.tsv))
SUG_FOLD_KINASE_NORM_SCORES_POS = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(foreach K,$(KINASES),$(OUTDIR)/sugiyama-folds/fold-$(F)/pos-bg/$(K)-sug-fold-scores.tsv))
SUG_FOLD_SCORES_BAYES = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(OUTDIR)/sugiyama-folds/fold-$(F)/sug-fold-scores-bayes.tsv)
SUG_FOLD_SCORES_BAYESP = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(OUTDIR)/sugiyama-folds/fold-$(F)/sug-fold-scores-bayes-plus.tsv)
SUG_FOLD_NORM_SCORES_GLOBAL = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(OUTDIR)/sugiyama-folds/fold-$(F)/sug-fold-scores-glob.tsv)
SUG_FOLD_NORM_SCORES_FREQ = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(OUTDIR)/sugiyama-folds/fold-$(F)/sug-fold-scores-freq.tsv)
SUG_FOLD_NORM_SCORES_POS = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(OUTDIR)/sugiyama-folds/fold-$(F)/sug-fold-scores-pos.tsv)
SUG_FOLD_CV = $(foreach F,$(shell seq 1 $(ML_K_FOLDS)),$(OUTDIR)/sugiyama-folds/fold-$(F)/sug-fold-cv.tsv)

PSP_KINASE_SCORES_GLOBAL = $(foreach K,$(KINASES),$(PSP_OUTDIR)/global-bg/$(K)-psp-scores.tsv)
PSP_KINASE_SCORES_FREQ = $(foreach K,$(KINASES),$(PSP_OUTDIR)/freq/$(K)-psp-scores.tsv)
PSP_KINASE_SCORES_POS = $(foreach K,$(KINASES),$(PSP_OUTDIR)/pos-bg/$(K)-psp-scores.tsv)
PSP_KINASE_SCORES_BAYES = $(foreach K,$(KINASES),$(PSP_OUTDIR)/bayes/$(K)-psp-scores.tsv)
PSP_KINASE_SCORES_BAYESP = $(foreach K,$(KINASES),$(PSP_OUTDIR)/bayes-plus/$(K)-psp-scores.tsv)
PSP_KINASE_NORM_SCORES_GLOBAL = $(foreach K,$(KINASES),$(PSP_NORM_OUTDIR)/global-bg/$(K)-psp-scores.tsv)
PSP_KINASE_NORM_SCORES_FREQ = $(foreach K,$(KINASES),$(PSP_NORM_OUTDIR)/freq/$(K)-psp-scores.tsv)
PSP_KINASE_NORM_SCORES_POS = $(foreach K,$(KINASES),$(PSP_NORM_OUTDIR)/pos-bg/$(K)-psp-scores.tsv)
PSP_SCORES_GLOBAL = $(PSP_OUTDIR)/psp-scores-glob.tsv
PSP_SCORES_FREQ = $(PSP_OUTDIR)/psp-scores-freq.tsv
PSP_SCORES_BAYES = $(PSP_OUTDIR)/psp-scores-bayes.tsv
PSP_SCORES_BAYESP = $(PSP_OUTDIR)/psp-scores-bayes-plus.tsv
PSP_SCORES_POS = $(PSP_OUTDIR)/psp-scores-pos.tsv
PSP_NORM_SCORES_GLOBAL = $(PSP_NORM_OUTDIR)/psp-scores-glob.tsv
PSP_NORM_SCORES_FREQ = $(PSP_NORM_OUTDIR)/psp-scores-freq.tsv
PSP_NORM_SCORES_POS = $(PSP_NORM_OUTDIR)/psp-scores-pos.tsv

PMAPPER_KINASE_SCORES_GLOBAL = $(foreach K,$(KINASES),$(PMAPPER_OUTDIR)/global-bg/$(K)-protmapper-scores.tsv)
PMAPPER_KINASE_SCORES_FREQ = $(foreach K,$(KINASES),$(PMAPPER_OUTDIR)/freq/$(K)-protmapper-scores.tsv)
PMAPPER_KINASE_SCORES_POS = $(foreach K,$(KINASES),$(PMAPPER_OUTDIR)/pos-bg/$(K)-protmapper-scores.tsv)
PMAPPER_KINASE_SCORES_BAYES = $(foreach K,$(KINASES),$(PMAPPER_OUTDIR)/bayes/$(K)-protmapper-scores.tsv)
PMAPPER_KINASE_SCORES_BAYESP = $(foreach K,$(KINASES),$(PMAPPER_OUTDIR)/bayes-plus/$(K)-protmapper-scores.tsv)
PMAPPER_KINASE_NORM_SCORES_GLOBAL = $(foreach K,$(KINASES),$(PMAPPER_NORM_OUTDIR)/global-bg/$(K)-protmapper-scores.tsv)
PMAPPER_KINASE_NORM_SCORES_FREQ = $(foreach K,$(KINASES),$(PMAPPER_NORM_OUTDIR)/freq/$(K)-protmapper-scores.tsv)
PMAPPER_KINASE_NORM_SCORES_POS = $(foreach K,$(KINASES),$(PMAPPER_NORM_OUTDIR)/pos-bg/$(K)-protmapper-scores.tsv)
PMAPPER_SCORES_GLOBAL = $(PMAPPER_OUTDIR)/protmapper-scores-glob.tsv
PMAPPER_SCORES_FREQ = $(PMAPPER_OUTDIR)/protmapper-scores-freq.tsv
PMAPPER_SCORES_BAYES = $(PMAPPER_OUTDIR)/protmapper-scores-bayes.tsv
PMAPPER_SCORES_BAYESP = $(PMAPPER_OUTDIR)/protmapper-scores-bayes-plus.tsv
PMAPPER_SCORES_POS = $(PMAPPER_OUTDIR)/protmapper-scores-pos.tsv
PMAPPER_NORM_SCORES_GLOBAL = $(PMAPPER_NORM_OUTDIR)/protmapper-scores-glob.tsv
PMAPPER_NORM_SCORES_FREQ = $(PMAPPER_NORM_OUTDIR)/protmapper-scores-freq.tsv
PMAPPER_NORM_SCORES_POS = $(PMAPPER_NORM_OUTDIR)/protmapper-scores-pos.tsv

PMAPPERVAL_KINASE_SCORES_GLOBAL = $(foreach K,$(KINASES),$(PMAPPERVAL_OUTDIR)/global-bg/$(K)-pmapperval-scores.tsv)
PMAPPERVAL_KINASE_SCORES_FREQ = $(foreach K,$(KINASES),$(PMAPPERVAL_OUTDIR)/freq/$(K)-pmapperval-scores.tsv)
PMAPPERVAL_KINASE_SCORES_POS = $(foreach K,$(KINASES),$(PMAPPERVAL_OUTDIR)/pos-bg/$(K)-pmapperval-scores.tsv)
PMAPPERVAL_KINASE_SCORES_BAYES = $(foreach K,$(KINASES),$(PMAPPERVAL_OUTDIR)/bayes/$(K)-pmapperval-scores.tsv)
PMAPPERVAL_KINASE_SCORES_BAYESP = $(foreach K,$(KINASES),$(PMAPPERVAL_OUTDIR)/bayes-plus/$(K)-pmapperval-scores.tsv)
PMAPPERVAL_KINASE_NORM_SCORES_GLOBAL = $(foreach K,$(KINASES),$(PMAPPERVAL_NORM_OUTDIR)/global-bg/$(K)-pmapperval-scores.tsv)
PMAPPERVAL_KINASE_NORM_SCORES_FREQ = $(foreach K,$(KINASES),$(PMAPPERVAL_NORM_OUTDIR)/freq/$(K)-pmapperval-scores.tsv)
PMAPPERVAL_KINASE_NORM_SCORES_POS = $(foreach K,$(KINASES),$(PMAPPERVAL_NORM_OUTDIR)/pos-bg/$(K)-pmapperval-scores.tsv)
PMAPPERVAL_SCORES_GLOBAL = $(PMAPPERVAL_OUTDIR)/pmapperval-scores-glob.tsv
PMAPPERVAL_SCORES_FREQ = $(PMAPPERVAL_OUTDIR)/pmapperval-scores-freq.tsv
PMAPPERVAL_SCORES_BAYES = $(PMAPPERVAL_OUTDIR)/pmapperval-scores-bayes.tsv
PMAPPERVAL_SCORES_BAYESP = $(PMAPPERVAL_OUTDIR)/pmapperval-scores-bayes-plus.tsv
PMAPPERVAL_SCORES_POS = $(PMAPPERVAL_OUTDIR)/pmapperval-scores-pos.tsv
PMAPPERVAL_SCORES_RF = $(PMAPPERVAL_OUTDIR)/pmapperval-scores-randforest.tsv
PMAPPERVAL_NORM_SCORES_GLOBAL = $(PMAPPERVAL_NORM_OUTDIR)/pmapperval-scores-glob.tsv
PMAPPERVAL_NORM_SCORES_FREQ = $(PMAPPERVAL_NORM_OUTDIR)/pmapperval-scores-freq.tsv
PMAPPERVAL_NORM_SCORES_POS = $(PMAPPERVAL_NORM_OUTDIR)/pmapperval-scores-pos.tsv

PHOPROTEOME_KINASE_SCORES_GLOBAL = $(foreach K,$(KINASES),$(PHOPROTEOME_OUTDIR)/global-bg/$(K)-phoproteome-scores.tsv)
PHOPROTEOME_KINASE_SCORES_FREQ = $(foreach K,$(KINASES),$(PHOPROTEOME_OUTDIR)/freq/$(K)-phoproteome-scores.tsv)
PHOPROTEOME_KINASE_SCORES_POS = $(foreach K,$(KINASES),$(PHOPROTEOME_OUTDIR)/pos-bg/$(K)-phoproteome-scores.tsv)
PHOPROTEOME_KINASE_SCORES_BAYES = $(foreach K,$(KINASES),$(PHOPROTEOME_OUTDIR)/bayes/$(K)-phoproteome-scores.tsv)
PHOPROTEOME_KINASE_SCORES_BAYESP = $(foreach K,$(KINASES),$(PHOPROTEOME_OUTDIR)/bayes-plus/$(K)-phoproteome-scores.tsv)
PHOPROTEOME_KINASE_NORM_SCORES_GLOBAL = $(foreach K,$(KINASES),$(PHOPROTEOME_NORM_OUTDIR)/global-bg/$(K)-phoproteome-scores.tsv)
PHOPROTEOME_KINASE_NORM_SCORES_FREQ = $(foreach K,$(KINASES),$(PHOPROTEOME_NORM_OUTDIR)/freq/$(K)-phoproteome-scores.tsv)
PHOPROTEOME_KINASE_NORM_SCORES_POS = $(foreach K,$(KINASES),$(PHOPROTEOME_NORM_OUTDIR)/pos-bg/$(K)-phoproteome-scores.tsv)
PHOPROTEOME_SCORES_GLOBAL = $(PHOPROTEOME_OUTDIR)/phoproteome-scores-glob.tsv
PHOPROTEOME_SCORES_FREQ = $(PHOPROTEOME_OUTDIR)/phoproteome-scores-freq.tsv
PHOPROTEOME_SCORES_BAYES = $(PHOPROTEOME_OUTDIR)/phoproteome-scores-bayes.tsv
PHOPROTEOME_SCORES_BAYESP = $(PHOPROTEOME_OUTDIR)/phoproteome-scores-bayes-plus.tsv
PHOPROTEOME_SCORES_POS = $(PHOPROTEOME_OUTDIR)/phoproteome-scores-pos.tsv
PHOPROTEOME_SCORES_RF = $(PHOPROTEOME_OUTDIR)/phoproteome-scores-randforest.tsv
PHOPROTEOME_NORM_SCORES_GLOBAL = $(PHOPROTEOME_NORM_OUTDIR)/phoproteome-scores-glob.tsv
PHOPROTEOME_NORM_SCORES_FREQ = $(PHOPROTEOME_NORM_OUTDIR)/phoproteome-scores-freq.tsv
PHOPROTEOME_NORM_SCORES_POS = $(PHOPROTEOME_NORM_OUTDIR)/phoproteome-scores-pos.tsv

WILKES_INHIB_KINASE_SCORES_GLOBAL = $(foreach K,$(KINASES),$(WILKES_INHIB_OUTDIR)/global-bg/$(K)-wilkes-inhib-scores.tsv)
WILKES_INHIB_KINASE_SCORES_FREQ = $(foreach K,$(KINASES),$(WILKES_INHIB_OUTDIR)/freq/$(K)-wilkes-inhib-scores.tsv)
WILKES_INHIB_KINASE_SCORES_POS = $(foreach K,$(KINASES),$(WILKES_INHIB_OUTDIR)/pos-bg/$(K)-wilkes-inhib-scores.tsv)
WILKES_INHIB_KINASE_SCORES_BAYES = $(foreach K,$(KINASES),$(WILKES_INHIB_OUTDIR)/bayes/$(K)-wilkes-inhib-scores.tsv)
WILKES_INHIB_KINASE_SCORES_BAYESP = $(foreach K,$(KINASES),$(WILKES_INHIB_OUTDIR)/bayes-plus/$(K)-wilkes-inhib-scores.tsv)
WILKES_INHIB_KINASE_NORM_SCORES_GLOBAL = $(foreach K,$(KINASES),$(WILKES_INHIB_NORM_OUTDIR)/global-bg/$(K)-wilkes-inhib-scores.tsv)
WILKES_INHIB_KINASE_NORM_SCORES_FREQ = $(foreach K,$(KINASES),$(WILKES_INHIB_NORM_OUTDIR)/freq/$(K)-wilkes-inhib-scores.tsv)
WILKES_INHIB_KINASE_NORM_SCORES_POS = $(foreach K,$(KINASES),$(WILKES_INHIB_NORM_OUTDIR)/pos-bg/$(K)-wilkes-inhib-scores.tsv)
WILKES_INHIB_SCORES_GLOBAL = $(WILKES_INHIB_OUTDIR)/wilkes-inhib-scores-glob.tsv
WILKES_INHIB_SCORES_FREQ = $(WILKES_INHIB_OUTDIR)/wilkes-inhib-scores-freq.tsv
WILKES_INHIB_SCORES_BAYES = $(WILKES_INHIB_OUTDIR)/wilkes-inhib-scores-bayes.tsv
WILKES_INHIB_SCORES_BAYESP = $(WILKES_INHIB_OUTDIR)/wilkes-inhib-scores-bayes-plus.tsv
WILKES_INHIB_SCORES_POS = $(WILKES_INHIB_OUTDIR)/wilkes-inhib-scores-pos.tsv
WILKES_INHIB_SCORES_RF = $(WILKES_INHIB_OUTDIR)/wilkes-inhib-scores-randforest.tsv
WILKES_INHIB_NORM_SCORES_GLOBAL = $(WILKES_INHIB_NORM_OUTDIR)/wilkes-inhib-scores-glob.tsv
WILKES_INHIB_NORM_SCORES_FREQ = $(WILKES_INHIB_NORM_OUTDIR)/wilkes-inhib-scores-freq.tsv
WILKES_INHIB_NORM_SCORES_POS = $(WILKES_INHIB_NORM_OUTDIR)/wilkes-inhib-scores-pos.tsv

SIM_KINASE_SCORES_GLOBAL = $(foreach K,$(KINASES),$(SIM_OUTDIR)/global-bg/$(K)-sim-scores.tsv)
SIM_KINASE_SCORES_FREQ = $(foreach K,$(KINASES),$(SIM_OUTDIR)/freq/$(K)-sim-scores.tsv)
SIM_KINASE_SCORES_POS = $(foreach K,$(KINASES),$(SIM_OUTDIR)/pos-bg/$(K)-sim-scores.tsv)
SIM_KINASE_SCORES_BAYES = $(foreach K,$(KINASES),$(SIM_OUTDIR)/bayes/$(K)-sim-scores.tsv)
SIM_KINASE_SCORES_BAYESP = $(foreach K,$(KINASES),$(SIM_OUTDIR)/bayes-plus/$(K)-sim-scores.tsv)
SIM_KINASE_NORM_SCORES_GLOBAL = $(foreach K,$(KINASES),$(SIM_NORM_OUTDIR)/global-bg/$(K)-sim-scores.tsv)
SIM_KINASE_NORM_SCORES_FREQ = $(foreach K,$(KINASES),$(SIM_NORM_OUTDIR)/freq/$(K)-sim-scores.tsv)
SIM_KINASE_NORM_SCORES_POS = $(foreach K,$(KINASES),$(SIM_NORM_OUTDIR)/pos-bg/$(K)-sim-scores.tsv)
SIM_SCORES_GLOBAL = $(SIM_OUTDIR)/sim-scores-glob.tsv
SIM_SCORES_FREQ = $(SIM_OUTDIR)/sim-scores-freq.tsv
SIM_SCORES_BAYES = $(SIM_OUTDIR)/sim-scores-bayes.tsv
SIM_SCORES_BAYESP = $(SIM_OUTDIR)/sim-scores-bayes-plus.tsv
SIM_SCORES_POS = $(SIM_OUTDIR)/sim-scores-pos.tsv
SIM_NORM_SCORES_GLOBAL = $(SIM_NORM_OUTDIR)/sim-scores-glob.tsv
SIM_NORM_SCORES_FREQ = $(SIM_NORM_OUTDIR)/sim-scores-freq.tsv
SIM_NORM_SCORES_POS = $(SIM_NORM_OUTDIR)/sim-scores-pos.tsv

.PHONY: scores
scores: psp protmapper phoproteome wilkes-inhib sim

.PHONY: sugiyama-fold
sugiyama-fold: $(SUG_FOLD_SCORES_BAYES) $(SUG_FOLD_SCORES_BAYESP) \
	$(SUG_FOLD_NORM_SCORES_GLOBAL) $(SUG_FOLD_NORM_SCORES_FREQ) \
	$(SUG_FOLD_NORM_SCORES_POS)

.PHONY: psp
psp: $(PSP_SCORES_GLOBAL) $(PSP_SCORES_FREQ) $(PSP_SCORES_BAYES)	\
	$(PSP_SCORES_BAYESP) $(PSP_SCORES_POS)				\
	$(PSP_NORM_SCORES_GLOBAL) $(PSP_NORM_SCORES_FREQ)		\
	$(PSP_NORM_SCORES_POS)

.PHONY: protmapper
protmapper: $(PMAPPER_SCORES_GLOBAL) $(PMAPPER_SCORES_FREQ)	\
	$(PMAPPER_SCORES_BAYES) $(PMAPPER_SCORES_BAYESP)	\
	$(PMAPPER_SCORES_POS) $(PMAPPER_NORM_SCORES_GLOBAL)	\
	$(PMAPPER_NORM_SCORES_FREQ) $(PMAPPER_NORM_SCORES_POS)

.PHONY: pmapperval
pmapperval: $(PMAPPERVAL_SCORES_GLOBAL) $(PMAPPERVAL_SCORES_FREQ)	\
	$(PMAPPERVAL_SCORES_BAYES) $(PMAPPERVAL_SCORES_BAYESP)	\
	$(PMAPPERVAL_SCORES_POS) $(PMAPPERVAL_NORM_SCORES_GLOBAL)	\
	$(PMAPPERVAL_NORM_SCORES_FREQ) $(PMAPPERVAL_NORM_SCORES_POS)

.PHONY: phoproteome
phoproteome: $(PHOPROTEOME_SCORES_GLOBAL) $(PHOPROTEOME_SCORES_FREQ)		\
	$(PHOPROTEOME_SCORES_BAYES) $(PHOPROTEOME_SCORES_BAYESP)		\
	$(PHOPROTEOME_SCORES_POS) $(PHOPROTEOME_NORM_SCORES_GLOBAL)		\
	$(PHOPROTEOME_NORM_SCORES_FREQ) $(PHOPROTEOME_NORM_SCORES_POS)

.PHONY: wilkes-inhib
wilkes-inhib: $(WILKES_INHIB_SCORES_GLOBAL)				\
	$(WILKES_INHIB_SCORES_FREQ) $(WILKES_INHIB_SCORES_BAYES)	\
	$(WILKES_INHIB_SCORES_BAYESP) $(WILKES_INHIB_SCORES_POS)	\
	$(WILKES_INHIB_NORM_SCORES_GLOBAL)				\
	$(WILKES_INHIB_NORM_SCORES_FREQ)				\
	$(WILKES_INHIB_NORM_SCORES_POS)

.PHONY: sim
sim: $(SIM_SCORES_GLOBAL) $(SIM_SCORES_FREQ) $(SIM_SCORES_BAYES)	\
	$(SIM_SCORES_BAYESP) $(SIM_SCORES_POS)				\
	$(SIM_NORM_SCORES_GLOBAL) $(SIM_NORM_SCORES_FREQ)		\
	$(SIM_NORM_SCORES_POS)

.PHONY: clean-psp-scores
clean-psp-scores:
	-rm $(PSP_SCORES_GLOBAL)
	-rm $(PSP_SCORES_FREQ)
	-rm $(PSP_SCORES_POS)
	-rm $(PSP_SCORES_BAYES)
	-rm $(PSP_SCORES_BAYESP)
	-rm $(PSP_NORM_SCORES_GLOBAL)
	-rm $(PSP_NORM_SCORES_FREQ)
	-rm $(PSP_NORM_SCORES_POS)

.PHONY: clean-protmapper-scores
clean-protmapper-scores:
	-rm $(PMAPPER_SCORES_GLOBAL)
	-rm $(PMAPPER_SCORES_FREQ)
	-rm $(PMAPPER_SCORES_POS)
	-rm $(PMAPPER_SCORES_BAYES)
	-rm $(PMAPPER_SCORES_BAYESP)
	-rm $(PMAPPER_NORM_SCORES_GLOBAL)
	-rm $(PMAPPER_NORM_SCORES_FREQ)
	-rm $(PMAPPER_NORM_SCORES_POS)

.PHONY: clean-phoproteome-scores
clean-phoproteome-scores:
	-rm $(PHOPROTEOME_SCORES_GLOBAL)
	-rm $(PHOPROTEOME_SCORES_FREQ)
	-rm $(PHOPROTEOME_SCORES_POS)
	-rm $(PHOPROTEOME_SCORES_BAYES)
	-rm $(PHOPROTEOME_SCORES_BAYESP)
	-rm $(PHOPROTEOME_NORM_SCORES_GLOBAL)
	-rm $(PHOPROTEOME_NORM_SCORES_FREQ)
	-rm $(PHOPROTEOME_NORM_SCORES_POS)

.PHONY: clean-wilkes-scores
clean-wilkes-scores:
	-rm $(WILKES_INHIB_SCORES_GLOBAL)
	-rm $(WILKES_INHIB_SCORES_FREQ)
	-rm $(WILKES_INHIB_SCORES_POS)
	-rm $(WILKES_INHIB_SCORES_BAYES)
	-rm $(WILKES_INHIB_SCORES_BAYESP)
	-rm $(WILKES_INHIB_NORM_SCORES_GLOBAL)
	-rm $(WILKES_INHIB_NORM_SCORES_FREQ)
	-rm $(WILKES_INHIB_NORM_SCORES_POS)

.PHONY: clean-sim-scores
clean-sim-scores:
	-rm $(SIM_SCORES_GLOBAL)
	-rm $(SIM_SCORES_FREQ)
	-rm $(SIM_SCORES_POS)
	-rm $(SIM_SCORES_BAYES)
	-rm $(SIM_SCORES_BAYESP)
	-rm $(SIM_NORM_SCORES_GLOBAL)
	-rm $(SIM_NORM_SCORES_FREQ)
	-rm $(SIM_NORM_SCORES_POS)

.PHONY: clean-scores
clean-scores: clean-psp-scores clean-protmapper-scores clean-phoproteome-scores clean-wilkes-scores clean-sim-scores

$(OUTDIR)/sugiyama-folds/fold-1/global-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/global-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-1/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-2/global-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/global-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-2/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-3/global-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/global-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-3/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-4/global-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/global-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-4/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-5/global-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/global-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-5/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-6/global-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/global-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-6/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-7/global-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/global-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-7/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-8/global-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/global-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-8/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-9/global-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/global-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-9/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-10/global-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/global-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-10/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-1/pos-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/pos-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-1/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-2/pos-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/pos-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-2/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-3/pos-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/pos-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-3/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-4/pos-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/pos-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-4/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-5/pos-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/pos-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-5/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-6/pos-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/pos-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-6/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-7/pos-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/pos-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-7/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-8/pos-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/pos-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-8/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-9/pos-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/pos-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-9/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-10/pos-bg/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/pos-bg/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-10/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-1/freq/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/freq/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-1/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-2/freq/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/freq/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-2/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-3/freq/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/freq/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-3/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-4/freq/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/freq/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-4/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-5/freq/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/freq/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-5/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-6/freq/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/freq/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-6/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-7/freq/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/freq/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-7/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-8/freq/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/freq/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-8/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-9/freq/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/freq/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-9/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-10/freq/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/freq/%-motif.h5 \
		$(DATADIR)/sugiyama-folds/fold-10/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-1/bayes/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-1/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-2/bayes/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-2/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-3/bayes/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-3/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-4/bayes/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-4/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-5/bayes/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-5/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-6/bayes/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-6/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-7/bayes/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-7/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-8/bayes/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-8/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-9/bayes/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-9/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-10/bayes/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-10/test/sugiyama-test-seqwins.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-1/bayes-plus/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes-plus/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes-plus/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-1/test/kinase-features/sugiyama-%-feats.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --feats-col=3 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-2/bayes-plus/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes-plus/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes-plus/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-2/test/kinase-features/sugiyama-%-feats.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --feats-col=3 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-3/bayes-plus/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes-plus/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes-plus/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-3/test/kinase-features/sugiyama-%-feats.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --feats-col=3 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-4/bayes-plus/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes-plus/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes-plus/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-4/test/kinase-features/sugiyama-%-feats.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --feats-col=3 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-5/bayes-plus/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes-plus/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes-plus/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-5/test/kinase-features/sugiyama-%-feats.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --feats-col=3 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-6/bayes-plus/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes-plus/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes-plus/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-6/test/kinase-features/sugiyama-%-feats.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --feats-col=3 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-7/bayes-plus/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes-plus/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes-plus/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-7/test/kinase-features/sugiyama-%-feats.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --feats-col=3 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-8/bayes-plus/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes-plus/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes-plus/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-8/test/kinase-features/sugiyama-%-feats.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --feats-col=3 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-9/bayes-plus/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes-plus/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes-plus/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-9/test/kinase-features/sugiyama-%-feats.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --feats-col=3 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-10/bayes-plus/%-sug-fold-scores.tsv: \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes-plus/%-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes-plus/%-compl.h5 \
		$(DATADIR)/sugiyama-folds/fold-10/test/kinase-features/sugiyama-%-feats.tsv
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --header --scale=relh --append --seq-col=2 --feats-col=3 --pvals=$(PVAL_PREC) $^ | \
		$(AWK) '{if (NR > 1){print "$*", $$0}}' >$@

$(OUTDIR)/sugiyama-folds/fold-%/sug-fold-scores-bayes.tsv: $(foreach K,$(KINASES),$(OUTDIR)/sugiyama-folds/fold-%/bayes/$(K)-sug-fold-scores.tsv)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(OUTDIR)/sugiyama-folds/fold-%/sug-fold-scores-bayes-plus.tsv: $(foreach K,$(KINASES),$(OUTDIR)/sugiyama-folds/fold-%/bayes-plus/$(K)-sug-fold-scores.tsv)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print $$1, $$2, $$3, $$8}' >$@

$(OUTDIR)/sugiyama-folds/fold-%/sug-fold-scores-glob.tsv: $(foreach K,$(KINASES),$(OUTDIR)/sugiyama-folds/fold-%/global-bg/$(K)-sug-fold-scores.tsv)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(OUTDIR)/sugiyama-folds/fold-%/sug-fold-scores-freq.tsv: $(foreach K,$(KINASES),$(OUTDIR)/sugiyama-folds/fold-%/freq/$(K)-sug-fold-scores.tsv)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(OUTDIR)/sugiyama-folds/fold-%/sug-fold-scores-pos.tsv: $(foreach K,$(KINASES),$(OUTDIR)/sugiyama-folds/fold-%/pos-bg/$(K)-sug-fold-scores.tsv)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PSP_OUTDIR)/global-bg/%-psp-scores.tsv: $(PSP_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 $(PSP_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PSP_OUTDIR)/freq/%-psp-scores.tsv: $(PSP_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --append --scale=relh --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 $(PSP_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PSP_OUTDIR)/pos-bg/%-psp-scores.tsv: $(PSP_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 $(PSP_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PSP_OUTDIR)/bayes/%-psp-scores.tsv: $(PSP_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-compl.h5 $(PSP_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PSP_OUTDIR)/bayes-plus/%-psp-scores.tsv: $(PSP_FEAT_DIR)/psp-%-feats.tsv \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --feats-col=3 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-compl.h5 $< | \
		$(AWK) '{print "$*", $$0}' >$@

$(PSP_NORM_OUTDIR)/global-bg/%-psp-scores.tsv: $(PSP_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 $(PSP_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PSP_NORM_OUTDIR)/freq/%-psp-scores.tsv: $(PSP_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --append --scale=relh --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 $(PSP_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PSP_NORM_OUTDIR)/pos-bg/%-psp-scores.tsv: $(PSP_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 $(PSP_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PSP_SCORES_GLOBAL): $(PSP_KINASE_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PSP_SCORES_FREQ): $(PSP_KINASE_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PSP_SCORES_POS): $(PSP_KINASE_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PSP_SCORES_BAYES): $(PSP_KINASE_SCORES_BAYES)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PSP_SCORES_BAYESP): $(PSP_KINASE_SCORES_BAYESP)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print $$1, $$2, $$3, $$8}' >$@

$(PSP_NORM_SCORES_GLOBAL): $(PSP_KINASE_NORM_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PSP_NORM_SCORES_FREQ): $(PSP_KINASE_NORM_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PSP_NORM_SCORES_POS): $(PSP_KINASE_NORM_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPER_OUTDIR)/global-bg/%-protmapper-scores.tsv: $(PMAPPER_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 $(PMAPPER_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPER_OUTDIR)/freq/%-protmapper-scores.tsv: $(PMAPPER_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --append --scale=relh --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 $(PMAPPER_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPER_OUTDIR)/pos-bg/%-protmapper-scores.tsv: $(PMAPPER_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 $(PMAPPER_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPER_OUTDIR)/bayes/%-protmapper-scores.tsv: $(PMAPPER_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-compl.h5 $(PMAPPER_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPER_OUTDIR)/bayes-plus/%-protmapper-scores.tsv: $(PMAPPER_FEAT_DIR)/protmapper-%-feats.tsv \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --feats-col=3 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-compl.h5 $< | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPER_NORM_OUTDIR)/global-bg/%-protmapper-scores.tsv: $(PMAPPER_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 $(PMAPPER_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPER_NORM_OUTDIR)/freq/%-protmapper-scores.tsv: $(PMAPPER_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --append --scale=relh --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 $(PMAPPER_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPER_NORM_OUTDIR)/pos-bg/%-protmapper-scores.tsv: $(PMAPPER_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 $(PMAPPER_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPER_SCORES_GLOBAL): $(PMAPPER_KINASE_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPER_SCORES_FREQ): $(PMAPPER_KINASE_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPER_SCORES_POS): $(PMAPPER_KINASE_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPER_SCORES_BAYES): $(PMAPPER_KINASE_SCORES_BAYES)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPER_SCORES_BAYESP): $(PMAPPER_KINASE_SCORES_BAYESP)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print $$1, $$2, $$3, $$8}' >$@

$(PMAPPER_NORM_SCORES_GLOBAL): $(PMAPPER_KINASE_NORM_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPER_NORM_SCORES_FREQ): $(PMAPPER_KINASE_NORM_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPER_NORM_SCORES_POS): $(PMAPPER_KINASE_NORM_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPERVAL_OUTDIR)/global-bg/%-pmapperval-scores.tsv: $(PMAPPERVAL_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 $(PMAPPERVAL_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPERVAL_OUTDIR)/freq/%-pmapperval-scores.tsv: $(PMAPPERVAL_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --append --scale=relh --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 $(PMAPPERVAL_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPERVAL_OUTDIR)/pos-bg/%-pmapperval-scores.tsv: $(PMAPPERVAL_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 $(PMAPPERVAL_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPERVAL_OUTDIR)/bayes/%-pmapperval-scores.tsv: $(PMAPPERVAL_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-compl.h5 $(PMAPPERVAL_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPERVAL_OUTDIR)/bayes-plus/%-pmapperval-scores.tsv: $(PMAPPERVAL_FEAT_DIR)/pmapperval-%-feats.tsv \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --feats-col=3 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-compl.h5 $< | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPERVAL_NORM_OUTDIR)/global-bg/%-pmapperval-scores.tsv: $(PMAPPERVAL_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 $(PMAPPERVAL_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPERVAL_NORM_OUTDIR)/freq/%-pmapperval-scores.tsv: $(PMAPPERVAL_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --append --scale=relh --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 $(PMAPPERVAL_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPERVAL_NORM_OUTDIR)/pos-bg/%-pmapperval-scores.tsv: $(PMAPPERVAL_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 $(PMAPPERVAL_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PMAPPERVAL_SCORES_GLOBAL): $(PMAPPERVAL_KINASE_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPERVAL_SCORES_FREQ): $(PMAPPERVAL_KINASE_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPERVAL_SCORES_POS): $(PMAPPERVAL_KINASE_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPERVAL_SCORES_BAYES): $(PMAPPERVAL_KINASE_SCORES_BAYES)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPERVAL_SCORES_BAYESP): $(PMAPPERVAL_KINASE_SCORES_BAYESP)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print $$1, $$2, $$3, $$8}' >$@

$(PMAPPERVAL_NORM_SCORES_GLOBAL): $(PMAPPERVAL_KINASE_NORM_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPERVAL_NORM_SCORES_FREQ): $(PMAPPERVAL_KINASE_NORM_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PMAPPERVAL_NORM_SCORES_POS): $(PMAPPERVAL_KINASE_NORM_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PHOPROTEOME_OUTDIR)/global-bg/%-phoproteome-scores.tsv: $(PHOPROTEOME_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 $(PHOPROTEOME_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PHOPROTEOME_OUTDIR)/freq/%-phoproteome-scores.tsv: $(PHOPROTEOME_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 $(PHOPROTEOME_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PHOPROTEOME_OUTDIR)/pos-bg/%-phoproteome-scores.tsv: $(PHOPROTEOME_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 $(PHOPROTEOME_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PHOPROTEOME_OUTDIR)/bayes/%-phoproteome-scores.tsv: $(PHOPROTEOME) \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-compl.h5 $(PHOPROTEOME) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PHOPROTEOME_OUTDIR)/bayes-plus/%-phoproteome-scores.tsv: $(PHOPROTEOME_FEAT_DIR)/human-phosphoproteome-%-feats.tsv \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --feats-col=3 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-compl.h5 $< | \
		$(AWK) '{print "$*", $$0}' >$@

$(PHOPROTEOME_NORM_OUTDIR)/global-bg/%-phoproteome-scores.tsv: $(PHOPROTEOME_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 $(PHOPROTEOME_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PHOPROTEOME_NORM_OUTDIR)/freq/%-phoproteome-scores.tsv: $(PHOPROTEOME_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --normalize --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 $(PHOPROTEOME_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PHOPROTEOME_NORM_OUTDIR)/pos-bg/%-phoproteome-scores.tsv: $(PHOPROTEOME_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 $(PHOPROTEOME_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(PHOPROTEOME_SCORES_GLOBAL): $(PHOPROTEOME_KINASE_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(PHOPROTEOME_SCORES_FREQ): $(PHOPROTEOME_KINASE_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(PHOPROTEOME_SCORES_POS): $(PHOPROTEOME_KINASE_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(PHOPROTEOME_SCORES_BAYES): $(PHOPROTEOME_KINASE_SCORES_BAYES)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(PHOPROTEOME_SCORES_BAYESP): $(PHOPROTEOME_KINASE_SCORES_BAYESP)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print $$1, $$2, $$3, $$8}' >$@

$(PHOPROTEOME_NORM_SCORES_GLOBAL): $(PHOPROTEOME_KINASE_NORM_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(PHOPROTEOME_NORM_SCORES_FREQ): $(PHOPROTEOME_KINASE_NORM_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(PHOPROTEOME_NORM_SCORES_POS): $(PHOPROTEOME_KINASE_NORM_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(OUTDIR)/pssm-info.tsv $(IMGDIR)/pssm-eval.pdf: $(KINASES_Y_FILE)	\
		$(KINASES_ST_FILE) $(PHOPROTEOME_SCORES_GLOBAL)		\
		$(PHOPROTEOME_SCORES_POS) $(PHOPROTEOME_SCORES_FREQ)		\
		$(PHOPROTEOME_NORM_SCORES_GLOBAL) $(PHOPROTEOME_NORM_SCORES_POS)	\
		$(PHOPROTEOME_NORM_SCORES_FREQ) $(PSSM_EVAL_SCRIPT)
	Rscript $(PSSM_EVAL_SCRIPT) $(SEQWIN_LEN)

$(WILKES_INHIB_OUTDIR)/global-bg/%-wilkes-inhib-scores.tsv: $(WILKES_INHIB) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	cut -f1,2 $< | sed '1d' | $(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) --seq-col=2 \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 | \
		$(AWK) '{print "$*", $$0}' >$@

$(WILKES_INHIB_OUTDIR)/freq/%-wilkes-inhib-scores.tsv: $(WILKES_INHIB) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	cut -f1,2 $< | sed '1d' | $(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) --seq-col=2 \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 | \
		$(AWK) '{print "$*", $$0}' >$@

$(WILKES_INHIB_OUTDIR)/pos-bg/%-wilkes-inhib-scores.tsv: $(WILKES_INHIB) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	cut -f1,2 $< | sed '1d' | $(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) --seq-col=2 \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 | \
		$(AWK) '{print "$*", $$0}' >$@

$(WILKES_INHIB_OUTDIR)/bayes/%-wilkes-inhib-scores.tsv: $(WILKES_INHIB) \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	cut -f1,2 $< | sed '1d' | $(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) --seq-col=2 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-compl.h5 | \
		$(AWK) '{print "$*", $$0}' >$@

$(WILKES_INHIB_OUTDIR)/bayes-plus/%-wilkes-inhib-scores.tsv: $(WILKES_FEAT_DIR)/wilkes-%-feats.tsv \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --seq-col=2 --feats-col=3 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-compl.h5 $< | \
		$(AWK) '{print "$*", $$0}' >$@

$(WILKES_INHIB_NORM_OUTDIR)/global-bg/%-wilkes-inhib-scores.tsv: $(WILKES_INHIB) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	cut -f1,2 $< | sed '1d' | $(MOTIF_SCORE) --normalize --scale=relh --append --pvals=$(PVAL_PREC) --seq-col=2 \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 | \
		$(AWK) '{print "$*", $$0}' >$@

$(WILKES_INHIB_NORM_OUTDIR)/freq/%-wilkes-inhib-scores.tsv: $(WILKES_INHIB) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	cut -f1,2 $< | sed '1d' | $(MOTIF_SCORE) --scale=relh --normalize --append --pvals=$(PVAL_PREC) --seq-col=2 \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 | \
		$(AWK) '{print "$*", $$0}' >$@

$(WILKES_INHIB_NORM_OUTDIR)/pos-bg/%-wilkes-inhib-scores.tsv: $(WILKES_INHIB) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	cut -f1,2 $< | sed '1d' | $(MOTIF_SCORE) --normalize --scale=relh --append --pvals=$(PVAL_PREC) --seq-col=2 \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 | \
		$(AWK) '{print "$*", $$0}' >$@

$(WILKES_INHIB_SCORES_GLOBAL): $(WILKES_INHIB_KINASE_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(WILKES_INHIB_SCORES_FREQ): $(WILKES_INHIB_KINASE_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(WILKES_INHIB_SCORES_POS): $(WILKES_INHIB_KINASE_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(WILKES_INHIB_SCORES_BAYES): $(WILKES_INHIB_KINASE_SCORES_BAYES)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(WILKES_INHIB_SCORES_BAYESP): $(WILKES_INHIB_KINASE_SCORES_BAYESP)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print $$1, $$2, $$3, $$8}' >$@

$(WILKES_INHIB_NORM_SCORES_GLOBAL): $(WILKES_INHIB_KINASE_NORM_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(WILKES_INHIB_NORM_SCORES_FREQ): $(WILKES_INHIB_KINASE_NORM_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

$(WILKES_INHIB_NORM_SCORES_POS): $(WILKES_INHIB_KINASE_NORM_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "site.id", "seq.win", "score"}{print}' >$@

#####

$(SIM_OUTDIR)/global-bg/%-sim-scores.tsv: $(SIM_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 $(SIM_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(SIM_OUTDIR)/freq/%-sim-scores.tsv: $(SIM_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 $(SIM_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(SIM_OUTDIR)/pos-bg/%-sim-scores.tsv: $(SIM_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 $(SIM_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(SIM_OUTDIR)/bayes/%-sim-scores.tsv: $(SIM_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-compl.h5 $(SIM_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(SIM_OUTDIR)/bayes-plus/%-sim-scores.tsv: $(SIM_FEAT_DIR)/sim-%-feats.tsv \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-compl.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --scale=relh --append \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-motif.h5 \
			$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-compl.h5 $< | \
		$(AWK) '{print "$*", $$0}' >$@

$(SIM_NORM_OUTDIR)/global-bg/%-sim-scores.tsv: $(SIM_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/global-bg/$*-motif.h5 $(SIM_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(SIM_NORM_OUTDIR)/freq/%-sim-scores.tsv: $(SIM_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/freq/$*-motif.h5 $(SIM_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(SIM_NORM_OUTDIR)/pos-bg/%-sim-scores.tsv: $(SIM_WINS) \
		$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5
	[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(MOTIF_SCORE) --normalize --scale=relh --append --pvals=$(PVAL_PREC) \
			$(SUGIYAMA_MOTIFS_DIR)/pos-bg/$*-motif.h5 $(SIM_WINS) | \
		$(AWK) '{print "$*", $$0}' >$@

$(SIM_SCORES_GLOBAL): $(SIM_KINASE_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(SIM_SCORES_FREQ): $(SIM_KINASE_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(SIM_SCORES_POS): $(SIM_KINASE_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(SIM_SCORES_BAYES): $(SIM_KINASE_SCORES_BAYES)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(SIM_SCORES_BAYESP): $(SIM_KINASE_SCORES_BAYESP)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print $$1, $$2, $$5}' >$@

$(SIM_NORM_SCORES_GLOBAL): $(SIM_KINASE_NORM_SCORES_GLOBAL)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(SIM_NORM_SCORES_FREQ): $(SIM_KINASE_NORM_SCORES_FREQ)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(SIM_NORM_SCORES_POS): $(SIM_KINASE_NORM_SCORES_POS)
	cat $^ | $(AWK) 'BEGIN{print "kinase", "seq.win", "score"}{print}' >$@

$(PMAPPERVAL_SCORES_RF) \
$(PHOPROTEOME_SCORES_RF) \
$(WILKES_INHIB_SCORES_RF): $(PSP_SEMSIM) $(PSP_SCORES_BAYESP) $(PSP) $(PMAPPERVAL) \
		$(PHOPROTEOME) $(PHOPROTEOME_SCORES_BAYESP) $(PHOPROTEOME_SEMSIM) $(PROTMAPPER) \
		$(PMAPPERVAL_SCORES_BAYESP) $(PMAPPERVAL_SEMSIM) $(WILKES_INHIB) \
		$(WILKES_SEMSIM)
	Rscript $(RANDFOREST_SCRIPT) $(SEQWIN_LEN)
