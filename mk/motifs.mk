KINASE_MOTIF_FNAMES = $(foreach K,$(KINASES),$(1)/$(K)-motif.h5)

SUGIYAMA_MOTIFS_OUT = $(foreach SCORE,$(SCORE_OPTS),$(SUGIYAMA_MOTIFS_DIR)/$(SCORE))
SUGIYAMA_MOTIFS = $(foreach M,$(SUGIYAMA_MOTIFS_OUT),$(call KINASE_MOTIF_FNAMES,$(M)))
# SUGIYAMA_DISTS = $(foreach SCORE,$(SCORE_OPTS),$(SUGIYAMA_DISTS_DIR)/$(SCORE)-method-dist.tsv))
# SUGIYAMA_KIN_DISTS = $(foreach K,$(KINASES),$(SUGIYAMA_DISTS_DIR)/kinase/$(K)-kinase-dist.tsv)

SUGIYAMA_FOLD_MOTIFS_OUT = $(foreach K,$(shell seq 1 $(K_FOLDS)),$(foreach SCORE,$(SCORE_OPTS),$(OUTDIR)/sugiyama-folds/fold-$(K)/motifs/$(SCORE)))
SUGIYAMA_FOLD_MOTIFS = $(foreach M,$(SUGIYAMA_FOLD_MOTIFS_OUT),$(call KINASE_MOTIF_FNAMES,$(M)))
# SUGIYAMA_FOLD_DISTS = $(foreach SCORE,$(SCORE_OPTS),$(SUGIYAMA_DISTS_DIR)/$(SCORE)-method-dist.tsv))
# SUGIYAMA_FOLD_KIN_DISTS = $(foreach K,$(KINASES),$(SUGIYAMA_DISTS_DIR)/kinase/$(K)-kinase-dist.tsv)

.PHONY: compile
compile: motifs # dists

.PHONY: motifs
motifs: $(SUGIYAMA_MOTIFS)

.PHONY: fold-motifs
fold-motifs:  $(SUGIYAMA_FOLD_MOTIFS)

.PHONY: dists
dists: $(SUGIYAMA_DISTS) $(SUGIYAMA_KIN_DISTS)

$(SUGIYAMA_MOTIFS_DIR)/global-bg/%-motif.h5: $(SUGIYAMA) $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(SUGIYAMA_MOTIFS_DIR)/freq/%-motif.h5: $(SUGIYAMA) $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(SUGIYAMA_MOTIFS_DIR)/pos-bg/%-motif.h5: $(SUGIYAMA) $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(SUGIYAMA_MOTIFS_DIR)/bayes/%-motif.h5 \
$(SUGIYAMA_MOTIFS_DIR)/bayes/%-compl.h5: $(SUGIYAMA)
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-motif.h5 \
$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/%-compl.h5: $(SUGIYAMA_FEAT_DIR)/sugiyama-%-feats.tsv
	mkdir -p $(@D)
	cut -f1,3-7 $(SUGIYAMA_FEAT_DIR)/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-motif.h5 \
		$(SUGIYAMA_MOTIFS_DIR)/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

# K-fold CV data

$(OUTDIR)/sugiyama-folds/fold-1/motifs/global-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-1/train/sugiyama-train.tsv $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-2/motifs/global-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-2/train/sugiyama-train.tsv $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-3/motifs/global-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-3/train/sugiyama-train.tsv $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-4/motifs/global-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-4/train/sugiyama-train.tsv $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-5/motifs/global-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-5/train/sugiyama-train.tsv $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-6/motifs/global-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-6/train/sugiyama-train.tsv $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-7/motifs/global-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-7/train/sugiyama-train.tsv $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-8/motifs/global-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-8/train/sugiyama-train.tsv $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-9/motifs/global-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-9/train/sugiyama-train.tsv $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-10/motifs/global-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-10/train/sugiyama-train.tsv $(PROTEOME)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-global $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-1/motifs/freq/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-1/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-2/motifs/freq/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-2/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-3/motifs/freq/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-3/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-4/motifs/freq/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-4/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-5/motifs/freq/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-5/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-6/motifs/freq/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-6/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-7/motifs/freq/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-7/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-8/motifs/freq/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-8/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-9/motifs/freq/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-9/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-10/motifs/freq/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-10/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=$*-freq $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-1/motifs/pos-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-1/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-2/motifs/pos-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-2/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-3/motifs/pos-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-3/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-4/motifs/pos-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-4/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-5/motifs/pos-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-5/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-6/motifs/pos-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-6/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-7/motifs/pos-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-7/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-8/motifs/pos-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-8/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-9/motifs/pos-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-9/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-10/motifs/pos-bg/%-motif.h5: $(DATADIR)/sugiyama-folds/fold-10/train/sugiyama-train.tsv $(SUGIYAMA_WINS)
	mkdir -p $(@D)
	$(AWK) '{if ($$1 == "$*"){print $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$(SUGIYAMA_WINS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=$*-pos $@.tmp $@
	rm $@.tmp

$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes/%-compl.h5: $(DATADIR)/sugiyama-folds/fold-1/train/sugiyama-train.tsv
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes/%-compl.h5: $(DATADIR)/sugiyama-folds/fold-2/train/sugiyama-train.tsv
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes/%-compl.h5: $(DATADIR)/sugiyama-folds/fold-3/train/sugiyama-train.tsv
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes/%-compl.h5: $(DATADIR)/sugiyama-folds/fold-4/train/sugiyama-train.tsv
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes/%-compl.h5: $(DATADIR)/sugiyama-folds/fold-5/train/sugiyama-train.tsv
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes/%-compl.h5: $(DATADIR)/sugiyama-folds/fold-6/train/sugiyama-train.tsv
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes/%-compl.h5: $(DATADIR)/sugiyama-folds/fold-7/train/sugiyama-train.tsv
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes/%-compl.h5: $(DATADIR)/sugiyama-folds/fold-8/train/sugiyama-train.tsv
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes/%-compl.h5: $(DATADIR)/sugiyama-folds/fold-9/train/sugiyama-train.tsv
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes/%-compl.h5: $(DATADIR)/sugiyama-folds/fold-10/train/sugiyama-train.tsv
	mkdir -p $(@D)
	cut -f2,3 $< | sed '1d' >$@-bg.tmp
	$(AWK) '{if ($$1 == "$*"){print $$2, $$3}}' $< >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--name=$*-bayes $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes-plus/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes-plus/%-compl.h5: sugiyama-fold-1-features
	mkdir -p $(@D)
	cut -f1,3-7 $(DATADIR)/sugiyama-folds/fold-1/train/kinase-features/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $(DATADIR)/sugiyama-folds/fold-1/train/kinase-features/sugiyama-$*-feats.tsv >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes-plus/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-1/motifs/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes-plus/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes-plus/%-compl.h5: sugiyama-fold-2-features
	mkdir -p $(@D)
	cut -f1,3-7 $(DATADIR)/sugiyama-folds/fold-2/train/kinase-features/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $(DATADIR)/sugiyama-folds/fold-2/train/kinase-features/sugiyama-$*-feats.tsv >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes-plus/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-2/motifs/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes-plus/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes-plus/%-compl.h5: sugiyama-fold-3-features
	mkdir -p $(@D)
	cut -f1,3-7 $(DATADIR)/sugiyama-folds/fold-3/train/kinase-features/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $(DATADIR)/sugiyama-folds/fold-3/train/kinase-features/sugiyama-$*-feats.tsv >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes-plus/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-3/motifs/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes-plus/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes-plus/%-compl.h5: # sugiyama-fold-4-features
	mkdir -p $(@D)
	cut -f1,3-7 $(DATADIR)/sugiyama-folds/fold-4/train/kinase-features/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $(DATADIR)/sugiyama-folds/fold-4/train/kinase-features/sugiyama-$*-feats.tsv >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes-plus/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-4/motifs/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes-plus/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes-plus/%-compl.h5: # sugiyama-fold-5-features
	mkdir -p $(@D)
	cut -f1,3-7 $(DATADIR)/sugiyama-folds/fold-5/train/kinase-features/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $(DATADIR)/sugiyama-folds/fold-5/train/kinase-features/sugiyama-$*-feats.tsv >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes-plus/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-5/motifs/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes-plus/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes-plus/%-compl.h5: # sugiyama-fold-6-features
	mkdir -p $(@D)
	cut -f1,3-7 $(DATADIR)/sugiyama-folds/fold-6/train/kinase-features/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $(DATADIR)/sugiyama-folds/fold-6/train/kinase-features/sugiyama-$*-feats.tsv >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes-plus/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-6/motifs/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes-plus/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes-plus/%-compl.h5: # sugiyama-fold-7-features
	mkdir -p $(@D)
	cut -f1,3-7 $(DATADIR)/sugiyama-folds/fold-7/train/kinase-features/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $(DATADIR)/sugiyama-folds/fold-7/train/kinase-features/sugiyama-$*-feats.tsv >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes-plus/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-7/motifs/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes-plus/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes-plus/%-compl.h5: # sugiyama-fold-8-features
	mkdir -p $(@D)
	cut -f1,3-7 $(DATADIR)/sugiyama-folds/fold-8/train/kinase-features/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $(DATADIR)/sugiyama-folds/fold-8/train/kinase-features/sugiyama-$*-feats.tsv >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes-plus/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-8/motifs/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes-plus/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes-plus/%-compl.h5: # sugiyama-fold-9-features
	mkdir -p $(@D)
	cut -f1,3-7 $(DATADIR)/sugiyama-folds/fold-9/train/kinase-features/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $(DATADIR)/sugiyama-folds/fold-9/train/kinase-features/sugiyama-$*-feats.tsv >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes-plus/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-9/motifs/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes-plus/%-motif.h5 \
$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes-plus/%-compl.h5: # sugiyama-fold-10-features
	mkdir -p $(@D)
	cut -f1,3-7 $(DATADIR)/sugiyama-folds/fold-10/train/kinase-features/sugiyama-$*-feats.tsv >$@-bg.tmp
	$(AWK) '{if ($$2 == "TRUE"){print $$1, $$3, $$4, $$5, $$6, $$7}}' $(DATADIR)/sugiyama-folds/fold-10/train/kinase-features/sugiyama-$*-feats.tsv >$@.tmp
	$(MOTIF_COMPILE) \
		--bg-type=pos \
		--bg-file=$@-bg.tmp \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=bayes \
		--features=bernoulli,bernoulli,bernoulli,bernoulli \
		--name=$*-bayes+ $@.tmp \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes-plus/$*-motif.h5 \
		$(OUTDIR)/sugiyama-folds/fold-10/motifs/bayes-plus/$*-compl.h5
	rm $@.tmp $@-bg.tmp

$(SUGIYAMA_DISTS_DIR)/%-method-dist.tsv: $(SUGIYAMA_MOTIFS)
	mkdir -p $(@D)
	$(MOTIF_DIST) $(SUGIYAMA_MOTIFS_DIR)/$(call OPTS2DIR,$*)/*-motif.h5 >$@.tmp
	sed 's/-[[:alpha:]-]*//g' $@.tmp >$@
	rm $@.tmp

$(SUGIYAMA_DISTS_DIR)/kinase/%-kinase-dist.tsv: $(SUGIYAMA_MOTIFS)
	mkdir -p $(@D)
	$(MOTIF_DIST) --what=pfm --method=kl \
		$(foreach D,$(SUGIYAMA_MOTIFS_OUT),$(D)/*-motif.h5) >$@

