.PHONY: eval
eval: $(IMGDIR)/pssm-eval.pdf $(IMGDIR)/psp-eval.pdf $(IMGDIR)/bayes-feats.pdf

$(IMGDIR)/ochoa-vs-proteome.pdf: $(OCHOA_V_PROT_SCRIPT) out/ochoa-motif.h5 \
		out/ochoa-ST-motif.h5 out/ochoa-Y-motif.h5
	Rscript $(OCHOA_V_PROT_SCRIPT)

$(IMGDIR)/bayes-feats-eval.pdf: $(SUGIYAMA_MOTIFS) $(BAYES_FEAT_SCRIPT)
	Rscript $(BAYES_FEAT_SCRIPT)

$(IMGDIR)/wilkes-eval.pdf: $(OUTDIR)/pssm-info.tsv $(KINASES_ST_FILE) \
		$(KINASES_Y_FILE) $(KINASE_ID_MAP) $(WILKES_INHIB_SCORES_RF) \
		$(WILKES_INHIB) $(PSP)
	Rscript $(WILKES_EVAL_SCRIPT) $(SEQWIN_LEN)

$(IMGDIR)/pmapperval-ml.pdf: $(PMAPPERVAL_SCORES_RF) $(PMAPPERVAL_EVAL_SCRIPT)
	Rscript $(PMAPPERVAL_EVAL_SCRIPT) $(SEQWIN_LEN)

$(SUG_FOLD_CV): $(SUG_FOLD_SCORES_BAYESP) $(SUG_FOLD_SCORES_BAYES) \
		$(SUG_FOLD_NORM_SCORES_GLOBAL) $(SUG_FOLD_NORM_SCORES_POS) \
		$(SUGIYAMA_CV_SCRIPT)
	Rscript $(SUGIYAMA_CV_SCRIPT) $(ML_K_FOLDS) $(SEQWIN_LEN)

$(IMGDIR)/sugiyama-ml-cv.pdf: $(SUG_FOLD_CV) $(SUGIYAMA_CV_PLOT_SCRIPT)
	Rscript $(SUGIYAMA_CV_PLOT_SCRIPT) $(ML_K_FOLDS) $(SEQWIN_LEN)

$(IMGDIR)/isoform-analysis.pdf: $(PSP_ALL) $(OCHOA_SCORES_RF)
	Rscript $(ISOFORM_SCRIPT)

$(IMGDIR)/phosphoproteome-assignments.pdf: $(PHOPROTEOME) $(PHOPROTEOME_SCORES_BP) \
		$(PHOPROTEOME_SCORES_RF) $(PSP) $(LINKPHINDER) $(KINASE_FILE) \
		$(ASSIGNMENTS_SCRIPT)
