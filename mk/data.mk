# Remote data
SUGIYAMA_SOURCE = https://static-content.springer.com/esm/art%3A10.1038%2Fs41598-019-46385-4/MediaObjects/41598_2019_46385_MOESM3_ESM.xlsx
UNIPROT_MIRROR = ftp://ftp.ebi.ac.uk/
PROTEOME_SOURCE = $(UNIPROT_MIRROR)/pub/databases/uniprot/current_release/knowledgebase/reference_proteomes/Eukaryota/UP000005640_9606.fasta.gz
PROTEOME_EXTRA_SOURCE = $(UNIPROT_MIRROR)/pub/databases/uniprot/current_release/knowledgebase/reference_proteomes/Eukaryota/UP000005640_9606_additional.fasta.gz
UNIPROT_IDMAPPING_SOURCE = $(UNIPROT_MIRROR)/pub/databases/uniprot/current_release/knowledgebase/reference_proteomes/Eukaryota/UP000005640_9606.idmapping
OCHOA_SOURCE = https://static-content.springer.com/esm/art%3A10.1038%2Fs41587-019-0344-3/MediaObjects/41587_2019_344_MOESM4_ESM.xlsx
PSP_VERSION = 2022-02-09
PSP_SOURCE = /mnt/data2/data/phosphositeplus/$(PSP_VERSION)/Kinase_Substrate_Dataset
PSP_SITES_SOURCE = /mnt/data2/data/phosphositeplus/$(PSP_VERSION)/Phosphorylation_site_dataset
BIOGRID_VERSION = 4.3.194
BIOGRID_SOURCE = https://downloads.thebiogrid.org/Download/BioGRID/Release-Archive/BIOGRID-$(BIOGRID_VERSION)/BIOGRID-ALL-$(BIOGRID_VERSION).tab3.zip
PFAM_SOURCE = http://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/proteomes/9606.tsv.gz
KINNET_SOURCE = https://ars.els-cdn.com/content/image/1-s2.0-S2405471220301484-mmc5.xlsx

# In vitro kinase-substrate relationships
# Sugiyama, Imamura & Ishihama. Scientific Reports volume 9, Article
# number: 10503 (2019).  Data taken from Supplementary Table S2
SUGIYAMA_RAW = $(RAW_DATADIR)/41598_2019_46385_MOESM3_ESM.xlsx
SUGIYAMA_FULL = $(DATADIR)/sugiyama-kinase-substrates-full.tsv
SUGIYAMA = $(DATADIR)/sugiyama-kinase-substrates.tsv
SUGIYAMA_ST = $(DATADIR)/sugiyama-kinase-substrates-ST.tsv
SUGIYAMA_Y = $(DATADIR)/sugiyama-kinase-substrates-Y.tsv
SUGIYAMA_FEAT = $(DATADIR)/sugiyama-kinase-substrates-plus.tsv
SUGIYAMA_FEAT_ST = $(DATADIR)/sugiyama-kinase-substrates-plus-ST.tsv
SUGIYAMA_FEAT_Y = $(DATADIR)/sugiyama-kinase-substrates-plus-Y.tsv
SUGIYAMA_FEAT_DIR = $(DATADIR)/sugiyama-kinase-features
SUGIYAMA_KIN_FEAT = $(foreach K,$(KINASES),$(SUGIYAMA_FEAT_DIR)/sugiyama-$(K)-feats.tsv)
SUGIYAMA_WINS = $(DATADIR)/sugiyama-seqwins.tsv
SUGIYAMA_FILT = $(DATADIR)/sugiyama-kinase-substrates-filt.tsv
SUGIYAMA_FILT_ST = $(DATADIR)/sugiyama-kinase-substrates-filt-ST.tsv
SUGIYAMA_FILT_Y = $(DATADIR)/sugiyama-kinase-substrates-filt-Y.tsv
# PhosphositePlus
PSP_RAW = $(RAW_DATADIR)/Kinase_Substrate_Dataset
PSP_ALL = $(DATADIR)/psp-kinase-substrates-all.tsv
PSP = $(DATADIR)/psp-kinase-substrates.tsv
PSP_ST = $(DATADIR)/psp-kinase-substrates-ST.tsv
PSP_Y = $(DATADIR)/psp-kinase-substrates-Y.tsv
PSP_WINS = $(DATADIR)/psp-seqwins.tsv
PSP_FEAT_DIR = $(DATADIR)/psp-kinase-features
PSP_KIN_FEAT = $(foreach K,$(KINASES),$(PSP_FEAT_DIR)/psp-$(K)-feats.tsv)
PSP_SEMSIM = $(DATADIR)/psp-kinase-substrates-go-semsim.tsv
PSP_SITES_RAW = $(RAW_DATADIR)/Phosphorylation_site_dataset
PSP_SITES = $(DATADIR)/psp-all-sites.tsv
# ProtMapper: Assembling a phosphoproteomic knowledge base using
#  ProtMapper to normalize phosphosite information from databases and
#  text mining John A. Bachman, Benjamin M. Gyori, Peter K. Sorger
#  bioRxiv 822668; doi: https://doi.org/10.1101/822668
PMAPPER_RAW = $(RAW_DATADIR)/protmapper/export.csv
PMAPPER = $(DATADIR)/protmapper-kinase-substrates.tsv
PMAPPER_WINS = $(DATADIR)/protmapper-seqwins.tsv
PMAPPER_FEAT_DIR = $(DATADIR)/protmapper-kinase-features
PMAPPER_KIN_FEAT = $(foreach K,$(KINASES),$(PMAPPER_FEAT_DIR)/protmapper-$(K)-feats.tsv)
PMAPPERVAL = $(DATADIR)/pmapperval-kinase-substrates.tsv
PMAPPERVAL_WINS = $(DATADIR)/pmapperval-seqwins.tsv
PMAPPERVAL_FEAT_DIR = $(DATADIR)/pmapperval-kinase-features
PMAPPERVAL_SEMSIM = $(DATADIR)/pmapperval-kinase-substrates-go-semsim.tsv
# Reference human phosphoproteome
# Ochoa et al. Nat Biotechnol. 2019 Dec 9.  Data taken from
# Supplementary Table 2
OCHOA_RAW = $(RAW_DATADIR)/41587_2019_344_MOESM4_ESM.xlsx
PHOPROTEOME = $(DATADIR)/human-phosphoproteome.tsv
PHOPROTEOME_WINS = $(DATADIR)/human-phosphoproteome-seqwins.txt
PHOPROTEOME_WINS_ST = $(DATADIR)/human-phosphoproteome-seqwins-ST.txt
PHOPROTEOME_WINS_Y = $(DATADIR)/human-phosphoproteome-seqwins-Y.txt
PHOPROTEOME_FEAT_DIR = $(DATADIR)/human-phosphoproteome-kinase-features
PHOPROTEOME_KIN_FEAT = $(foreach K,$(KINASES),$(PHOPROTEOME_FEAT_DIR)/ochoa-$(K)-feats.tsv)
PHOPROTEOME_SUB = $(DATADIR)/human-phosphoproteome-randsubset.tsv
PHOPROTEOME_SUB2 = $(DATADIR)/human-phosphoproteome-randsubset2.tsv
PHOPROTEOME_SEMSIM = $(DATADIR)/human-phosphoproteome-go-semsim.tsv
# Kinase Inhibitor experiment from Wilkes et al 2015
WILKES_DIR = $(RAW_DATADIR)/wilkes-et-al-2015
WILKES_INHIB_RAW = $(WILKES_DIR)/wilkes-inhib.csv
WILKES_INHIB_PEPDB = $(WILKES_DIR)/wilkes-inhib-pepdb.csv
WILKES_INHIB = $(DATADIR)/wilkes-et-al-2015-inhib.tsv
WILKES_FEAT_DIR = $(DATADIR)/wilkes-kinase-features
WILKES_KIN_FEAT = $(foreach K,$(KINASES),$(WILKES_FEAT_DIR)/wilkes-inhib-$(K)-feats.tsv)
WILKES_SEMSIM = $(DATADIR)/wilkes-go-semsim.tsv
# linkPhinder
LINKPHINDER_RAW = $(RAW_DATADIR)/linkPhinder_data.csv
LINKPHINDER = $(DATADIR)/linkphinder.tsv

SIM_WINS = $(DATADIR)/simulated-seqwins.txt
SIM_FEAT_DIR = $(DATADIR)/sim-kinase-features
SIM_KIN_FEAT = $(foreach K,$(KINASES),$(SIM_FEAT_DIR)/sim-$(K)-feats.tsv)

# Uniprot human proteome
PROTEOME = $(DATADIR)/uniprot-human-proteome.fasta
UNIPROT_IDMAPPING_RAW = $(RAW_DATADIR)/UP000005640_9606.idmapping
KINASE_FILE = $(DATADIR)/kinases.txt
KINASE_ST_FILE = $(DATADIR)/kinases-ST.txt
KINASE_Y_FILE = $(DATADIR)/kinases-Y.txt
RAW_PKINFAM = $(RAW_DATADIR)/pkinfam.txt
PKINFAM = $(DATADIR)/pkinfam.tsv
KINASE_ID_MAP = $(DATADIR)/kinase-id-map.tsv
ENTREZ_ID_MAP = $(DATADIR)/uniprot-to-entrez.tsv
UNIPROT_ID_MAP = $(DATADIR)/uniprot-human-idmap.tsv
KINASE_CITATIONS = $(DATADIR)/kinase-citations.tsv
# BIOGRID, BioPLEX, and SEC Kinase Interactions
BIOGRID_RAW = $(RAW_DATADIR)/BIOGRID-ALL-4.3.194.tab3.txt
BIOGRID = $(DATADIR)/human-biogrid.tsv
BIOPLEX_RAW = $(RAW_DATADIR)/BioPlex_293T_Network_10K_Dec_2019.tsv
KINSEC_RAW = $(RAW_DATADIR)/buljan-et-al-2020/Final_interactome_2021-03-15.txt
KIN_PHYS_INT = $(DATADIR)/human-kinase-interactions.tsv
# PFAM
PFAM_RAW = $(RAW_DATADIR)/Pfam-9606.tsv
PFAM = $(DATADIR)/pfam-human-domains.tsv

KINASES = $(shell cat $(KINASE_FILE))
KINASES_ST = $(shell cat $(KINASE_ST_FILE))
KINASES_Y = $(shell cat $(KINASE_Y_FILE))

SUGIYAMA_WEIGHTS = $(SUGIYAMA_WEIGHTS_DIR)/sugiyama-full-weights.tsv

.PHONY: prepare
data: $(PROTEOME) $(SUGIYAMA) $(SUGIYAMA_ST) $(SUGIYAMA_Y)		\
	$(PHOPROTEOME) $(PSP) $(PSP_ST) $(PSP_Y) $(PSP_WINS) $(PHOPROTEOME_WINS)	\
	$(SIM_WINS) $(KINASE_FILE) $(KINASE_ST_FILE) $(KINASE_Y_FILE) \
	$(PMAPPER) $(PMAPPER_WINS) $(PMAPPERVAL)

.PHONY: feats
feats: sugiyama-feats psp-feats ochoa-feats wilkes-feats sim-feats protmapper-feats

.PHONY: semsim
semsim: $(PMAPPERVAL_SEMSIM) $(PSP_SEMSIM) $(PHOPROTEOME_SEMSIM) $(WILKES_SEMSIM)

.PHONY: sugiyama-feats
sugiyama-feats: $(SUGIYAMA)
	mkdir -p $(SUGIYAMA_FEAT_DIR)
	if [[ ! -e $@ ]]; then Rscript $(SUGIYAMA_FEATS_SCRIPT); fi

.PHONY: psp-feats
psp-feats: $(PSP)
	mkdir -p $(PSP_FEAT_DIR)
	if [[ ! -e $@ ]]; then Rscript $(ADD_FEATS_SCRIPT) $<; fi

.PHONY: protmapper-feats
protmapper-feats: $(PMAPPER)
	mkdir -p $(PMAPPER_FEAT_DIR)
	if [[ ! -e $@ ]]; then Rscript $(PMAPPER_FEATS_SCRIPT); fi

.PHONY: pmapperval-feats
pmapperval-feats: $(PMAPPERVAL)
	mkdir -p $(PMAPPERVAL_FEAT_DIR)
	if [[ ! -e $@ ]]; then Rscript $(ADD_FEATS_SCRIPT) $<; fi

.PHONY: phoproteome-feats
phoproteome-feats: $(PHOPROTEOME)
	mkdir -p $(PHOPROTEOME_FEAT_DIR)
	if [[ ! -e $@ ]]; then Rscript $(PHOPROTEOME_FEATS_SCRIPT); fi

.PHONY: wilkes-feats
wilkes-feats: $(WILKES_INHIB)
	mkdir -p $(WILKES_FEAT_DIR)
	if [[ ! -e $@ ]]; then Rscript $(WILKES_FEATS_SCRIPT); fi

.PHONY: sim-feats
sim-feats: $(SIM_WINS)
	mkdir -p $(SIM_FEAT_DIR)
	if [[ ! -e $@ ]]; then Rscript $(SIM_FEATS_SCRIPT); fi

$(PROTEOME):
	mkdir -p $(PROTEOME_DIR)
	wget $(PROTEOME_SOURCE) -O $@.main.gz
	gunzip -f $@.main.gz
	wget $(PROTEOME_EXTRA_SOURCE) -O $@.extra.gz
	gunzip -f $@.extra.gz
	cat $@.main $@.extra >$@
	rm $@.main $@.extra

$(PFAM_RAW):
	wget $(PFAM_SOURCE) -O $@.gz
	gunzip -f $@.gz

$(PFAM): $(PFAM_RAW)
	sed -E '1,2d;3{s/^#//;s/<([a-zA-Z -]+)> /\1\t/g;s/<([a-zA-Z -]+)>/\1/g}' $< >$@

$(PKINFAM): $(PKINFAM_RAW) $(PKINFAM_SCRIPT)
	python $(PKINFAM_SCRIPT) $< >$@

# Be sure to include the isoforms listed in the Sugiyama dataset, eg
# LYNa and LYNb
$(KINASE_FILE): $(PKINFAM) $(DATADIR)/sugiyama-id-map.tsv
	cut -f1 $< >$@
	grep "-" $(DATADIR)/sugiyama-id-map.tsv | cut -f2 >>$@

$(KINASE_ST_FILE): $(PKINFAM) $(DATADIR)/sugiyama-id-map.tsv
	$(AWK) '{if ($$2 == "STK"){print $$1}}' $< >$@
	echo "P05771-2" >>$@
	echo "Q13976-2" >>$@

$(KINASE_Y_FILE): $(PKINFAM)
	$(AWK) '{if ($$2 == "YK"){print $$1}}' $< >$@
	echo "P07948-2" >>$@

$(KINASE_ID_MAP): $(KINASE_FILE) $(PROTEOME)
	sed -E 's/(.*)/|\1|/' $(KINASE_FILE) | grep -f - $(PROTEOME) | \
		sed -E 's/^>..\|(.*)\|.*_HUMAN /\1\t/;s/ OS=Homo sapiens \(Human\) OX=9606//;s/GN=([A-Za-z0-9_-]+).*$$/\t\1/' >$@

$(UNIPROT_IDMAPPING_RAW):
	wget $(UNIPROT_IDMAPPING_SOURCE) -O $@

$(ENTREZ_ID_MAP): $(UNIPROT_IDMAPPING_RAW) $(KINASE_FILE)
	$(AWK) '{if ($$2 == "GeneID"){print $$1, $$3}}' $< |
		grep -f $(KINASE_FILE) >$@

$(UNIPROT_ID_MAP): $(PROTEOME)
		sed -n '/^>/p' $<  | \
		cut -f1 -d' ' | \
		cut -f2,3 -d'|' | \
		tr '|' '\t' >$@

$(KINASE_CITATIONS): $(KINASE_FILE) $(ENTREZ_ID_MAP)
	python $(KINASE_CITES_SCRIPT)

$(SUGIYAMA_RAW):
	mkdir -p $(RAW_DATADIR)
	wget $(SUGIYAMA_SOURCE) -O $@

$(SUGIYAMA_FULL): $(SUGIYAMA_RAW) $(PROTEOME) $(SUGIYAMA_SEQWIN_SCRIPT) $(DATADIR)/sugiyama-id-map.tsv
	$(call EXCEL2TSV,$(DATADIR),$<)
	mv $(DATADIR)/$(notdir $(basename $(SUGIYAMA_RAW))).csv $@.tmp
	cut -d'	' -f1-3,5-7 $@.tmp | \
		sed '1,3d;s/"//g;s/\/[[:alnum:]/ ]*//' | \
		$(AWK) '{if ($$1 !~ /mutant/ && $$1 != "LK"){print}}' | \
		sed 's/ü/TRUE/g;s/\t$$/\tFALSE/;s/\t\t/\tFALSE\t/' >$@.tmp2
	rm $@.tmp
	python $(SUGIYAMA_SEQWIN_SCRIPT) $(SEQWIN_LEN) $(PROTEOME) <$@.tmp2 >$(DATADIR)/seqwins.tmp
	paste $@.tmp2 $(DATADIR)/seqwins.tmp >$@
	rm $@.tmp2 $(DATADIR)/seqwins.tmp

$(SUGIYAMA_WEIGHTS_DIR)/%-seq-weights.tsv: $(SUGIYAMA_FULL)
	mkdir -p $(dir $@)
	$(AWK) '{if ($$7 == "$*" && $$8 != "NA"){print $$7, $$8}}' $< >$@.tmp
	cut -f2 $@.tmp | $(MOTIF_COMPILE) --print-weights - $@.tmp.h5 >$@.tmp2
	rm $@.tmp.h5
	paste $@.tmp <(cut -f2 $@.tmp2) >$@
	rm $@.tmp $@.tmp2

$(SUGIYAMA_WEIGHTS): $(foreach K,$(KINASES),$(SUGIYAMA_WEIGHTS_DIR)/$(K)-seq-weights.tsv)
	cat $^ >$@

$(SUGIYAMA_FILT): $(SUGIYAMA_FULL) $(SUGIYAMA_SCRIPT) $(KINASE_ID_MAP)
	Rscript $(SUGIYAMA_SCRIPT) $(SEQWIN_LEN)

$(SUGIYAMA): $(SUGIYAMA_FILT)
	$(AWK) '{if ($$8 != "NA"){printf("%s\t%s-%s\t%s\n", $$7, $$10, $$4, $$8)}}' $< | \
		sed '1s/-/./' >$@

$(SUGIYAMA_ST): $(SUGIYAMA) $(KINASE_ST_FILE)
	grep -w -f <(sed -E 's/(.*)/^\1/' $(KINASE_ST_FILE)) $(SUGIYAMA) >$@

$(SUGIYAMA_Y): $(SUGIYAMA) $(KINASE_Y_FILE)
	grep -w -f <(sed -E 's/(.*)/^\1/' $(KINASE_Y_FILE)) $(SUGIYAMA) >$@

$(SUGIYAMA_FEAT): $(SUGIYAMA) $(KIN_PHYS_INT) $(SUGIYAMA_FEATS_SCRIPT)
	Rscript $(SUGIYAMA_FEATS_SCRIPT)

$(SUGIYAMA_FEAT_ST): $(SUGIYAMA_FEAT) $(KINASE_ST_FILE)
	grep -w -f <(sed -E 's/(.*)/^\1/' $(KINASE_ST_FILE)) $(SUGIYAMA_FEAT) >$@

$(SUGIYAMA_FEAT_Y): $(SUGIYAMA_FEAT) $(KINASE_Y_FILE)
	grep -w -f <(sed -E 's/(.*)/^\1/' $(KINASE_Y_FILE)) $(SUGIYAMA_FEAT) >$@

$(SUGIYAMA_WINS): $(SUGIYAMA)
	sed '1d' $< | cut -f3 | sort | uniq >$@

# PhosphositePlus human kinase-substrate relationships, minus
# autophosphorylation
$(PSP_RAW): $(PSP_SOURCE)
	mkdir -p $(RAW_DATADIR)
	cp $(PSP_SOURCE) $@

$(PSP): $(PSP_RAW) $(FORMAT_PSP_SCRIPT)
	python $(FORMAT_PSP_SCRIPT) $(PSP_RAW) $(PROTEOME) $(SEQWIN_LEN) --in-vivo |
		sort |
		uniq >$@

$(PSP_ALL): $(PSP_RAW) $(FORMAT_PSP_SCRIPT)
	python $(FORMAT_PSP_SCRIPT) $(PSP_RAW) $(PROTEOME) $(SEQWIN_LEN) |
		sort |
		uniq >$@

$(PSP_ST): $(PSP) $(KINASE_ST_FILE)
	grep -w -f $(KINASE_ST_FILE) $(PSP) >$@

$(PSP_Y): $(PSP) $(KINASE_Y_FILE)
	grep -w -f $(KINASE_Y_FILE) $(PSP) >$@

$(PSP_WINS): $(PSP)
	cut -f2,3 $< | sort | uniq >$@

$(PSP_SITES_RAW): $(PSP_SITES_SOURCE)
	mkdir -p $(RAW_DATADIR)
	cp $(PSP_SITES_SOURCE) $@

$(PSP_SITES): $(PSP_SITES_RAW) $(FORMAT_PSP_SITES_SCRIPT)
	python $(FORMAT_PSP_SITES_SCRIPT) $(PSP_SITES_RAW) $(PROTEOME) $(SEQWIN_LEN) |
		sort |
		uniq >$@

$(PMAPPER): $(PMAPPER_RAW) $(FORMAT_PMAPPER_SCRIPT)
	python $(FORMAT_PMAPPER_SCRIPT) $(PMAPPER_RAW) $(PROTEOME) $(SEQWIN_LEN) |
		sort |
		uniq >$@

$(PMAPPER_WINS): $(PMAPPER)
	cut -f2,3 $< | sort | uniq >$@

$(PMAPPERVAL): $(PMAPPER) $(PHOPROTEOME) $(PMAPPERVAL_SCRIPT)
	Rscript $(PMAPPERVAL_SCRIPT) 15

$(PMAPPERVAL_WINS): $(PMAPPERVAL)
	cut -f2,3 $< | sort | uniq >$@

$(OCHOA_RAW):
	mkdir -p $(RAW_DATADIR)
	wget $(OCHOA_SOURCE) -O $@

$(PHOPROTEOME): $(OCHOA_RAW) $(PROTEOME) $(OCHOA_SEQWIN_SCRIPT) $(PSP_SITES)
	$(call EXCEL2TSV,$(DATADIR),$<)
	mv $(DATADIR)/$(notdir $(basename $(OCHOA_RAW))).csv $@.tmp
	cut -f1-3 $@.tmp | \
		sed '1d;s/"//g' | \
		$(AWK) '{printf("%s\t%s%d\n", $$1, $$3, $$2)}' >$@.tmp2
	rm $@.tmp
	python $(OCHOA_SEQWIN_SCRIPT) $(SEQWIN_LEN) $(PROTEOME) <$@.tmp2 >$(DATADIR)/seqwins.tmp
	paste $@.tmp2 $(DATADIR)/seqwins.tmp >$@.tmp3
	rm $@.tmp2 $(DATADIR)/seqwins.tmp
	$(AWK) '{if ($$3 != "NA"){printf("%s-%s\t%s\n", $$1, $$2, $$3)}}' $@.tmp3 >$@.tmp4
	rm $@.tmp3
	cat $(PSP_SITES) $@.tmp4 | sort | uniq >$@
	rm $@.tmp4

$(PHOPROTEOME_WINS): $(PHOPROTEOME)
	cut -f2 $< | sort | uniq >$@

$(PHOPROTEOME_WINS_ST): $(PHOPROTEOME_WINS)
	sed -E '/.{$(SEQWIN_HALFLEN)}Y.{$(SEQWIN_HALFLEN)}/d' $< >$@

$(PHOPROTEOME_WINS_Y): $(PHOPROTEOME_WINS)
	sed -E '/.{$(SEQWIN_HALFLEN)}[ST].{$(SEQWIN_HALFLEN)}/d' $< >$@

$(PHOPROTEOME_SUB): $(PHOPROTEOME)
	ny=`cut -f1 $(PSP) | grep -f $(KINASE_Y_FILE) | wc -l`
	n=`wc -l $(PSP) | cut -f1 -d' '`
	nst=`echo "$$n - $$ny" | bc`
	if [[ $$nst -gt 0 ]]; then
		$(AWK) '{if ($$2 ~ /.{$(SEQWIN_HALFLEN)}[ST].{$(SEQWIN_HALFLEN)}/){print}}' $(PHOPROTEOME) | \
			sort | uniq | \
			shuf -n $$nst >$@.tmp
	fi
	if [[ $$ny -gt 0 ]]; then
		$(AWK) '{if ($$2 ~ /.{$(SEQWIN_HALFLEN)}Y.{$(SEQWIN_HALFLEN)}/){print}}' $(PHOPROTEOME) | \
			sort | uniq | \
			shuf -n $$ny >>$@.tmp
	fi
	if [[ -e $@.tmp ]]; then
		shuf $@.tmp >$@
		rm $@.tmp
	fi

$(PHOPROTEOME_SUB2): $(PHOPROTEOME)
	ny=`cut -f1 $(PSP) | grep -f $(KINASE_Y_FILE) | wc -l`
	n=`wc -l $(PSP) | cut -f1 -d' '`
	nst=`echo "$$n - $$ny" | bc`
	if [[ $$nst -gt 0 ]]; then
		$(AWK) '{if ($$2 ~ /.{$(SEQWIN_HALFLEN)}[ST].{$(SEQWIN_HALFLEN)}/){print}}' $(PHOPROTEOME) | \
			sort | uniq | \
			shuf -n $$nst >$@.tmp
	fi
	if [[ $$ny -gt 0 ]]; then
		$(AWK) '{if ($$2 ~ /.{$(SEQWIN_HALFLEN)}Y.{$(SEQWIN_HALFLEN)}/){print}}' $(PHOPROTEOME) | \
			sort | uniq | \
			shuf -n $$ny >>$@.tmp
	fi
	if [[ -e $@.tmp ]]; then
		shuf $@.tmp >$@
		rm $@.tmp
	fi

$(WILKES_INHIB): $(WILKES_DIR)/wilkes-inhib-pepdb.csv \
		$(WILKES_DIR)/wilkes-inhib.csv $(PROTEOME) \
		$(FORMAT_WILKES_SCRIPT)
	python $(FORMAT_WILKES_SCRIPT) $(wordlist 1,3,$^) $(SEQWIN_LEN) >$@

$(LINKPHINDER): $(LINKPHINDER_RAW)
	$(AWK) '{if (NR == 1){print "kinase", "site.id", "score"}else{printf("%s\t%s-%s\t%f\n", $$2, $$4, $$6, $$7)}}' $< >$@

$(OUTDIR)/ochoa-motif.h5: $(PHOPROTEOME_WINS) $(PROTEOME)
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=ochoa-full $< $@

$(OUTDIR)/ochoa-ST-motif.h5: $(PHOPROTEOME_WINS_ST) $(PROTEOME)
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=ochoa-ST $< $@

$(OUTDIR)/ochoa-Y-motif.h5: $(PHOPROTEOME_WINS_Y) $(PROTEOME)
	$(MOTIF_COMPILE) \
		--bg-type=global \
		--bg-file=$(PROTEOME) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--name=ochoa-Y $< $@

$(OUTDIR)/ochoa-motif-freq.h5: $(PHOPROTEOME_WINS) $(PROTEOME)
	$(MOTIF_COMPILE) \
		--bg-type=none \
		--pseudo-dist=equal \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=ochoa-full $< $@

$(OUTDIR)/ochoa-ST-motif-freq.h5: $(PHOPROTEOME_WINS_ST) $(PROTEOME)
	$(MOTIF_COMPILE) \
		--bg-type=none \
		--pseudo-dist=equal \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=ochoa-ST $< $@

$(OUTDIR)/ochoa-Y-motif-freq.h5: $(PHOPROTEOME_WINS_Y) $(PROTEOME)
	$(MOTIF_COMPILE) \
		--bg-type=none \
		--pseudo-dist=equal \
		--pseudo-n=pos \
		--pseudo-param=1 \
		--score-method=freq \
		--name=ochoa-Y $< $@

$(SIM_WINS): $(OUTDIR)/ochoa-ST-motif.h5 $(OUTDIR)/ochoa-Y-motif.h5
	stprop=`awk '{if ($$1 ~ /.{$(SEQWIN_HALFLEN)}[ST].{$(SEQWIN_HALFLEN)}/){n += 1}}END{print n/NR}' $(PHOPROTEOME_WINS)`
	stn=`printf "%.0f" $$(echo "$(NUM_SIMSEQS) * $$stprop + 1" | bc -l)`
	dstn=`echo "$${stn} * 2" | bc -l`
	$(MOTIF_SIM) --what=pfm --n=$${dstn} $(OUTDIR)/ochoa-ST-motif.h5 >$@.tmp
	sed -n -E '/.{7}[ST].{7}/p' $@.tmp | shuf -n $$stn >$@.tmp2
	yprop=`awk '{if ($$1 ~ /.{$(SEQWIN_HALFLEN)}Y.{$(SEQWIN_HALFLEN)}/){n += 1}}END{print n/NR}' $(PHOPROTEOME_WINS)`
	yn=`printf "%.0f" $$(echo "$(NUM_SIMSEQS) * $$yprop" | bc -l)`
	dyn=`echo "$${yn} * 2" | bc -l`
	$(MOTIF_SIM) --what=pfm --n=$${dyn} $(OUTDIR)/ochoa-Y-motif.h5 >$@.tmp3
	sed -n -E '/.{7}[Y].{7}/p' $@.tmp3 | shuf -n $$yn >$@.tmp4
	cat $@.tmp2 $@.tmp4 | sort | uniq >$@
	rm $@.tmp $@.tmp2 $@.tmp3 $@.tmp4

$(BIOGRID_RAW):
	mkdir -p $(RAW_DATADIR)
	wget $(BIOGRID_SOURCE) -O $@.zip
	unzip $@.zip -d $(RAW_DATADIR)
	rm $@.zip

$(BIOGRID): $(BIOGRID_RAW)
	python $(FORMAT_BIOGRID_SCRIPT) $< >$@

$(KIN_PHYS_INT): $(BIOGRID_RAW) $(BIOPLEX_RAW) $(KINSEC_RAW) $(KIN_INTXNS_SCRIPT)
	Rscript $(KIN_INTXNS_SCRIPT)


$(DATADIR)/sugiyama-folds/fold-%/sugiyama-train.tsv \
$(DATADIR)/sugiyama-folds/fold-%/sugiyama-test.tsv \
$(DATADIR)/sugiyama-folds/fold-%/sugiyama-test-seqwins.tsv: $(SUGIYAMA) \
		$(SUGIYAMA_FOLD_SCRIPT)
	Rscript $(SUGIYAMA_FOLD_SCRIPT) $(K_FOLDS)

$(OUTDIR)/sugiyama-folds/fold-%/train/kinase-sub-domains.tsv: \
		$(DATADIR)/sugiyama-folds/fold-%/train/sugiyama-train.tsv \
		$(KIN_SUB_DOMS_SCRIPT)
	mkdir -p $(@D)
	Rscript $(KIN_SUB_DOMS_SCRIPT) $<

$(OUTDIR)/kinase-sub-domains.tsv: $(SUGIYAMA) $(KIN_SUB_DOMS_SCRIPT)
	Rscript $(KIN_SUB_DOMS_SCRIPT) $<

$(OUTDIR)/kinase-int-domains-sig.tsv: $(KIN_PHYS_INT) $(KIN_SUB_DOMS_SCRIPT)
	Rscript $(KIN_INT_DOMS_SCRIPT)

.PHONY: sugiyama-fold-features
sugiyama-fold-features: $(foreach K,$(shell seq 1 $(ML_K_FOLDS)),sugiyama-fold-$(K)-train-features) \
	$(foreach K,$(shell seq 1 $(ML_K_FOLDS)),sugiyama-fold-$(K)-test-features)

# .PHONY: $(foreach K,$(shell seq 1 $(K_FOLDS)),sugiyama-fold-$(K)-features)
sugiyama-fold-%-train-features: $(SUGIYAMA_FEATS_SCRIPT) \
		$(DATADIR)/sugiyama-folds/fold-%/train/sugiyama-train.tsv \
		$(OUTDIR)/sugiyama-folds/fold-%/train/kinase-sub-domains.tsv
	Rscript $^
	touch $@   		# I don't know why the phony rule isn't working so here's the lazy man's way out

sugiyama-fold-%-test-features: $(SUGIYAMA_FEATS_SCRIPT) \
		$(DATADIR)/sugiyama-folds/fold-%/test/sugiyama-test.tsv \
		$(OUTDIR)/sugiyama-folds/fold-%/train/kinase-sub-domains.tsv
	Rscript $^
	touch $@   		# I don't know why the phony rule isn't working so here's the lazy man's way out

.PHONY: sugiyama-features
sugiyama-features: $(SUGIYAMA_FEATS_SCRIPT) \
		$(SUGIYAMA) \
		$(OUTDIR)/kinase-sub-domains.tsv
	Rscript $^

$(PMAPPERVAL_SEMSIM): $(PMAPPERVAL) $(PMAPPERVAL_SEMSIM_SCRIPT)
	Rscript $(PMAPPERVAL_SEMSIM_SCRIPT)

$(PSP_SEMSIM): $(PSP) $(PSP_SEMSIM_SCRIPT)
	Rscript $(PSP_SEMSIM_SCRIPT)

$(PHOPROTEOME_SEMSIM): $(PHOPROTEOME) $(PHOPROTEOME_SEMSIM_SCRIPT)
	Rscript $(PHOPROTEOME_SEMSIM_SCRIPT)

$(WILKES_SEMSIM): $(WILKES_INHIB) $(WILKES_SEMSIM_SCRIPT)
	Rscript $(WILKES_SEMSIM_SCRIPT)
