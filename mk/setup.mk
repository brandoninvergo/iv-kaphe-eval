# Scripts
SRCDIR = src
FORMAT_PSP_SCRIPT = $(SRCDIR)/format-psp.py
FORMAT_PMAPPER_SCRIPT = $(SRCDIR)/format-protmapper.py
KINASE_CITES_SCRIPT = $(SRCDIR)/get-kinase-citations.py
PKINFAM_SCRIPT = $(SRCDIR)/process-pkinfam.py
SUGIYAMA_SEQWIN_SCRIPT = $(SRCDIR)/sugiyama-seq-windows.py
SUGIYAMA_SCRIPT = $(SRCDIR)/sugiyama-analysis.r
OCHOA_SEQWIN_SCRIPT = $(SRCDIR)/ochoa-seq-windows.py
PSSM_EVAL_SCRIPT = $(SRCDIR)/pssm-eval.r
PHOPROTEOME_V_PROT_SCRIPT = $(SRCDIR)/phoproteome-vs-proteome.r
FORMAT_WILKES_SCRIPT = $(SRCDIR)/format-wilkes.py
FORMAT_BIOGRID_SCRIPT = $(SRCDIR)/format-biogrid.py
SUGIYAMA_FEATS_SCRIPT = $(SRCDIR)/sugiyama-add-features.r
PSP_FEATS_SCRIPT = $(SRCDIR)/kinsub-add-features.r
PMAPPER_FEATS_SCRIPT = $(SRCDIR)/protmapper-add-features.r
PMAPPERVAL_FEATS_SCRIPT = $(SRCDIR)/pmapperval-add-features.r
PHOPROTEOME_FEATS_SCRIPT = $(SRCDIR)/phoproteome-add-features.r
WILKES_FEATS_SCRIPT = $(SRCDIR)/wilkes-add-features.r
SIM_FEATS_SCRIPT = $(SRCDIR)/sim-add-features.r
BAYES_FEAT_SCRIPT = $(SRCDIR)/bayes-feats-eval.r
OMNIPATH_SCRIPT = $(SRCDIR)/get-omnipath.r
KINNET_SCRIPT = $(SRCDIR)/gen-kin-reg-net.r
KIN_INTXNS_SCRIPT = $(SRCDIR)/get-kinase-interactions.r
SUGIYAMA_FOLD_SCRIPT = $(SRCDIR)/gen-sugiyama-folds.r
KIN_SUB_DOMS_SCRIPT = $(SRCDIR)/get-kin-sub-domains.r
KIN_INT_DOMS_SCRIPT = $(SRCDIR)/get-kin-int-domains.r
SUGIYAMA_FEATS_SCRIPT = $(SRCDIR)/sugiyama-add-features.r
SUGIYAMA_CV_SCRIPT = $(SRCDIR)/sugiyama-ml-fold-cv.r
SUGIYAMA_CV_PLOT_SCRIPT = $(SRCDIR)/sugiyama-ml-fold-cv-plot.r
PMAPPERVAL_SCRIPT = $(SRCDIR)/make-protmapper-val.r
ADD_FEATS_SCRIPT = $(SRCDIR)/kinsub-add-features.r
PMAPPERVAL_SEMSIM_SCRIPT = $(SRCDIR)/pmapperval-get-go-semsim.r
PSP_SEMSIM_SCRIPT = $(SRCDIR)/psp-get-go-semsim.r
PHOPROTEOME_SEMSIM_SCRIPT = $(SRCDIR)/phoproteome-get-go-semsim.r
WILKES_SEMSIM_SCRIPT = $(SRCDIR)/wilkes-get-go-semsim.r
PMAPPERVAL_EVAL_SCRIPT = $(SRCDIR)/pmapperval-multilabel-eval.r
ISOFORM_SCRIPT = $(SRDIR)/isoform-analysis.r
WILKES_EVAL_SCRIPT = $(SRCDIR)/wilkes-eval.r
FORMAT_PSP_SITES_SCRIPT = $(SRCDIR)/format-psp-sites.py
RANDFOREST_SCRIPT = $(SRCDIR)/rand-forest.r
ASSIGNMENTS_SCRIPT = $(SRCDIR)/phoproteome-eval.r

# Commands
EXCEL2TSV = localc --convert-to csv:"Text - txt - csv (StarCalc)":"9,34,0,0" --outdir $(1) $(2)
AWK = awk -F"\t" -vOFS="\t"
OPTS2DIR = $(subst _,/,$(1))
PDFMERGE = gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=$(1) $(2)

MOTIF_COMPILE = $(shell which motif-compile)
MOTIF_DIST = $(shell which motif-dist)
MOTIF_CV = $(shell which motif-cv) \
		--k=$(K_FOLDS) \
		--pseudo-dist=freq \
		--pseudo-n=pos \
		--pseudo-param=1
MOTIF_SCORE = $(shell which motif-score) # --omit=$(SEQWIN_MID)
MOTIF_SIM = $(shell which motif-sim)

# Data directories
DATADIR = data
RAW_DATADIR = $(DATADIR)/raw
PROTEOME_DIR = $(DATADIR)/proteome

# Output
OUTDIR = out
SUGIYAMA_WEIGHTS_DIR = $(OUTDIR)/weights
SUGIYAMA_MOTIFS_DIR = $(OUTDIR)/motifs
SUGIYAMA_DISTS_DIR = $(OUTDIR)/dists
SUGIYAMA_CV_DIR = $(OUTDIR)/cv
PSP_OUTDIR = $(OUTDIR)/psp
PSP_NORM_OUTDIR = $(OUTDIR)/psp-norm
PMAPPER_OUTDIR = $(OUTDIR)/protmapper
PMAPPER_NORM_OUTDIR = $(OUTDIR)/protmapper-norm
PMAPPERVAL_OUTDIR = $(OUTDIR)/pmapperval
PMAPPERVAL_NORM_OUTDIR = $(OUTDIR)/pmapperval-norm
PHOPROTEOME_OUTDIR = $(OUTDIR)/phoproteome
PHOPROTEOME_NORM_OUTDIR = $(OUTDIR)/phoproteome-norm
WILKES_INHIB_OUTDIR = $(OUTDIR)/wilkes-inhib
WILKES_INHIB_NORM_OUTDIR = $(OUTDIR)/wilkes-inhib-norm
SIM_OUTDIR = $(OUTDIR)/sim
SIM_NORM_OUTDIR = $(OUTDIR)/sim-norm

# Images
IMGDIR = img
CV_IMG_DIR = $(IMGDIR)/cv
