# Don't delete intermediate files
.SECONDARY:
# Do all computations in a recipe within the same shell process
.ONESHELL:
# Use Bash instead of POSIX shell
SHELL = /bin/bash

empty :=

# Parameters
SEQWIN_LEN = 15
SEQWIN_HALFLEN = 7
SEQWIN_MID = 8
CV_REPS = 50
K_FOLDS = 3
ML_K_FOLDS = 10
SCORE_OPTS = global-bg pos-bg freq bayes bayes-plus
S_OPTS = scaled unscaled
PVAL_PREC = 0 # 6
NUM_SIMSEQS = 100000

MKDIR = mk
# Directory definitions, script/executable definitions, etc.
include $(MKDIR)/setup.mk
# Pulling raw remote data and formatting to the final data sources
include $(MKDIR)/data.mk
# Compiling motifs
include $(MKDIR)/motifs.mk
# Calculating motif scores for various data sets
include $(MKDIR)/scores.mk
# Final analyses
include $(MKDIR)/analysis.mk

.PHONY: setup
setup: data

.PHONY: prepare
prepare: compile scores

.PHONY: figures-1
figures-1: figure2 figure3 figure4-prep figure6

.PHONY: figures-2
figures-2: figure4-prep figure7

.PHONY: figure2
figure2: $(IMGDIR)/sugiyama-ml-cv.pdf

.PHONY: figure3
figure3: $(IMGDIR)/bayes-features-eval.pdf

.PHONY: figure4-prep
figure4-prep: $(PMAPPERVAL_SCORES_RF)

# Before running this you have to manually run NetworKIN 3.0 and GPS
# 5.0 on the substrates in the data/pmapperval-kinase-substates.tsv
# validation set
.PHONY: figure4
figure4: $(IMGDIR)/pmapperval-ml.pdf

.PHONY: figure5
figure5: $(IMGDIR)/phosphoproteome-assignments.pdf

# Also generate Supplemental Tables S1-3
.PHONY: figure6
figure6: $(IMGDIR)/isoform-analysis.pdf

# Before running this you have to manually run NetworKIN 3.0 and GPS
# 5.0 on the substrates in the data/wilkes-et-al-2015-inhib.tsv data
# file
.PHONY: figure7
figure7: $(IMGDIR)/wilkes-eval.pdf

print-var-%:
	@echo "$($*)"

